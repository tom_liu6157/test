from dc.settings import cleanser
from dc.mapping import ownerChannelMapping, channelMapping
import dc.utils
import pandas as pd
import numpy as np
from io import BytesIO
import os
import re
import dc.mapping
from dc.cp.__init__ import __all__
from dc.mapping import ownerChannelMapping
import shutil
from imp import reload

# Prerequisite
cleansers=[]
renew_channel={}
renew_module = {}
with open('dc/settings.py', encoding='utf-8', mode='r') as infile:
        begin = 0
        begin1 = 0
        tmp = ''
        for line in infile:
            tmp = tmp + str(line)
            if '"linear" : OrderedDict([' in line:
                begin = True
                continue
            elif "        ])," in line:
                begin = False
            if '"nonlinear" : OrderedDict([' in line:
                begin1 = True
                continue
            elif "        ])" in line:
                begin1 = False
            if begin and not line.strip().startswith('#') and len(line.strip()) > 0:
                cleansers.append(str(line).strip())
            if begin1 and not line.strip().startswith('#') and len(line.strip()) > 0:
                cleansers.append(str(line).strip())
        infile.close() 
for i in cleansers:
    match = re.search('\("(.+)",\s*([^\W_]+)\.[^\s]+\),\s*#\s*([\d]+)', i)
    if match is not None and match.group(1) is not None and ownerChannelMapping.get(match.group(1),"") == "" and type(channelMapping.get(match.group(3))) == dict:           
        renew_channel[match.group(3)]=(match.group(1), channelMapping.get(match.group(3)).get("name"), match.group(2))   
for channel_no, pair in renew_channel.items():
    if re.sub('[\W_]+', '', pair[0]) != pair[2]:
        tmp = ''
        for channel_no1, pair1 in renew_channel.items():
            if re.sub('[\W_]+', '', pair1[0]) == pair[2]:
                tmp = channel_no1
                break
        if tmp != '': 
            renew_channel[channel_no] = (renew_channel[channel_no][0], renew_channel[channel_no][1], renew_channel[channel_no][2], re.sub('[\W_]+', '', renew_channel[tmp][1]))     
        else:
            renew_channel[channel_no] = (renew_channel[channel_no][0], renew_channel[channel_no][1], renew_channel[channel_no][2], renew_channel[channel_no][2]) 
    else:
        renew_channel[channel_no] = (renew_channel[channel_no][0], renew_channel[channel_no][1], renew_channel[channel_no][2], re.sub('[\W_]+', '', renew_channel[channel_no][1]))
        if renew_channel[channel_no][2] != re.sub('[\W_]+', '', renew_channel[channel_no][1]):
            renew_module[renew_channel[channel_no][2]] = re.sub('[\W_]+', '', renew_channel[channel_no][1])
count_module = {}
for oriModule, newModule in renew_module.items():                
    count_module[oriModule] = 1
for i in cleansers:
    match = re.search('\("(.+)",\s*([^\W_]+)\.[^\s]+\),\s*#\s*([\d]+)', i)
    match1 = re.search('\("(.+)",\s*([^\W_]+)\.[^\s]+\),\s*#\s*(.+)', i)
    if match is not None:
        if channelMapping.get(match.group(3)) == None and count_module.get(match.group(2)) == None:                                                 
            count_module[match.group(2)] = 1
        elif match.group(2) not in list(renew_module.keys()) and count_module.get(match.group(2)) != None:                                                 
            count_module[match.group(2)] = count_module[match.group(2)] + 1
    elif match1 is not None:
        if count_module.get(match1.group(2)) != None:
            count_module[match1.group(2)] = count_module[match1.group(2)] + 1


# Rename channel
with open('dc/cp/__init__.py', encoding='utf-8', mode='w') as infile:
    for channel_name in list(renew_channel.values()):
        if channel_name[2] in __all__ and channel_name[3] not in __all__:
            __all__.insert(__all__.index(channel_name[2]), channel_name[3])                                 
            __all__.remove(channel_name[2])
            shutil.move('dc/cp/'+channel_name[2]+'.py', 'dc/cp/'+channel_name[3]+'.py')
    infile.write("__all__ = "+re.sub("'","\"",str(__all__)))
    infile.close()
for channel_name in list(renew_channel.values()):    
    if renew_channel[channel_no][2] == re.sub('[\W_]+', '', renew_channel[channel_no][0]):                                                      
        ori = ''
        with open('dc/cp/'+channel_name[3]+'.py', encoding='utf-8', mode='r') as infile:
            ori = infile.read()
            infile.close()
            if channel_name[0] in ori:
                with open('dc/cp/'+channel_name[3]+'.py', encoding='utf-8', mode='w') as infile:
                    new = ori.replace(channel_name[0], channel_name[1])
                    infile.write(new)
                    infile.close()
tmp = ""
with open('dc/settings.py', encoding='utf-8', mode='r+') as infile:
    for line in infile:
        match = re.search('\("(.+)",\s*([^\W_]+)\.[^\s]+\),\s*#\s*([\d]+)', str(line))
        matchnl = re.search('\("(.+)",\s*([^\W_]+)\.[^\s]+\),\s*#\s*(.+)', str(line))
        if match is not None:
            if match.group(3) in list(renew_channel.keys()):
                tmp = tmp + "        (\""+renew_channel[match.group(3)][1]+"\", "+renew_channel[match.group(3)][3]+".convert_linear), # " + match.group(3) + "\n"                                     
                continue
            elif renew_module != {} and match.group(2) in list(renew_module.keys()):
                tmp = tmp + "        (\""+match.group(1)+"\", "+renew_module[match.group(2)]+".convert_linear), # " + match.group(3) + "\n"                                     
                continue
        elif matchnl is not None:
            changed = False
            for renewch in list(renew_channel.values()):
                if matchnl.group(1) == renewch[0]:
                    if matchnl.group(2) in list(renew_module.keys()):
                        tmp = tmp + "        (\""+renewch[1]+"\", "+renew_module[matchnl.group(2)]+".convert_linear), # " + renewch[1] + "\n"  
                        changed = True
                    else:
                        tmp = tmp + "        (\""+renewch[1]+"\", "+matchnl.group(2)+".convert_linear), # " + renewch[1] + "\n"  
                        changed = True
                    break 
            if changed:
                continue
            elif matchnl.group(2) in list(renew_module.keys()):
                tmp = tmp + "        (\""+matchnl.group(1)+"\", "+renew_module[matchnl.group(2)]+".convert_linear), # " + matchnl.group(3) + "\n"                                     
                continue
        tmp = tmp + str(line)
    infile.close()
with open('dc/settings.py', encoding='utf-8', mode='w') as infile:
    infile.write(str(tmp))
    infile.close()


# Delete channel
tmp = ""
with open('dc/settings.py', encoding='utf-8', mode='r+') as infile:
    for line in infile:
        match = re.search('\("(.+)",\s*([^\W_]+)\.[^\s]+\),\s*#\s*([\d]+)', str(line))
        if match is not None and channelMapping.get(match.group(3)) == None: 
            continue
        tmp = tmp + str(line)
    infile.close()
with open('dc/settings.py', encoding='utf-8', mode='w') as infile:
    infile.write(str(tmp))
    infile.close()
with open('dc/cp/__init__.py', encoding='utf-8', mode='w') as infile:
    for module, count in count_module.items():
        if count == 1 and module in __all__:
            __all__.remove(module)
            if module+'.py' in os.listdir('dc/cp'):
                os.remove('dc/cp/'+module+'.py')
    infile.write("__all__ = "+re.sub("'","\"",str(__all__)))
    infile.close()


# Reload Setting & Init
cps = reload(dc.cp)
settings = reload(dc.settings)


