import pandas as pd
import numpy as np
import datetime
import math
import csv
import re
import calendar
from collections import OrderedDict
import dc.utils
import dc.io
import dc.header
import dc.mapping
import dc.settings

def convert_linear(in_io, out_io, out_format="preV1506", owner_channel="WarnerTV"):
    "Convert linear schedule for WarnerTV"

    # Read input file
    indf = pd.read_excel(in_io, sheetname=0, skiprows=0, na_values=["n/a"], keep_default_na=True)
    indf.columns = [s.strip() for s in indf.columns]
    indf = indf.rename(columns={'Full Title (English)': 'fulltitleenglishmax40characters'})
    indf = indf.rename(columns={'Full Title (Chinese)': 'fulltitlechinesemax24characters'})
    indf = indf.rename(columns={'Season No.': 'seasonno.max3digits'})
    indf = indf.rename(columns={'Episode no.': 'episodeno.max5digits'})
    indf = indf.rename(columns={'Short Synopsis (English)': 'shortsynopsisenglishmax120chars'})
    indf = indf.rename(columns={'Short Synopsis (Chinese)': 'Short Synopsis (Chinese) (max 120 chars)'})
    indf = indf.rename(columns={'Short Synopsis (Chinese)': 'Short Synopsis (Chinese) (max 120 chars)'})
    indf = indf.rename(columns={'Original language': 'Original language (max 50 chars)'})
    indf = indf.rename(columns={'Region Code': 'Region Code (max 2 chars)'})
    indf = indf.rename(columns={'First release year': 'First release year (max 4 digits)'})
    indf.columns = ["".join(s.split()).replace("(", "").replace(")", "").replace("/", "").lower() for s in indf.columns]
    indf = indf.rename(columns={'cpinternaluniqueid.1': 'cpinternalseriesid'})

    
    # Create output DataFrame
    outdf = pd.DataFrame(columns=dc.header.internalHeader)

    # Extract data from full title


    # Fill output DataFrame with related info
    excepted_column = ["PID", "Premier", "Recordable", "Is NPVR Prog", "Is Restart TV", "effective_date", "expiration_date", "Media ID (max. 12 chars)"]
    for internal_column, indf_column in dc.header.postV1506HeaderMapping.items():
        if (indf_column not in excepted_column): 
            edited_indf_column = "".join(indf_column.split()).replace("(", "").replace(")", "").replace("/", "").lower()
            if (edited_indf_column in indf.columns):
                outdf[internal_column] = indf[edited_indf_column]
                if (outdf[internal_column].dtype == object):
                    outdf[internal_column] = outdf[internal_column].where(outdf[internal_column].apply(lambda x: pd.notnull(x) and len(str(x).strip()) > 0))
    outdf["TXDate"] = outdf["TXDate"].apply(lambda x: pd.to_datetime(x, format="%Y%m%d")).apply(lambda x: x.strftime("%Y%m%d"))
    outdf["ActualTime"] = indf["actualtime"].apply(lambda x: pd.to_datetime(x, format="%H:%M") if isinstance(x, str) else x)
    start_hour = 6
    outdf["TXDate"] = outdf["TXDate"][outdf["ActualTime"].apply(lambda x: int(x.strftime("%H"))) < start_hour].apply(lambda x: (pd.to_datetime(x) + pd.to_timedelta("1 days")).strftime("%Y%m%d")).combine_first(outdf["TXDate"])
    outdf["ActualTime"] = outdf["ActualTime"].apply(lambda x: x.strftime("%H:%M"))
    outdf["SponsorTextStuntChi"].fillna(outdf["SponsorTextStuntEng"], inplace=True)
    outdf["SponsorTextStuntEng"].fillna(outdf["SponsorTextStuntChi"], inplace=True)
    outdf["BrandNameChi"].fillna(outdf["BrandNameEng"], inplace=True)
    outdf["BrandNameEng"].fillna(outdf["BrandNameChi"], inplace=True)
    outdf["SeasonNameChi"].fillna(outdf["SeasonNameEng"], inplace=True)
    outdf["SeasonNameEng"].fillna(outdf["SeasonNameChi"], inplace=True)
    outdf["EpisodeNameChi"].fillna(outdf["EpisodeNameEng"], inplace=True)
    outdf["EpisodeNameEng"].fillna(outdf["EpisodeNameChi"], inplace=True)
    outdf["SynopsisEng"].fillna(outdf["ShortSynopsisEng"], inplace=True)
    outdf["SynopsisEng"].fillna(outdf["SynopsisChi"], inplace=True)
    outdf["SynopsisChi"].fillna(outdf["SynopsisEng"], inplace=True)
    outdf["SynopsisChi"] = outdf["SynopsisChi"].apply(lambda x: dc.utils.jianfan(str(x))).str.translate(dc.mapping.charTranslateTable)
    outdf["ShortSynopsisChi"].fillna(outdf["ShortSynopsisEng"], inplace=True)
    outdf["ShortSynopsisChi"] = outdf["ShortSynopsisChi"].apply(lambda x: dc.utils.jianfan(str(x))).str.translate(dc.mapping.charTranslateTable)
    outdf["ShortSynopsisEng"].fillna(outdf["ShortSynopsisChi"], inplace=True)
    outdf["SeasonNo"] = outdf["SeasonNo"].apply(lambda x: np.nan if isinstance(x, str) else x)
    outdf["SeasonNo"] = outdf["SeasonNo"][outdf["SeasonNo"].notnull()].astype(int).astype(str)
    outdf["EpisodeNo"] = outdf["EpisodeNo"][outdf["EpisodeNo"].notnull()].astype(int).astype(str)
    outdf["FirstReleaseYear"] = outdf["FirstReleaseYear"][outdf["FirstReleaseYear"].notnull()].astype(int).astype(str)
    outdf["PremierOld"] = "0"
    outdf["IsEpisodic"] = (outdf["EpisodeNo"].notnull() | outdf["EpisodeNameEng"].notnull() | outdf["EpisodeNameChi"].notnull()).map({True:"Y", False:"N"})
    text_column = ["SponsorTextStuntEng", "SponsorTextStuntChi", "BrandNameEng", "BrandNameChi", "EditionVersionEng", "EditionVersionChi", "SeasonNameEng", "SeasonNameChi", "EpisodeNameEng", "EpisodeNameChi", "SynopsisEng", "SynopsisChi", "ShortSynopsisEng", "ShortSynopsisChi", "DirectorProducerEng", "DirectorProducerChi", "CastEng", "CastChi"]
    for column in text_column:
        outdf[column] = outdf[column].astype(str).where(outdf[column].notnull()).str.translate(dc.mapping.charTranslateTable)
    dc.utils.title_df(outdf)
    outdf[["FullTitleEng", "FullTitleChi"]] = outdf.apply(dc.utils.constructFullTitle, axis=1)
    outdf[["ProgrammeNameEng", "ProgrammeNameChi"]] = outdf.apply(dc.utils.constructProgrammeName, axis=1)
    dc.utils.translateChar(outdf)
    dc.utils.chopCharacters(outdf)


    # Append EOF
    dc.utils.appendEOF(outdf)


    # Fill Channel #
    outdf['OwnerChannel'] = owner_channel
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)

    # Write output file
    dc.io.to_io(outdf, out_io, out_format)

def convert_nonlinear(in_io, out_io, format="preV1506", owner_channel="WarnerTV"):
    "Convert nonlinear for WarnerTV"
    return None

