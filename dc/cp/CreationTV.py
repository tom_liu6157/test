import pandas as pd
import numpy as np
import datetime
import math
import csv
import re
import calendar
from collections import OrderedDict
import dc.utils
import dc.io
import dc.header
import dc.mapping
import dc.settings

def convert_linear(in_io, out_io, out_format="preV1506", owner_channel="Creation TV"):
    "Convert linear schedule for Creation TV"

    # Read input file
    indf = pd.read_excel(in_io, sheetname=0, na_values=["NIL"], keep_default_na=True)

    
    # Create output DataFrame
    outdf = pd.DataFrame(columns=dc.header.internalHeader)

    # Extract data from full title
    fullTitleEng = indf["Full Title \n(English) \nMax 40 char"][indf["Full Title \n(English) \nMax 40 char"].notnull()].apply(lambda x: str(x).strip())
    fullTitleChi = indf["Full Title \n(Chinese) \nMax 24 char"][indf["Full Title \n(Chinese) \nMax 24 char"].notnull()].apply(lambda x: str(x).strip())
    extractedInfo = pd.DataFrame(columns=["BrandNameEng", "BrandNameChi", "SeasonNo", "EpisodeNo", "EpisodeNameEng", "EpisodeNameChi"])
    tempExtractedInfo = [
        fullTitleEng.str.extract("(?P<BrandNameEng>.*?)[:﹕：](?P<EpisodeNameEng>.+)", expand=False),
        fullTitleChi.str.extract("(?P<BrandNameChi>.*?)[:﹕：](?P<EpisodeNameChi>.+)", expand=False),
    ]
    extractedInfo["BrandNameEng"] = tempExtractedInfo[0]["BrandNameEng"]
    extractedInfo["BrandNameEng"].fillna(fullTitleEng, inplace=True)
    extractedInfo["BrandNameChi"] = tempExtractedInfo[1]["BrandNameChi"]
    extractedInfo["BrandNameChi"].fillna(fullTitleChi, inplace=True)
    extractedInfo["EpisodeNameEng"] = tempExtractedInfo[0]["EpisodeNameEng"]
    extractedInfo["EpisodeNameChi"] = tempExtractedInfo[1]["EpisodeNameChi"]


    # Fill output DataFrame with related info
    outdf["TXDate"] = indf["TX Date"].apply(lambda x: pd.to_datetime(x, format="%Y%m%d")).apply(lambda x:x.strftime("%Y%m%d"))
    outdf["ActualTime"] = indf["Actual \nStart \nTime"].apply(lambda x: pd.to_datetime(x, format="%H:%M:%S")).apply(lambda x:x.strftime("%H:%M"))
    outdf["BrandNameEng"] = extractedInfo["BrandNameEng"].str.replace("(（|\()Bilingual Broadcast(）|\))", "").str.translate(dc.mapping.charTranslateTable).str.strip()
    outdf["BrandNameChi"] = extractedInfo["BrandNameChi"].str.replace("(（|\()(國語|雙語廣播)(）|\))", "").str.translate(dc.mapping.charTranslateTable).str.strip()
    outdf["BrandNameChi"].fillna(outdf["BrandNameEng"], inplace=True)
    outdf["BrandNameEng"].fillna(outdf["BrandNameChi"], inplace=True)
    outdf["SynopsisEng"] = indf["Program Synopsis \n(English) \nMax 180 char"][indf["Program Synopsis \n(English) \nMax 180 char"].notnull()].astype(str).str.translate(dc.mapping.charTranslateTable)
    outdf["ShortSynopsisEng"] = outdf["SynopsisEng"][outdf["SynopsisEng"].notnull()].apply(lambda x: x[0:dc.settings.char_limit.get("ShortSynopsisEng")])
    outdf["SynopsisChi"] = indf["Program Synopsis \n(Chinese) \nMax 130 char"][indf["Program Synopsis \n(Chinese) \nMax 130 char"].notnull()].astype(str).str.translate(dc.mapping.charTranslateTable)
    outdf["ShortSynopsisChi"] = outdf["SynopsisChi"][outdf["SynopsisChi"].notnull()].apply(lambda x: x[0:dc.settings.char_limit.get("ShortSynopsisChi")])
    outdf["EpisodeNameEng"] = extractedInfo["EpisodeNameEng"][extractedInfo["EpisodeNameEng"].notnull()].astype(str).str.replace("(（|\()Bilingual Broadcast(）|\))", "").str.translate(dc.mapping.charTranslateTable).str.strip()
    outdf["EpisodeNameChi"] = extractedInfo["EpisodeNameChi"][extractedInfo["EpisodeNameChi"].notnull()].astype(str).str.replace("(（|\()(國語|雙語廣播)(）|\))", "").str.translate(dc.mapping.charTranslateTable).str.strip()
    outdf["EpisodeNameChi"].fillna(outdf["EpisodeNameEng"], inplace=True)
    outdf["EpisodeNameEng"].fillna(outdf["EpisodeNameChi"], inplace=True)
    outdf["EpisodeNo"] = extractedInfo["EpisodeNo"]
    outdf["PremierOld"] = "0"
    outdf["Premier"] = indf["Premier"]
    outdf["IsLive"] = "N"
    outdf["IsEpisodic"] = (outdf["EpisodeNo"].notnull() | outdf["EpisodeNameEng"].notnull()).map({True:"Y", False:"N"})
    outdf["Classification"] = indf["Classification"]
    outdf["Genre"] = indf["Genre"].apply(lambda x: dc.mapping.genreMapping.get(x.lower(), x) if pd.notnull(x) else None)
    outdf["SubGenre"] = indf["Sub Genre"][indf["Sub Genre"].notnull()].astype(str).str.split("/").apply(lambda subGenres: sorted(set([item for subGenre in subGenres for item in dc.mapping.subGenreMapping.get(subGenre.lower(), [subGenre])]))).str.join("/").fillna(indf["Sub Genre"]).apply(lambda x: x[0:dc.settings.char_limit.get("SubGenre")])
    outdf["OriginalLang"] = (extractedInfo["BrandNameChi"].str.contains("[（\(]國語[）\)]") | extractedInfo["EpisodeNameChi"].str.contains("[（\(]國語[）\)]")).map({True:"Mandarin", False:None})
    outdf["AudioLang"] = outdf["OriginalLang"]
    dc.utils.title_df(outdf)
    outdf[["FullTitleEng", "FullTitleChi"]] = outdf.apply(dc.utils.constructFullTitle, axis=1)
    outdf[["ProgrammeNameEng", "ProgrammeNameChi"]] = outdf.apply(dc.utils.constructProgrammeName, axis=1)
    dc.utils.translateChar(outdf)
    dc.utils.chopCharacters(outdf)


    # Append EOF
    dc.utils.appendEOF(outdf)


    # Fill Channel #
    outdf['OwnerChannel'] = owner_channel
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)

    # Write output file
    dc.io.to_io(outdf, out_io, out_format)

def convert_nonlinear(in_io, out_io, format="preV1506", owner_channel="Creation TV"):
    "Convert nonlinear for Creation TV"
    return None

