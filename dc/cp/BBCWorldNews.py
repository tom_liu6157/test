import pandas as pd
import numpy as np
import datetime
import math
import csv
import re
import calendar
from collections import OrderedDict
import dc.utils
import dc.io
import dc.header
import dc.mapping
import dc.settings

def convert_linear(in_io, out_io, out_format="preV1506", owner_channel="BBC World News"):
    "Convert linear schedule for BBC World News"

    # Read input file
    indf = pd.read_excel(in_io, sheetname=0, skiprows=0)

    
    # Create output DataFrame
    outdf = pd.DataFrame(columns=dc.header.internalHeader)

    # Extract data from full title
    fullEpisodeTitle = indf["Episode"][indf["Episode"].notnull()].apply(lambda x: str(x).strip())
    extractedInfo = pd.DataFrame(columns=["EpisodeNo", "EpisodeNameEng"])
    tempExtractedInfo = fullEpisodeTitle.str.extract("Episode (?P<EpisodeNo>\d+)", expand=False)
    extractedInfo["EpisodeNo"] = tempExtractedInfo
    extractedInfo["EpisodeNameEng"] = fullEpisodeTitle[tempExtractedInfo.isnull()]


    # Fill output DataFrame with related info
    outdf["TXDate"] = indf["Date"].apply(lambda x: pd.to_datetime(x, format="%Y-%m-%d")).apply(lambda x:x.strftime("%Y%m%d"))
    outdf["ActualTime"] = indf["Time"].apply(lambda x: pd.to_datetime(x, format="%H:%M:%S")).apply(lambda x:x.strftime("%H:%M"))
    outdf["BrandNameEng"]=indf["Programme"]
    outdf["BrandNameChi"]=outdf["BrandNameEng"]
    outdf["EpisodeNo"] = extractedInfo["EpisodeNo"][extractedInfo["EpisodeNo"].notnull()].astype(int).astype(str)
    outdf["EpisodeNameEng"] = extractedInfo["EpisodeNameEng"]
    outdf["EpisodeNameChi"] = outdf["EpisodeNameEng"]
    outdf["SynopsisEng"] = indf["Billing"].str.translate(dc.mapping.charTranslateTable)
    outdf["SynopsisChi"] = outdf["SynopsisEng"]
    outdf["ShortSynopsisEng"] = outdf["SynopsisEng"][outdf["SynopsisEng"].notnull()].apply(lambda x: x[0:dc.settings.char_limit.get("ShortSynopsisEng")])
    outdf["ShortSynopsisChi"] = outdf["ShortSynopsisEng"]
    outdf["PremierOld"] = "0"
    outdf["Premier"] = "N"
    outdf["IsLive"] = "N"
    outdf["IsEpisodic"] = (outdf["EpisodeNo"].notnull() | outdf["EpisodeNameEng"].notnull()).map({True:"Y", False:"N"})
    dc.utils.title_df(outdf)
    outdf[["FullTitleEng", "FullTitleChi"]] = outdf.apply(dc.utils.constructFullTitle, axis=1)
    outdf[["ProgrammeNameEng", "ProgrammeNameChi"]] = outdf.apply(dc.utils.constructProgrammeName, axis=1)
    dc.utils.translateChar(outdf)
    dc.utils.chopCharacters(outdf)


    # Append EOF
    temp =str("00:30:00")
    lastProgDuration = pd.Timedelta(temp)
    dc.utils.appendEOF(outdf, lastprog_duration=lastProgDuration)


    # Fill Channel #
    outdf['OwnerChannel'] = owner_channel
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)

    # Write output file
    dc.io.to_io(outdf, out_io, out_format)

def convert_nonlinear(in_io, out_io, format="preV1506", owner_channel="BBC World News"):
    "Convert nonlinear for BBC World News"
    return None

