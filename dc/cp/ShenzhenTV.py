import pandas as pd
import numpy as np
import datetime
import math
import csv
import re
import calendar
from collections import OrderedDict
import dc.utils
import dc.io
import dc.header
import dc.mapping
import dc.settings

def convert_linear(in_io, out_io, out_format="preV1506", owner_channel="Shenzhen TV"):
    "Convert linear schedule for Shenzhen TV"

    # Read input file
    indf = pd.read_excel(in_io, sheetname=0, na_values=["n/a"], keep_default_na=True, skiprows=0)

    
    # Create output DataFrame
    outdf = pd.DataFrame(columns=dc.header.internalHeader)

    # Extract data from full title


    # Fill output DataFrame with related info
    outdf["ChannelNo"] = indf["Channel #\n台号"]
    outdf["TXDate"] = indf["TX Date\n播放日期"].apply(lambda x: pd.to_datetime(x, format="%d/%m/%Y")).apply(lambda x:x.strftime("%Y%m%d"))
    outdf["ActualTime"] = indf["Actual Time\n播放时间"].apply(lambda x: pd.to_datetime(x, format="%H:%M:%S")).apply(lambda x:x.strftime("%H:%M"))
    outdf["FullTitleEng"] = indf["Full Title (English) Max 40 Characters\n节目全名（含赞助等副名）\n（英文）最多40字"]
    outdf["FullTitleChi"] = indf["Full Title (Chinese) Max 24 Characters\n节目全名（含赞助等副名）\n（中文）最多24字"][indf["Full Title (Chinese) Max 24 Characters\n节目全名（含赞助等副名）\n（中文）最多24字"].notnull()].apply(lambda x: dc.utils.jianfan(x)).apply(lambda x: str(x).strip())
    outdf["SponsorTextStuntEng"] = indf["Sponsor text / Stunt (English)\n赞助文字（英文）"]
    outdf["SponsorTextStuntChi"] = indf["Sponsor text / Stunt (Chinese)\n赞助文字（中文）"][indf["Sponsor text / Stunt (Chinese)\n赞助文字（中文）"].notnull()].apply(lambda x: dc.utils.jianfan(x)).apply(lambda x: str(x).strip())
    outdf["BrandNameEng"] = indf["Brand name (English)\n纯节目名称(英文)"]
    outdf["BrandNameChi"] = indf["Brand name (Chinese)\n纯节目名称(中文)"][indf["Brand name (Chinese)\n纯节目名称(中文)"].notnull()].apply(lambda x: dc.utils.jianfan(x)).apply(lambda x: str(x).strip())
    outdf["EditionVersionEng"] = indf["Edition / Version (English)\n版本（英文）"]
    outdf["EditionVersionChi"] = indf["Edition / Version (Chinese)\n版本（中文）"][indf["Edition / Version (Chinese)\n版本（中文）"].notnull()].apply(lambda x: dc.utils.jianfan(x)).apply(lambda x: str(x).strip())
    outdf["SeasonNo"] = indf["Season no. (max 3 digits)\n季度（最多填3位数字）"]
    outdf["SeasonNameEng"] = indf["Season Name (English)\n季度名称\n（英文）"]
    outdf["SeasonNameChi"] = indf["Season Name (Chinese)\n季度名称\n（中文）"][indf["Season Name (Chinese)\n季度名称\n（中文）"].notnull()].apply(lambda x: dc.utils.jianfan(x)).apply(lambda x: str(x).strip())
    outdf["EpisodeNo"] = indf["Episode no. (max 5 digits)\n分集（最多填5位数字）"]
    outdf["EpisodeNameEng"] = indf["Episode name (English)\n分集名称\n（英文）"]
    outdf["EpisodeNameChi"] = indf["Episode name (Chinese)\n分集名称\n（中文）"][indf["Episode name (Chinese)\n分集名称\n（中文）"].notnull()].apply(lambda x: dc.utils.jianfan(x)).apply(lambda x: str(x).strip())
    outdf["SynopsisEng"] = indf["Synopsis (English)\n节目介绍（英文）"]
    outdf["SynopsisChi"] = indf["Synopsis (Chinese)\n节目介绍（中文）"][indf["Synopsis (Chinese)\n节目介绍（中文）"].notnull()].apply(lambda x: dc.utils.jianfan(x)).apply(lambda x: str(x).strip())
    outdf["ShortSynopsisEng"] = indf["Short Synopsis (English) \n(max 120 chars)\n简介（英文）（最多120字）"]
    outdf["ShortSynopsisChi"] = indf["Short Synopsis (Chinese) (max 120 chars)\n简介（中文）（最多120字）"][indf["Short Synopsis (Chinese) (max 120 chars)\n简介（中文）（最多120字）"].notnull()].apply(lambda x: dc.utils.jianfan(x)).apply(lambda x: str(x).strip())
    outdf["Premier"] = indf["Premiere (Y/N)\n首播（是／否）"]
    outdf["IsLive"] = indf["Is Live (Y/N)\n直播（是／否）"]
    outdf["Classification"] = indf["Classification\n节目级別"]
    outdf["Genre"] = indf["Genre\n节目类型"]
    outdf["SubGenre"] = indf["Sub-Genre\n节目分类"]
    outdf["DirectorProducerEng"] = indf["Director / Producer (English)\n导演／监制\n（英文）"]
    outdf["DirectorProducerChi"] = indf["Director / Producer (Chinese)\n导演／监制\n（中文）"][indf["Director / Producer (Chinese)\n导演／监制\n（中文）"].notnull()].apply(lambda x: dc.utils.jianfan(x)).apply(lambda x: str(x).strip())
    outdf["CastEng"] = indf["Cast (English)\n主演（英文）"]
    outdf["CastChi"] = indf["Cast (Chinese)\n主演（中文）"][indf["Cast (Chinese)\n主演（中文）"].notnull()].apply(lambda x: dc.utils.jianfan(x)).apply(lambda x: str(x).strip())
    outdf["OriginalLang"] = indf["Original language (max 50 chars)\n原语言\n（最多50字）"]
    outdf["AudioLang"] = indf["Audio language\n音频语言"]
    outdf["SubtitleLang"] = indf["Subtitle language\n字幕语言"]
    outdf["RegionCode"] = indf["Region Code (max 2 chars)\n区码（最多2字）"]
    outdf["FirstReleaseYear"] = indf["First release year (max 4 digits)\n首演年份（最多4位数字）"]
    outdf["IsEpisodic"] = indf["Episodic (Y/N)\n剧集（是／否）"]
    outdf["PortraitImage"] = indf["Portrait Image\n图片档名"]
    outdf["ImageWithTitle"] = indf["Image with title  (Y/N)\n图名附节目名称\n（是／否）"]
    outdf["RecordableForCatchUp"] = indf["Recordable for Catch-up (Y/N)\n可录即日重温\n（是／否）"]
    outdf["CPInternalUniqueID"] = indf["CP Internal unique ID\n提供商內部使用ID"]
    outdf["CPInternalSeriesID"] = indf["CP Internal Series ID\n提供商內部使用系列ID"]
    text_column = ["SponsorTextStuntEng", "SponsorTextStuntChi", "BrandNameEng", "BrandNameChi", "EditionVersionEng", "EditionVersionChi", "SeasonNameEng", "SeasonNameChi", "EpisodeNameEng", "EpisodeNameChi", "SynopsisEng", "SynopsisChi", "ShortSynopsisEng", "ShortSynopsisChi", "DirectorProducerEng", "DirectorProducerChi", "CastEng", "CastChi"]
    for column in text_column:
        outdf[column] = outdf[column].astype(str).where(outdf[column].notnull()).str.translate(dc.mapping.charTranslateTable)
    dc.utils.title_df(outdf)
    outdf[["FullTitleEng", "FullTitleChi"]] = outdf.apply(dc.utils.constructFullTitle, axis=1)
    outdf[["ProgrammeNameEng", "ProgrammeNameChi"]] = outdf.apply(dc.utils.constructProgrammeName, axis=1)
    dc.utils.translateChar(outdf)
    dc.utils.chopCharacters(outdf)


    # Append EOF
    dc.utils.appendEOF(outdf)


    # Fill Channel #
    outdf['OwnerChannel'] = owner_channel
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)

    # Write output file
    dc.io.to_io(outdf, out_io, out_format)

def convert_nonlinear(in_io, out_io, format="preV1506", owner_channel="Shenzhen TV"):
    "Convert nonlinear for Shenzhen TV"
    return None

