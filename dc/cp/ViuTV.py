import pandas as pd
import numpy as np
import datetime
import math
import csv
import re
import calendar
from collections import OrderedDict
import dc.utils
import dc.io
import dc.header
import dc.mapping
import dc.settings

def convert_linear(in_io, out_io, out_format="preV1506", owner_channel="ViuTV"):
    "Convert linear schedule for ViuTV"

    # Read input file
    indf = pd.read_excel(in_io, sheetname=0, na_values=["n/a"], keep_default_na=True, skiprows=0)

    
    # Create output DataFrame
    outdf = pd.DataFrame(columns=dc.header.internalHeader)

    # Extract data from full title


    # Fill output DataFrame with related info
    indf = indf[pd.notnull(indf["Planning Date"])]
    outdf['TXDate'] = indf["Planning Date"].apply(lambda x: pd.to_datetime(str(x), format="%Y-%m-%d %H:%M:%S"))
    outdf["ActualTime"] = outdf["TXDate"].astype('str').str.slice(-8).str.slice(0, 5).str.zfill(5)
    outdf['TXDate'] = outdf['TXDate'].apply(lambda x: x.strftime("%Y%m%d"))
    outdf['SponsorTextStuntChi'] = indf['Programme Sponsorship Alt Title'].replace('^-$',np.nan, regex=True)
    outdf['SponsorTextStuntEng'] = indf['Programme Sponsorship Title'].replace('^-$',np.nan, regex=True)
    outdf['BrandNameChi'] = indf['Brand_Name_Chinese'].replace('^-$',np.nan, regex=True)
    outdf['BrandNameEng'] = indf['Brand_Name_English'].replace('^-$',np.nan, regex=True)
    outdf['EditionVersionChi'] = indf['Edition_Version_Chinese'].replace('^-$',np.nan, regex=True)
    outdf['EditionVersionEng'] = indf['Edition_Version_English'].replace('^-$',np.nan, regex=True)
    outdf['SeasonNameChi'] = indf['Season_Name_Chinese'].replace('^-$',np.nan, regex=True)
    outdf['SeasonNameEng'] = indf['Season_Name_English'].replace('^-$',np.nan, regex=True)
    outdf['EpisodeNameChi'] = indf['EPISODEALTTITLE'].replace('^-$',np.nan, regex=True)
    outdf['EpisodeNameEng'] = indf['Episode Title'].replace('^-$',np.nan, regex=True)
    outdf['EpisodeNameEng'] = outdf['EpisodeNameEng'].replace('Episode Number \d+', np.nan, regex=True)
    outdf['SynopsisChi'] = indf['Episode Mini Synopsis'].replace('^-$',np.nan, regex=True)
    outdf['SynopsisChi'].fillna(indf['Programme Mini Synopsis'].replace('^-$',np.nan, regex=True), inplace=True)
    outdf['ChannelNo'] = 99
    outdf['Classification'] = indf['Classification'].replace('^-$',np.nan, regex=True)
    outdf["Premier"] = indf['PLANNINPREMIER'].replace('^-$',np.nan, regex=True)
    outdf["Premier"] = outdf["Premier"].apply(lambda x : 'Y' if x else 'N')
    outdf["Genre"] = indf['GENRE'].replace('^-$',np.nan, regex=True)
    outdf["SubGenre"] = indf['SUBGENRE'].replace('^-$',np.nan, regex=True)
    outdf["EpisodeNo"] = indf['Episode#'].replace('^-$',np.nan, regex=True)
    outdf["SeasonNo"] = indf['Series No'].replace('^-$',np.nan, regex=True)
    outdf["EpisodeNo"] = indf['Episode#'].replace('^-$',np.nan, regex=True)
    outdf["OriginalLang"] = indf['Original Language'].replace('^-$',np.nan, regex=True)
    if 'Billingual' in indf.columns:
        outdf["Bilingual"] = indf['Billingual']
    outdf["PortraitImage"] = indf["Portrait_Image"].replace('^-$',np.nan, regex=True)
    outdf["IsEpisodic"] = indf['Episodic(Y/N)']
    outdf["IsEpisodic"] = outdf[["IsEpisodic", "BrandNameEng"]].apply(lambda x: "Y" if x["BrandNameEng"] == "Midday Market Wrap" or x["BrandNameEng"] == "Smart Investor" else x["IsEpisodic"], axis=1)
    if 'VOD' in indf.columns:
        outdf["IsVOD"] = indf['VOD']
    outdf["CPInternalUniqueID"] = indf['PLANNINGID'][indf['PLANNINGID'].notnull()].astype(int).astype(str)
    outdf["MediaID"] = indf['Media No']
    if 'Clean Version ID' in indf.columns:
        outdf['HKTAMChannelID'] = indf['Clean Version ID'].replace('^-$',np.nan, regex=True).apply(lambda x: str(int(re.match("^([0-9]+)(\.0*)?$", str(x)).groups()[0])) if re.match("^[0-9]+(\.0*)?$", str(x)) else None)
    if 'Programme ID' in indf.columns:
        outdf['HKTAMProgramID'] = indf['Programme ID'].replace('^-$',np.nan, regex=True).apply(lambda x: str(int(re.match("^([0-9]+)(\.0*)?$", str(x)).groups()[0])) if re.match("^[0-9]+(\.0*)?$", str(x)) else None)
    if 'Episode ID' in indf.columns:
        outdf['HKTAMEpisodeID'] = indf['Episode ID'].replace('^-$',np.nan, regex=True).apply(lambda x: str(int(re.match("^([0-9]+)(\.0*)?$", str(x)).groups()[0])) if re.match("^[0-9]+(\.0*)?$", str(x)) else None)
    if 'Live Event' in indf.columns:
        outdf["IsLive"]=(indf['Live Event'] == "T").map({True:"Y", False:"N"})
    from datetime import timedelta    
    txDate = outdf[['TXDate', 'ActualTime']].apply(lambda x: ' '.join(x), axis=1).apply(lambda x: pd.to_datetime(str(x), format="%Y%m%d %H:%M"))
    txDate = txDate.apply(lambda x: x - timedelta(days=1) if x.hour < 6 else x).apply(lambda x: x.strftime("%Y%m%d"))
    outdf["EpisodeNameChi"].fillna(txDate[outdf['BrandNameEng'] == "About Sports"], inplace=True)
    outdf["EpisodeNameChi"].fillna(txDate[outdf['BrandNameEng'] == "15 mins Enews"], inplace=True)
    outdf["EpisodeNameEng"] = outdf[["TXDate", "ActualTime","BrandNameEng","EpisodeNameEng"]].apply(lambda x: x["TXDate"]+" "+x["ActualTime"] if x["BrandNameEng"] == "Smart Investor" else x["TXDate"] if x["BrandNameEng"] == "Midday Market Wrap" else x["EpisodeNameEng"], axis=1)                                  
    outdf["EpisodeNameChi"] = outdf[["TXDate", "ActualTime","BrandNameEng","EpisodeNameChi"]].apply(lambda x: x["TXDate"]+" "+x["ActualTime"] if x["BrandNameEng"] == "Smart Investor" else x["TXDate"] if x["BrandNameEng"] == "Midday Market Wrap" else x["EpisodeNameChi"], axis=1)                                  
    outdf["SponsorTextStuntChi"].fillna(outdf["SponsorTextStuntEng"], inplace=True)
    outdf["SponsorTextStuntEng"].fillna(outdf["SponsorTextStuntChi"], inplace=True)
    outdf["BrandNameChi"].fillna(outdf["BrandNameEng"], inplace=True)
    outdf["BrandNameEng"].fillna(outdf["BrandNameChi"], inplace=True)
    outdf["SeasonNameChi"].fillna(outdf["SeasonNameEng"], inplace=True)
    outdf["SeasonNameEng"].fillna(outdf["SeasonNameChi"], inplace=True)
    outdf["EpisodeNameChi"].fillna(outdf["EpisodeNameEng"], inplace=True)
    outdf["EpisodeNameEng"].fillna(outdf["EpisodeNameChi"], inplace=True)
    outdf["SynopsisChi"].fillna(outdf["SynopsisEng"], inplace=True)
    outdf["SynopsisEng"].fillna(outdf["SynopsisChi"], inplace=True)
    outdf["ShortSynopsisEng"] = outdf["SynopsisEng"][outdf["SynopsisEng"].notnull()].apply(lambda x: x[0:dc.settings.char_limit.get("ShortSynopsisEng")])
    outdf["ShortSynopsisChi"] = outdf["SynopsisChi"][outdf["SynopsisChi"].notnull()].apply(lambda x: x[0:dc.settings.char_limit.get("ShortSynopsisChi")])
    outdf["ShortSynopsisChi"].fillna(outdf["ShortSynopsisEng"], inplace=True)
    outdf["ShortSynopsisEng"].fillna(outdf["ShortSynopsisChi"], inplace=True)
    outdf["Genre"] = outdf["Genre"][outdf["Genre"].notnull()].str.lower().map(dc.mapping.genreMapping).fillna(outdf["Genre"])
    outdf["SubGenre"] = outdf["SubGenre"][outdf["SubGenre"].notnull()].str.lower().map(dc.mapping.genreMapping).fillna(outdf["SubGenre"])
    outdf["OriginalLang"] = outdf["OriginalLang"][outdf["OriginalLang"].notnull()].str.lower().map(dc.mapping.audioLangMapping).fillna(outdf["OriginalLang"])
    outdf["SeasonNo"] = outdf["SeasonNo"][outdf["SeasonNo"].notnull()].astype(int).astype(str)
    outdf["EpisodeNo"] = outdf["EpisodeNo"][outdf["EpisodeNo"].notnull()].astype(int).astype(str)
    text_column = ["SponsorTextStuntEng", "SponsorTextStuntChi", "BrandNameEng", "BrandNameChi", "EditionVersionEng", "EditionVersionChi", "SeasonNameEng", "SeasonNameChi", "EpisodeNameEng", "EpisodeNameChi", "SynopsisEng", "SynopsisChi", "ShortSynopsisEng", "ShortSynopsisChi", "DirectorProducerEng", "DirectorProducerChi", "CastEng", "CastChi"]
    for column in text_column:
        outdf[column] = outdf[column].astype(str).where(outdf[column].notnull()).apply(lambda x: dc.utils.jianfan(x) if isinstance(x, str) else x)
        outdf[column] = outdf[column].astype(str).where(outdf[column].notnull()).str.translate(dc.mapping.charTranslateTable)
    outdf[["FullTitleEng", "FullTitleChi"]] = outdf.apply(dc.utils.constructFullTitle, axis=1)
    outdf[["ProgrammeNameEng", "ProgrammeNameChi"]] = outdf.apply(dc.utils.constructProgrammeName, axis=1)
    dc.utils.translateChar(outdf)
    dc.utils.chopCharacters(outdf)


    # Append EOF
    dc.utils.appendEOF(outdf)


    # Fill Channel #
    outdf['OwnerChannel'] = owner_channel
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)

    # Write output file
    dc.io.to_io(outdf, out_io, out_format)

def convert_nonlinear(in_io, out_io, format="preV1506", owner_channel="ViuTV"):
    "Convert nonlinear for ViuTV"
    return None

