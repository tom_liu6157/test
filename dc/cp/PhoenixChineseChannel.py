import pandas as pd
import numpy as np
import datetime
import math
import csv
import re
import calendar
from collections import OrderedDict
import dc.utils
import dc.io
import dc.header
import dc.mapping
import dc.settings

def convert_linear(in_io, out_io, out_format="preV1506", owner_channel="Phoenix Chinese Channel"):
    "Convert linear schedule for Phoenix Chinese Channel"

    # Read input file
    indf = pd.read_excel(in_io, sheetname=0, skiprows=[0,1], na_values=["NIL"], keep_default_na=True)

    
    # Create output DataFrame
    outdf = pd.DataFrame(columns=dc.header.internalHeader)

    # Extract data from full title
    fullTitleEng = indf["Program Name In English"][indf["Program Name In English"].notnull()].apply(lambda x: str(x).strip())
    fullTitleChi = indf["Program Name In Chinese"][indf["Program Name In Chinese"].notnull()].apply(lambda x: str(x).strip())
    extractedInfo = pd.DataFrame(columns=["BrandNameEng", "BrandNameChi", "SeasonNo", "EpisodeNo", "EpisodeNameEng", "EpisodeNameChi"])
    tempExtractedInfo = [
        fullTitleEng.str.extract("(?P<BrandNameEng>.*?):(?P<EpisodeNameEng>.+)\s*\((?P<EpisodeNo>\d+)\)$", expand=False),
        fullTitleEng.str.extract("(?P<BrandNameEng>.*?) \((?P<EpisodeNo>\d+)\)$", expand=False),
        fullTitleEng.str.extract("(?P<BrandNameEng>.*?) \((?P<EpisodeNameEng>[0-9]+/[0-9]+/[0-9]+\s*)\)$", expand=False),
        fullTitleChi.str.extract("(?P<BrandNameChi>.*?):(?P<EpisodeNameChi>.+)\s*\((?P<EpisodeNo>\d+)\)$", expand=False),
        fullTitleChi.str.extract("(?P<BrandNameChi>.*?) \((?P<EpisodeNo>\d+)\)$", expand=False),
        fullTitleChi.str.extract("(?P<BrandNameChi>.*?) \((?P<EpisodeNameChi>[0-9]+/[0-9]+/[0-9]+\s*)\)$", expand=False)
    ]
    extractedInfo["BrandNameEng"] = tempExtractedInfo[0]["BrandNameEng"]
    extractedInfo["BrandNameEng"].fillna(tempExtractedInfo[1]["BrandNameEng"], inplace=True)
    extractedInfo["BrandNameEng"].fillna(tempExtractedInfo[2]["BrandNameEng"], inplace=True)
    extractedInfo["BrandNameEng"].fillna(fullTitleEng, inplace=True)
    extractedInfo["BrandNameChi"] = tempExtractedInfo[3]["BrandNameChi"]
    extractedInfo["BrandNameChi"].fillna(tempExtractedInfo[4]["BrandNameChi"], inplace=True)
    extractedInfo["BrandNameChi"].fillna(tempExtractedInfo[5]["BrandNameChi"], inplace=True)
    extractedInfo["BrandNameChi"].fillna(fullTitleChi, inplace=True)
    extractedInfo["EpisodeNo"] = tempExtractedInfo[0]["EpisodeNo"]
    extractedInfo["EpisodeNo"].fillna(tempExtractedInfo[1]["EpisodeNo"], inplace=True)
    extractedInfo["EpisodeNo"].fillna(tempExtractedInfo[3]["EpisodeNo"], inplace=True)
    extractedInfo["EpisodeNo"].fillna(tempExtractedInfo[4]["EpisodeNo"], inplace=True)
    extractedInfo["EpisodeNameEng"] = tempExtractedInfo[0]["EpisodeNameEng"]
    extractedInfo["EpisodeNameEng"].fillna(tempExtractedInfo[2]["EpisodeNameEng"], inplace=True)
    extractedInfo["EpisodeNameChi"] = tempExtractedInfo[3]["EpisodeNameChi"]
    extractedInfo["EpisodeNameChi"].fillna(tempExtractedInfo[5]["EpisodeNameChi"], inplace=True)


    # Fill output DataFrame with related info
    outdf["TXDate"] = indf["Date"].apply(lambda x: pd.to_datetime(x, format="%d/%m/%Y")).apply(lambda x:x.strftime("%Y%m%d"))
    outdf["ActualTime"] = indf["HK Time"].apply(lambda x: pd.to_datetime(x, format="%H:%M")).apply(lambda x:x.strftime("%H:%M"))
    outdf["BrandNameEng"] = extractedInfo["BrandNameEng"].str.translate(dc.mapping.charTranslateTable)
    outdf["BrandNameChi"] = extractedInfo["BrandNameChi"].str.translate(dc.mapping.charTranslateTable)
    outdf["BrandNameChi"].fillna(outdf["BrandNameEng"], inplace=True)
    outdf["BrandNameEng"].fillna(outdf["BrandNameChi"], inplace=True)
    outdf["SynopsisEng"] = indf["Program Syp. (English)"].str.translate(dc.mapping.charTranslateTable)
    outdf["ShortSynopsisEng"] = outdf["SynopsisEng"][outdf["SynopsisEng"].notnull()].apply(lambda x: x[0:dc.settings.char_limit.get("ShortSynopsisEng")])
    outdf["SynopsisChi"] = indf["Program Syp. (Chinese)"].str.translate(dc.mapping.charTranslateTable)
    outdf["ShortSynopsisChi"] = outdf["SynopsisChi"][outdf["SynopsisChi"].notnull()].apply(lambda x: x[0:dc.settings.char_limit.get("ShortSynopsisChi")])
    outdf["EpisodeNameEng"] = extractedInfo["EpisodeNameEng"][extractedInfo["EpisodeNameEng"].notnull()].astype(str).str.translate(dc.mapping.charTranslateTable)
    outdf["EpisodeNameChi"] = extractedInfo["EpisodeNameChi"][extractedInfo["EpisodeNameChi"].notnull()].astype(str).str.translate(dc.mapping.charTranslateTable)
    outdf["EpisodeNo"] = extractedInfo["EpisodeNo"]
    outdf["PremierOld"] = "0"
    outdf["Premier"] = "N"
    outdf["IsLive"] = "N"
    outdf["IsEpisodic"] = (outdf["EpisodeNo"].notnull() | outdf["EpisodeNameEng"].notnull()).map({True:"Y", False:"N"})
    dc.utils.title_df(outdf)
    outdf[["FullTitleEng", "FullTitleChi"]] = outdf.apply(dc.utils.constructFullTitle, axis=1)
    outdf[["ProgrammeNameEng", "ProgrammeNameChi"]] = outdf.apply(dc.utils.constructProgrammeName, axis=1)
    dc.utils.translateChar(outdf)
    dc.utils.chopCharacters(outdf)


    # Append EOF
    dc.utils.appendEOF(outdf)


    # Fill Channel #
    outdf['OwnerChannel'] = owner_channel
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)

    # Write output file
    dc.io.to_io(outdf, out_io, out_format)

def convert_nonlinear(in_io, out_io, format="preV1506", owner_channel="Phoenix Chinese Channel"):
    "Convert nonlinear for Phoenix Chinese Channel"
    return None

