import pandas as pd
import numpy as np
import datetime
import math
import csv
import re
import calendar
from collections import OrderedDict
import dc.utils
import dc.io
import dc.header
import dc.mapping
import dc.settings

def convert_linear(in_io, out_io, out_format="preV1506", owner_channel="Now Premier League 2"):
    "Convert linear schedule for Now Premier League 2"

    # Read input file
    indf = pd.read_excel(in_io, sheetname=0)
    indf.dropna(subset=["TX Date"], inplace=True)

    
    # Create output DataFrame
    outdf = pd.DataFrame(columns=dc.header.internalHeader)

    # Extract data from full title
    channel_no = dc.mapping.ownerChannelMapping.get(owner_channel, owner_channel)


    # Fill output DataFrame with related info
    outdf["TXDate"] = indf["TX Date"].apply(lambda x: re.match("^([0-9]+)(\.0*)?$", str(x)).groups()[0] if re.match("^([0-9]+)(\.0*)?$", str(x)) else x.strftime("%Y%m%d") if isinstance(x, datetime.datetime) or isinstance(x, datetime.time) else pd.to_datetime(str(x)).strftime("%Y%m%d") if "-" in str(x) or "/" in str(x) else x)
    outdf["ActualTime"] = indf["Actual Time"].astype('str').str.slice(-8).str.slice(0, 5).str.zfill(5)
    if channel_no == "620" or channel_no == "621":
        extractedInfo = pd.DataFrame(columns=["BrandNameEng", "BrandNameChi", "SeasonNo"])
        extractedInfo["BrandNameEng"] = indf["Brand name (English)"].str.replace("(?i)SERIES \d+", "").str.replace("\s*:\s*$", "").str.replace("\s*-\s*$", "").str.strip()
        extractedInfo["BrandNameChi"] = indf["Brand name (Chinese)"].str.replace("\s*:\s*$", "").str.replace("\s*-\s*$", "").str.strip()
        extractedInfo["SeasonNo"] = indf["Brand name (English)"].str.extract(".*(?i)SERIES (\d+).*", expand=False)
        outdf["BrandNameEng"] = extractedInfo["BrandNameEng"].str.translate(dc.mapping.charTranslateTable).str.strip()
        outdf["BrandNameChi"] = extractedInfo["BrandNameChi"].str.translate(dc.mapping.charTranslateTable).str.strip()
        outdf["SeasonNo"] = extractedInfo["SeasonNo"] 
    else:
        outdf["BrandNameEng"] = indf["Brand name (English)"].str.translate(dc.mapping.charTranslateTable).str.strip()
        outdf["BrandNameChi"] = indf["Brand name (Chinese)"].str.translate(dc.mapping.charTranslateTable).str.strip()
        outdf["SeasonNo"] = indf["Season no. (max 3 digits)"]
    outdf["BrandNameChi"].fillna(outdf["BrandNameEng"], inplace=True)
    outdf["BrandNameEng"].fillna(outdf["BrandNameChi"], inplace=True)
    outdf["EpisodeNo"] = indf["Episode no. (max 5 digits)"].apply(lambda x: "" if x == "-" else re.match("^([0-9]+)(\.0+)?$", str(x)).groups()[0] if re.match("^[0-9]+(\.0+)?$", str(x)) else x)                                                                                                                                   
    outdf["EpisodeNameEng"] = indf["Episode name (English)"][indf["Episode name (English)"].notnull()].astype(str).str.translate(dc.mapping.charTranslateTable).str.strip().apply(lambda x: np.NaN if str(x).strip()=="" else x)                                                                                                             
    outdf["EpisodeNameEng"] = outdf[["EpisodeNo", "EpisodeNameEng"]].apply(lambda x: "" if "Episode Number "+str(x["EpisodeNo"]) == str(x["EpisodeNameEng"]) or re.match("^\s*Episode Number\s*\d*\s*$", str(x["EpisodeNameEng"]),re.I) is not None else x["EpisodeNameEng"],axis=1)
    outdf["EpisodeNameChi"] = indf["Episode name (Chinese)"][indf["Episode name (Chinese)"].notnull()].astype(str).str.translate(dc.mapping.charTranslateTable).str.strip().apply(lambda x: np.NaN if str(x).strip()=="" else x)
    if channel_no in ["638", "639", "632"]:
        outdf["EpisodeNo"] = outdf["EpisodeNo"].mask(outdf["EpisodeNo"].astype(str).str.replace("\.0*","") == "0")
        outdf["EpisodeNameEng"] = outdf["EpisodeNameEng"][outdf["EpisodeNameEng"].notnull()].astype(str).str.split(" vs ").str.join(" V ").apply(lambda x: np.NaN if str(x).strip() in ["Episode Number 0", "Teams TBC", "Team TBC", "TBC"] else x)       
        outdf["EpisodeNameChi"] = outdf["EpisodeNameChi"][outdf["EpisodeNameChi"].notnull()].astype(str).str.split(" vs ").str.join(" V ").apply(lambda x: np.NaN if str(x).strip() in ["Episode Number 0", "Teams TBC", "Team TBC", "TBC"] else x)       
    if channel_no in ["643", "644", "645"]:
        outdf["EpisodeNameEng"] = outdf["EpisodeNameEng"][outdf["EpisodeNameEng"].notnull()].astype(str).str.split(" vs ").str.join(" V ")      
        outdf["EpisodeNameChi"] = outdf["EpisodeNameChi"][outdf["EpisodeNameChi"].notnull()].astype(str).str.split(" vs ").str.join(" V ")    
    outdf["EpisodeNameChi"].fillna(outdf["EpisodeNameEng"], inplace=True)
    outdf["EpisodeNameEng"].fillna(outdf["EpisodeNameChi"], inplace=True)
    outdf["Premier"] = indf["Premiere (Y/N)"]
    outdf["IsLive"] = indf["Is Live (Y/N)"]
    outdf["Recordable"] = "Y"
    outdf["Genre"] = indf["Genre"].apply(lambda x: dc.mapping.genreMapping.get(x.lower(), x) if pd.notnull(x) else None)
    outdf["SubGenre"] = indf["Sub-Genre"][indf["Sub-Genre"].notnull()]
    outdf["SubGenre"] = outdf[["SubGenre", "EpisodeNameEng", "EpisodeNameChi"]].apply(lambda x: str(x["SubGenre"])+"/Match" 
                                if (re.match(".*\sv\s.*", str(x["EpisodeNameEng"]),re.I) is not None or re.match(".*\s對\s.*", str(x["EpisodeNameChi"]),re.I) is not None) and "Match".upper() not in re.sub('\s','',str(x["SubGenre"])).upper().split("/") and pd.notnull(x["SubGenre"]) and str(x["SubGenre"]).strip() != ''
                                else "Match" if (re.match(".*\sv\s.*", str(x["EpisodeNameEng"]),re.I) is not None or re.match(".*\s對\s.*", str(x["EpisodeNameChi"]),re.I) is not None) and (pd.isnull(x["SubGenre"]) or str(x["SubGenre"]).strip() == '')
                                else x["SubGenre"], axis = 1)
    outdf["SubGenre"] = outdf["SubGenre"].astype(str).str.split("/").apply(lambda subGenres: sorted(set([item for subGenre in subGenres for item in dc.mapping.subGenreMapping.get(subGenre.lower(), [subGenre])]))).str.join("/").apply(lambda x: x[0:dc.settings.char_limit.get("SubGenre")])
    outdf["EpisodeNo"] = outdf[["EpisodeNo","EpisodeNameEng"]].apply(lambda x: "" if (pd.notnull(x["EpisodeNameEng"]) and re.match("^\s*Week\s+(\d+)\s*$", str(x["EpisodeNameEng"]), re.I) is not None and re.search("^\s*Week\s+(\d+)\s*$", str(x["EpisodeNameEng"]), re.I).group(1) ==  str(x["EpisodeNo"])) or str(x["EpisodeNo"]) == "-" else x["EpisodeNo"],axis=1)
    outdf["EpisodeNo"] = outdf[["EpisodeNo","EpisodeNameEng","EpisodeNameChi"]].apply(lambda x: "" if (pd.notnull(x["EpisodeNameEng"]) and str(x["EpisodeNameEng"]).strip() != "") or (pd.notnull(x["EpisodeNameChi"]) and str(x["EpisodeNameChi"]).strip() != "") else x["EpisodeNo"],axis=1)
    if  "Episodic (Y/N)" in indf.columns:
        outdf["IsEpisodic"] = np.where(indf["Episodic (Y/N)"].notnull() & ~indf["Episodic (Y/N)"].astype(str).str.match("^[\s]*$"), indf["Episodic (Y/N)"], "N")
    else:
        outdf["IsEpisodic"] = "N"
    outdf["IsRestartTV"] = "Y"
    outdf["PortraitImage"] = indf["Portrait Image"]
    outdf["ImageWithTitle"] = indf["Image with title  (Y/N)"]
    if "Bilingual (Y/N)" in indf.columns:
        outdf["Bilingual"] = indf["Bilingual (Y/N)"]
    if channel_no not in ["643","644","645"]:
        outdf["IsNPVRProg"] = np.where(np.logical_and(outdf["IsLive"].astype("str").str.upper() == "Y",np.logical_or(
        outdf["SubGenre"].apply(lambda x: (re.sub("\s","",str(x)).upper() == "LaLiga/Soccer/Match".upper())
                                       or (re.sub("\s","",str(x)).upper() == "LaLiga/Match/Soccer".upper())
                                       or (re.sub("\s","",str(x)).upper() == "Soccer/LaLiga/Match".upper())
                                       or (re.sub("\s","",str(x)).upper() == "Soccer/Match/LaLiga".upper())
                                       or (re.sub("\s","",str(x)).upper() == "Match/LaLiga/Soccer".upper())
                                       or (re.sub("\s","",str(x)).upper() == "Match/Soccer/LaLiga".upper())),
        outdf["SubGenre"].apply(lambda x: (re.sub("\s","",str(x)).upper() == "PL/Soccer/Match".upper())
                                       or (re.sub("\s","",str(x)).upper() == "Soccer/PL/Match".upper())
                                       or (re.sub("\s","",str(x)).upper() == "Match/Soccer/PL".upper())
                                       or (re.sub("\s","",str(x)).upper() == "Soccer/Match/PL".upper())
                                       or (re.sub("\s","",str(x)).upper() == "Match/PL/Soccer".upper())
                                       or (re.sub("\s","",str(x)).upper() == "PL/Match/Soccer".upper())
                               ))) ,"Y", "N")
    else:
        outdf["IsNPVRProg"] = "Y"
    outdf["ExpirationDate"] = np.where(                                    (outdf["Recordable"] == "Y")                                    & (outdf["IsNPVRProg"] == "N")                                    & (outdf["IsRestartTV"] == "Y") #                                   & (outdf["IsLive"] != "Y") \
                                       & 
                                       ((outdf["IsLive"].astype("str").str.upper() == "N") | (outdf["IsLive"].isnull() | outdf["IsLive"].astype(str).str.match("^[\s]*$"))) \
                                         , '1', \
                                      '')
    if channel_no not in ["638", "639", "632", "643", "644", "645"]:
        outdf['OwnerChannel'] = owner_channel
        outdf['ChannelNo'] = channel_no
        dc.utils.title_df(outdf)
    outdf[["FullTitleEng", "FullTitleChi"]] = outdf.apply(dc.utils.constructFullTitle, axis=1)
    outdf[["ProgrammeNameEng", "ProgrammeNameChi"]] = outdf.apply(dc.utils.constructProgrammeName, axis=1)
    if channel_no == "620":
        outdf.ix[((indf["Episode no. (max 5 digits)"].notnull() & ~indf["Episode no. (max 5 digits)"].astype(str).str.match("^[\s]*$"))              | (indf["Episode name (English)"].notnull() & ~indf["Episode name (English)"].astype(str).str.match("^[\s]*$"))              | (indf["Episode name (Chinese)"].notnull() & ~indf["Episode name (Chinese)"].astype(str).str.match("^[\s]*$")))             & indf["Brand name (English)"].apply(lambda x: True if str(x).upper() not in ["MATCHDAY", "MATCHDAY LIVE"] else False), "IsEpisodic"] = "Y"
        outdf.ix[indf["Brand name (English)"].apply(lambda x: True if str(x).upper() in ["MATCHDAY", "MATCHDAY LIVE"] else False), "IsEpisodic"] = "N"
        outdf.ix[(indf["Episode no. (max 5 digits)"].isnull() | indf["Episode no. (max 5 digits)"].astype(str).str.match("^[\s]*$")) & (indf["Episode name (English)"].isnull() | indf["Episode name (English)"].astype(str).str.match("^[\s]*$")) & (indf["Episode name (Chinese)"].isnull() | indf["Episode name (Chinese)"].astype(str).str.match("^[\s]*$")), "IsEpisodic"] = "N"
        outdf["IsEpisodic"] = outdf[["IsEpisodic","SubGenre","EpisodeNo","EpisodeNameEng","EpisodeNameChi"]].apply(lambda x : "N" if "Match".upper() in re.sub('\s','',str(x["SubGenre"])).upper().split("/") else x["IsEpisodic"],axis=1)
    else:
        outdf["IsEpisodic"] = outdf[["IsEpisodic","SubGenre","EpisodeNo","EpisodeNameEng","EpisodeNameChi"]].apply(lambda x : "N" if "Match".upper() in re.sub('\s','',str(x["SubGenre"])).upper().split("/") else "Y" if (pd.notnull(x["EpisodeNo"]) and str(x["EpisodeNo"]).strip() != "") or (pd.notnull(x["EpisodeNameEng"]) and str(x["EpisodeNameEng"]).strip() != "") or (pd.notnull(x["EpisodeNameChi"]) and str(x["EpisodeNameChi"]).strip() != "") else x["IsEpisodic"],axis=1)
    dc.utils.translateChar(outdf)
    dc.utils.chopCharacters(outdf)


    # Append EOF
    end_of_program = outdf[["TXDate", "ActualTime"]][(outdf["BrandNameEng"].astype('str').replace('\s','',regex=True).str.upper() == "ENDOFPROGRAM")
                                                     | (outdf["BrandNameEng"].astype('str').replace('\s','',regex=True).str.upper() == "ENDOFFILE")]
    if len(end_of_program.index) > 0:
        outdf = outdf[(outdf["BrandNameEng"].astype('str').replace('\s','',regex=True).str.upper() != "ENDOFPROGRAM")
                       & (outdf["BrandNameEng"].astype('str').replace('\s','',regex=True).str.upper() != "ENDOFFILE")]
        outdf.loc[outdf.last_valid_index() + 1, ["TXDate", "ActualTime", "FullTitleEng", "FullTitleChi", "BrandNameEng", "BrandNameChi", "ProgrammeNameEng", "ProgrammeNameChi"]] = [end_of_program["TXDate"][end_of_program.index[0]], end_of_program["ActualTime"][end_of_program.index[0]], "END OF FILE", "END OF FILE", "END OF FILE", "END OF FILE", "END OF FILE", "END OF FILE"]
    else:
        dc.utils.appendEOF(outdf)


    # Fill Channel #
    outdf['OwnerChannel'] = owner_channel
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)

    # Write output file
    dc.io.to_io(outdf, out_io, out_format)

def convert_nonlinear(in_io, out_io, format="preV1506", owner_channel="Now Premier League 2"):
    "Convert nonlinear for Now Premier League 2"
    return None

