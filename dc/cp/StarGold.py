import pandas as pd
import numpy as np
import datetime
import math
import csv
import re
import calendar
from collections import OrderedDict
import dc.utils
import dc.io
import dc.header
import dc.mapping
import dc.settings

def convert_linear(in_io, out_io, out_format="preV1506", owner_channel="Star Gold"):
    "Convert linear schedule for Star Gold"

    # Read input file
    indfs = pd.read_excel(in_io, sheetname=None)                                                 
    indf = pd.DataFrame()
    for key, value in indfs.items():
        indf = indf.append(value, ignore_index=True)
    indf = indf.sort_values(by=['Date','GMT'])
    indf.index = range(1,len(indf) + 1)   # fix index
    def getSubGerneName(subGenre):
        if(pd.notnull(subGenre)):
            subGenre = dc.mapping.subGenreMapping.get(subGenre.lower(), [subGenre])
        return subGenre

    
    # Create output DataFrame
    outdf = pd.DataFrame(columns=dc.header.internalHeader)

    # Extract data from full title
    fullTitleEng = indf["Movie Name"][indf["Movie Name"].notnull()].apply(lambda x: str(x).strip())


    # Fill output DataFrame with related info
    mask = indf["GMT"].astype('str').str.slice(-8).str.slice(0, 5).str.zfill(5).apply(lambda x: int(x[:2]) >= 16)
    outdf["TXDate"] = indf["Date"][mask].apply(lambda x: (pd.to_datetime(x, format="%Y%m%d") + pd.Timedelta(1,'D')).strftime("%Y%m%d")).combine_first(indf["Date"]).apply(lambda x: x.strftime("%Y%m%d"))
    outdf["ActualTime"] = indf["GMT"].apply(lambda x: pd.to_datetime(x, format="%H:%M:%S") + pd.Timedelta(8,'h')).apply(lambda x:x.strftime("%H:%M"))
    outdf["BrandNameEng"] = fullTitleEng.str.translate(dc.mapping.charTranslateTable)
    outdf["BrandNameChi"].fillna(outdf["BrandNameEng"], inplace=True)
    outdf["EpisodeNo"] = indf["Ep no"][indf["Ep no"].notnull()].astype(int).astype(str)
    outdf["SynopsisEng"] = indf["TV Event Synopsis"].apply(lambda x: str(x).strip()).str.replace('(\s*,\s*|\s*、\s*|\s*，\s*|\s*﹑\s*|\s*ʼ\s*)', ', ').str.translate(dc.mapping.charTranslateTable)
    outdf["SynopsisChi"].fillna(outdf["SynopsisEng"], inplace=True)
    outdf["ShortSynopsisEng"] = outdf["SynopsisEng"][outdf["SynopsisEng"].notnull()].apply(lambda x: x[0:dc.settings.char_limit.get("ShortSynopsisEng")])
    outdf["ShortSynopsisChi"] = outdf["SynopsisChi"][outdf["SynopsisChi"].notnull()].apply(lambda x: x[0:dc.settings.char_limit.get("ShortSynopsisChi")])
    outdf["PremierOld"] = "0"
    outdf["Premier"] = "N"
    outdf["IsLive"] = "N"
    outdf["Genre"] = indf["Event Genre"].apply(lambda x: dc.mapping.genreMapping.get(x.lower(), x) if pd.notnull(x) else x)
    outdf["SubGenre"] = indf["Sub Genre"][indf["Sub Genre"].notnull()].apply(lambda subGenres: getSubGerneName(subGenres)).str.join("/").apply(lambda x: x[0:dc.settings.char_limit.get("SubGenre")])
    dc.utils.title_df(outdf)
    outdf[["FullTitleEng", "FullTitleChi"]] = outdf.apply(dc.utils.constructFullTitle, axis=1)
    outdf[["ProgrammeNameEng", "ProgrammeNameChi"]] = outdf.apply(dc.utils.constructProgrammeName, axis=1)
    dc.utils.translateChar(outdf)
    dc.utils.chopCharacters(outdf)


    # Append EOF
    temp = str(indf["Movie Duration"][indf.last_valid_index()]) + " min"
    lastProgDuration = pd.Timedelta(temp)
    dc.utils.appendEOF(outdf, lastprog_duration=lastProgDuration)


    # Fill Channel #
    outdf['OwnerChannel'] = owner_channel
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)

    # Write output file
    dc.io.to_io(outdf, out_io, out_format)

def convert_nonlinear(in_io, out_io, format="preV1506", owner_channel="Star Gold"):
    "Convert nonlinear for Star Gold"
    return None

