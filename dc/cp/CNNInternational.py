import pandas as pd
import numpy as np
import datetime
import math
import csv
import re
import calendar
from collections import OrderedDict
import dc.utils
import dc.io
import dc.header
import dc.mapping
import dc.settings

def convert_linear(in_io, out_io, out_format="preV1506", owner_channel="CNN International"):
    "Convert linear schedule for CNN International"

    # Read input file
    indf = pd.read_excel(in_io, sheetname=0)

    
    # Create output DataFrame
    outdf = pd.DataFrame(columns=dc.header.internalHeader)

    # Extract data from full title
    fullTitleEng = indf["Full Title (English)"][indf["Full Title (English)"].notnull()].apply(lambda x: str(x).strip())
    fullTitleChi = indf["Full Title (Chinese)"][indf["Full Title (Chinese)"].notnull()].apply(lambda x: str(x).strip())
    extractedInfo = pd.DataFrame(columns=["BrandNameEng", "BrandNameChi", "SeasonNo", "EpisodeNo", "EpisodeNameEng", "EpisodeNameChi", "Genre", "Subgenre"])
    tempExtractedInfo = [
        fullTitleEng.str.extract("(?P<BrandNameEng>.*?)\s+S(?P<SeasonNo>\d)(?P<EpisodeNo>\d+)\\s*:*\s*(?P<EpisodeNameEng>.*)", expand=False),
        fullTitleChi.str.extract("(?P<BrandNameChi>.*?)\((第(?P<SeasonNo>\d+)季)*第(?P<EpisodeNo>\d+)集\)", expand=False),
        indf["Genre"].str.extract("(?P<Genre>\w+):?\s*(?P<SubGenre>.*)", expand=False)
    ]
    extractedInfo["BrandNameEng"] = tempExtractedInfo[0]["BrandNameEng"]
    extractedInfo["SeasonNo"] = tempExtractedInfo[0]["SeasonNo"]
    extractedInfo["EpisodeNo"] = tempExtractedInfo[0]["EpisodeNo"]
    extractedInfo["EpisodeNameEng"] = tempExtractedInfo[0]["EpisodeNameEng"]
    extractedInfo["BrandNameEng"].fillna(fullTitleEng, inplace=True)
    extractedInfo["BrandNameChi"] = tempExtractedInfo[1]["BrandNameChi"]
    extractedInfo["BrandNameChi"].fillna(fullTitleChi, inplace=True)
    extractedInfo["EpisodeNo"] = extractedInfo["EpisodeNo"].apply(lambda x: str(int(x)) if isinstance(x, str) else x)
    extractedInfo["EpisodeNameEng"] = extractedInfo["EpisodeNameEng"].apply(lambda x: None if x == "" else x)
    extractedInfo["Genre"] = tempExtractedInfo[2]["Genre"]
    extractedInfo["SubGenre"] = tempExtractedInfo[2]["SubGenre"]
    extractedInfo["SubGenre"] = extractedInfo["SubGenre"].apply(lambda x: x if isinstance(x, str) else "")


    # Fill output DataFrame with related info
    outdf["TXDate"] = indf["TX Date"].apply(lambda x: x.strftime("%Y%m%d"))
    outdf["ActualTime"] = indf["Actual Start Time"].apply(lambda x: x.strftime('%H:%M'))
    outdf["BrandNameEng"] = extractedInfo["BrandNameEng"].str.translate(dc.mapping.charTranslateTable)
    outdf["BrandNameChi"] = extractedInfo["BrandNameChi"].str.translate(dc.mapping.charTranslateTable)
    outdf["BrandNameChi"].fillna(outdf["BrandNameEng"], inplace=True)
    outdf["BrandNameEng"].fillna(outdf["BrandNameChi"], inplace=True)
    outdf["SeasonNo"] = extractedInfo["SeasonNo"]
    outdf["EpisodeNo"] = extractedInfo["EpisodeNo"]
    outdf["EpisodeNameEng"] = extractedInfo["EpisodeNameEng"][extractedInfo["EpisodeNameEng"].notnull()].astype(str).str.translate(dc.mapping.charTranslateTable)
    outdf["SynopsisEng"] = indf["Synopsis (English)"].str.translate(dc.mapping.charTranslateTable)
    outdf["SynopsisChi"].fillna(outdf["SynopsisEng"], inplace=True)
    outdf["ShortSynopsisEng"] = outdf["SynopsisEng"][outdf["SynopsisEng"].notnull()].apply(lambda x: x[0:dc.settings.char_limit.get("ShortSynopsisEng")])
    outdf["ShortSynopsisChi"] = outdf["SynopsisChi"][outdf["SynopsisChi"].notnull()].apply(lambda x: x[0:dc.settings.char_limit.get("ShortSynopsisChi")])
    outdf["PremierOld"] = "0"
    outdf["Premier"] = "N"
    outdf["IsLive"] = "N"
    outdf["Classification"] = indf["Classification"]
    outdf["Genre"] = extractedInfo["Genre"].str.lower().map(dc.mapping.genreMapping).fillna(indf["Genre"])
    outdf["SubGenre"] = extractedInfo["SubGenre"].str.split("/").apply(lambda subGenres: sorted(set([item for subGenre in subGenres for item in dc.mapping.subGenreMapping.get(subGenre.lower(), [subGenre])]))).str.join("/").fillna(indf["Genre"])
    outdf["IsEpisodic"] = (outdf["EpisodeNo"].notnull() | outdf["EpisodeNameEng"].notnull() | outdf["EpisodeNameChi"].notnull()).map({True:"Y", False:"N"})
    dc.utils.title_df(outdf)
    outdf[["FullTitleEng", "FullTitleChi"]] = outdf.apply(dc.utils.constructFullTitle, axis=1)
    outdf[["ProgrammeNameEng", "ProgrammeNameChi"]] = outdf.apply(dc.utils.constructProgrammeName, axis=1)
    dc.utils.translateChar(outdf)
    dc.utils.chopCharacters(outdf)


    # Append EOF
    dc.utils.appendEOF(outdf)


    # Fill Channel #
    outdf['OwnerChannel'] = owner_channel
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)

    # Write output file
    dc.io.to_io(outdf, out_io, out_format)

def convert_nonlinear(in_io, out_io, format="preV1506", owner_channel="CNN International"):
    "Convert nonlinear for CNN International"
    return None

