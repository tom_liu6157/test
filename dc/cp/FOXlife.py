import pandas as pd
import numpy as np
import datetime
import math
import csv
import re
import calendar
from collections import OrderedDict
import dc.utils
import dc.io
import dc.header
import dc.mapping
import dc.settings

def convert_linear(in_io, out_io, out_format="preV1506", owner_channel="FOXlife"):
    "Convert linear schedule for FOXlife"

    # Read input file
    indf = pd.read_excel(in_io, sheetname=0, skiprows=2, na_values=["n/a"], keep_default_na=True)
    indf.columns = [s.strip() for s in indf.columns]
    indf = indf.rename(columns={'Full Title (English)': 'fulltitleenglishmax40characters'})
    indf = indf.rename(columns={'Full Title (Chinese)': 'fulltitlechinesemax24characters'})
    indf = indf.rename(columns={'Season No.': 'seasonno.max3digits'})
    indf = indf.rename(columns={'Episode no.': 'episodeno.max5digits'})
    indf = indf.rename(columns={'Short Synopsis (English)': 'shortsynopsisenglishmax120chars'})
    indf = indf.rename(columns={'Short Synopsis (Chinese)': 'Short Synopsis (Chinese) (max 120 chars)'})
    indf = indf.rename(columns={'Short Synopsis (Chinese)': 'Short Synopsis (Chinese) (max 120 chars)'})
    indf = indf.rename(columns={'Original language': 'Original language (max 50 chars)'})
    indf = indf.rename(columns={'Region Code': 'Region Code (max 2 chars)'})
    indf = indf.rename(columns={'First release year': 'First release year (max 4 digits)'})
    indf.columns = ["".join(s.split()).replace("(", "").replace(")", "").replace("/", "").lower() for s in indf.columns]

    
    # Create output DataFrame
    outdf = pd.DataFrame(columns=dc.header.internalHeader)

    # Extract data from full title


    # Fill output DataFrame with related info
    excepted_column = ["PID", "Premier", "Recordable", "Is NPVR Prog", "Is Restart TV", "effective_date", "expiration_date", "Media ID (max. 12 chars)"]
    for internal_column, indf_column in dc.header.postV1506HeaderMapping.items():
        if (indf_column not in excepted_column): 
            edited_indf_column = "".join(indf_column.split()).replace("(", "").replace(")", "").replace("/", "").lower()
            if (edited_indf_column in indf.columns):
                outdf[internal_column] = indf[edited_indf_column]
                if (outdf[internal_column].dtype == object):
                    outdf[internal_column] = outdf[internal_column].where(outdf[internal_column].apply(lambda x: pd.notnull(x) and len(str(x).strip()) > 0))
    outdf['FirstReleaseYear'] = outdf['FirstReleaseYear'][outdf['FirstReleaseYear'].notnull()].apply(lambda x: str(x))
    outdf["TXDate"] = outdf["TXDate"].apply(lambda x: pd.to_datetime(str(x), format="%Y-%m-%d") if '-' in str(x) else pd.to_datetime(str(x), format="%Y%m%d")).apply(lambda x: x.strftime("%Y%m%d"))
    outdf["ActualTime"] = indf["actualtime"].astype('str').str.slice(-8).str.slice(0, 5).str.zfill(5)
    outdf["EpisodeNameEng"] = outdf["EpisodeNameEng"][outdf["EpisodeNameEng"].notnull()].astype('str').str.replace("Ep \d*", "").str.replace("Part \d*", "").str.strip()
    outdf["EpisodeNameEng"] = outdf["EpisodeNameEng"][outdf["EpisodeNameEng"].notnull()].astype('str').str.strip().str.extract("(.*?)(S\d*?)?(,\s*\d*)?$", expand=False)[0].str.strip()
    outdf["BrandNameChi"] = outdf["BrandNameChi"].str.strip().str.extract("(.*?)\s*(S\d*?)*\s*(（*第.*[季集]）*.*)*?$", expand=False)[0].str.strip()
    outdf["EpisodeNameEng"] = outdf[["BrandNameEng", "EpisodeNameEng"]].apply(lambda x: None if pd.notnull(x["BrandNameEng"]) and pd.notnull(x["EpisodeNameEng"]) and str(x["BrandNameEng"]).strip() == str(x["EpisodeNameEng"]).strip() else x["EpisodeNameEng"], axis=1)
    outdf["EpisodeNameChi"] = outdf[["BrandNameChi", "EpisodeNameChi"]].apply(lambda x: None if pd.notnull(x["BrandNameChi"]) and pd.notnull(x["EpisodeNameChi"]) and str(x["BrandNameChi"]).strip() == str(x["EpisodeNameChi"]).strip() else x["EpisodeNameChi"], axis=1)
    outdf["SeasonNameEng"] = outdf[["BrandNameEng", "SeasonNameEng"]].apply(lambda x: None if pd.notnull(x["BrandNameEng"]) and pd.notnull(x["SeasonNameEng"]) and str(x["SeasonNameEng"]).strip() == str(x["EpisodeNameEng"]).strip() else x["SeasonNameEng"], axis=1)
    outdf["SeasonNameChi"] = outdf[["BrandNameChi", "SeasonNameChi"]].apply(lambda x: None if pd.notnull(x["BrandNameChi"]) and pd.notnull(x["SeasonNameChi"]) and str(x["SeasonNameChi"]).strip() == str(x["EpisodeNameChi"]).strip() else x["SeasonNameChi"], axis=1)
    outdf["BrandNameChi"] = outdf["BrandNameChi"].apply(lambda x: np.nan if isinstance(x, str) and len(x.strip()) == 0 else x)
    outdf["BrandNameChi"] = outdf["BrandNameChi"][outdf["BrandNameChi"].notnull()].apply(lambda x: dc.utils.jianfan(str(x))).astype(str).str.translate(dc.mapping.charTranslateTable).str.strip()
    outdf["BrandNameChi"].fillna(outdf["BrandNameEng"], inplace=True)
    outdf["BrandNameEng"].fillna(outdf["BrandNameChi"], inplace=True)
    outdf["SponsorTextStuntChi"].fillna(outdf["SponsorTextStuntEng"], inplace=True)
    outdf["SponsorTextStuntEng"].fillna(outdf["SponsorTextStuntChi"], inplace=True)
    outdf["BrandNameChi"].fillna(outdf["BrandNameEng"], inplace=True)
    outdf["BrandNameEng"].fillna(outdf["BrandNameChi"], inplace=True)
    outdf["SeasonNameChi"].fillna(outdf["SeasonNameEng"], inplace=True)
    outdf["SeasonNameEng"].fillna(outdf["SeasonNameChi"], inplace=True)
    outdf["EpisodeNameChi"].fillna(outdf["EpisodeNameEng"], inplace=True)
    outdf["EpisodeNameEng"].fillna(outdf["EpisodeNameChi"], inplace=True)
    outdf["EpisodeNameChi"].fillna(outdf["EpisodeNameEng"], inplace=True)
    outdf["EpisodeNameChi"] = outdf["EpisodeNameChi"][outdf["EpisodeNameChi"].notnull()].apply(lambda x: dc.utils.jianfan(str(x))).astype(str).str.translate(dc.mapping.charTranslateTable)
    outdf["EpisodeNameEng"].fillna(outdf["EpisodeNameChi"], inplace=True)
    outdf["SynopsisEng"].fillna(outdf["ShortSynopsisEng"], inplace=True)
    outdf["SynopsisEng"].fillna(outdf["SynopsisChi"], inplace=True)
    outdf["SynopsisChi"].fillna(outdf["SynopsisEng"], inplace=True)
    outdf["SynopsisChi"] = outdf["SynopsisChi"][outdf["SynopsisChi"].notnull()].apply(lambda x: dc.utils.jianfan(str(x))).astype(str).str.translate(dc.mapping.charTranslateTable)
    outdf["ShortSynopsisChi"].fillna(outdf["ShortSynopsisEng"], inplace=True)
    outdf["ShortSynopsisChi"] = outdf["ShortSynopsisChi"][outdf["ShortSynopsisChi"].notnull()].apply(lambda x: dc.utils.jianfan(str(x))).astype(str).str.translate(dc.mapping.charTranslateTable)
    outdf["ShortSynopsisEng"].fillna(outdf["ShortSynopsisChi"], inplace=True)
    outdf["SynopsisChi"]=outdf["SynopsisChi"][outdf["SynopsisChi"].notnull()].replace("nan",'')
    outdf["ShortSynopsisChi"]=outdf["ShortSynopsisChi"][outdf["ShortSynopsisChi"].notnull()].replace("nan",'')
    outdf["ShortSynopsisEng"]=outdf["ShortSynopsisEng"][outdf["ShortSynopsisEng"].notnull()].replace("nan",'')
    outdf["SeasonNo"] = outdf["SeasonNo"][outdf["SeasonNo"].notnull()].astype(int).astype(str)
    outdf["EpisodeNo"] = outdf["EpisodeNo"][outdf["EpisodeNo"].notnull()].astype(int).astype(str)
    outdf["FirstReleaseYear"] = outdf["FirstReleaseYear"][outdf["FirstReleaseYear"].notnull()].astype(float).astype(int).astype(str)
    outdf["PremierOld"] = "0"
    outdf["IsEpisodic"] = (outdf["EpisodeNo"].notnull() | outdf["EpisodeNameEng"].notnull() | outdf["EpisodeNameChi"].notnull()).map({True:"Y", False:"N"})
    text_column = ["SponsorTextStuntEng", "SponsorTextStuntChi", "BrandNameEng", "BrandNameChi", "EditionVersionEng", "EditionVersionChi", "SeasonNameEng", "SeasonNameChi", "EpisodeNameEng", "EpisodeNameChi", "SynopsisEng", "SynopsisChi", "ShortSynopsisEng", "ShortSynopsisChi", "DirectorProducerEng", "DirectorProducerChi", "CastEng", "CastChi"]
    for column in text_column:
        outdf[column] = outdf[column].astype(str).where(outdf[column].notnull()).str.translate(dc.mapping.charTranslateTable)
    dc.utils.title_df(outdf)
    outdf[["FullTitleEng", "FullTitleChi"]] = outdf.apply(dc.utils.constructFullTitle, axis=1)
    outdf[["ProgrammeNameEng", "ProgrammeNameChi"]] = outdf.apply(dc.utils.constructProgrammeName, axis=1)
    dc.utils.translateChar(outdf)
    dc.utils.chopCharacters(outdf)


    # Append EOF
    dc.utils.appendEOF(outdf)


    # Fill Channel #
    outdf['OwnerChannel'] = owner_channel
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)

    # Write output file
    dc.io.to_io(outdf, out_io, out_format)

def convert_nonlinear(in_io, out_io, format="preV1506", owner_channel="FOXlife"):
    "Convert nonlinear for FOXlife"
    return None

