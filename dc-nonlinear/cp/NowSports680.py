import pandas as pd
import numpy as np
import datetime
import math
import csv
import re
import calendar
from collections import OrderedDict
import dc.utils
import dc.io
import dc.header
import dc.mapping
import dc.settings

def convert_linear(in_io, out_io, out_format="preV1506", owner_channel="Now Sports 680"):
    "Convert linear schedule for Now Sports 680"

    # Read input file
    indf = pd.read_excel(in_io, sheetname=0, skiprows=1, skip_footer=0)
    indf = indf.drop(indf[pd.isnull(indf["Date"])].index)
    indf["HKT"] = indf["HKT"].apply(lambda x: str(x)[10:].strip() if len(str(x))>8 else str(x))

    
    # Create output DataFrame
    outdf = pd.DataFrame(columns=dc.header.internalHeader)

    # Extract data from full title
    fullEpisodeEng = indf["Episode"][indf["Episode"].notnull()].apply(lambda x: str(x).strip())
    extractedInfo = pd.DataFrame(columns=["SponsorTextEng", "SponsorTextChi", "BrandNameEng", "BrandNameChi", "EditionVersionEng", "EditionVersionChi", "SeasonNo", "SeasonNameEng", "SeasonNameChi", "EpisodeNo", "EpisodeNameEng", "EpisodeNameChi", "RegionCode", "FirstReleaseYear", "isLive"])
    tempExtractedInfo = [
        fullEpisodeEng.str.extract("^(?P<EpisodeNameEng>.*?)\s*(#(?P<EpisodeNo>\d+))*$", expand=False),
        fullEpisodeEng.str.extract("^(?P<EpisodeNameEng>.*?)Episode (?P<EpisodeNo>\d+)$", expand=False)
    ]
    extractedInfo["EpisodeNo"] = tempExtractedInfo[1]["EpisodeNo"]
    extractedInfo["EpisodeNo"].fillna(tempExtractedInfo[0]["EpisodeNo"], inplace=True)
    extractedInfo["EpisodeNameEng"] = tempExtractedInfo[1]["EpisodeNameEng"]
    extractedInfo["EpisodeNameEng"].fillna(tempExtractedInfo[0]["EpisodeNameEng"], inplace=True)


    # Fill output DataFrame with related info
    outdf["TXDate"] = indf["Date"].apply(lambda x: pd.to_datetime(x, format="%Y/%m/%d %H:%M:%S")).apply(lambda x:x.strftime("%Y%m%d"))
    outdf["ActualTime"] = indf["HKT"].apply(lambda x: pd.to_datetime(x, format="%H:%M:%S")).apply(lambda x:x.strftime("%H:%M"))
    outdf["BrandNameEng"] = indf["Title"][indf["Title"].notnull()].str.translate(dc.mapping.charTranslateTable).apply(lambda x: str(x)[0:dc.settings.char_limit.get("BrandNameEng")])
    outdf["BrandNameChi"].fillna(outdf["BrandNameEng"], inplace=True)
    outdf["EpisodeNo"] = extractedInfo["EpisodeNo"]
    outdf["EpisodeNameEng"] = extractedInfo["EpisodeNameEng"][extractedInfo["EpisodeNameEng"].notnull()].str.translate(dc.mapping.charTranslateTable).apply(lambda x: x[0:dc.settings.char_limit.get("EpisodeNameEng")])
    outdf[["EpisodeNameEng"]] = outdf.apply(dc.utils.constructEpisodeNameEng, axis=1)
    outdf["EpisodeNameEng"] = outdf.apply(lambda x: x["EpisodeNameEng"][len(x["BrandNameEng"]):] if pd.notnull(x["EpisodeNameEng"]) and x["EpisodeNameEng"].startswith(x["BrandNameEng"]) else x["EpisodeNameEng"], axis=1).str.strip()
    outdf["EpisodeNo"] = outdf["EpisodeNo"].apply(lambda x: None if x == "-" else re.match("^([0-9]+)(?:\.0+|[0-9]*)$", str(x)).groups()[0] if re.match("^[0-9]+(?:\.0+|[0-9]*)$", str(x)) else x)
    outdf["EpisodeNameChi"].fillna(outdf["EpisodeNameEng"], inplace=True)
    outdf["PremierOld"] = "0"
    outdf["Premier"] = "N"
    outdf["IsLive"] = "N"
    outdf["IsLive"] = indf["Status"][indf["Status"].notnull()].apply(lambda x: "Y" if x=="Live" else "N")
    outdf["IsLive"].fillna("N", inplace=True)
    outdf["Classification"] = indf["Classification"].apply(lambda x: str(x).strip())
    outdf["IsEpisodic"] = outdf["BrandNameEng"].apply(lambda x: "Y" if x.lower()=="epic tv magazine show" or x.lower()=="si film" or x.lower()=="si now" or x.lower()=="si wire" or x.lower()=="nhl on the fly" else "N")
    outdf["IsEpisodic"] = outdf[["IsEpisodic","SubGenre","EpisodeNo","EpisodeNameEng","EpisodeNameChi"]].apply(lambda x : "N" if "Match".upper() in re.sub('\s','',str(x["SubGenre"])).upper().split("/") else "Y" if (pd.notnull(x["EpisodeNo"]) and str(x["EpisodeNo"]).strip() != "") or (pd.notnull(x["EpisodeNameEng"]) and str(x["EpisodeNameEng"]).strip() != "") or (pd.notnull(x["EpisodeNameChi"]) and str(x["EpisodeNameChi"]).strip() != "") else x["IsEpisodic"],axis=1)
    outdf['OwnerChannel'] = "Now Sports 680"
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)
    dc.utils.title_df(outdf)
    outdf[["FullTitleEng", "FullTitleChi"]] = outdf.apply(dc.utils.constructFullTitle, axis=1)
    outdf[["ProgrammeNameEng", "ProgrammeNameChi"]] = outdf.apply(dc.utils.constructProgrammeName, axis=1)
    dc.utils.translateChar(outdf)
    dc.utils.chopCharacters(outdf)


    # Append EOF
    temp = ""
    if isinstance(indf["Duration"][indf.last_valid_index()], str):
        temp = pd.Timedelta(pd.to_datetime(indf["Duration"][indf.last_valid_index()]).strftime("%H:%M:%S"))
    else:
        temp = str(indf["Duration"][indf.last_valid_index()])
    lastProgDuration = pd.Timedelta(temp)
    dc.utils.appendEOF(outdf, lastprog_duration=lastProgDuration)


    # Fill Channel #
    outdf['OwnerChannel'] = owner_channel
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)

    # Write output file
    dc.io.to_io(outdf, out_io, out_format)

def convert_nonlinear(in_io, out_io, format="preV1506", owner_channel="Now Sports 680"):
    "Convert nonlinear for Now Sports 680"
    return None

