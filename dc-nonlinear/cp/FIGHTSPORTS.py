import pandas as pd
import numpy as np
import datetime
import math
import csv
import re
import calendar
from collections import OrderedDict
import dc.utils
import dc.io
import dc.header
import dc.mapping
import dc.settings

def convert_linear(in_io, out_io, out_format="preV1506", owner_channel="FIGHT SPORTS"):
    "Convert linear schedule for FIGHT SPORTS"

    # Read input file
    indf = pd.read_excel(in_io, sheetname=0)

    
    # Create output DataFrame
    outdf = pd.DataFrame(columns=dc.header.internalHeader)

    # Extract data from full title


    # Fill output DataFrame with related info
    outdf["TXDate"] = indf[indf.columns[1]].apply(lambda x: pd.to_datetime(re.match("^([0-9]+([-/][0-9]{0,2}){0,2}|([0-9]{0,2}[-/]){0,2}[0-9]+)(\s[0-9]{2}(:{0,1}[0-9]{2}){0,2}){0,1}(\.[0-9]+){0,1}$", str(x)).groups()[0]).strftime("%Y%m%d") if re.match("^([0-9]+([-/][0-9]{0,2}){0,2}|([0-9]{0,2}[-/]){0,2}[0-9]+)(\s[0-9]{2}(:{0,1}[0-9]{2}){0,2}){0,1}(\.[0-9]+){0,1}$", str(x)) else x.strftime("%Y%m%d") if isinstance(x, datetime.datetime) or isinstance(x, datetime.time) else x)
    outdf["ActualTime"] = indf[indf.columns[2]].astype('str').str.slice(-8).str.slice(0, 5).str.zfill(5)
    outdf["BrandNameEng"] = indf[indf.columns[4]]
    outdf["BrandNameChi"].fillna(outdf["BrandNameEng"],inplace=True)
    outdf["EpisodeNameEng"] = indf[indf.columns[5]]
    outdf["EpisodeNameChi"].fillna(outdf["EpisodeNameEng"],inplace=True)
    outdf["SeasonNo"] = indf[indf.columns[6]][indf[indf.columns[6]].notnull()].apply(lambda x: str(dc.utils.transferNumeric(x)))
    outdf["EpisodeNo"] = indf[indf.columns[7]][indf[indf.columns[7]].notnull()].apply(lambda x: str(dc.utils.transferNumeric(x)))
    outdf["SubGenre"] = indf[indf.columns[8]][indf[indf.columns[8]].notnull()].astype(str).str.split("/").apply(lambda subGenres: sorted(set([item for subGenre in subGenres for item in dc.mapping.subGenreMapping.get(subGenre.lower(), [subGenre])]))).str.join("/")
    outdf["SynopsisEng"] = indf[indf.columns[10]]
    outdf["SynopsisChi"] = outdf["SynopsisEng"]
    outdf["ShortSynopsisEng"] = outdf["SynopsisEng"][outdf["SynopsisEng"].notnull()].apply(lambda x: x[0:dc.settings.char_limit.get("ShortSynopsisEng")])
    outdf["ShortSynopsisChi"] = outdf["SynopsisChi"][outdf["SynopsisChi"].notnull()].apply(lambda x: x[0:dc.settings.char_limit.get("ShortSynopsisChi")])
    outdf["IsEpisodic"] = outdf[["IsEpisodic","SubGenre","EpisodeNo","EpisodeNameEng","EpisodeNameChi"]].apply(lambda x : "N" if "Match".upper() in re.sub('\s','',str(x["SubGenre"])).upper().split("/") else "Y" if (pd.notnull(x["EpisodeNo"]) and str(x["EpisodeNo"]).strip() != "") or (pd.notnull(x["EpisodeNameEng"]) and str(x["EpisodeNameEng"]).strip() != "") or (pd.notnull(x["EpisodeNameChi"]) and str(x["EpisodeNameChi"]).strip() != "") else x["IsEpisodic"],axis=1)
    outdf[["FullTitleEng", "FullTitleChi"]] = outdf.apply(dc.utils.constructFullTitle, axis=1)
    outdf[["ProgrammeNameEng", "ProgrammeNameChi"]] = outdf.apply(dc.utils.constructProgrammeName, axis=1)
    dc.utils.translateChar(outdf)
    dc.utils.chopCharacters(outdf)


    # Append EOF
    dc.utils.appendEOF(outdf)


    # Fill Channel #
    outdf['OwnerChannel'] = owner_channel
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)

    # Write output file
    dc.io.to_io(outdf, out_io, out_format)

def convert_nonlinear(in_io, out_io, format="preV1506", owner_channel="FIGHT SPORTS"):
    "Convert nonlinear for FIGHT SPORTS"
    return None

