import pandas as pd
import numpy as np
import datetime
import math
import csv
import re
import calendar
from collections import OrderedDict
import dc.utils
import dc.io
import dc.header
import dc.mapping
import dc.settings

def convert_linear(in_io, out_io, out_format="preV1506", owner_channel="AXN"):
    "Convert linear schedule for AXN"

    # Read input file
    indf = pd.read_excel(in_io, sheetname=0, na_values=["n/a"], keep_default_na=True)
    indf.columns = ["".join(s.split()).replace("(", "").replace(")", "").replace("/", "").lower() for s in indf.columns]

    
    # Create output DataFrame
    outdf = pd.DataFrame(columns=dc.header.internalHeader)

    # Extract data from full title


    # Fill output DataFrame with related info
    excepted_column = ["PID", "Premier", "Recordable", "Is NPVR Prog", "Is Restart TV", "effective_date", "expiration_date", "Media ID (max. 12 chars)"]
    for internal_column, indf_column in dc.header.postV1506HeaderMapping.items():
        if (indf_column not in excepted_column): 
            edited_indf_column = "".join(indf_column.split()).replace("(", "").replace(")", "").replace("/", "").lower()
            if (edited_indf_column in indf.columns):
                outdf[internal_column] = indf[edited_indf_column]
                if (outdf[internal_column].dtype == object):
                    outdf[internal_column] = outdf[internal_column].where(outdf[internal_column].apply(lambda x: pd.notnull(x) and len(str(x).strip()) > 0))
    outdf['ChannelNo'] = 512
    outdf['FirstReleaseYear'] = outdf['FirstReleaseYear'].apply(lambda x: str(x))
    outdf["ActualTime"] = indf["actualtime"].where(indf["actualtime"].apply(lambda x: isinstance(x, str))).apply(lambda x: pd.to_datetime(x, format="%H:%M"))
    outdf["ActualTime"].fillna(indf["actualtime"], inplace=True)
    outdf["ActualTime"] = outdf["ActualTime"].apply(lambda x: x.strftime("%H:%M"))
    outdf['FirstReleaseYear'] = outdf['FirstReleaseYear'].apply(lambda x: str(x))
    outdf["TXDate"] = outdf["TXDate"].astype(str)
    start_hour = 6
    outdf["TXDate"] = outdf["TXDate"][outdf["ActualTime"].apply(lambda x: pd.to_datetime(x, format="%H:%M")).apply(lambda x: int(x.strftime("%H"))) < start_hour].apply(lambda x: (pd.to_datetime(x) + pd.to_timedelta("1 days")).strftime("%Y%m%d")).combine_first(outdf["TXDate"])
    outdf["BrandNameEng"] = indf["brandnameenglish"].str.strip().str.extract("(.*?)(\d*)$", expand=False)[0].str.strip().str.translate(dc.mapping.charTranslateTable)
    outdf["BrandNameChi"] = indf["fulltitlechinesemax24characters"].str.replace("[(（].*?季.*?[)）]", "").str.translate(dc.mapping.charTranslateTable)
    outdf["BrandNameChi"] = outdf["BrandNameChi"].str.replace("　", " ").str.strip().apply(lambda x: dc.utils.jianfan(str(x)))
    outdf["EpisodeNameEng"] = outdf["EpisodeNameEng"].str.replace("EPISODE \d", "")
    outdf["EpisodeNameEng"].fillna("", inplace=True)
    outdf["EpisodeNameChi"].fillna(outdf["EpisodeNameEng"], inplace=True)
    outdf["EpisodeNameChi"].fillna("", inplace=True)
    outdf["EpisodeNameEng"] = outdf["EpisodeNameEng"].str.translate(dc.mapping.charTranslateTable)
    outdf["EpisodeNameChi"] = outdf["EpisodeNameChi"].str.translate(dc.mapping.charTranslateTable)
    outdf["EpisodeNameEng"] = outdf["EpisodeNameEng"].apply(lambda x: x if len(x) > 0 else None)
    outdf["EpisodeNameChi"] = outdf["EpisodeNameChi"].apply(lambda x: x if len(x) > 0 else None)
    outdf["FirstReleaseYear"] = outdf["FirstReleaseYear"].astype(str).str.replace("\D.*", "")
    outdf["SponsorTextStuntChi"].fillna(outdf["SponsorTextStuntEng"], inplace=True)
    outdf["SponsorTextStuntEng"].fillna(outdf["SponsorTextStuntChi"], inplace=True)
    outdf["BrandNameChi"].fillna(outdf["BrandNameEng"], inplace=True)
    outdf["BrandNameEng"].fillna(outdf["BrandNameChi"], inplace=True)
    outdf["SeasonNameChi"].fillna(outdf["SeasonNameEng"], inplace=True)
    outdf["SeasonNameEng"].fillna(outdf["SeasonNameChi"], inplace=True)
    outdf["EpisodeNameChi"].fillna(outdf["EpisodeNameEng"], inplace=True)
    outdf["EpisodeNameEng"].fillna(outdf["EpisodeNameChi"], inplace=True)
    outdf["EpisodeNameChi"].fillna(outdf["EpisodeNameEng"], inplace=True)
    outdf["EpisodeNameEng"].fillna(outdf["EpisodeNameChi"], inplace=True)
    outdf["SynopsisChi"].fillna(outdf["SynopsisEng"], inplace=True)
    outdf["SynopsisEng"].fillna(outdf["SynopsisChi"], inplace=True)
    outdf["ShortSynopsisChi"].fillna(outdf["ShortSynopsisEng"], inplace=True)
    outdf["ShortSynopsisEng"].fillna(outdf["ShortSynopsisChi"], inplace=True)
    outdf["SeasonNo"] = outdf["SeasonNo"][outdf["SeasonNo"].notnull()].astype(int).astype(str)
    outdf["EpisodeNo"] = outdf["EpisodeNo"][outdf["EpisodeNo"].notnull()].astype(int).astype(str)
    outdf["PremierOld"] = "0"
    outdf["Premier"] = "N"
    outdf["IsLive"] = "N"
    outdf["IsEpisodic"] = (outdf["EpisodeNo"].notnull() | outdf["EpisodeNameEng"].notnull() | outdf["EpisodeNameChi"].notnull()).map({True:"Y", False:"N"})
    text_column = ["SponsorTextStuntEng", "SponsorTextStuntChi", "BrandNameEng", "BrandNameChi", "EditionVersionEng", "EditionVersionChi", "SeasonNameEng", "SeasonNameChi", "EpisodeNameEng", "EpisodeNameChi", "SynopsisEng", "SynopsisChi", "ShortSynopsisEng", "ShortSynopsisChi", "DirectorProducerEng", "DirectorProducerChi", "CastEng", "CastChi"]
    for column in text_column:
        outdf[column] = outdf[column].astype(str).where(outdf[column].notnull()).str.translate(dc.mapping.charTranslateTable)
    dc.utils.title_df(outdf)
    outdf[["FullTitleEng", "FullTitleChi"]] = outdf.apply(dc.utils.constructFullTitle, axis=1)
    outdf[["ProgrammeNameEng", "ProgrammeNameChi"]] = outdf.apply(dc.utils.constructProgrammeName, axis=1)
    dc.utils.translateChar(outdf)
    dc.utils.chopCharacters(outdf)


    # Append EOF
    dc.utils.appendEOF(outdf)


    # Fill Channel #
    outdf['OwnerChannel'] = owner_channel
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)

    # Write output file
    dc.io.to_io(outdf, out_io, out_format)

def convert_nonlinear(in_io, out_io, format="preV1506", owner_channel="AXN"):
    "Convert nonlinear for AXN"
    return None

