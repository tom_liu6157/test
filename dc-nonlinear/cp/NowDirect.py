import pandas as pd
import numpy as np
import datetime
import math
import csv
import re
import calendar
from collections import OrderedDict
import dc.utils
import dc.io
import dc.header
import dc.mapping
import dc.settings

def convert_linear(in_io, out_io, out_format="preV1506", owner_channel="Now Direct"):
    "Convert linear schedule for Now Direct"

    # Read input file
    indf = pd.read_excel(in_io, sheetname=None)
    if type(in_io) != str:
        in_io.seek(0)
    if 'EPG' in indf.keys():
        if 'now Direct Schedule'.upper().replace(" ","") in indf.get('EPG').columns[0].upper().replace(" ",""):
            indf = pd.read_excel(in_io, sheetname='EPG', skiprows=3)
        else:
            indf = pd.read_excel(in_io, sheetname='EPG', skiprows=0)
    elif 'NOW DIRECT' in indf.keys():
        if 'now Direct Schedule'.upper().replace(" ","") in indf.get('NOW DIRECT').columns[0].upper().replace(" ",""):
            indf = pd.read_excel(in_io, sheetname='NOW DIRECT', skiprows=3)
        else:
            indf = pd.read_excel(in_io, sheetname='NOW DIRECT', skiprows=0)
    else:
        if 'now Direct Schedule'.upper().replace(" ","") in indf.get(list(indf.keys())[0]).columns[0].upper().replace(" ",""):
            indf = pd.read_excel(in_io, sheetname=0, skiprows=3)
        else:
            indf = pd.read_excel(in_io, sheetname=0, skiprows=0)
    datekey = "";
    for key in indf :
        if(key != "Time" and key != "Program Name (English)" and key != "Program Name (Chinese)" and key != "Genre" and key != "Sub Genre") :
            datekey = key;
    count = 0
    preDate = ""
    for date in indf[datekey] :
        if (pd.isnull(date)) :
            indf.loc[count, datekey] = preDate
        else :
            preDate = date
        count = count + 1
    count = 0
    for name in indf["Program Name (English)"] :            
        if (pd.isnull(name) or name == "Program Name (English)") :
            for key in indf :
                del indf[key][count]
        count = count + 1

    
    # Create output DataFrame
    outdf = pd.DataFrame(columns=dc.header.internalHeader)

    # Extract data from full title
    extractedInfo = pd.DataFrame(columns=["SponsorTextStuntEng", "BrandNameEng", "EpisodeNo", "EditionVersionEng", "EpisodeNameEng"])
    fullTitleEng = indf["Program Name (English)"].apply(lambda x: str(x).strip())
    tempExtractedInfoEng = [
        fullTitleEng.str.extract("(.*#\s*(?P<EpisodeNo>\d+))*", expand=False),
    ]
    extractedInfo["EpisodeNo"] = tempExtractedInfoEng[0]["EpisodeNo"].apply(lambda x: None if x == "" else x)


    # Fill output DataFrame with related info
    outdf["BrandNameEng"]= indf["Program Name (English)"].apply(lambda x:re.sub("\s*#.*", "", x))                           
    outdf["BrandNameEng"] = outdf["BrandNameEng"].str.translate(dc.mapping.charTranslateTable)
    outdf["EpisodeNo"] = extractedInfo["EpisodeNo"]
    outdf["BrandNameChi"] = indf["Program Name (Chinese)"].apply(lambda x:re.sub("\s*#.*", "", x))                          
    outdf["TXDate"] = indf[datekey].apply(lambda x: pd.to_datetime(x, format="%Y-%m-%d")).apply(lambda x:x.strftime("%Y%m%d"))
    outdf["ActualTime"] = indf["Time"].apply(lambda x: pd.to_datetime(x, format="%H:%M:%S")).apply(lambda x:x.strftime("%H:%M"))
    outdf["IsEpisodic"] = (outdf["EpisodeNo"].notnull() | outdf["EpisodeNameEng"].notnull() | outdf["EpisodeNameChi"].notnull()).map({True:"Y", False:"N"})
    outdf["Genre"] = indf["Genre"][indf["Genre"].notnull()].str.lower().map(dc.mapping.genreMapping).fillna(indf["Genre"])
    outdf["SubGenre"] = indf["Sub Genre"][indf["Sub Genre"].notnull()].str.lower().map(dc.mapping.subGenreMapping).str.join("/").fillna(indf["Sub Genre"])
    outdf["PremierOld"] = "0"
    outdf["Premier"] = "N"
    outdf["IsLive"] = "N"
    dc.utils.title_df(outdf)
    outdf[["FullTitleEng", "FullTitleChi"]] = outdf.apply(dc.utils.constructFullTitle, axis=1)
    outdf[["ProgrammeNameEng", "ProgrammeNameChi"]] = outdf.apply(dc.utils.constructProgrammeName, axis=1)
    dc.utils.translateChar(outdf)
    dc.utils.chopCharacters(outdf)


    # Append EOF
    x = pd.to_datetime(outdf["TXDate"] + " " + outdf["ActualTime"], format="%Y%m%d %H:%M")
    lastProgStartTimestamp = x.max()
    eofTimestamp = None
    oneDay = pd.Timedelta(1,"D")
    oneWeek = pd.Timedelta(1,"W")
    oneDayBefore = lastProgStartTimestamp - oneDay
    oneWeekBefore = lastProgStartTimestamp - oneWeek
    if x.min() <= oneWeekBefore:
        eofTimestamp = x[x > oneWeekBefore].min() + oneWeek
    elif x.min() <= oneDayBefore:
        eofTimestamp = x[x > oneDayBefore].min() + oneDay
    else:
        eofTimestamp = x.min() + oneDay
    outdf.loc[count-1, ["TXDate", "ActualTime", "FullTitleEng", "FullTitleChi", "ProgrammeNameEng", "ProgrammeNameChi"]] = [eofTimestamp.strftime("%Y%m%d"), eofTimestamp.strftime("%H:%M"), "END OF FILE", "END OF FILE", "END OF FILE", "END OF FILE"]


    # Fill Channel #
    outdf['OwnerChannel'] = owner_channel
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)

    # Write output file
    dc.io.to_io(outdf, out_io, out_format)

def convert_nonlinear(in_io, out_io, format="preV1506", owner_channel="Now Direct"):
    "Convert nonlinear for Now Direct"
    return None

