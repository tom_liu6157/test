import pandas as pd
import numpy as np
import datetime
import math
import csv
import re
import calendar
from collections import OrderedDict
import dc.utils
import dc.io
import dc.header
import dc.mapping
import dc.settings

def convert_linear(in_io, out_io, out_format="preV1506", owner_channel="Zhejiang Satellite TV"):
    "Convert linear schedule for Zhejiang Satellite TV"

    # Read input file
    indf = pd.read_excel(in_io, sheetname=0, na_values=["n/a"], keep_default_na=True, skiprows=0)

    
    # Create output DataFrame
    outdf = pd.DataFrame(columns=dc.header.internalHeader)

    # Extract data from full title


    # Fill output DataFrame with related info
    outdf["ChannelNo"] = indf["Channel #"]
    outdf["TXDate"] = indf["TX Date"].apply(lambda x: pd.to_datetime(x, format="%Y%m%d")).apply(lambda x:x.strftime("%Y%m%d"))
    outdf["ActualTime"] = indf["Actual Time"].apply(lambda x: pd.to_datetime(x, format="%H:%M:%S")).apply(lambda x:x.strftime("%H:%M"))
    outdf["FullTitleEng"] = indf["Full Title (English) Max 40 Characters"]
    outdf["FullTitleChi"] = indf["Full Title (Chinese) Max 24 Characters"]
    outdf["SponsorTextStuntEng"] = indf["Sponsor text / Stunt (English)"]
    outdf["SponsorTextStuntChi"] = indf["Sponsor text / Stunt (Chinese)"]
    outdf["BrandNameEng"] = indf["Brand name (English)"]
    outdf["BrandNameChi"] = indf["Brand name (Chinese)"]
    outdf["EditionVersionEng"] = indf["Edition / Version (English)"]
    outdf["EditionVersionChi"] = indf["Edition / Version (Chinese)"]
    outdf["SeasonNo"] = indf["Season no. (max 3 digits)"]
    outdf["SeasonNameEng"] = indf["Synopsis (English)"]
    outdf["SeasonNameChi"] = indf["Synopsis (Chinese)"]
    outdf["EpisodeNo"] = indf["Episode no. (max 5 digits)"]
    outdf["EpisodeNameEng"] = indf["Episode name (English)"]
    outdf["EpisodeNameChi"] = indf["Episode name (Chinese)"]
    outdf["SynopsisEng"] = indf["Synopsis (English)"]
    outdf["SynopsisChi"] = indf["Synopsis (Chinese)"]
    outdf["ShortSynopsisEng"] = indf["Short Synopsis (English) (max 120 chars)"]
    outdf["ShortSynopsisChi"] = indf["Short Synopsis (Chinese) (max 120 chars)"]
    outdf["Premier"] = indf["Premiere (Y/N)"]
    outdf["IsLive"] = indf["Is Live (Y/N)"]
    outdf["Classification"] = indf["Classification"]
    outdf["Genre"] = indf["Genre"]
    outdf["SubGenre"] = indf["Sub-Genre"]
    outdf["DirectorProducerEng"] = indf["Director / Producer (English)"]
    outdf["DirectorProducerChi"] = indf["Director / Producer (Chinese)"]
    outdf["CastEng"] = indf["Cast (English)"]
    outdf["CastChi"] = indf["Cast (Chinese)"]
    outdf["OriginalLang"] = indf["Original language (max 50 chars)"]
    outdf["AudioLang"] = indf["Audio language"]
    outdf["SubtitleLang"] = indf["Subtitle language"]
    outdf["RegionCode"] = indf["Region Code (max 2 chars)"]
    outdf["FirstReleaseYear"] = indf["First release year (max 4 digits)"]
    outdf["IsEpisodic"] = indf["Episodic (Y/N)"]
    outdf["PortraitImage"] = indf["Portrait Image"]
    outdf["ImageWithTitle"] = indf["Image with title  (Y/N)"]
    outdf["RecordableForCatchUp"] = indf["Recordable for Catch-up (Y/N)"]
    outdf["CPInternalUniqueID"] = indf["CP Internal unique ID"]
    outdf["CPInternalSeriesID"] = indf["CP Internal Series ID"]
    dc.utils.title_df(outdf)
    outdf[["FullTitleEng", "FullTitleChi"]] = outdf.apply(dc.utils.constructFullTitle, axis=1)
    outdf[["ProgrammeNameEng", "ProgrammeNameChi"]] = outdf.apply(dc.utils.constructProgrammeName, axis=1)
    dc.utils.translateChar(outdf)
    dc.utils.chopCharacters(outdf)


    # Append EOF
    dc.utils.appendEOF(outdf)


    # Fill Channel #
    outdf['OwnerChannel'] = owner_channel
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)

    # Write output file
    dc.io.to_io(outdf, out_io, out_format)

def convert_nonlinear(in_io, out_io, format="preV1506", owner_channel="Zhejiang Satellite TV"):
    "Convert nonlinear for Zhejiang Satellite TV"
    return None

