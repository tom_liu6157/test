import pandas as pd
import numpy as np
import datetime
import math
import csv
import re
import calendar
from collections import OrderedDict
import dc.utils
import dc.io
import dc.header
import dc.mapping
import dc.settings

def convert_linear(in_io, out_io, out_format="preV1506", owner_channel="FOX Action Movies"):
    "Convert linear schedule for FOX Action Movies"

    # Read input file
    indf = pd.read_excel(in_io, sheetname=0, skiprows=2)
    indf.columns = ['channel#', 'txdate', 'actualtime', 'fulltitleenglishmax40characters',
           'fulltitlechinesemax24characters', 'sponsortextstuntenglish',
           'sponsortextstuntchinese', 'brandnameenglish', 'brandnamechinese', 'editionversionenglish',
           'editionversionchinese', 'seasonno.max3digits', 'seasonnameenglish',
           'seasonnamechinese', 'episodeno.max5digits', 'episodenameenglish',
           'episodenamechinese', 'synopsisenglish', 'synopsischinese',
           'shortsynopsisenglishmax120chars', 'shortsynopsischinesemax120chars', 'premiereyn',
           'isliveyn', 'classification', 'genre', 'sub-genre',
           'directorproducerenglish', 'directorproducerchinese', 'castenglish',
           'castchinese', 'originallanguagemax50chars', 'audiolanguage', 'subtitlelanguage',
           'regioncodemax2chars', 'firstreleaseyearmax4digits', 'episodicyn', 'portraitimage',
           'imagewithtitleyn', 'recordableforcatch-upyn', 'cpinternaluniqueid',
           'cpinternalseriesid']
    indf.dropna(how="any", subset=["txdate", "actualtime"], inplace=True)

    
    # Create output DataFrame
    outdf = pd.DataFrame(columns=dc.header.internalHeader)

    # Extract data from full title


    # Fill output DataFrame with related info
    excepted_column = ["PID", "Premier", "Recordable", "Is NPVR Prog", "Is Restart TV", "effective_date", "expiration_date", "Media ID (max. 12 chars)"]
    for internal_column, indf_column in dc.header.postV1506HeaderMapping.items():
        if (indf_column not in excepted_column): 
            edited_indf_column = "".join(indf_column.split()).replace("(", "").replace(")", "").replace("/", "").lower()
            if (edited_indf_column in indf.columns):
                outdf[internal_column] = indf[edited_indf_column]
                if (outdf[internal_column].dtype == object):
                    outdf[internal_column] = outdf[internal_column].where(outdf[internal_column].apply(lambda x: pd.notnull(x) and len(str(x).strip()) > 0))
    outdf["TXDate"] = outdf["TXDate"].astype(int).astype(str)
    outdf["SponsorTextStuntChi"].fillna(outdf["SponsorTextStuntEng"], inplace=True)
    outdf["SponsorTextStuntEng"].fillna(outdf["SponsorTextStuntChi"], inplace=True)
    outdf["BrandNameChi"].fillna(outdf["BrandNameEng"], inplace=True)
    outdf["BrandNameEng"].fillna(outdf["BrandNameChi"], inplace=True)
    outdf["SeasonNameChi"].fillna(outdf["SeasonNameEng"], inplace=True)
    outdf["SeasonNameEng"].fillna(outdf["SeasonNameChi"], inplace=True)
    outdf["EpisodeNameChi"].fillna(outdf["EpisodeNameEng"], inplace=True)
    outdf["EpisodeNameEng"].fillna(outdf["EpisodeNameChi"], inplace=True)
    outdf["EpisodeNameChi"].fillna(outdf["EpisodeNameEng"], inplace=True)
    outdf["EpisodeNameEng"].fillna(outdf["EpisodeNameChi"], inplace=True)
    outdf["SynopsisChi"].fillna(outdf["SynopsisEng"], inplace=True)
    outdf["SynopsisEng"].fillna(outdf["SynopsisChi"], inplace=True)
    outdf["ShortSynopsisChi"].fillna(outdf["ShortSynopsisEng"], inplace=True)
    outdf["ShortSynopsisEng"].fillna(outdf["ShortSynopsisChi"], inplace=True)
    outdf["CastChi"].fillna(outdf["CastEng"], inplace=True)
    outdf["CastEng"].fillna(outdf["CastChi"], inplace=True)
    outdf["EpisodeNameEng"] = outdf["EpisodeNameEng"].where((outdf["BrandNameEng"] != outdf["EpisodeNameEng"]) & (outdf["BrandNameChi"] != outdf["EpisodeNameEng"]))
    outdf["EpisodeNameChi"] = outdf["EpisodeNameChi"].where((outdf["BrandNameEng"] != outdf["EpisodeNameChi"]) & (outdf["BrandNameChi"] != outdf["EpisodeNameChi"]))
    text_column = ["SponsorTextStuntEng", "SponsorTextStuntChi", "BrandNameEng", "BrandNameChi", "EditionVersionEng", "EditionVersionChi", "SeasonNameEng", "SeasonNameChi", "EpisodeNameEng", "EpisodeNameChi", "SynopsisEng", "SynopsisChi", "ShortSynopsisEng", "ShortSynopsisChi", "DirectorProducerEng", "DirectorProducerChi", "CastEng", "CastChi"]
    for column in text_column:
        outdf[column] = outdf[column].astype(str).where(outdf[column].notnull()).str.translate(dc.mapping.charTranslateTable)
    dc.utils.title_df(outdf)
    outdf[["FullTitleEng", "FullTitleChi"]] = outdf.apply(dc.utils.constructFullTitle, axis=1)
    outdf[["ProgrammeNameEng", "ProgrammeNameChi"]] = outdf.apply(dc.utils.constructProgrammeName, axis=1)
    dc.utils.translateChar(outdf)
    dc.utils.chopCharacters(outdf)


    # Append EOF
    dc.utils.appendEOF(outdf)


    # Fill Channel #
    outdf['OwnerChannel'] = owner_channel
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)

    # Write output file
    dc.io.to_io(outdf, out_io, out_format)

def convert_nonlinear(in_io, out_io, format="preV1506", owner_channel="FOX Action Movies"):
    "Convert nonlinear for FOX Action Movies"
    return None

