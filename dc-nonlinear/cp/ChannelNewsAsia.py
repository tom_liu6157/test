import pandas as pd
import numpy as np
import datetime
import math
import csv
import re
import calendar
from collections import OrderedDict
import dc.utils
import dc.io
import dc.header
import dc.mapping
import dc.settings

def convert_linear(in_io, out_io, out_format="preV1506", owner_channel="Channel NewsAsia"):
    "Convert linear schedule for Channel NewsAsia"

    # Read input file
    indf = pd.read_excel(in_io, sheetname=0, skiprows=0, skip_footer=0)

    
    # Create output DataFrame
    outdf = pd.DataFrame(columns=dc.header.internalHeader)

    # Extract data from full title
    fullEpisodeEng = indf["Episode_Title"][indf["Episode_Title"].notnull()].apply(lambda x: str(x).strip())
    extractedInfo = pd.DataFrame(columns=["SponsorTextEng", "SponsorTextChi", "BrandNameEng", "BrandNameChi", "SeasonNo", "SeasonNameEng", "SeasonNameChi", "EpisodeNo", "EpisodeNameEng", "EpisodeNameChi", "RegionCode", "FirstReleaseYear"])
    if fullEpisodeEng.count() > 0:
        tempExtractedInfo = [
            fullEpisodeEng.str.extract("^(Episode)*\s*(\d+)*\s*(-)*\s*(?P<EpisodeNameEng>.*)$", expand=False)
        ]
        extractedInfo["EpisodeNameEng"] = tempExtractedInfo[0]["EpisodeNameEng"]


    # Fill output DataFrame with related info
    outdf["TXDate"] = indf["StartDate"].apply(lambda x: pd.to_datetime(x, format="%Y-%m-%d")).apply(lambda x:x.strftime("%Y%m%d"))
    outdf["ActualTime"] = indf["StartTime"].apply(lambda x: (str(x)+":00") if len(str(x))==5 else str(x)).apply(lambda x: pd.to_datetime(x, format="%H:%M:%S")).apply(lambda x:x.strftime("%H:%M"))
    outdf["BrandNameEng"] = indf["Programe_Title"][indf["Programe_Title"].notnull()].str.translate(dc.mapping.charTranslateTable).apply(lambda x: x[0:dc.settings.char_limit.get("BrandNameEng")])
    outdf["BrandNameChi"].fillna(outdf["BrandNameEng"], inplace=True)
    outdf["EpisodeNo"] = indf["Episode_Number"][indf["Episode_Number"].notnull()].astype(int).astype(str)
    outdf["EpisodeNameEng"] = extractedInfo["EpisodeNameEng"][extractedInfo["EpisodeNameEng"].notnull()].str.translate(dc.mapping.charTranslateTable).apply(lambda x: x[0:dc.settings.char_limit.get("EpisodeNameEng")])
    outdf[["EpisodeNameEng"]] = outdf.apply(dc.utils.constructEpisodeNameEng, axis=1)
    outdf["EpisodeNameChi"].fillna(outdf["EpisodeNameEng"], inplace=True)
    outdf["SynopsisEng"] = indf["Synopsis"][indf["Synopsis"].notnull()].str.translate(dc.mapping.charTranslateTable).apply(lambda x: x[0:dc.settings.char_limit.get("SynopsisEng")])
    outdf["SynopsisEng"] = outdf["SynopsisEng"][outdf["SynopsisEng"] != ','] # added for issue #285
    outdf["SynopsisChi"].fillna(outdf["SynopsisEng"], inplace=True)
    outdf["ShortSynopsisEng"] = outdf["SynopsisEng"][outdf["SynopsisEng"].notnull()].apply(lambda x: x[0:dc.settings.char_limit.get("ShortSynopsisEng")])
    outdf["ShortSynopsisChi"] = outdf["SynopsisChi"][outdf["SynopsisChi"].notnull()].apply(lambda x: x[0:dc.settings.char_limit.get("ShortSynopsisChi")])
    outdf["PremierOld"] = "0"
    outdf["Premier"] = "N"
    outdf["IsLive"] = "N"
    outdf["IsEpisodic"] = (outdf["EpisodeNo"].notnull() | outdf["EpisodeNameEng"].notnull()).map({True:"Y", False:"N"})
    dc.utils.title_df(outdf)
    outdf[["FullTitleEng", "FullTitleChi"]] = outdf.apply(dc.utils.constructFullTitle, axis=1)
    outdf[["ProgrammeNameEng", "ProgrammeNameChi"]] = outdf.apply(dc.utils.constructProgrammeName, axis=1)
    dc.utils.translateChar(outdf)
    dc.utils.chopCharacters(outdf)


    # Append EOF
    dc.utils.appendEOF(outdf)


    # Fill Channel #
    outdf['OwnerChannel'] = owner_channel
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)

    # Write output file
    dc.io.to_io(outdf, out_io, out_format)

def convert_nonlinear(in_io, out_io, format="preV1506", owner_channel="Channel NewsAsia"):
    "Convert nonlinear for Channel NewsAsia"
    return None

