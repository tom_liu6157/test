import pandas as pd
import numpy as np
import datetime
import math
import csv
import re
import calendar
from collections import OrderedDict
import dc.utils
import dc.io
import dc.header
import dc.mapping
import dc.settings

def convert_linear(in_io, out_io, out_format="preV1506", owner_channel="MUTV"):
    "Convert linear schedule for MUTV"

    # Read input file
    indf = pd.read_excel(in_io, sheetname=0, skiprows=1)

    
    # Create output DataFrame
    outdf = pd.DataFrame(columns=dc.header.internalHeader)

    # Extract data from full title


    # Fill output DataFrame with related info
    outdf["TXDate"] = indf["TX Date"].apply(lambda x: pd.to_datetime(x, format="%Y%m%d")).apply(lambda x:x.strftime("%Y%m%d"))
    outdf["ActualTime"] = indf["Actual Time"].astype('str').str.slice(-8).str.slice(0, 5).str.zfill(5)
    outdf["BrandNameEng"] = indf["Brand name (English)"].astype(str)
    outdf["BrandNameChi"] = indf["Brand name (Chinese)"].astype(str)
    outdf["BrandNameEng"].fillna(outdf["BrandNameChi"], inplace=True)
    outdf["BrandNameChi"].fillna(outdf["BrandNameEng"], inplace=True)
    outdf["EpisodeNameEng"] = indf["Episode name (English)"]
    outdf["EpisodeNameChi"] = indf["Episode name (Chinese)"]
    outdf["EpisodeNameEng"].fillna(outdf["EpisodeNameChi"], inplace=True)
    outdf["EpisodeNameChi"].fillna(outdf["EpisodeNameEng"], inplace=True)
    outdf["PremierOld"] = "0"
    outdf["Premier"] = indf["Premiere (Y/N)"]
    outdf["Premier"].fillna("N", inplace=True)
    outdf["IsLive"] = indf["Is Live (Y/N)"]
    outdf["IsLive"].fillna("N", inplace=True)
    outdf["Genre"] = indf["Genre"]
    outdf["SubGenre"] = indf["Sub-Genre"]
    outdf["Recordable"] = indf["Recordable"]
    outdf["Recordable"].fillna("N", inplace=True)
    outdf["IsNPVRProg"] = indf["Is NPVR Prog"]
    outdf["IsNPVRProg"].fillna("N", inplace=True)
    outdf["SeasonNo"] = indf["Season no Max 3 digits"]
    outdf["SeasonNameEng"] = indf["Season Name (English)"]
    outdf["SeasonNameChi"] = indf["Season Name (Chinese)"]
    outdf["EditionVersionChi"] = indf["Edition / Version (Chinese)"]
    outdf["EditionVersionEng"] = indf["Edition / Version (English)"]
    outdf["EpisodeNo"] = indf["Episode no. (max 5 digits)"].apply(lambda x: None if x == "-" else re.match("^([0-9]+)(?:\.0+|[0-9]*)$", str(x)).groups()[0] if re.match("^[0-9]+(?:\.0+|[0-9]*)$", str(x)) else x)
    outdf["IsEpisodic"] = np.where(np.logical_and(indf["Episodic (Y/N)"].notnull(),~indf["Episodic (Y/N)"].astype(str).str.match("^[\s]*$")), indf["Episodic (Y/N)"], "N")
    outdf["IsEpisodic"] = outdf[["IsEpisodic","SubGenre","EpisodeNo","EpisodeNameEng","EpisodeNameChi"]].apply(lambda x : "N" if "Match".upper() in re.sub('\s','',str(x["SubGenre"])).upper().split("/") else "Y" if (pd.notnull(x["EpisodeNo"]) and str(x["EpisodeNo"]).strip() != "") or (pd.notnull(x["EpisodeNameEng"]) and str(x["EpisodeNameEng"]).strip() != "") or (pd.notnull(x["EpisodeNameChi"]) and str(x["EpisodeNameChi"]).strip() != "") else x["IsEpisodic"],axis=1)
    outdf["IsRestartTV"] = indf["Is Restart TV"]
    outdf["PortraitImage"] = indf["Portrait Image"]
    outdf["ImageWithTitle"] = indf["Image with title  (Y/N)"]
    outdf["Classification"] = indf["Classification"]
    outdf["SponsorTextStuntEng"] = indf["Sponsor text / Stunt (English)"]
    outdf["SponsorTextStuntChi"] = indf["Sponsor text / Stunt (Chinese)"]
    outdf["SynopsisEng"] = indf["Programme Synopsis (English) Max 180 Characters"]
    outdf["SynopsisChi"] = indf["Programme Synopsis (Chinese) Max 130 Characters"]
    outdf["ShortSynopsisEng"] = indf["Short Programme Synopsis (English) (max. 120 Chars)"]
    outdf["ShortSynopsisChi"] = indf["Short Programme Synopsis (Chinese) (max. 120 Chars)"]
    outdf["DirectorProducerEng"] = indf["Director (English) Max 100 Characters"]
    outdf["DirectorProducerChi"] = indf["Director (Chinese) Max 100 Characters"]
    outdf["CastEng"] = indf["Actor (English) Max 100 Characters"]
    outdf["CastChi"] = indf["Actor (Chinese) Max 100 Characters"]
    outdf["OriginalLang"] = indf["Original Language Max 40 Characters"]
    outdf["AudioLang"] = indf["Audio language"]
    outdf["SubtitleLang"] = indf["Subtitle language"]
    outdf["RegionCode"] = indf["Region Code Max  2 Characters"]
    outdf["FirstReleaseYear"] = indf["First release year Max 4 digits"]
    outdf["CPInternalUniqueID"] = indf["CP internal unique ID"]
    outdf["EffectiveDate"] = indf["effective_date"]
    outdf["ExpirationDate"] = indf["expiration_date"]
    outdf['OwnerChannel'] = "MUTV"
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)
    dc.utils.title_df(outdf)
    outdf[["FullTitleEng", "FullTitleChi"]] = outdf.apply(dc.utils.constructFullTitle, axis=1)
    outdf[["ProgrammeNameEng", "ProgrammeNameChi"]] = outdf.apply(dc.utils.constructProgrammeName, axis=1)
    dc.utils.translateChar(outdf)
    dc.utils.chopCharacters(outdf)


    # Append EOF
    dc.utils.appendEOF(outdf)


    # Fill Channel #
    outdf['OwnerChannel'] = owner_channel
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)

    # Write output file
    dc.io.to_io(outdf, out_io, out_format)

def convert_nonlinear(in_io, out_io, format="preV1506", owner_channel="MUTV"):
    "Convert nonlinear for MUTV"
    return None

