import pandas as pd
import numpy as np
import datetime
import math
import csv
import re
import calendar
from collections import OrderedDict
import dc.utils
import dc.io
import dc.header
import dc.mapping
import dc.settings

def convert_linear(in_io, out_io, out_format="preV1506", owner_channel="Fox News"):
    "Convert linear schedule for Fox News"

    # Read input file
    from io import BytesIO
    indf = pd.read_excel(in_io, sheetname=0)
    if type(in_io) == BytesIO:
        in_io.seek(0)
    if indf.columns[0] == "Fox News":
        indf = pd.read_excel(in_io, sheetname=0, skiprows=3)

    
    # Create output DataFrame
    outdf = pd.DataFrame(columns=dc.header.internalHeader)

    # Extract data from full title
    if len(indf.columns) == 4:
        extractedInfo = pd.DataFrame(columns=["SponsorTextStuntEng", "BrandNameEng", "EditionVersionEng", "EpisodeNameEng"])
        fullTitleEng = indf["Programme"].apply(lambda x: str(x).strip())
        tempExtractedInfo = fullTitleEng.str.extract("((?P<SponsorTextStuntEng>.+):)?(?P<BrandNameEng>[^()]+)(\s+\((?P<EditionVersionEng>.+)\))?(\s*(?P<EpisodeNameEng>\d{2}/\d{2}/\d{2})|$)", expand=False)
        extractedInfo["SponsorTextStuntEng"] = tempExtractedInfo["SponsorTextStuntEng"]
        extractedInfo["BrandNameEng"] = tempExtractedInfo["BrandNameEng"]
        extractedInfo["EditionVersionEng"] = tempExtractedInfo["EditionVersionEng"]
        extractedInfo["EpisodeNameEng"] = tempExtractedInfo["EpisodeNameEng"]
    else :
        indf.columns = ["".join(s.split()).replace("(", "").replace(")", "").replace("/", "").lower() for s in indf.columns]
        excepted_column = ["PID", "Premier", "Recordable", "Is NPVR Prog", "Is Restart TV", "effective_date", "expiration_date", "Media ID (max. 12 chars)"]
        for internal_column, indf_column in dc.header.postV1506HeaderMapping.items():
            if (indf_column not in excepted_column): 
                edited_indf_column = "".join(indf_column.split()).replace("(", "").replace(")", "").replace("/", "").lower() #indf_column
                if (edited_indf_column in indf.columns):
                    outdf[internal_column] = indf[edited_indf_column]
                    if (outdf[internal_column].dtype == object):
                         outdf[internal_column] = outdf[internal_column].where(outdf[internal_column].apply(lambda x: pd.notnull(x) and len(str(x).strip()) > 0))


    # Fill output DataFrame with related info
    if len(indf.columns) == 4:
        outdf["TXDate"] = indf["Date"].apply(lambda x: pd.to_datetime(x, format="%d-%m-%Y")).apply(lambda x:x.strftime("%Y%m%d"))
        outdf["ActualTime"] = indf["Time"].astype('str').str.slice(-8).str.slice(0, 5).str.zfill(5)
        outdf["BrandNameEng"]=indf["Programme"].apply(lambda x:re.sub("\(\w+\)", "", x)).apply(lambda x:re.sub(".+:", "", x)).apply(lambda x:re.sub("\d+/\d+/\d+", "", x))
        outdf["SponsorTextStuntEng"] = tempExtractedInfo["SponsorTextStuntEng"].str.translate(dc.mapping.charTranslateTable)
        outdf["BrandNameEng"] = tempExtractedInfo["BrandNameEng"].str.translate(dc.mapping.charTranslateTable)
        outdf["EditionVersionEng"] = tempExtractedInfo["EditionVersionEng"].str.translate(dc.mapping.charTranslateTable)
        outdf["EpisodeNameEng"] = tempExtractedInfo["EpisodeNameEng"].str.translate(dc.mapping.charTranslateTable)
        outdf["SponsorTextStuntChi"] = tempExtractedInfo["SponsorTextStuntEng"]
        outdf["BrandNameChi"] = tempExtractedInfo["BrandNameEng"]
        outdf["EditionVersionChi"] =tempExtractedInfo["EditionVersionEng"]
        outdf["EpisodeNameChi"] = outdf["EpisodeNameEng"]
        outdf["SynopsisEng"] = indf["Synopsis"]
        outdf["SynopsisChi"] = indf["Synopsis"]
        outdf["ShortSynopsisEng"] = indf["Synopsis"].apply(lambda x: x[0:dc.settings.char_limit.get("ShortSynopsisEng")])
        outdf["ShortSynopsisChi"] = indf["Synopsis"].apply(lambda x: x[0:dc.settings.char_limit.get("ShortSynopsisChi")])
        outdf["PremierOld"] = "0"
        outdf["Premier"] = "N"
        outdf["IsLive"] = "N"
        outdf["Genre"] = "New/ Finance" 
        outdf["SubGenre"] = "Finance"
        outdf["AudioLang"] = "English"
        outdf["RegionCode"] = "US"
        outdf["SubtitleLang"] = "English"
        outdf["IsEpisodic"] = (outdf["EpisodeNo"].notnull() | outdf["EpisodeNameEng"].notnull()).map({True:"Y", False:"N"})
        outdf[["ProgrammeNameEng", "ProgrammeNameChi"]] = outdf.apply(dc.utils.constructProgrammeName, axis=1)
    else:
        outdf["TXDate"] = indf["txdate"].apply(lambda x: pd.to_datetime(x, format="%d-%m-%Y")).apply(lambda x:x.strftime("%Y%m%d"))
        outdf["ActualTime"] = indf["actualtime"].astype('str').str.slice(-8).str.slice(0, 5).str.zfill(5)
        outdf["BrandNameChi"] = outdf["BrandNameChi"].where(outdf["BrandNameChi"].apply(lambda x: re.match("^[\s]*$",str(x)) is not None or pd.isnull(x))).fillna(outdf["BrandNameEng"])
        outdf["SynopsisChi"] = outdf["SynopsisChi"].where(outdf["SynopsisChi"].apply(lambda x: re.match("^[\s]*$",str(x)) is not None or pd.isnull(x))).fillna(outdf["SynopsisEng"])
        outdf["SynopsisEng"] = outdf["SynopsisEng"][outdf["SynopsisEng"].notnull()].apply(lambda x: x[0:dc.settings.char_limit.get("SynopsisEng")])
        outdf["SynopsisChi"] = outdf["SynopsisChi"][outdf["SynopsisChi"].notnull()].apply(lambda x: x[0:dc.settings.char_limit.get("SynopsisChi")])
        outdf["ShortSynopsisEng"] = outdf["SynopsisEng"][outdf["SynopsisEng"].notnull()].apply(lambda x: x[0:dc.settings.char_limit.get("ShortSynopsisEng")])
        outdf["ShortSynopsisChi"] = outdf["SynopsisChi"][outdf["SynopsisChi"].notnull()].apply(lambda x: x[0:dc.settings.char_limit.get("ShortSynopsisChi")])
    outdf[["FullTitleEng", "FullTitleChi"]] = outdf.apply(dc.utils.constructFullTitle, axis=1)
    dc.utils.translateChar(outdf)
    dc.utils.chopCharacters(outdf)


    # Append EOF
    dailyStartTime = pd.to_datetime("01:00", format="%H:%M")
    dc.utils.appendEOF(outdf, daily_starttime=dailyStartTime)


    # Fill Channel #
    outdf['OwnerChannel'] = owner_channel
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)

    # Write output file
    dc.io.to_io(outdf, out_io, out_format)

def convert_nonlinear(in_io, out_io, format="preV1506", owner_channel="Fox News"):
    "Convert nonlinear for Fox News"
    return None

