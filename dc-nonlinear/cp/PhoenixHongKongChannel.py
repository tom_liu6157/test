import pandas as pd
import numpy as np
import datetime
import math
import csv
import re
import calendar
from collections import OrderedDict
import dc.utils
import dc.io
import dc.header
import dc.mapping
import dc.settings

def convert_linear(in_io, out_io, out_format="preV1506", owner_channel="Phoenix Hong Kong Channel"):
    "Convert linear schedule for Phoenix Hong Kong Channel"

    # Read input file
    indf = pd.read_excel(in_io, sheetname=0, skiprows=3)
    indf["Date"] = indf["Date"].apply(lambda x: None if x == "Date" else x)
    indf = indf[indf["Date"].notnull()]

    
    # Create output DataFrame
    outdf = pd.DataFrame(columns=dc.header.internalHeader)

    # Extract data from full title
    fullTitleEng = indf["Programme (English)"][indf["Programme (English)"].notnull()].apply(lambda x: str(x).strip())
    fullTitleChi = indf["Programme (Chinese)"][indf["Programme (Chinese)"].notnull()].apply(lambda x: str(x).strip())
    extractedInfo = pd.DataFrame(columns=["EpisodeNameEng", "BrandNameEng", "EpisodeNameChi", "BrandNameChi", "EpisodeNo"])
    tempExtractedInfo = [
        fullTitleEng.str.extract("(?P<BrandNameEng>.+)\s*\((?P<EpisodeNo>[0-9]+\s*)\)$", expand=False),
        fullTitleEng.str.extract("(?P<BrandNameEng>.+)\s*\((?P<EpisodeNo>[0-9]+)/[0-9]+\s*\)$", expand=False),
        fullTitleEng.str.extract("(?P<BrandNameEng>.+)\s*\((?P<EpisodeNo>[0-9]+)\s+(?P<EpisodeNameEng>.*)\s*\)$", expand=False),
        fullTitleEng.str.extract("(?P<BrandNameEng>.+?)\s*\(*(?P<EpisodeNameEng>[0-9]+/[0-9]+/[0-9]+\s*)\)$", expand=False),
        fullTitleChi.str.extract("(?P<BrandNameChi>.+)\s*\((?P<EpisodeNo>[0-9]+\s*)\)$", expand=False),
        fullTitleChi.str.extract("(?P<BrandNameChi>.+)\s*\((?P<EpisodeNo>[0-9]+)/[0-9]+\s*\)$", expand=False),
        fullTitleChi.str.extract("(?P<BrandNameChi>.+)\s*\((?P<EpisodeNo>[0-9]+)\s+(?P<EpisodeNameChi>.*)\s*\)$", expand=False),
        fullTitleChi.str.extract("(?P<BrandNameChi>.+?)\s*\(*(?P<EpisodeNameChi>[0-9]+/[0-9]+/[0-9]+\s*)\)$", expand=False),
    ]
    extractedInfo["BrandNameEng"] = tempExtractedInfo[0]["BrandNameEng"].str.strip()
    extractedInfo["EpisodeNo"] = tempExtractedInfo[0]["EpisodeNo"]
    extractedInfo["BrandNameEng"].fillna(tempExtractedInfo[1]["BrandNameEng"].str.strip(), inplace=True)
    extractedInfo["EpisodeNo"].fillna(tempExtractedInfo[1]["EpisodeNo"], inplace=True)
    extractedInfo["BrandNameEng"].fillna(tempExtractedInfo[2]["BrandNameEng"], inplace=True)
    extractedInfo["EpisodeNo"].fillna(tempExtractedInfo[2]["EpisodeNo"], inplace=True)
    extractedInfo["EpisodeNameEng"].fillna(tempExtractedInfo[2]["EpisodeNameEng"], inplace=True)
    extractedInfo["BrandNameEng"].fillna(tempExtractedInfo[3]["BrandNameEng"].str.strip(), inplace=True)
    extractedInfo["EpisodeNameEng"].fillna(tempExtractedInfo[3]["EpisodeNameEng"], inplace=True)
    extractedInfo["BrandNameEng"].fillna(fullTitleEng, inplace=True)
    extractedInfo["BrandNameChi"] = tempExtractedInfo[4]["BrandNameChi"].str.strip()
    extractedInfo["EpisodeNo"] = tempExtractedInfo[4]["EpisodeNo"]
    extractedInfo["BrandNameChi"].fillna(tempExtractedInfo[5]["BrandNameChi"].str.strip(), inplace=True)
    extractedInfo["EpisodeNo"].fillna(tempExtractedInfo[5]["EpisodeNo"], inplace=True)
    extractedInfo["BrandNameChi"].fillna(tempExtractedInfo[6]["BrandNameChi"], inplace=True)
    extractedInfo["EpisodeNo"].fillna(tempExtractedInfo[6]["EpisodeNo"], inplace=True)
    extractedInfo["EpisodeNameChi"].fillna(tempExtractedInfo[6]["EpisodeNameChi"], inplace=True)
    extractedInfo["BrandNameChi"].fillna(tempExtractedInfo[7]["BrandNameChi"].str.strip(), inplace=True)
    extractedInfo["EpisodeNameChi"].fillna(tempExtractedInfo[7]["EpisodeNameChi"], inplace=True)
    extractedInfo["BrandNameChi"].fillna(fullTitleChi, inplace=True)
    extractedInfo['EpisodeNameEng'] = extractedInfo['EpisodeNameEng'].apply(lambda x: pd.to_datetime(x, format='%d/%m/%y').strftime("%Y%m%d") if pd.notnull(x) and re.match('\d+/\d+/\d+', x) else x)
    extractedInfo['EpisodeNameChi'] = extractedInfo['EpisodeNameChi'].apply(lambda x: pd.to_datetime(x, format='%d/%m/%y').strftime("%Y%m%d") if pd.notnull(x) and re.match('\d+/\d+/\d+', x) else x)
    season_extraction = extractedInfo['BrandNameEng'].str.extract("(?P<BrandNameEng>.*?)\s*\(SERIES (?P<SeasonNo>\d+)\)", expand=False)
    extractedInfo["SeasonNo"] = season_extraction["SeasonNo"]


    # Fill output DataFrame with related info
    outdf["TXDate"] = indf["Date"].apply(lambda x: pd.to_datetime(x, format="%d/%m/%Y")).apply(lambda x:x.strftime("%Y%m%d"))
    outdf["ActualTime"] = indf["HK Time"].apply(lambda x: pd.to_datetime(x, format="%H:%M")).apply(lambda x:x.strftime("%H:%M"))
    outdf["BrandNameEng"]=extractedInfo["BrandNameEng"]
    outdf["BrandNameChi"]=extractedInfo["BrandNameChi"]
    outdf["BrandNameChi"].fillna(outdf["BrandNameEng"], inplace=True)
    outdf["BrandNameEng"].fillna(outdf["BrandNameChi"], inplace=True)
    outdf["EpisodeNameEng"]=extractedInfo["EpisodeNameEng"]
    outdf["EpisodeNameChi"]=extractedInfo["EpisodeNameChi"]
    outdf["EpisodeNameChi"].fillna(outdf["EpisodeNameEng"], inplace=True)
    outdf["EpisodeNameEng"].fillna(outdf["EpisodeNameChi"], inplace=True)
    outdf["EpisodeNo"]=extractedInfo["EpisodeNo"]
    outdf["PremierOld"] = "0"
    outdf["Premier"] = "N"
    outdf["IsLive"] = "N"
    outdf["IsEpisodic"] = (outdf["EpisodeNo"].notnull() | outdf["EpisodeNameEng"].notnull()).map({True:"Y", False:"N"})
    dc.utils.title_df(outdf)
    outdf[["FullTitleEng", "FullTitleChi"]] = outdf.apply(dc.utils.constructFullTitle, axis=1)
    outdf[["ProgrammeNameEng", "ProgrammeNameChi"]] = outdf.apply(dc.utils.constructProgrammeName, axis=1)
    dc.utils.translateChar(outdf)
    dc.utils.chopCharacters(outdf)


    # Append EOF
    lastProgDuration = pd.Timedelta("00:30:00")
    dc.utils.appendEOF(outdf, lastprog_duration=lastProgDuration)


    # Fill Channel #
    outdf['OwnerChannel'] = owner_channel
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)

    # Write output file
    dc.io.to_io(outdf, out_io, out_format)

def convert_nonlinear(in_io, out_io, format="preV1506", owner_channel="Phoenix Hong Kong Channel"):
    "Convert nonlinear for Phoenix Hong Kong Channel"
    return None

