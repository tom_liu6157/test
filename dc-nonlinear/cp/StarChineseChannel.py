import pandas as pd
import numpy as np
import datetime
import math
import csv
import re
import calendar
from collections import OrderedDict
import dc.utils
import dc.io
import dc.header
import dc.mapping
import dc.settings

def convert_linear(in_io, out_io, out_format="preV1506", owner_channel="Star Chinese Channel"):
    "Convert linear schedule for Star Chinese Channel"

    # Read input file
    indf = pd.read_excel(in_io, sheetname=0, skiprows=[0,1], na_values=["NIL"], keep_default_na=True)

    
    # Create output DataFrame
    outdf = pd.DataFrame(columns=dc.header.internalHeader)

    # Extract data from full title
    fullTitleEng = indf["Programme (Eng)"][indf["Programme (Eng)"].notnull()].apply(lambda x: str(x).strip())
    fullTitleChi = indf["Programme (Chi)"][indf["Programme (Chi)"].notnull()].apply(lambda x: str(x).strip())
    extractedInfo = pd.DataFrame(columns=["BrandNameEng", "BrandNameChi", "SeasonNo", "EpisodeNo", "EpisodeNameEng", "EpisodeNameChi"])
    tempExtractedInfo = [
        fullTitleEng.str.extract("(?P<BrandNameEng>.*?)\s+S(?P<SeasonNo>\d)(?P<EpisodeNo>\d+)\\s*:*\s*(?P<EpisodeNameEng>.*)", expand=False),
        fullTitleEng.str.extract("(?P<BrandNameEng>HOLLYWOOD ON SET)\s+(?P<EpisodeNo>\d+)", expand=False),
        fullTitleChi.str.extract("(?P<BrandNameChi>.*?)\((第(?P<SeasonNo>\d+)季)*第(?P<EpisodeNo>\d+)集\)", expand=False)
    ]
    extractedInfo["BrandNameEng"] = tempExtractedInfo[0]["BrandNameEng"]
    extractedInfo["BrandNameEng"].fillna(tempExtractedInfo[1]["BrandNameEng"], inplace=True)
    extractedInfo["BrandNameEng"].fillna(fullTitleEng, inplace=True)
    extractedInfo["BrandNameChi"] = tempExtractedInfo[2]["BrandNameChi"]
    extractedInfo["BrandNameChi"].fillna(fullTitleChi, inplace=True)


    # Fill output DataFrame with related info
    outdf["TXDate"] = indf["Date"].apply(lambda x: pd.to_datetime(x, format="%d/%m/%Y")).apply(lambda x:x.strftime("%Y%m%d"))
    outdf["ActualTime"] = indf["Start Time"].apply(lambda x: pd.to_datetime(x, format="%H:%M:%S")).apply(lambda x:x.strftime("%H:%M"))
    outdf["BrandNameEng"] = extractedInfo["BrandNameEng"].str.translate(dc.mapping.charTranslateTable)
    outdf["BrandNameChi"] = extractedInfo["BrandNameChi"].str.translate(dc.mapping.charTranslateTable)
    outdf["BrandNameChi"].fillna(outdf["BrandNameEng"], inplace=True)
    outdf["BrandNameEng"].fillna(outdf["BrandNameChi"], inplace=True)
    outdf["EpisodeNo"] = indf["Episode #"][indf["Episode #"].notnull()].astype(int).astype(str)
    outdf["SynopsisEng"] = indf["EPG Synopses (Eng)"].str.translate(dc.mapping.charTranslateTable)
    outdf["SynopsisChi"] = indf["EPG Synopses (Chi)"].str.translate(dc.mapping.charTranslateTable)
    outdf["ShortSynopsisEng"] = outdf["SynopsisEng"][outdf["SynopsisEng"].notnull()].apply(lambda x: x[0:dc.settings.char_limit.get("ShortSynopsisEng")])
    outdf["ShortSynopsisChi"] = outdf["SynopsisChi"][outdf["SynopsisChi"].notnull()].apply(lambda x: x[0:dc.settings.char_limit.get("ShortSynopsisChi")])
    outdf["PremierOld"] = "0"
    outdf["Premier"] = "N"
    outdf["IsLive"] = "N"
    outdf["CastEng"] = indf["Cast (Eng)"]
    outdf["CastChi"] = indf["Cast (Chi)"].apply(lambda x: str(x).strip()).str.replace('(\s*,\s*|\s*、\s*|\s*，\s*|\s*﹑\s*|\s*ʼ\s*)', ', ').str.translate(dc.mapping.charTranslateTable)
    outdf["IsEpisodic"] = (outdf["EpisodeNo"].notnull() | outdf["EpisodeNameEng"].notnull()).map({True:"Y", False:"N"})
    outdf["SubGenre"] = indf["Genre"].str.split("/").apply(lambda subGenres: sorted(set([item for subGenre in subGenres for item in dc.mapping.subGenreMapping.get(subGenre.lower(), [subGenre])]))).str.join("/").fillna(indf["Genre"])
    dc.utils.title_df(outdf)
    outdf[["FullTitleEng", "FullTitleChi"]] = outdf.apply(dc.utils.constructFullTitle, axis=1)
    outdf[["ProgrammeNameEng", "ProgrammeNameChi"]] = outdf.apply(dc.utils.constructProgrammeName, axis=1)
    dc.utils.translateChar(outdf)
    dc.utils.chopCharacters(outdf)


    # Append EOF
    temp = pd.Timedelta(str(indf["Duration"][indf.last_valid_index()]+":00"))
    lastProgDuration = pd.Timedelta(temp)
    dc.utils.appendEOF(outdf, lastprog_duration=lastProgDuration)


    # Fill Channel #
    outdf['OwnerChannel'] = owner_channel
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)

    # Write output file
    dc.io.to_io(outdf, out_io, out_format)

def convert_nonlinear(in_io, out_io, format="preV1506", owner_channel="Star Chinese Channel"):
    "Convert nonlinear for Star Chinese Channel"
    return None

