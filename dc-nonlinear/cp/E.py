import pandas as pd
import numpy as np
import datetime
import math
import csv
import re
import calendar
from collections import OrderedDict
import dc.utils
import dc.io
import dc.header
import dc.mapping
import dc.settings

def convert_linear(in_io, out_io, out_format="preV1506", owner_channel="E!"):
    "Convert linear schedule for E!"

    # Read input file
    import dc.TestReadingExcel
    from pandas.compat import (lrange)
    import dateutil
    data = dc.TestReadingExcel.read_excel(in_io, sheetname=0)
    month = ''
    year = ''
    for i in data:
        for j in i:
            if re.match("E!AsiaEPGScheduleListing", re.sub("\s","",str(j)), re.I) is not None:
                temp = j.split()
                if re.fullmatch("[\d]{4}",temp[-1]) is not None:
                    year = temp[-1]
                    month = dateutil.parser.parserinfo().month(temp[-2])
                else:
                    month = dateutil.parser.parserinfo().month(temp[-1])
                break
    if type(in_io) != str:
        in_io.seek(0)
    indf = pd.read_excel(in_io, sheetname=0, skiprows=1)
    if pd.isnull(month) or month == '':
        month = pd.to_datetime(indf["Transmission Date"][0]).strftime("%Y%m%d")[4:6]
    if year == '':
        year = pd.to_datetime(indf["Transmission Date"][0]).strftime("%Y%m%d")[0:4]

    
    # Create output DataFrame
    outdf = pd.DataFrame(columns=dc.header.internalHeader)

    # Extract data from full title
    indf["Episode Title"] = indf["Episode Title"].apply(lambda x: pd.to_datetime(str(x)).strftime("%Y%m%d")[0:4] + pd.to_datetime(str(x)).strftime("%Y%m%d")[6:] + pd.to_datetime(str(x)).strftime("%Y%m%d")[4:6] 
                                                        if dc.utils.validateDatetime(str(x))
                                                        and int(pd.to_datetime(str(x)).strftime("%Y%m%d")[4:6]) != int(month)
                                                        else pd.to_datetime(str(x)).strftime("%Y%m%d") 
                                                        if dc.utils.validateDatetime(str(x))
                                                        and int(pd.to_datetime(str(x)).strftime("%Y%m%d")[4:6]) == int(month) else x)
    count = 0
    for i in indf["Episode Title"]:
        if re.fullmatch("[\d]{8}", str(i)) is not None:
            try:
                t = pd.to_datetime(str(i))
            except ValueError:
                indf.set_value(count, "Episode Title", str(i)[0:4]+str(i)[6:]+str(i)[4:6])
        count = count + 1
    fullTitleEng = indf["Programme Title"][indf["Programme Title"].notnull()].apply(lambda x: str(x).strip())
    fullEpisodeEng = indf["Episode Title"].apply(lambda x: x.strftime("%Y%m%d") if isinstance(x, datetime.datetime) else "" if str(x).strip() == "TBD" else x)    # change date format to string
    fullEpisodeEng = fullEpisodeEng[fullEpisodeEng.notnull()].apply(lambda x: str(x).strip())
    tempExtractedInfo = [
        fullTitleEng.str.extract("^(?P<BrandNameEng>.+?)\s*(S\d+)*$", expand=False),    # discard season no.
        fullEpisodeEng.str.extract("^(?P<EpisodeNameEng>.*?)\s*(S\d+)*\s*(Episode\s\d+)*$", expand=False)    # discard season no. and episode no.
    ]
    extractedInfo = pd.concat([tempExtractedInfo[0]["BrandNameEng"], tempExtractedInfo[1]["EpisodeNameEng"]], axis=1)
    extractedInfo["EpisodeNameEng"] = extractedInfo["EpisodeNameEng"][extractedInfo.apply(lambda x: not x["BrandNameEng"] == x["EpisodeNameEng"], axis=1)]    # remove episode title if it is same as brand name


    # Fill output DataFrame with related info
    outdf["TXDate"] = indf["Transmission Date"].where(indf["SCHEDULED TIME 24 HOUR CLOCK (HH:MM) SIN/HK"].apply(lambda x: int(x.strftime("%H"))) <6).apply(lambda x: x + np.timedelta64(1,'D')).fillna(indf["Transmission Date"]).apply(lambda x: x.strftime("%Y%m%d"))
    outdf["ActualTime"] = indf["SCHEDULED TIME 24 HOUR CLOCK (HH:MM) SIN/HK"].apply(lambda x: pd.to_datetime(x, format="%H:%M:%S")).apply(lambda x:x.strftime("%H:%M"))
    outdf["BrandNameEng"] = extractedInfo["BrandNameEng"][extractedInfo["BrandNameEng"].notnull()].str.translate(dc.mapping.charTranslateTable).apply(lambda x: x[0:dc.settings.char_limit.get("BrandNameEng")])
    outdf["BrandNameEng"] = outdf["BrandNameEng"][outdf["BrandNameEng"].notnull()].str.replace("Soup, The", "The Soup")
    outdf["BrandNameChi"].fillna(outdf["BrandNameEng"], inplace=True)
    outdf["SeasonNo"] = indf["Series"][indf["Series"].notnull()].astype(int).astype(str)
    outdf["EpisodeNo"] = indf["Episode Number"][indf["Episode Number"].notnull()].apply(                                                                                     lambda x:                                                                                         str(int(re.match("^([0-9]+)(?:\.0+|[0-9]*)$", str(x)).groups()[0])) if re.match("^[0-9]+(?:\.0+|[0-9]*)$", str(x))                                                                                         else "")
    outdf["EpisodeNameEng"] = extractedInfo["EpisodeNameEng"][extractedInfo["EpisodeNameEng"].notnull()].str.translate(dc.mapping.charTranslateTable).apply(lambda x: x[0:dc.settings.char_limit.get("EpisodeNameEng")])
    cond1 = indf["Programme Title"].apply(lambda x: re.match("^The Tonight Show Starring Jimmy Fallon S(?:[456789]|10)$",x) is not None) 
    cond2 = outdf["EpisodeNameEng"].apply(lambda x: re.match("^[\s]*$",str(x)) is not None or pd.isnull(x)) 
    cond3 = outdf["EpisodeNo"].apply(lambda x: re.match("^[\s]*$",str(x)) is not None or pd.isnull(x)) 
    outdf["EpisodeNameEng"] = indf["Transmission Date"].where(np.logical_and(cond1,np.logical_and(cond2,cond3))).apply(lambda x: (x - np.timedelta64(1,'D')).strftime("%Y%m%d") if not pd.isnull(x) else None).fillna(outdf["EpisodeNameEng"])
    outdf["EpisodeNameChi"].fillna(outdf["EpisodeNameEng"], inplace=True)
    outdf["SynopsisEng"] = indf["Synopsis"][indf["Synopsis"].notnull()].str.translate(dc.mapping.charTranslateTable).apply(lambda x: x[0:dc.settings.char_limit.get("SynopsisEng")])
    outdf["SynopsisChi"].fillna(outdf["SynopsisEng"], inplace=True)
    outdf["ShortSynopsisEng"] = outdf["SynopsisEng"][outdf["SynopsisEng"].notnull()].apply(lambda x: x[0:dc.settings.char_limit.get("ShortSynopsisEng")])
    outdf["ShortSynopsisChi"] = outdf["SynopsisChi"][outdf["SynopsisChi"].notnull()].apply(lambda x: x[0:dc.settings.char_limit.get("ShortSynopsisChi")])
    outdf["PremierOld"] = "0"
    outdf["Premier"] = "N"
    outdf["IsLive"] = "N"
    outdf["Classification"] = indf["Rating"].apply(lambda x: str(x).strip())
    outdf["OriginalLang"] = indf["Language"].str.split('; ').apply(lambda audioLangs: [dc.mapping.audioLangMapping[audioLang.lower()] for audioLang in audioLangs]).str.join(', ')
    outdf["AudioLang"].fillna(outdf["OriginalLang"], inplace=True)
    outdf["FirstReleaseYear"] = indf["Year"][indf["Year"].notnull()].astype(int).astype(str)
    outdf["Classification"] = indf["Rating"].replace("TV-U","TV-G")
    outdf["IsEpisodic"] = (outdf["EpisodeNo"].notnull() | outdf["EpisodeNameEng"].notnull()).map({True:"Y", False:"N"})
    dc.utils.title_df(outdf)
    outdf[["FullTitleEng", "FullTitleChi"]] = outdf.apply(dc.utils.constructFullTitle, axis=1)
    outdf[["ProgrammeNameEng", "ProgrammeNameChi"]] = outdf.apply(dc.utils.constructProgrammeName, axis=1)
    dc.utils.translateChar(outdf)
    dc.utils.chopCharacters(outdf)


    # Append EOF
    temp =str(indf["Scheduled Duration"][indf.last_valid_index()])
    lastProgDuration = pd.Timedelta(temp)
    dc.utils.appendEOF(outdf, lastprog_duration=lastProgDuration)


    # Fill Channel #
    outdf['OwnerChannel'] = owner_channel
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)

    # Write output file
    dc.io.to_io(outdf, out_io, out_format)

def convert_nonlinear(in_io, out_io, format="preV1506", owner_channel="E!"):
    "Convert nonlinear for E!"
    return None

