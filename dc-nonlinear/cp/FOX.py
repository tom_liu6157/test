import pandas as pd
import numpy as np
import datetime
import math
import csv
import re
import calendar
from collections import OrderedDict
import dc.utils
import dc.io
import dc.header
import dc.mapping
import dc.settings

def convert_linear(in_io, out_io, out_format="preV1506", owner_channel="FOX"):
    "Convert linear schedule for FOX"

    # Read input file
    indf = pd.read_excel(in_io, sheetname=0, skiprows=[0,1])
    indf.columns = ["".join(s.split()).replace("(", "").replace(")", "").replace("/", "").lower() for s in indf.columns]

    
    # Create output DataFrame
    outdf = pd.DataFrame(columns=dc.header.internalHeader)

    # Extract data from full title


    # Fill output DataFrame with related info
    excepted_column = ["PID",  "Full Title (English) Max 40 Characters", "Full Title (Chinese) Max 24 Characters", "Season no. (max 3 digits)", "Episode no. (max 5 digits)", "Short Synopsis (English) (max 120 chars)", "Short Synopsis (Chinese) (max 120 chars)", "Original language (max 50 chars)", "Region Code (max 2 chars)", "First release year (max 4 digits)", "Premier", "Recordable", "Is NPVR Prog", "Is Restart TV", "effective_date", "expiration_date", "Media ID (max. 12 chars)"]
    for internal_column, indf_column in dc.header.postV1506HeaderMapping.items():
        if (indf_column not in excepted_column): 
            edited_indf_column = "".join(indf_column.split()).replace("(", "").replace(")", "").replace("/", "").lower()
            if (edited_indf_column in indf.columns):
                outdf[internal_column] = indf[edited_indf_column]
                if (outdf[internal_column].dtype != np.float64) and (outdf[internal_column].dtype != np.int64):
                    outdf[internal_column] = outdf[internal_column].where(outdf[internal_column].apply(lambda x: pd.notnull(x) and len(str(x).strip()) > 0))
                else:
                    outdf[internal_column] = outdf[internal_column][outdf[internal_column].notnull()].astype(int).astype(str)
    outdf["FullTitleEng"] = indf["fulltitleenglish"].where(indf["fulltitleenglish"].str.strip().apply(lambda x: pd.notnull(x) and len(x) > 0))
    outdf["FullTitleChi"] = indf["fulltitlechinese"].where(indf["fulltitlechinese"].str.strip().apply(lambda x: pd.notnull(x) and len(x) > 0))
    outdf["SeasonNo"] = indf["seasonno."][indf["seasonno."].notnull()].astype(int).astype(str)
    outdf["EpisodeNo"] = indf["episodeno."][indf["episodeno."].notnull()].astype(int).astype(str)
    outdf["ShortSynopsisEng"] = indf["shortsynopsisenglish"].where(indf["shortsynopsisenglish"].str.strip().apply(lambda x: pd.notnull(x) and len(x) > 0))
    outdf["ShortSynopsisChi"] = indf["shortsynopsischinese"].where(indf["shortsynopsischinese"].str.strip().apply(lambda x: pd.notnull(x) and len(x) > 0))
    outdf["OriginalLang"] = indf["originallanguage"].where(indf["originallanguage"].str.strip().apply(lambda x: pd.notnull(x) and len(x) > 0))
    outdf["RegionCode"] = indf["regioncode"].where(indf["regioncode"].str.strip().apply(lambda x: pd.notnull(x) and len(x) > 0))
    outdf["FirstReleaseYear"] = indf["firstreleaseyear"][indf["firstreleaseyear"].notnull()].astype(int).astype(str)
    outdf["CPInternalSeriesID"] = outdf["CPInternalUniqueID"]
    outdf["CPInternalUniqueID"] = None
    outdf["EpisodeNameChi"].fillna(outdf["EpisodeNameEng"], inplace=True)
    outdf["EpisodeNameEng"].fillna(outdf["EpisodeNameChi"], inplace=True)
    outdf["PremierOld"] = ((outdf["Premier"] == "Y") | (outdf["IsLive"] == "Y")).map({True: "1", False: "0"})
    text_column = ["SponsorTextStuntEng", "SponsorTextStuntChi", "BrandNameEng", "BrandNameChi", "EditionVersionEng", "EditionVersionChi", "SeasonNameEng", "SeasonNameChi", "EpisodeNameEng", "EpisodeNameChi", "SynopsisEng", "SynopsisChi", "ShortSynopsisEng", "ShortSynopsisChi", "DirectorProducerEng", "DirectorProducerChi", "CastEng", "CastChi"]
    for column in text_column:
        outdf[column] = outdf[column].astype(str).where(outdf[column].notnull()).str.translate(dc.mapping.charTranslateTable)
    dc.utils.title_df(outdf)
    outdf[["FullTitleEng", "FullTitleChi"]] = outdf.apply(dc.utils.constructFullTitle, axis=1)
    outdf[["ProgrammeNameEng", "ProgrammeNameChi"]] = outdf.apply(dc.utils.constructProgrammeName, axis=1)
    dc.utils.translateChar(outdf)
    dc.utils.chopCharacters(outdf)


    # Append EOF
    dc.utils.appendEOF(outdf)


    # Fill Channel #
    outdf['OwnerChannel'] = owner_channel
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)

    # Write output file
    dc.io.to_io(outdf, out_io, out_format)

def convert_nonlinear(in_io, out_io, format="preV1506", owner_channel="FOX"):
    "Convert nonlinear for FOX"
    
    # Read input file
    sheet_name = "VOD "
    indf = pd.read_excel(in_io, sheetname=sheet_name, keep_default_na=False)

    
    # Create output DataFrame
    outdf = pd.DataFrame(columns=dc.utils.extendnlInternal(indf.columns))

    # Extract data from full title


    # Fill output DataFrame with related info
    excepted_column = []
    dc.utils.fillOutdf(outdf,indf,excepted_column)
    excepted_column = ["AdvisorySlate", "TVClassification"]
    for internal_column in dc.utils.extendnlInternal(indf.columns):
        if (internal_column not in excepted_column):
            outdf[internal_column] = outdf[internal_column].apply(lambda x: None if isinstance(x, str) and x.lower().strip() in ["n/a", ""] else x)
    outdf["StartDate"] = outdf["StartDate"].apply(lambda x: re.match("^([0-9]+)(?:\.0+|[0-9]*)$", str(x)).groups()[0] if re.match("^[0-9]+(?:\.0+|[0-9]*)$", str(x)) else x.strftime("%Y%m%d") if isinstance(x, datetime.datetime) or isinstance(x, datetime.time) else x)
    outdf["EndDate"] = outdf["EndDate"].apply(lambda x: re.match("^([0-9]+)(?:\.0+|[0-9]*)$", str(x)).groups()[0] if re.match("^[0-9]+(?:\.0+|[0-9]*)$", str(x)) else x.strftime("%Y%m%d") if isinstance(x, datetime.datetime) or isinstance(x, datetime.time) else x)
    outdf["Duration"] = outdf["Duration"].astype(str)
    outdf["BrandNameEng"] = outdf["BrandNameEng"].str.extract("^(.*?)\s*(S\d+)*\s*(\(Ep\.\s*\d+\))*$", expand=False)[0]
    outdf["BrandNameChi"] = outdf["BrandNameChi"].str.extract("^(.*?)(\s*-\s*(第\d+季)*\s*(第\d+集)*)*$", expand=False)[0]
    outdf["DirectorProducerEng"] = outdf["DirectorProducerEng"][outdf["DirectorProducerEng"].notnull()].astype(str).str.split(",").apply(lambda x: [item.strip() for item in x]).str.join(', ')
    outdf["DirectorProducerChi"] = outdf["DirectorProducerChi"][outdf["DirectorProducerChi"].notnull()].astype(str).str.split("、").apply(lambda x: [item.strip() for item in x]).str.join(', ')
    outdf["DirectorProducerChi"] = outdf["DirectorProducerChi"][outdf["DirectorProducerChi"].notnull()].astype(str).str.split(",").apply(lambda x: [item.strip() for item in x]).str.join(', ')
    outdf["CastEng"] = outdf["CastEng"][outdf["CastEng"].notnull()].astype(str).str.split(",").apply(lambda x: [item.strip() for item in x]).str.join(', ')
    outdf["CastChi"] = outdf["CastChi"][outdf["CastChi"].notnull()].astype(str).str.split("、").apply(lambda x: [item.strip() for item in x]).str.join(', ')
    outdf["CastChi"] = outdf["CastChi"][outdf["CastChi"].notnull()].astype(str).str.split(",").apply(lambda x: [item.strip() for item in x]).str.join(', ')
    outdf["Genre"] = outdf["Genre"].astype(str).str.lower().map(dc.mapping.genreMapping).apply(lambda x: "TV Series" if pd.isnull(x) or x != "TV Series" else x)
    outdf["SubGenre"] = outdf["SubGenre"][outdf["SubGenre"].notnull()].astype(str).str.split("/").apply(lambda subGenres: sorted(set([subGenre.strip() for subGenre in subGenres]))).str.join("/").fillna(outdf["SubGenre"])
    outdf["SubGenre"] = outdf[["SubGenre","Genre"]].apply(lambda x: str(x["SubGenre"])+"/Western" if pd.notnull(x["SubGenre"]) and "Western" not in str(x["SubGenre"]) and x["Genre"] == "TV Series" else "Western" if (pd.isnull(x["SubGenre"]) or str(x["SubGenre"]).strip() == "") and x["Genre"] == "TV Series" else x,axis=1)
    outdf["OriginalLang"] = outdf["OriginalLang"].astype(str).str.lower().map(dc.mapping.audioLangMapping)
    outdf["AudioLang"] = outdf["AudioLang"].astype(str).str.lower().map(dc.mapping.audioLangMapping)
    outdf["SubtitleLang"] = outdf["SubtitleLang"].astype(str).str.lower().map(dc.mapping.subtitleMapping)
    outdf["RegionCode"] = outdf["RegionCode"].astype(str).str.lower().map(dc.mapping.regionCodeMapping)


    # Write output file
    dc.io.to_io_nonlinear(outdf, out_io, format)

