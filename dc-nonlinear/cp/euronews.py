import pandas as pd
import numpy as np
import datetime
import math
import csv
import re
import calendar
from collections import OrderedDict
import dc.utils
import dc.io
import dc.header
import dc.mapping
import dc.settings

def convert_linear(in_io, out_io, out_format="preV1506", owner_channel="euronews"):
    "Convert linear schedule for euronews"

    # Read input file
    indf = pd.read_excel(in_io, sheetname=0, skiprows=0, skip_footer=0)

    
    # Create output DataFrame
    outdf = pd.DataFrame(columns=dc.header.internalHeader)

    # Extract data from full title


    # Fill output DataFrame with related info
    outdf["TXDate"] = indf["Date"].where(indf["Start Time"].apply(lambda x: int(x.strftime("%H"))) >= 23).apply(lambda x: x + np.timedelta64(1,'D')).fillna(indf["Date"]).apply(lambda x: x.strftime("%Y%m%d"))
    outdf["ActualTime"] = indf["Start Time"].apply(lambda x: pd.to_datetime(x, format="%H:%M:%S") + pd.Timedelta(1,'h')).apply(lambda x:x.strftime("%H:%M"))
    outdf["BrandNameEng"] = indf["Programme Title"][indf["Programme Title"].notnull()].str.translate(dc.mapping.charTranslateTable).apply(lambda x: x[0:dc.settings.char_limit.get("BrandNameEng")])
    outdf["BrandNameChi"].fillna(outdf["BrandNameEng"], inplace=True)
    outdf["SynopsisEng"] = indf["EPG Synopsis"][indf["EPG Synopsis"].notnull()].str.translate(dc.mapping.charTranslateTable).apply(lambda x: x[0:dc.settings.char_limit.get("SynopsisEng")])
    outdf["SynopsisChi"].fillna(outdf["SynopsisEng"], inplace=True)
    outdf["ShortSynopsisEng"] = outdf["SynopsisEng"][outdf["SynopsisEng"].notnull()].apply(lambda x: x[0:dc.settings.char_limit.get("ShortSynopsisEng")])
    outdf["ShortSynopsisChi"] = outdf["SynopsisChi"][outdf["SynopsisChi"].notnull()].apply(lambda x: x[0:dc.settings.char_limit.get("ShortSynopsisChi")])
    outdf["PremierOld"] = indf["Premiere"].map(lambda x: "1" if pd.notnull(x) and x.lower() == "yes" else "0")
    outdf["Premier"] = outdf["PremierOld"].map({"1" : "Y", "0": "N"})
    outdf["IsLive"] = "N"
    outdf["Classification"] = indf["PG Rating"].apply(lambda x: str(x).strip())
    outdf["Genre"] = indf["Logline"][indf["Logline"].notnull()].str.lower().map(dc.mapping.genreMapping).fillna(indf["Logline"])
    outdf["SubGenre"] = indf["Theme"].str.split("/")[indf["Theme"].notnull()].apply(lambda subGenres: sorted(set([item for subGenre in subGenres for item in dc.mapping.subGenreMapping.get(subGenre.lower().strip(), [subGenre])]))).str.join("/").fillna(indf["Theme"]).apply(lambda x: x[0:dc.settings.char_limit.get("SubGenre")])
    outdf["IsEpisodic"] = (outdf["EpisodeNo"].notnull() | outdf["EpisodeNameEng"].notnull()).map({True:"Y", False:"N"})
    dc.utils.title_df(outdf)
    outdf[["FullTitleEng", "FullTitleChi"]] = outdf.apply(dc.utils.constructFullTitle, axis=1)
    outdf[["ProgrammeNameEng", "ProgrammeNameChi"]] = outdf.apply(dc.utils.constructProgrammeName, axis=1)
    dc.utils.translateChar(outdf)
    dc.utils.chopCharacters(outdf)


    # Append EOF
    dc.utils.appendEOF(outdf)


    # Fill Channel #
    outdf['OwnerChannel'] = owner_channel
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)

    # Write output file
    dc.io.to_io(outdf, out_io, out_format)

def convert_nonlinear(in_io, out_io, format="preV1506", owner_channel="euronews"):
    "Convert nonlinear for euronews"
    return None

