import pandas as pd
import numpy as np
import datetime
import math
import csv
import re
import calendar
from collections import OrderedDict
import dc.utils
import dc.io
import dc.header
import dc.mapping
import dc.settings

def convert_linear(in_io, out_io, out_format="preV1506", owner_channel="HBO"):
    "Convert linear schedule for HBO"

    # Read input file
    indf = pd.read_excel(in_io, sheetname=0)
    indf.columns = ["".join(s.split()).replace("(", "").replace(")", "").replace("/", "").lower() for s in indf.columns]

    
    # Create output DataFrame
    outdf = pd.DataFrame(columns=dc.header.internalHeader)

    # Extract data from full title


    # Fill output DataFrame with related info
    excepted_column = ["PID", "Premier", "Recordable", "Is NPVR Prog", "Is Restart TV", "effective_date", "expiration_date", "Media ID (max. 12 chars)"]
    for internal_column, indf_column in dc.header.postV1506HeaderMapping.items():
        if (indf_column not in excepted_column): 
            edited_indf_column = "".join(indf_column.split()).replace("(", "").replace(")", "").replace("/", "").lower()
            if (edited_indf_column in indf.columns):
                outdf[internal_column] = indf[edited_indf_column]
                if (outdf[internal_column].dtype == object):
                    outdf[internal_column] = outdf[internal_column].where(outdf[internal_column].apply(lambda x: pd.notnull(x) and len(str(x).strip()) > 0))
    start_hour = 6
    if (owner_channel == "HBO Hits") or (owner_channel == "HBO Family"):
        start_hour=7
    outdf["TXDate"] = outdf["TXDate"].where(outdf["ActualTime"].apply(lambda x: int(x.strftime("%H"))) < start_hour).apply(lambda x: x + pd.to_timedelta("1 days")).fillna(outdf["TXDate"])
    outdf["TXDate"] = outdf["TXDate"].apply(lambda x: x.strftime("%Y%m%d"))
    outdf["ActualTime"] = indf["actualtime"].apply(lambda x: x.strftime("%H:%M"))
    fullTitleEng = outdf["FullTitleEng"][outdf["FullTitleEng"].notnull()].apply(lambda x: str(x).strip())
    fullTitleChi = outdf["FullTitleChi"][outdf["FullTitleChi"].notnull()].apply(lambda x: str(x).strip())
    extractedInfo = pd.DataFrame(columns=["BrandNameEng", "BrandNameChi", "SeasonNo", "EpisodeNo", "EpisodeNameEng", "EpisodeNameChi", "EditionVersionEng", "EditionVersionChi"])
    tempExtractedInfo = [
        fullTitleEng.str.extract("(?P<BrandNameEng>.*?)\s+S(?P<SeasonNo>\d)(?P<EpisodeNo>\d+)\s*:*\s*(?P<EpisodeNameEng>.*)", expand=False),
        fullTitleEng.str.extract("(?P<BrandNameEng>HOLLYWOOD ON SET)\s+(?P<EpisodeNo>\d+)", expand=False),
        fullTitleChi.str.extract("(?P<BrandNameChi>.*?)\(*(第(?P<SeasonNo>\d+)季)*第(?P<EpisodeNo>\d+)集*\)*", expand=False),
        fullTitleChi.str.extract("(?P<BrandNameChi>.*?)\((?!上集|下集|(第\d+季)*第\d+集)(?P<EditionVersionChi>.*)\)", expand=False),
        fullTitleEng.str.extract("(?P<BrandNameEng>.*?)\((?P<EditionVersionEng>.*)\)", expand=False)
    ]
    extractedInfo["BrandNameEng"] = tempExtractedInfo[4]["BrandNameEng"]
    extractedInfo["SeasonNo"] = tempExtractedInfo[0]["SeasonNo"]
    extractedInfo["EpisodeNo"] = tempExtractedInfo[0]["EpisodeNo"]
    extractedInfo["EpisodeNameEng"] = tempExtractedInfo[0]["EpisodeNameEng"]
    extractedInfo["BrandNameEng"].fillna(tempExtractedInfo[0]["BrandNameEng"], inplace=True)
    extractedInfo["BrandNameEng"].fillna(tempExtractedInfo[1]["BrandNameEng"], inplace=True)
    extractedInfo["BrandNameEng"].fillna(fullTitleEng, inplace=True)
    extractedInfo["BrandNameChi"] = tempExtractedInfo[3]["BrandNameChi"]
    extractedInfo["BrandNameChi"].fillna(tempExtractedInfo[2]["BrandNameChi"], inplace=True)
    extractedInfo["BrandNameChi"].fillna(fullTitleChi, inplace=True)
    extractedInfo["EpisodeNo"].fillna(tempExtractedInfo[1]["EpisodeNo"], inplace=True)
    extractedInfo["EpisodeNo"] = extractedInfo["EpisodeNo"].apply(lambda x: str(int(x)) if isinstance(x, str) else x)
    extractedInfo["EpisodeNameEng"] = extractedInfo["EpisodeNameEng"].apply(lambda x: None if x == "" else x)
    extractedInfo["EditionVersionChi"] = tempExtractedInfo[3]["EditionVersionChi"].apply(lambda x: None if x == "" else x)
    extractedInfo["EditionVersionEng"] = tempExtractedInfo[4]["EditionVersionEng"].apply(lambda x: None if x == "" else x)
    outdf["BrandNameEng"] = extractedInfo["BrandNameEng"].str.strip()
    outdf["BrandNameChi"] = extractedInfo["BrandNameChi"].str.strip()
    outdf["SeasonNameEng"] = outdf[["BrandNameEng", "SeasonNameEng"]].apply(lambda x: None if pd.notnull(x["SeasonNameEng"]) and x["SeasonNameEng"].startswith(x["BrandNameEng"]) else x["SeasonNameEng"], axis=1)
    outdf["SeasonNameChi"] = outdf[["BrandNameChi", "SeasonNameChi"]].apply(lambda x: None if pd.notnull(x["SeasonNameChi"]) and x["SeasonNameChi"].startswith(x["BrandNameChi"]) else x["SeasonNameChi"], axis=1)
    outdf["EpisodeNameEng"] = outdf[["BrandNameEng", "EpisodeNameEng"]].apply(lambda x: None if pd.notnull(x["EpisodeNameEng"]) and x["EpisodeNameEng"].startswith(x["BrandNameEng"]) else x["EpisodeNameEng"], axis=1)
    outdf["EpisodeNameChi"] = outdf[["BrandNameChi", "EpisodeNameChi"]].apply(lambda x: None if pd.notnull(x["EpisodeNameChi"]) and x["EpisodeNameChi"].startswith(x["BrandNameChi"]) else x["EpisodeNameChi"], axis=1)
    outdf["PremierOld"] = "0"
    outdf["Premier"] = "N"
    outdf["IsLive"] = "N"
    outdf["AudioLang"] = outdf['AudioLang'].str.split('; ').apply(lambda audioLangs: [dc.mapping.audioLangMapping[audioLang.lower()] for audioLang in audioLangs]).str.join(', ')
    outdf["SubGenre"] = outdf["SubGenre"][outdf["SubGenre"].notnull()].str.lower().map(dc.mapping.subGenreMapping).str.join("/").fillna(outdf["SubGenre"])
    outdf["SponsorTextStuntChi"].fillna(outdf["SponsorTextStuntEng"], inplace=True)
    outdf["SponsorTextStuntEng"].fillna(outdf["SponsorTextStuntChi"], inplace=True)
    outdf["BrandNameChi"].fillna(outdf["BrandNameEng"], inplace=True)
    outdf["BrandNameEng"].fillna(outdf["BrandNameChi"], inplace=True)
    outdf["SeasonNameChi"].fillna(outdf["SeasonNameEng"], inplace=True)
    outdf["SeasonNameEng"].fillna(outdf["SeasonNameChi"], inplace=True)
    outdf["EpisodeNameChi"].fillna(outdf["EpisodeNameEng"], inplace=True)
    outdf["EpisodeNameEng"].fillna(outdf["EpisodeNameChi"], inplace=True)
    outdf["EpisodeNameChi"].fillna(outdf["EpisodeNameEng"], inplace=True)
    outdf["EpisodeNameEng"].fillna(outdf["EpisodeNameChi"], inplace=True)
    outdf["SynopsisChi"].fillna(outdf["SynopsisEng"], inplace=True)
    outdf["SynopsisEng"].fillna(outdf["SynopsisChi"], inplace=True)
    outdf["ShortSynopsisChi"].fillna(outdf["ShortSynopsisEng"], inplace=True)
    outdf["ShortSynopsisEng"].fillna(outdf["ShortSynopsisChi"], inplace=True)
    outdf["SeasonNo"] = outdf["SeasonNo"][outdf["SeasonNo"].notnull()].astype(int).astype(str)
    outdf["EpisodeNo"] = outdf["EpisodeNo"][outdf["EpisodeNo"].notnull()].astype(int).astype(str)
    outdf["FirstReleaseYear"] = outdf["FirstReleaseYear"][outdf["FirstReleaseYear"].notnull()].astype(int).astype(str)
    text_column = ["SponsorTextStuntEng", "SponsorTextStuntChi", "BrandNameEng", "BrandNameChi", "EditionVersionEng", "EditionVersionChi", "SeasonNameEng", "SeasonNameChi", "EpisodeNameEng", "EpisodeNameChi", "SynopsisEng", "SynopsisChi", "ShortSynopsisEng", "ShortSynopsisChi", "DirectorProducerEng", "DirectorProducerChi", "CastEng", "CastChi"]
    for column in text_column:
        outdf[column] = outdf[column].astype(str).where(outdf[column].notnull()).str.translate(dc.mapping.charTranslateTable)
    dc.utils.title_df(outdf)
    outdf[["FullTitleEng", "FullTitleChi"]] = outdf.apply(dc.utils.constructFullTitle, axis=1)
    outdf[["ProgrammeNameEng", "ProgrammeNameChi"]] = outdf.apply(dc.utils.constructProgrammeName, axis=1)
    dc.utils.translateChar(outdf)
    dc.utils.chopCharacters(outdf)


    # Append EOF
    dc.utils.appendEOF(outdf)


    # Fill Channel #
    outdf['OwnerChannel'] = owner_channel
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)

    # Write output file
    dc.io.to_io(outdf, out_io, out_format)

def convert_nonlinear(in_io, out_io, format="preV1506", owner_channel="HBO"):
    "Convert nonlinear for HBO"
    
    # Read input file
    indf = pd.read_excel(in_io, sheetname=0, keep_default_na=False)

    
    # Create output DataFrame
    outdf = pd.DataFrame(columns=dc.utils.extendnlInternal(indf.columns))

    # Extract data from full title


    # Fill output DataFrame with related info
    excepted_column = []
    dc.utils.fillOutdf(outdf,indf,excepted_column)
    excepted_column = ["AdvisorySlate", "TVClassification"]
    for internal_column in dc.utils.extendnlInternal(indf.columns):
        if (internal_column not in excepted_column):
            outdf[internal_column] = outdf[internal_column].apply(lambda x: None if isinstance(x, str) and x.lower().strip() in ["n/a", ""] else x)
    outdf["StartDate"] = outdf["StartDate"].apply(lambda x: re.match("^([0-9]+)(?:\.0+|[0-9]*)$", str(x)).groups()[0] if re.match("^[0-9]+(?:\.0+|[0-9]*)$", str(x)) else x.strftime("%Y%m%d") if isinstance(x, datetime.datetime) or isinstance(x, datetime.time) else x)
    outdf["EndDate"] = outdf["EndDate"].apply(lambda x: re.match("^([0-9]+)(?:\.0+|[0-9]*)$", str(x)).groups()[0] if re.match("^[0-9]+(?:\.0+|[0-9]*)$", str(x)) else x.strftime("%Y%m%d") if isinstance(x, datetime.datetime) or isinstance(x, datetime.time) else x)
    outdf["Duration"] = outdf["Duration"].astype(str)
    outdf["BrandNameEng"] = outdf["BrandNameEng"].astype(str).str.extract("^(.*?)\s*(S\d+)*\s*(\(Ep\.\s*\d+\))*$", expand=False)[0]
    outdf["BrandNameChi"] = outdf["BrandNameChi"].astype(str).str.extract("^(.*?)(\s*-\s*(第\d+季)*\s*(第\d+集)*)*$", expand=False)[0]
    outdf["SynopsisEng"] = outdf["SynopsisEng"].apply(lambda x: None if str(x).strip() == "" else x).fillna(outdf["SynopsisChi"])
    outdf["SynopsisChi"] = outdf["SynopsisChi"].apply(lambda x: None if str(x).strip() == "" else x).fillna(outdf["SynopsisEng"])
    outdf["ShortSynopsisEng"] = outdf[["ShortSynopsisEng","SynopsisEng"]].apply(lambda x:x["SynopsisEng"][0:dc.settings.char_limit.get("ShortSynopsisEng")] if pd.isnull(x["ShortSynopsisEng"]) or str(x["ShortSynopsisEng"]).strip() == "" else x["ShortSynopsisEng"] ,axis=1)
    outdf["ShortSynopsisChi"] = outdf[["ShortSynopsisChi","SynopsisChi"]].apply(lambda x:x["SynopsisChi"][0:dc.settings.char_limit.get("ShortSynopsisChi")] if pd.isnull(x["ShortSynopsisChi"]) or str(x["ShortSynopsisChi"]).strip() == "" else x["ShortSynopsisChi"] ,axis=1)
    outdf["DirectorProducerEng"] = outdf["DirectorProducerEng"][outdf["DirectorProducerEng"].notnull()].astype(str).str.split(",").apply(lambda x: [item.strip() for item in x]).str.join(', ')
    outdf["DirectorProducerChi"] = outdf["DirectorProducerChi"][outdf["DirectorProducerChi"].notnull()].astype(str).str.split("、").apply(lambda x: [item.strip() for item in x]).str.join(', ')
    outdf["DirectorProducerChi"] = outdf["DirectorProducerChi"][outdf["DirectorProducerChi"].notnull()].astype(str).str.split(",").apply(lambda x: [item.strip() for item in x]).str.join(', ')
    outdf["CastEng"] = outdf["CastEng"][outdf["CastEng"].notnull()].astype(str).str.split(",").apply(lambda x: [item.strip() for item in x]).str.join(', ')
    outdf["CastChi"] = outdf["CastChi"][outdf["CastChi"].notnull()].astype(str).str.split("、").apply(lambda x: [item.strip() for item in x]).str.join(', ')
    outdf["CastChi"] = outdf["CastChi"][outdf["CastChi"].notnull()].astype(str).str.split(",").apply(lambda x: [item.strip() for item in x]).str.join(', ')
    outdf["Genre"] = outdf["Genre"].astype(str).str.lower().map(dc.mapping.genreMapping)
    outdf["SubGenre"] = outdf["SubGenre"][outdf["SubGenre"].notnull()].astype(str).apply(lambda x: [i.capitalize() if i not in ['TV'] else i for i in re.split('([^a-zA-Z\'\.\-]+)', x)]).str.join("").astype(str).str.split("/").apply(lambda subGenres: sorted(set([subGenre.strip() for subGenre in subGenres]))).str.join("/").fillna(outdf["SubGenre"])
    outdf["SubGenre"] = outdf[["SubGenre","Genre"]].apply(lambda x: str(x["SubGenre"])+"/Western" if pd.notnull(x["SubGenre"]) and "Western" not in str(x["SubGenre"]) and x["Genre"] == "TV Series" else "Western" if (pd.isnull(x["SubGenre"]) or str(x["SubGenre"]).strip() == "") and x["Genre"] == "TV Series" else x,axis=1)
    outdf["OriginalLang"] = outdf["OriginalLang"].astype(str).str.lower().map(dc.mapping.audioLangMapping)
    outdf["AudioLang"] = outdf["AudioLang"].astype(str).str.lower().map(dc.mapping.audioLangMapping)
    outdf["SubtitleLang"] = outdf["SubtitleLang"].astype(str).str.lower().map(dc.mapping.subtitleMapping)
    outdf["RegionCode"] = outdf["RegionCode"].astype(str).str.lower().map(dc.mapping.regionCodeMapping)


    # Write output file
    dc.io.to_io_nonlinear(outdf, out_io, format)

