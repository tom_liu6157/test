import pandas as pd
import numpy as np
import datetime
import math
import csv
import re
import calendar
from collections import OrderedDict
import dc.utils
import dc.io
import dc.header
import dc.mapping
import dc.settings

def convert_linear(in_io, out_io, out_format="preV1506", owner_channel="STAR Cricket"):
    "Convert linear schedule for STAR Cricket"

    # Read input file
    indf = pd.read_excel(in_io, sheetname=0, skiprows=1, skip_footer=0)
    indf.dropna(how='all', inplace=True)
    duration = re.sub(".*(\d{2}:\d{2}).*", r"\1"+":00", indf["Brand name (English)"][indf["Brand name (English)"].last_valid_index()])
    indf.dropna(axis=0, subset=["TX Date"], inplace=True)

    
    # Create output DataFrame
    outdf = pd.DataFrame(columns=dc.header.internalHeader)

    # Extract data from full title


    # Fill output DataFrame with related info
    outdf["ChannelNo"] = indf["Channel #"]
    outdf["TXDate"] = indf["TX Date"].apply(lambda x: pd.to_datetime(x, format="%d/%m/%Y")).apply(lambda x:x.strftime("%Y%m%d"))
    outdf["ActualTime"] = indf["Actual Time"].astype('str').str.slice(-8).str.slice(0, 5).str.zfill(5)
    outdf["FullTitleEng"] = indf["Brand name (English)"][indf["Brand name (English)"].notnull()].astype('str').str.translate(dc.mapping.charTranslateTable).apply(lambda x : np.NaN if str(x).strip() == "" else x)
    outdf["FullTitleChi"].fillna(outdf["FullTitleEng"], inplace=True)
    outdf["BrandNameEng"].fillna(outdf["FullTitleEng"], inplace=True)
    outdf["BrandNameChi"].fillna(outdf["FullTitleChi"], inplace=True)
    outdf["EpisodeNo"] = indf["Episode no Max 5 digits"].apply(lambda x: None if x == "-" else re.match("^([0-9]+)(?:\.0+|[0-9]*)$", str(x)).groups()[0] if re.match("^[0-9]+(?:\.0+|[0-9]*)$", str(x)) else x)
    outdf["EpisodeNameEng"] = indf["Episode Name (English) (max 50 Chars)"][indf["Episode Name (English) (max 50 Chars)"].notnull()].astype(str).str.translate(dc.mapping.charTranslateTable).str.strip()
    outdf["EpisodeNameEng"].fillna(outdf["EpisodeNameChi"], inplace=True)
    outdf["EpisodeNameChi"].fillna(outdf["EpisodeNameEng"], inplace=True)
    outdf["PremierOld"] = "0"
    outdf["Premier"] = indf["Premiere (Y/N)"]
    outdf["IsLive"] = indf["Is Live (Y/N)"]
    outdf["Classification"] = indf["Classification"]
    outdf["Genre"] = indf["Genre"][indf["Genre"].notnull()].apply(lambda x: dc.mapping.genreMapping[x.lower()] if x in dc.mapping.genreMapping else x)
    outdf["SubGenre"] = indf["Sub-Genre"]
    outdf["IsEpisodic"] = np.where(np.logical_and(indf["Episodic (Y/N)"].notnull(),~indf["Episodic (Y/N)"].astype(str).str.match("^[\s]*$")), indf["Episodic (Y/N)"], "N")
    outdf["IsEpisodic"] = outdf[["IsEpisodic","SubGenre","EpisodeNo","EpisodeNameEng","EpisodeNameChi"]].apply(lambda x : "N" if "Match" in str(x["SubGenre"]) else "Y" if (pd.notnull(x["EpisodeNo"]) and str(x["EpisodeNo"]).strip() != "") or (pd.notnull(x["EpisodeNameEng"]) and str(x["EpisodeNameEng"]).strip() != "") or (pd.notnull(x["EpisodeNameChi"]) and str(x["EpisodeNameChi"]).strip() != "") else x["IsEpisodic"],axis=1)
    outdf["PortraitImage"] = indf["Portrait Image"]
    outdf["ImageWithTitle"] = indf["Image with title  (Y/N)"]
    outdf["Recordable"] = indf["Recordable"]
    outdf["IsNPVRProg"] = indf["Is NPVR Prog"]
    outdf["IsRestartTV"] = indf["Is Restart TV"]
    outdf['OwnerChannel'] = "STAR Cricket"
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)
    dc.utils.title_df(outdf)
    outdf[["FullTitleEng", "FullTitleChi"]] = outdf.apply(dc.utils.constructFullTitle, axis=1)
    outdf[["ProgrammeNameEng", "ProgrammeNameChi"]] = outdf.apply(dc.utils.constructProgrammeName, axis=1)
    dc.utils.translateChar(outdf)
    dc.utils.chopCharacters(outdf)


    # Append EOF
    lastProgDuration = pd.Timedelta(duration)
    dc.utils.appendEOF(outdf, lastprog_duration=lastProgDuration)


    # Fill Channel #
    outdf['OwnerChannel'] = owner_channel
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)

    # Write output file
    dc.io.to_io(outdf, out_io, out_format)

def convert_nonlinear(in_io, out_io, format="preV1506", owner_channel="STAR Cricket"):
    "Convert nonlinear for STAR Cricket"
    return None

