import pandas as pd
import numpy as np
import datetime
import math
import csv
import re
import calendar
from collections import OrderedDict
import dc.utils
import dc.io
import dc.header
import dc.mapping
import dc.settings

def convert_linear(in_io, out_io, out_format="preV1506", owner_channel="COLORS"):
    "Convert linear schedule for COLORS"

    # Read input file
    indf = pd.read_excel(in_io)
    indf.dropna(how='all', inplace=True)

    
    # Create output DataFrame
    outdf = pd.DataFrame(columns=dc.header.internalHeader)

    # Extract data from full title


    # Fill output DataFrame with related info
    outdf["TXDate"] = indf["TX Date"].apply(lambda x: pd.to_datetime(x, format="%d %b, %Y")).apply(lambda x:x.strftime("%Y%m%d"))
    outdf["ActualTime"] = indf["Actual Start Time"].apply(lambda x: pd.to_datetime(x, format="%H:%M:%S")).apply(lambda x:x.strftime("%H:%M"))
    outdf["FullTitleEng"] = indf["Full Title \n(English) "][indf["Full Title \n(English) "].notnull()].str.translate(dc.mapping.charTranslateTable)
    outdf["FullTitleChi"].fillna(outdf["FullTitleEng"], inplace=True)
    outdf["BrandNameEng"].fillna(outdf["FullTitleEng"], inplace=True)
    outdf["BrandNameChi"].fillna(outdf["FullTitleChi"], inplace=True)
    outdf["EpisodeNo"] = indf["Program Episode Number"][indf["Program Episode Number"].notnull()].astype(int).astype(str)
    outdf["SynopsisEng"] = indf["Program EPG Text"][indf["Program EPG Text"].notnull()].str.translate(dc.mapping.charTranslateTable)
    outdf["SynopsisChi"].fillna(outdf["SynopsisEng"], inplace=True)
    outdf["ShortSynopsisEng"] = outdf["SynopsisEng"][outdf["SynopsisEng"].notnull()].apply(lambda x: x[0:dc.settings.char_limit.get("ShortSynopsisEng")])
    outdf["ShortSynopsisChi"] = outdf["SynopsisChi"][outdf["SynopsisChi"].notnull()].apply(lambda x: x[0:dc.settings.char_limit.get("ShortSynopsisChi")])
    outdf["PremierOld"] = "0"
    outdf["Premier"] = "N"
    outdf["IsLive"] = "N"
    outdf["IsEpisodic"] = (outdf["EpisodeNo"].notnull() | outdf["EpisodeNameEng"].notnull()).map({True:"Y", False:"N"})
    dc.utils.title_df(outdf)
    outdf[["ProgrammeNameEng", "ProgrammeNameChi"]] = outdf.apply(dc.utils.constructProgrammeName, axis=1)
    dc.utils.translateChar(outdf)
    dc.utils.chopCharacters(outdf)


    # Append EOF
    lastProgDuration = outdf["TXDate"].last_valid_index() + np.timedelta64(1,'D') - outdf["TXDate"].last_valid_index() - outdf["ActualTime"].last_valid_index()# default end of program time is 23:59
    lastProgDateTime = outdf["TXDate"].last_valid_index() + outdf["ActualTime"].last_valid_index()
    lastProgram = outdf[outdf["BrandNameEng"] == outdf["BrandNameEng"][outdf["BrandNameEng"].last_valid_index()]]
    if len(lastProgram.index)>1: # last program has been played in previous timeslot
        firstProgIndex = lastProgram.index.values[0]
        lastProgDuration = pd.to_datetime((outdf["TXDate"][firstProgIndex+1] + ' ' + outdf["ActualTime"][firstProgIndex+1]), format="%Y%m%d %H:%M") - pd.to_datetime((outdf["TXDate"][firstProgIndex] + ' ' + outdf["ActualTime"][firstProgIndex]), format="%Y%m%d %H:%M")
    dc.utils.appendEOF(outdf, lastprog_duration=lastProgDuration)


    # Fill Channel #
    outdf['OwnerChannel'] = owner_channel
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)

    # Write output file
    dc.io.to_io(outdf, out_io, out_format)

def convert_nonlinear(in_io, out_io, format="preV1506", owner_channel="COLORS"):
    "Convert nonlinear for COLORS"
    return None

