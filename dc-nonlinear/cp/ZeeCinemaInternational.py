import pandas as pd
import numpy as np
import datetime
import math
import csv
import re
import calendar
from collections import OrderedDict
import dc.utils
import dc.io
import dc.header
import dc.mapping
import dc.settings

def convert_linear(in_io, out_io, out_format="preV1506", owner_channel="Zee Cinema International"):
    "Convert linear schedule for Zee Cinema International"

    # Read input file
    indf = pd.read_excel(in_io, sheetname=0)

    
    # Create output DataFrame
    outdf = pd.DataFrame(columns=dc.header.internalHeader)

    # Extract data from full title


    # Fill output DataFrame with related info
    start_hour=4
    indf["Airing Date"]=indf["Airing Date"][indf["Airing Date"].notnull()].str.replace(" ","/")
    indf["Airing Date"]=indf["Airing Date"][indf["Airing Date"].notnull()].str.replace(",","")
    indf["Airing Date"]=indf["Airing Date"][indf["Airing Date"].notnull()].str.lower()
    monthMapping={"january": "1","jan": "1","february":"2","feb":"2","march":"3","mar":"3","april":"4","apr":"4","may":"5","june":"6","jun":"6","july":"7","jul":"7","august":"8","aug":"8","september":"9","sep":"9","october":"10","oct":"10","november":"11", "nov":"11", "december":"12","dec":"12"}
    month=["january","jan","february","feb","march","mar","april","apr","may","june","jun","july","jul","august","aug","september","sep","october","oct","november", "nov", "december","dec"]
    for i in month:
        indf["Airing Date"]=indf["Airing Date"][indf["Airing Date"].notnull()].str.replace(i,monthMapping[i])
    outdf["TXDate"] = indf["Airing Date"].apply(lambda x: pd.to_datetime(x, format="%d/%m/%Y").strftime("%Y%m%d"))
    outdf["ActualTime"] = indf["Airing Time"].apply(lambda x: pd.to_datetime(x, format="%H:%M:%S")).apply(lambda x:x.strftime("%H:%M"))
    outdf["BrandNameEng"] = indf["Program Title"]
    outdf["BrandNameEng"] = outdf["BrandNameEng"].apply(lambda x:re.sub("\s*(?i)(Season|season)\.?\s*\d+.*", "", x))
    outdf["BrandNameEng"] = outdf["BrandNameEng"].apply(lambda x:re.sub("\s*\(\w+\).*", "", x))
    outdf["BrandNameChi"].fillna(outdf["BrandNameEng"], inplace=True)
    outdf["SeasonNo"] = indf["Program Episode Number"]
    outdf["Genre"] = indf["Program Category"][indf["Program Category"].notnull()].str.lower().map(dc.mapping.genreMapping).fillna(indf["Program Category"])
    outdf["SubGenre"] = indf["Program Sub Category"][indf["Program Sub Category"].notnull()].str.lower().map(dc.mapping.genreMapping).fillna(indf["Program Sub Category"])
    outdf["SynopsisEng"] = indf["Program Description"][indf["Program Description"].notnull()].str.translate(dc.mapping.charTranslateTable).str.strip()
    outdf["SynopsisChi"].fillna(outdf["SynopsisEng"], inplace=True)
    outdf["ShortSynopsisEng"].fillna(outdf["SynopsisEng"], inplace=True)
    outdf["ShortSynopsisChi"].fillna(outdf["SynopsisEng"], inplace=True)
    outdf["OriginalLang"] = indf["Program Language"][indf["Program Language"].notnull()].str.lower().str.strip().map(dc.mapping.audioLangMapping).fillna(indf["Program Language"])
    dc.utils.title_df(outdf)
    outdf[["FullTitleEng", "FullTitleChi"]] = outdf.apply(dc.utils.constructFullTitle, axis=1)
    outdf[["ProgrammeNameEng", "ProgrammeNameChi"]] = outdf.apply(dc.utils.constructProgrammeName, axis=1)
    dc.utils.translateChar(outdf)
    dc.utils.chopCharacters(outdf)


    # Append EOF
    dc.utils.appendEOF(outdf)


    # Fill Channel #
    outdf['OwnerChannel'] = owner_channel
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)

    # Write output file
    dc.io.to_io(outdf, out_io, out_format)

def convert_nonlinear(in_io, out_io, format="preV1506", owner_channel="Zee Cinema International"):
    "Convert nonlinear for Zee Cinema International"
    return None

