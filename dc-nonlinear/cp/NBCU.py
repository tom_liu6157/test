import pandas as pd
import numpy as np
import datetime
import math
import csv
import re
import calendar
from collections import OrderedDict
import dc.utils
import dc.io
import dc.header
import dc.mapping
import dc.settings

def convert_linear(in_io, out_io, out_format="preV1506", owner_channel="NBCU"):
    "Convert linear schedule for NBCU"
    return None

def convert_nonlinear(in_io, out_io, format="preV1506", owner_channel="NBCU"):
    "Convert nonlinear for NBCU"
    
    # Read input file
    indf = pd.read_excel(in_io, sheetname=0, keep_default_na=False)

    
    # Create output DataFrame
    outdf = pd.DataFrame(columns=dc.utils.extendnlInternal(indf.columns))

    # Extract data from full title


    # Fill output DataFrame with related info
    excepted_column = []
    dc.utils.fillOutdf(outdf,indf,excepted_column)
    excepted_column = ["AdvisorySlate", "TVClassification"]
    for internal_column in dc.utils.extendnlInternal(indf.columns):
        if (internal_column not in excepted_column):
            outdf[internal_column] = outdf[internal_column].apply(lambda x: None if isinstance(x, str) and x.lower().strip() in ["n/a", ""] else x)
    outdf["StartDate"] = outdf["StartDate"].apply(lambda x: re.match("^([0-9]+)(?:\.0+|[0-9]*)$", str(x)).groups()[0] if re.match("^[0-9]+(?:\.0+|[0-9]*)$", str(x)) else x.strftime("%Y%m%d") if isinstance(x, datetime.datetime) or isinstance(x, datetime.time) else x)
    outdf["EndDate"] = outdf["EndDate"].apply(lambda x: re.match("^([0-9]+)(?:\.0+|[0-9]*)$", str(x)).groups()[0] if re.match("^[0-9]+(?:\.0+|[0-9]*)$", str(x)) else x.strftime("%Y%m%d") if isinstance(x, datetime.datetime) or isinstance(x, datetime.time) else x)
    outdf["Duration"] = outdf["Duration"].astype(str)
    outdf["BrandNameEng"] = outdf["BrandNameEng"].astype(str).str.extract("^(.*?)\s*(S\d+)*\s*(\(Ep\.\s*\d+\))*$", expand=False)[0]
    outdf["BrandNameChi"] = outdf["BrandNameChi"].astype(str).str.extract("^(.*?)(\s*-\s*(第\d+季)*\s*(第\d+集)*)*$", expand=False)[0]
    outdf["DirectorProducerEng"] = outdf["DirectorProducerEng"][outdf["DirectorProducerEng"].notnull()].astype(str).str.split(",").apply(lambda x: [item.strip() for item in x]).str.join(', ')
    outdf["DirectorProducerChi"] = outdf["DirectorProducerChi"][outdf["DirectorProducerChi"].notnull()].astype(str).str.split("、").apply(lambda x: [item.strip() for item in x]).str.join(', ')
    outdf["DirectorProducerChi"] = outdf["DirectorProducerChi"][outdf["DirectorProducerChi"].notnull()].astype(str).str.split(",").apply(lambda x: [item.strip() for item in x]).str.join(', ')
    outdf["CastEng"] = outdf["CastEng"][outdf["CastEng"].notnull()].astype(str).str.split(",").apply(lambda x: [item.strip() for item in x]).str.join(', ')
    outdf["CastChi"] = outdf["CastChi"][outdf["CastChi"].notnull()].astype(str).str.split("、").apply(lambda x: [item.strip() for item in x]).str.join(', ')
    outdf["CastChi"] = outdf["CastChi"][outdf["CastChi"].notnull()].astype(str).str.split(",").apply(lambda x: [item.strip() for item in x]).str.join(', ')
    outdf["Genre"] = outdf["Genre"].astype(str).str.lower().map(dc.mapping.genreMapping)
    outdf["SubGenre"] = outdf["SubGenre"][outdf["SubGenre"].notnull()].astype(str).str.split("/").apply(lambda subGenres: sorted(set([subGenre.strip() for subGenre in subGenres]))).str.join("/").fillna(outdf["SubGenre"])
    outdf["SubGenre"] = outdf[["SubGenre","Genre"]].apply(lambda x: str(x["SubGenre"])+"/Western" if pd.notnull(x["SubGenre"]) and "Western" not in str(x["SubGenre"]) and x["Genre"] == "TV Series" else "Western" if (pd.isnull(x["SubGenre"]) or str(x["SubGenre"]).strip() == "") and x["Genre"] == "TV Series" else x,axis=1)
    outdf["OriginalLang"] = outdf["OriginalLang"].astype(str).str.lower().map(dc.mapping.audioLangMapping)
    outdf["AudioLang"] = outdf["AudioLang"].astype(str).str.lower().map(dc.mapping.audioLangMapping)
    outdf["SubtitleLang"] = outdf["SubtitleLang"].astype(str).str.lower().map(dc.mapping.subtitleMapping)
    outdf["RegionCode"] = outdf["RegionCode"].astype(str).str.lower().map(dc.mapping.regionCodeMapping)


    # Write output file
    dc.io.to_io_nonlinear(outdf, out_io, format)

