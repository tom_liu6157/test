import pandas as pd
import numpy as np
import datetime
import math
import csv
import re
import calendar
from collections import OrderedDict
import dc.utils
import dc.io
import dc.header
import dc.mapping
import dc.settings

def convert_linear(in_io, out_io, out_format="preV1506", owner_channel="Beijing TV"):
    "Convert linear schedule for Beijing TV"

    # Read input file
    indf = pd.read_excel(in_io, sheetname=0, skiprows=0, skip_footer=0)

    
    # Create output DataFrame
    outdf = pd.DataFrame(columns=dc.header.internalHeader)

    # Extract data from full title
    fullTitleChi = indf["节目名称"][indf["节目名称"].notnull()].apply(lambda x: dc.utils.jianfan(x)).apply(lambda x: str(x).strip())
    extractedInfo = pd.DataFrame(columns=["SponsorTextEng", "SponsorTextChi", "BrandNameEng", "BrandNameChi", "EditionVersionEng", "EditionVersionChi", "SeasonNo", "SeasonNameEng", "SeasonNameChi", "EpisodeNo", "EpisodeNameEng", "EpisodeNameChi", "RegionCode", "FirstReleaseYear", "isLive"])
    tempExtractedInfo = [
        fullTitleChi.str.extract("^☆*★*(重播)*(《(?P<SponsorTextChi>.*)》)*\s*(\d+集劇：)*(?P<BrandNameChi>.*?)\s*(（(?P<EditionVersionChi>.*版)）)*\s*(（(?P<isLive>直播)）)*\s*(（(?P<EpisodeNo>\d+)）)*\s*(（(?P<EpisodeNameChi>\d+-\d+-\d+)）(重播)*)*$", expand=False)
    ]
    extractedInfo["SponsorTextChi"] = tempExtractedInfo[0]["SponsorTextChi"]
    extractedInfo["BrandNameChi"] = tempExtractedInfo[0]["BrandNameChi"]
    extractedInfo["EditionVersionChi"] = tempExtractedInfo[0]["EditionVersionChi"]
    extractedInfo["EpisodeNo"] = tempExtractedInfo[0]["EpisodeNo"]
    extractedInfo["EpisodeNameChi"] = tempExtractedInfo[0]["EpisodeNameChi"]
    extractedInfo["isLive"] = tempExtractedInfo[0]["isLive"].apply(lambda x: "Y" if x=="直播" else "N")


    # Fill output DataFrame with related info
    outdf["TXDate"] = indf["日期"].astype(int).astype(str)
    outdf["ActualTime"] = indf["24小时制"].apply(lambda x: (str(x)+":00") if len(str(x))==5 else str(x)).apply(lambda x: pd.to_datetime(x, format="%H:%M:%S")).apply(lambda x:x.strftime("%H:%M"))
    outdf["SponsorTextStuntChi"] = extractedInfo["SponsorTextChi"].str.translate(dc.mapping.charTranslateTable)
    outdf["SponsorTextStuntEng"].fillna(outdf["SponsorTextStuntChi"], inplace=True)
    outdf["BrandNameChi"] = extractedInfo["BrandNameChi"][extractedInfo["BrandNameChi"].notnull()].str.translate(dc.mapping.charTranslateTable).apply(lambda x: x[0:dc.settings.char_limit.get("BrandNameChi")])
    outdf["BrandNameEng"].fillna(outdf["BrandNameChi"], inplace=True)
    outdf["EditionVersionEng"] = extractedInfo["EditionVersionEng"].str.translate(dc.mapping.charTranslateTable)
    outdf["EditionVersionChi"] = extractedInfo["EditionVersionChi"].str.translate(dc.mapping.charTranslateTable)
    outdf["EditionVersionEng"].fillna(outdf["EditionVersionChi"], inplace=True)
    outdf["EditionVersionChi"].fillna(outdf["EditionVersionEng"], inplace=True)
    outdf["EpisodeNo"] = extractedInfo["EpisodeNo"]
    outdf["EpisodeNameChi"] = extractedInfo["EpisodeNameChi"][extractedInfo["EpisodeNameChi"].notnull()].str.translate(dc.mapping.charTranslateTable).apply(lambda x: x[0:dc.settings.char_limit.get("EpisodeNameChi")])
    outdf[["EpisodeNameChi"]] = outdf.apply(dc.utils.constructEpisodeNameChi, axis=1)
    outdf["EpisodeNameEng"].fillna(outdf["EpisodeNameChi"], inplace=True)
    outdf["PremierOld"] = "0"
    outdf["Premier"] = "N"
    outdf["IsLive"] = extractedInfo["isLive"]
    outdf["IsEpisodic"] = (outdf["EpisodeNo"].notnull() | outdf["EpisodeNameEng"].notnull()).map({True:"Y", False:"N"})
    dc.utils.title_df(outdf)
    outdf[["FullTitleEng", "FullTitleChi"]] = outdf.apply(dc.utils.constructFullTitle, axis=1)
    outdf[["ProgrammeNameEng", "ProgrammeNameChi"]] = outdf.apply(dc.utils.constructProgrammeName, axis=1)
    dc.utils.translateChar(outdf)
    dc.utils.chopCharacters(outdf)


    # Append EOF
    temp =str(indf["时长"][len(indf.index)-1])
    lastProgDuration = pd.Timedelta(temp)
    dc.utils.appendEOF(outdf, lastprog_duration=lastProgDuration)


    # Fill Channel #
    outdf['OwnerChannel'] = owner_channel
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)

    # Write output file
    dc.io.to_io(outdf, out_io, out_format)

def convert_nonlinear(in_io, out_io, format="preV1506", owner_channel="Beijing TV"):
    "Convert nonlinear for Beijing TV"
    return None

