import pandas as pd
import numpy as np
import datetime
import math
import csv
import re
import calendar
from collections import OrderedDict
import dc.utils
import dc.io
import dc.header
import dc.mapping
import dc.settings

def convert_linear(in_io, out_io, out_format="preV1506", owner_channel="Dim Sum TV"):
    "Convert linear schedule for Dim Sum TV"

    # Read input file
    indf = pd.read_excel(in_io, sheetname=0)
    indf.dropna(how="all", inplace=True) 

    
    # Create output DataFrame
    outdf = pd.DataFrame(columns=dc.header.internalHeader)

    # Extract data from full title
    indf["Full Title (English)"].fillna(indf["Full Title (Chinese)"], inplace = True)
    indf["Full Title (Chinese)"].fillna(indf["Full Title (English)"], inplace = True)
    fullTitleEng = indf["Full Title (English)"][indf["Full Title (English)"].notnull()].apply(lambda x: str(x).strip())
    fullTitleChi = indf["Full Title (Chinese)"][indf["Full Title (Chinese)"].notnull()].apply(lambda x: str(x).strip())
    extractedInfo = pd.DataFrame(columns=["BrandNameEng", "BrandNameChi", "SeasonNo", "EpisodeNo", "EpisodeNameEng", "EpisodeNameChi"])
    tempExtractedInfo = [
        fullTitleEng.str.extract("(?P<BrandNameEng>.*?)\s*S(?P<SeasonNo>\d)\s(?P<EpisodeNo>\d+)\\s*:*\s*(?P<EpisodeNameEng>.*)", expand=False),
        fullTitleEng.str.extract("(?P<BrandNameEng>.*?)\s*-\s*Ep\s*(?P<EpisodeNo>\d+)", expand=False),
        fullTitleChi.str.extract("(?P<BrandNameChi>.*?)\s*-\s*Ep\s*(?P<EpisodeNo>\d+)", expand=False)
    ]
    extractedInfo["BrandNameEng"] = tempExtractedInfo[0]["BrandNameEng"]
    extractedInfo["EpisodeNo"] = tempExtractedInfo[0]["EpisodeNo"]
    extractedInfo["EpisodeNameEng"] = tempExtractedInfo[0]["EpisodeNameEng"]
    extractedInfo["BrandNameEng"].fillna(tempExtractedInfo[1]["BrandNameEng"], inplace=True)
    extractedInfo["BrandNameEng"].fillna(fullTitleEng, inplace=True)
    extractedInfo["BrandNameChi"] = tempExtractedInfo[2]["BrandNameChi"]
    extractedInfo["BrandNameChi"].fillna(fullTitleChi, inplace=True)
    extractedInfo["EpisodeNo"].fillna(tempExtractedInfo[1]["EpisodeNo"], inplace=True)
    extractedInfo["EpisodeNo"] = fullTitleEng.str.extract('(\d+)', expand=False)
    extractedInfo["EpisodeNameEng"] = extractedInfo["EpisodeNameEng"].apply(lambda x: None if x == "" else x)


    # Fill output DataFrame with related info
    outdf["TXDate"] = indf["TX Date"].apply(lambda x: pd.to_datetime(x, format="%d-%m-%Y")).apply(lambda x:x.strftime("%Y%m%d"))
    outdf["ActualTime"] = indf["Actual Start Time"].astype('str').str.slice(-8).str.slice(0, 5).str.zfill(5)
    outdf["BrandNameEng"] = extractedInfo["BrandNameEng"].str.translate(dc.mapping.charTranslateTable)
    outdf["BrandNameChi"] = extractedInfo["BrandNameChi"].str.translate(dc.mapping.charTranslateTable)
    outdf["BrandNameChi"].fillna(outdf["BrandNameEng"], inplace=True)
    outdf["BrandNameEng"].fillna(outdf["BrandNameChi"], inplace=True)
    outdf["EpisodeNo"] = extractedInfo["EpisodeNo"]
    outdf["EpisodeNameEng"] = extractedInfo["EpisodeNameEng"][extractedInfo["EpisodeNameEng"].notnull()].astype(str).str.translate(dc.mapping.charTranslateTable)
    outdf["SynopsisChi"] = indf["Programme Synopsis (Chinese)"].str.translate(dc.mapping.charTranslateTable)
    outdf["ShortSynopsisChi"] = outdf["SynopsisChi"][outdf["SynopsisChi"].notnull()].apply(lambda x: x[0:dc.settings.char_limit.get("ShortSynopsisChi")])
    outdf["SynopsisEng"] = outdf["SynopsisChi"]
    outdf["ShortSynopsisEng"] = outdf["ShortSynopsisChi"]
    outdf["PremierOld"] = "0"
    outdf["Premier"] = "N"
    outdf["IsLive"] = "N"
    outdf["Classification"] = indf["Classification"]
    outdf["IsEpisodic"] = (outdf["EpisodeNo"].notnull() | outdf["EpisodeNameEng"].notnull() | outdf["EpisodeNameChi"].notnull()).map({True:"Y", False:"N"})
    dc.utils.title_df(outdf)
    outdf[["FullTitleEng", "FullTitleChi"]] = outdf.apply(dc.utils.constructFullTitle, axis=1)
    outdf[["ProgrammeNameEng", "ProgrammeNameChi"]] = outdf.apply(dc.utils.constructProgrammeName, axis=1)
    dc.utils.translateChar(outdf)
    dc.utils.chopCharacters(outdf)


    # Append EOF
    dc.utils.appendEOF(outdf)


    # Fill Channel #
    outdf['OwnerChannel'] = owner_channel
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)

    # Write output file
    dc.io.to_io(outdf, out_io, out_format)

def convert_nonlinear(in_io, out_io, format="preV1506", owner_channel="Dim Sum TV"):
    "Convert nonlinear for Dim Sum TV"
    return None

