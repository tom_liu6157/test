import pandas as pd
import numpy as np
import datetime
import math
import csv
import re
import calendar
from collections import OrderedDict
import dc.utils
import dc.io
import dc.header
import dc.mapping
import dc.settings

def convert_linear(in_io, out_io, out_format="preV1506", owner_channel="Toonami"):
    "Convert linear schedule for Toonami"

    # Read input file
    indf = pd.read_excel(in_io, sheetname=0, skiprows=0)

    
    # Create output DataFrame
    outdf = pd.DataFrame(columns=dc.header.internalHeader)

    # Extract data from full title
    firstEpisodeTitle = indf["EPISODE_TITLE"][indf["EPISODE_TITLE"].notnull()].apply(lambda x: str(x).strip().split("/"))
    firstEpisodeTitle = firstEpisodeTitle.apply(lambda x: x[0])
    programmeTitle = indf["PROGRAMME_TITLE"][indf["PROGRAMME_TITLE"].notnull()]
    extractedInfo = pd.DataFrame(columns=["EpisodeNo", "EpisodeNameEng", "EditionVersionEng", "BrandNameEng", "SeasonNo"])
    tempExtractedInfo = [
        firstEpisodeTitle.str.extract("Episode Number (?P<EpisodeNo>\d+)", expand=False)
        ,firstEpisodeTitle.str.extract("(?P<EpisodeNameEng>.*)\s?Eps.#(?P<EpisodeNo>\d+)", expand=False)
        ,programmeTitle.str.extract("Thundercats\s?(?P<EditionVersionEng>.+)", expand=False)
        ,programmeTitle.str.extract("(?P<BrandNameEng>.*?)\s*Season\s*(?P<SeasonNo>\d+).*$", expand=False)
    ]
    extractedInfo["BrandNameEng"] = tempExtractedInfo[3]["BrandNameEng"].fillna(programmeTitle)
    extractedInfo["SeasonNo"] = tempExtractedInfo[3]["SeasonNo"]
    extractedInfo["EditionVersionEng"] = tempExtractedInfo[2]
    extractedInfo["BrandNameEng"][extractedInfo["EditionVersionEng"].notnull()] = "Thundercats"
    extractedInfo["EpisodeNo"] = tempExtractedInfo[0]
    extractedInfo["EpisodeNo"].fillna(tempExtractedInfo[1]["EpisodeNo"], inplace=True)
    extractedInfo["EpisodeNameEng"] = indf["EPISODE_TITLE"][indf["EPISODE_TITLE"].notnull()].apply(lambda x: str(x).strip())
    extractedInfo["EpisodeNameEng"] = extractedInfo["EpisodeNameEng"].str.replace("(Episode Number \d+|\s?Eps.#\d+\s?)", "").str.replace("/", " / ")
    extractedInfo["EpisodeNo"] = extractedInfo["EpisodeNo"][indf["EPISODE_NUMBER"].apply(lambda x: pd.notnull(x) and ((not isinstance(x, str)) or (isinstance(x, str) and "/" not in x)))]
    extractedInfo["EpisodeNameEng"] = extractedInfo["EpisodeNameEng"][indf["EPISODE_NUMBER"].apply(lambda x: pd.notnull(x) and ((not isinstance(x, str)) or (isinstance(x, str) and "/" not in x)))]


    # Fill output DataFrame with related info
    outdf["TXDate"] = indf["ONAIR_DATE_DISPLAY"].apply(lambda x: pd.to_datetime(x, format="%Y-%m-%d")).where(indf["ONAIR_TIME_DISPLAY"].apply(lambda x: int(pd.to_datetime(x, format="%H%M AEST").strftime("%H"))) <6).apply(lambda x: x + np.timedelta64(1,'D')).fillna(indf["ONAIR_DATE_DISPLAY"]).apply(lambda x: pd.to_datetime(x, format="%Y-%m-%d")).apply(lambda x:x.strftime("%Y%m%d"))
    outdf["ActualTime"] = indf["ONAIR_TIME_DISPLAY"].apply(lambda x: pd.to_datetime(x, format="%H%M AEST")).apply(lambda x:x.strftime("%H:%M"))
    outdf["BrandNameEng"]=extractedInfo["BrandNameEng"]
    outdf["BrandNameChi"]=outdf["BrandNameEng"]
    outdf["EditionVersionEng"]=extractedInfo["EditionVersionEng"] 
    outdf["EditionVersionChi"]=outdf["EditionVersionEng"]
    outdf["SeasonNo"] = extractedInfo["SeasonNo"]
    outdf["EpisodeNo"] = indf["EPISODE_NUMBER"][indf["EPISODE_NUMBER"].apply(lambda x: pd.notnull(x) and ((not isinstance(x, str)) or (isinstance(x, str) and "/" not in x)))].astype(int).astype(str)
    outdf["EpisodeNo"].fillna(extractedInfo["EpisodeNo"][extractedInfo["EpisodeNo"].apply(lambda x: pd.notnull(x) and ((not isinstance(x, str)) or (isinstance(x, str) and "/" not in x)))].astype(int).astype(str), inplace=True)
    outdf["EpisodeNameEng"] = extractedInfo["EpisodeNameEng"][indf["EPISODE_NUMBER"].apply(lambda x: pd.notnull(x) and ((not isinstance(x, str)) or (isinstance(x, str) and "/" not in x)))].str.translate(dc.mapping.charTranslateTable)
    outdf["EpisodeNameEng"] = outdf["EpisodeNameEng"][outdf["BrandNameEng"].str.lower() != outdf["EpisodeNameEng"].str.lower()]
    outdf["EpisodeNameChi"] = outdf["EpisodeNameEng"]
    outdf["SynopsisEng"] = indf["WEBSITE_SYNOPSIS"].str.translate(dc.mapping.charTranslateTable)
    outdf["SynopsisChi"] = outdf["SynopsisEng"]
    outdf["ShortSynopsisEng"] = indf["DIGITAL_EPG_SYNOPSIS"][indf["DIGITAL_EPG_SYNOPSIS"].notnull()].str.translate(dc.mapping.charTranslateTable).apply(lambda x: x[0:dc.settings.char_limit.get("ShortSynopsisEng")])
    outdf["ShortSynopsisChi"] = outdf["ShortSynopsisEng"]
    outdf["PremierOld"] = "0"
    outdf["Premier"] = "N"
    outdf["IsLive"] = "N"
    outdf["Classification"] = indf["CLASSIFICATION"]
    outdf["Genre"] = "Kids"
    outdf["SubGenre"] = "Animation"
    outdf["IsEpisodic"] = (outdf["EpisodeNo"].notnull() | outdf["EpisodeNameEng"].notnull()).map({True:"Y", False:"N"})
    dc.utils.title_df(outdf)
    outdf[["FullTitleEng", "FullTitleChi"]] = outdf.apply(dc.utils.constructFullTitle, axis=1)
    outdf[["ProgrammeNameEng", "ProgrammeNameChi"]] = outdf.apply(dc.utils.constructProgrammeName, axis=1)
    dc.utils.translateChar(outdf)
    dc.utils.chopCharacters(outdf)


    # Append EOF
    lastProgDurationInMinutes = indf["NOMINAL_LENGTH"][indf.last_valid_index()]
    lastProgDuration = pd.to_timedelta(lastProgDurationInMinutes, unit='m')
    dc.utils.appendEOF(outdf, lastprog_duration=lastProgDuration)


    # Fill Channel #
    outdf['OwnerChannel'] = owner_channel
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)

    # Write output file
    dc.io.to_io(outdf, out_io, out_format)

def convert_nonlinear(in_io, out_io, format="preV1506", owner_channel="Toonami"):
    "Convert nonlinear for Toonami"
    return None

