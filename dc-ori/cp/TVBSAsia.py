import pandas as pd
import numpy as np
import datetime
import math
import csv
import re
import calendar
from collections import OrderedDict
import dc.utils
import dc.io
import dc.header
import dc.mapping
import dc.settings

def convert_linear(in_io, out_io, out_format="preV1506", owner_channel="TVBS Asia"):
    "Convert linear schedule for TVBS Asia"

    # Read input file
    indf = pd.read_excel(in_io, sheetname=0)
    count = 0
    for name in indf[indf.columns[0]] :            
        if (pd.isnull(name) or name == indf.columns[0]) :
            for key in indf :
                del indf[key][count]
        count = count + 1

    
    # Create output DataFrame
    outdf = pd.DataFrame(columns=dc.header.internalHeader)

    # Extract data from full title


    # Fill output DataFrame with related info
    outdf["TXDate"] = indf[indf.columns[0]].apply(lambda x: re.match("^([0-9]+)(?:\.0+|[0-9]*)$", str(x)).groups()[0] if re.match("^[0-9]+(?:\.0+|[0-9]*)$", str(x)) else x.strftime("%Y%m%d") if isinstance(x, datetime.datetime) or isinstance(x, datetime.time) else x)
    outdf["ActualTime"] = indf[indf.columns[1]].astype('str').str.slice(-8).str.slice(0, 5).str.zfill(5)
    outdf["BrandNameChi"] = indf[indf.columns[2]]
    outdf["BrandNameEng"] = indf[indf.columns[3]]
    outdf["BrandNameChi"].fillna(outdf["BrandNameEng"],inplace=True)
    outdf["BrandNameEng"].fillna(outdf["BrandNameChi"],inplace=True)
    outdf["Genre"] = "Entertainment"
    outdf["SubGenre"] = "Variety"
    outdf['OwnerChannel'] = "TVBS Asia"
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)
    outdf[["Genre","SubGenre"]] = outdf.apply(dc.utils.genMapping, axis=1)
    dc.utils.title_df(outdf)
    outdf[["FullTitleEng", "FullTitleChi"]] = outdf.apply(dc.utils.constructFullTitle, axis=1)
    outdf[["ProgrammeNameEng", "ProgrammeNameChi"]] = outdf.apply(dc.utils.constructProgrammeName, axis=1)
    dc.utils.translateChar(outdf)
    dc.utils.chopCharacters(outdf)


    # Append EOF
    dc.utils.appendEOF(outdf)


    # Fill Channel #
    outdf['OwnerChannel'] = owner_channel
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)

    # Write output file
    dc.io.to_io(outdf, out_io, out_format)

def convert_nonlinear(in_io, out_io, format="preV1506", owner_channel="TVBS Asia"):
    "Convert nonlinear for TVBS Asia"
    return None

