import pandas as pd
import numpy as np
import datetime
import math
import csv
import re
import calendar
from collections import OrderedDict
import dc.utils
import dc.io
import dc.header
import dc.mapping
import dc.settings

def convert_linear(in_io, out_io, out_format="preV1506", owner_channel="Sony TV (India)"):
    "Convert linear schedule for Sony TV (India)"

    # Read input file
    indf = pd.read_excel(in_io, sheetname=0, na_values=["n/a"], keep_default_na=True)
    count = 0
    for name in indf[indf.columns[0]] :            
        if (pd.isnull(name) or str(name).strip() == "" or name == indf.columns[0]) :
            for key in indf :
                del indf[key][count]
        count = count + 1

    
    # Create output DataFrame
    outdf = pd.DataFrame(columns=dc.header.internalHeader)

    # Extract data from full title


    # Fill output DataFrame with related info
    outdf["ChannelNo"] = indf["Channel #"]
    outdf["TXDate"] = indf["TX Date"]
    outdf["ActualTime"] = indf["Actual Time"].astype('str').str.slice(-8).str.slice(0, 5).str.zfill(5)
    outdf["FullTitleEng"] = indf["Full Title (English) Max 40 Characters"]
    outdf["FullTitleChi"] = indf["Full Title (Chinese) Max 24 Characters"]
    outdf["SponsorTextStuntEng"] = indf["Sponsor text / Stunt (English)"]
    outdf["SponsorTextStuntChi"] = indf["Sponsor text / Stunt (Chinese)"]
    outdf["BrandNameEng"] = indf["Brand name (English)"]
    outdf["BrandNameChi"] = indf["Brand name (Chinese)"]
    if "Edition / Version (English)" in indf.columns:
        outdf["EditionVersionEng"] = indf["Edition / Version (English)"]
    if "Edition / Version (Chinese)" in indf.columns:
        outdf["EditionVersionChi"] = indf["Edition / Version (Chinese)"]
    if "Season no. (max 3 digits)" in indf.columns:    
        outdf["SeasonNo"] = indf["Season no. (max 3 digits)"]
    outdf["SeasonNameEng"] = indf["Season Name (English)"]
    outdf["SeasonNameChi"] = indf["Season Name (Chinese)"]
    outdf["EpisodeNo"] = indf["Episode no. (max 5 digits)"][indf["Episode no. (max 5 digits)"].notnull()].apply(lambda x: str(dc.utils.transferNumeric(x)))         
    outdf["EpisodeNameEng"] = indf["Episode name (English)"]
    outdf["EpisodeNameChi"] = indf["Episode name (Chinese)"]
    outdf["SynopsisEng"] = indf["Synopsis (English)"]
    outdf["SynopsisChi"] = indf["Synopsis (Chinese)"]
    outdf["ShortSynopsisEng"] = indf["Short Synopsis (English) (max 120 chars)"]
    outdf["ShortSynopsisChi"] = indf["Short Synopsis (Chinese) (max 120 chars)"]
    outdf["Premier"] = indf["Premiere (Y/N)"]
    outdf["IsLive"] = indf["Is Live (Y/N)"]
    outdf["Classification"] = indf["Classification"]
    outdf["Genre"] = indf["Genre"]
    outdf["SubGenre"] = indf["Sub-Genre"]
    outdf["DirectorProducerEng"] = indf["Director / Producer (English)"]
    outdf["DirectorProducerChi"] = indf["Director / Producer (Chinese)"]
    outdf["CastEng"] = indf["Cast (English)"]
    outdf["CastChi"] = indf["Cast (Chinese)"]
    outdf["OriginalLang"] = indf["Original language (max 50 chars)"]
    outdf["AudioLang"] = indf["Audio language"]
    outdf["SubtitleLang"] = indf["Subtitle language"]
    outdf["RegionCode"] = indf["Region Code (max 2 chars)"]
    outdf["FirstReleaseYear"] = indf["First release year (max 4 digits)"].apply(lambda x: str(int(re.match("^([0-9]+)(\.0*)?$", str(x)).groups()[0])) if re.match("^[0-9]+(\.0*)?$", str(x)) else x)
    outdf["IsVOD"] = "N"
    outdf["PortraitImage"] = indf["Portrait Image"]
    outdf["ImageWithTitle"] = indf["Image with title  (Y/N)"]
    outdf["RecordableForCatchUp"] = indf["Recordable for Catch-up (Y/N)"]
    outdf["CPInternalUniqueID"] = indf["CP Internal unique ID"]
    outdf["CPInternalSeriesID"] = indf["CP Internal Series ID"]
    cond1 = outdf["EpisodeNo"].apply(lambda x: re.match("^[\s]*$",str(x)) is None and pd.notnull(x)) 
    cond2 = outdf["EpisodeNameEng"].apply(lambda x: re.match("^[\s]*$",str(x)) is None and pd.notnull(x)) 
    cond3 = outdf["EpisodeNameChi"].apply(lambda x: re.match("^[\s]*$",str(x)) is None and pd.notnull(x))
    outdf["IsEpisodic"] = np.where(                                np.logical_or(cond1,                                np.logical_or(cond2,                                                cond3                                )),                               "Y", "N"                            )
    text_column = ["SponsorTextStuntEng", "SponsorTextStuntChi", "BrandNameEng", "BrandNameChi", "EditionVersionEng", "EditionVersionChi", "SeasonNameEng", "SeasonNameChi", "EpisodeNameEng", "EpisodeNameChi", "SynopsisEng", "SynopsisChi", "ShortSynopsisEng", "ShortSynopsisChi", "DirectorProducerEng", "DirectorProducerChi", "CastEng", "CastChi"]
    for column in text_column:
        outdf[column] = outdf[column].astype(str).where(outdf[column].notnull()).str.translate(dc.mapping.charTranslateTable)
    dc.utils.title_df(outdf)
    outdf[["FullTitleEng", "FullTitleChi"]] = outdf.apply(dc.utils.constructFullTitle, axis=1)
    outdf[["ProgrammeNameEng", "ProgrammeNameChi"]] = outdf.apply(dc.utils.constructProgrammeName, axis=1)
    dc.utils.translateChar(outdf)
    dc.utils.chopCharacters(outdf)
    outdf["TXDate"] = outdf["TXDate"][outdf["TXDate"].notnull()].astype(int).astype(str)
    outdf["ActualTime"] = outdf["ActualTime"].astype(str)


    # Append EOF
    dc.utils.appendEOF(outdf)


    # Fill Channel #
    outdf['OwnerChannel'] = owner_channel
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)

    # Write output file
    dc.io.to_io(outdf, out_io, out_format)

def convert_nonlinear(in_io, out_io, format="preV1506", owner_channel="Sony TV (India)"):
    "Convert nonlinear for Sony TV (India)"
    return None

