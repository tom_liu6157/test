import pandas as pd
import numpy as np
import datetime
import math
import csv
import re
import calendar
from collections import OrderedDict
import dc.utils
import dc.io
import dc.header
import dc.mapping
import dc.settings

def convert_linear(in_io, out_io, out_format="preV1506", owner_channel="RT"):
    "Convert linear schedule for RT"

    # Read input file
    indf = pd.read_excel(in_io, sheetname=0, skiprows=1)
    count = 0
    for name in indf[indf.columns[0]] :            
        if (pd.isnull(name) or name == indf.columns[0]) :
            for key in indf :
                del indf[key][count]
        count = count + 1
    premiere = indf.columns[9]
    synopsis = indf.columns[8]
    subgenre = indf.columns[5]
    for i in indf.columns:
        temp = str(i).split()
        for j in temp:
            if "Synopsis".lower() in str(j).lower():
                synopsis = str(i)
            if "Premiere".lower() in str(j).lower():
                premiere = str(i)
            if "Genre".lower() in str(j).lower():
                subgenre = str(i)

    
    # Create output DataFrame
    outdf = pd.DataFrame(columns=dc.header.internalHeader)

    # Extract data from full title


    # Fill output DataFrame with related info
    outdf["TXDate"] = indf[indf.columns[0]].apply(lambda x: pd.to_datetime(x, format="%Y-%m-%d").strftime("%Y%m%d"))
    outdf["ActualTime"] = indf[indf.columns[1]].astype('str').str.slice(-8).str.slice(0, 5).str.zfill(5)
    outdf["BrandNameEng"] = indf[indf.columns[3]]
    outdf["BrandNameChi"].fillna(outdf["BrandNameEng"],inplace=True)
    outdf["Genre"] = "News"
    outdf["SubGenre"] = indf[subgenre].apply(lambda x: dc.mapping.subGenreMapping.get(x.lower(), x) if pd.notnull(x) else x).apply(lambda y: "/".join(y)[0:dc.settings.char_limit.get("SubGenre")] if np.all(pd.notnull(y)) and not isinstance(y,str) else y).fillna(indf[subgenre])
    outdf["AudioLang"] = indf[indf.columns[6]]
    outdf["SubtitleLang"] = indf[indf.columns[7]]
    outdf["SynopsisEng"] = indf[synopsis]
    outdf["SynopsisChi"] = outdf["SynopsisEng"]
    outdf["ShortSynopsisEng"] = outdf["SynopsisEng"][outdf["SynopsisEng"].notnull()].apply(lambda x: x[0:dc.settings.char_limit.get("ShortSynopsisEng")])
    outdf["ShortSynopsisChi"] = outdf["SynopsisChi"][outdf["SynopsisChi"].notnull()].apply(lambda x: x[0:dc.settings.char_limit.get("ShortSynopsisChi")])
    outdf["Premier"] = indf[premiere]
    outdf["Classification"] = indf[indf.columns[10]]
    dc.utils.title_df(outdf)
    outdf[["FullTitleEng", "FullTitleChi"]] = outdf.apply(dc.utils.constructFullTitle, axis=1)
    outdf[["ProgrammeNameEng", "ProgrammeNameChi"]] = outdf.apply(dc.utils.constructProgrammeName, axis=1)
    dc.utils.translateChar(outdf)
    dc.utils.chopCharacters(outdf)


    # Append EOF
    dc.utils.appendEOF(outdf)


    # Fill Channel #
    outdf['OwnerChannel'] = owner_channel
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)

    # Write output file
    dc.io.to_io(outdf, out_io, out_format)

def convert_nonlinear(in_io, out_io, format="preV1506", owner_channel="RT"):
    "Convert nonlinear for RT"
    return None

