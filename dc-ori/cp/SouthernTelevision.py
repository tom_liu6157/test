import pandas as pd
import numpy as np
import datetime
import math
import csv
import re
import calendar
from collections import OrderedDict
import dc.utils
import dc.io
import dc.header
import dc.mapping
import dc.settings

def convert_linear(in_io, out_io, out_format="preV1506", owner_channel="Southern Television"):
    "Convert linear schedule for Southern Television"

    # Read input file
    from io import BytesIO
    names = ["Date", "ActualTime", "BrandNameEng", "SynopsisEng", "Genre", "SubGenre"]
    namesChi = ["Date", "ActualTime", "BrandNameChi", "Genre"]
    indfEng = pd.read_excel(in_io, sheetname=0, names=names)
    if type(in_io) == BytesIO:
        in_io.seek(0)
    indfChi = pd.read_excel(in_io, sheetname=1, names=namesChi)
    if type(in_io) == BytesIO:
        in_io.seek(0)
    if len(indfChi) == 0 or indfChi.dropna().empty:
        names = ["Date", "ActualTime", "BrandNameChi", "BrandNameEng", "Genre", "SynopsisEng"]
        indfEng = pd.read_excel(in_io, sheetname=0, names=names)

    
    # Create output DataFrame
    outdf = pd.DataFrame(columns=dc.header.internalHeader)

    # Extract data from full title
    fullTitleEng = indfEng["BrandNameEng"][indfEng["BrandNameEng"].notnull()].apply(lambda x: str(x).strip())
    if len(indfChi) == 0 or indfChi.dropna().empty:
        fullTitleChi = indfEng["BrandNameChi"][indfEng["BrandNameChi"].notnull()].apply(lambda x: str(x).strip())
    else:
        fullTitleChi = indfChi["BrandNameChi"][indfChi["BrandNameChi"].notnull()].apply(lambda x: str(x).strip())
    tempExtractedInfo = [
        indfEng["Genre"].str.extract("(?P<SubGenre>.*?)\s\(.*", expand=False),
        indfEng["Genre"].str.extract("(?P<SubGenre>.*?):", expand=False),
        indfEng["Genre"].str.extract("(?P<SubGenre>.*?)!", expand=False)
    ]
    global last_row
    last_row = None
    for index, row in indfEng.iterrows():
        if(pd.notnull(row.Date) and index > 0 and index < indfEng.last_valid_index() and pd.isnull(indfEng.loc[index+1, "Date"])):
            indfEng.loc[index - 1, "Date"] = row.Date
        last_row = row
    def getSubGerneName(subGenres):
        if(isinstance(subGenres, list)):
            for subGenre in subGenres:
                subGenre = dc.mapping.subGenreMapping.get(subGenre.lower(), [subGenre])
            subGenres = sorted(subGenres)
        return subGenres


    # Fill output DataFrame with related info
    lastVaildDate = pd.NaT
    def handleTXDate(date):
        global lastVaildDate
        if(pd.notnull(date) and not isinstance(date,str)):
            lastVaildDate = date.strftime("%Y%m%d")
            return lastVaildDate
        else:
            if(pd.isnull(lastVaildDate)):
                return date
            else:
                return lastVaildDate
    outdf["TXDate"] = indfEng["Date"].apply(lambda x: handleTXDate(x))
    outdf["ActualTime"] = indfEng["ActualTime"].astype('str').str.slice(-8).str.slice(0, 5).str.zfill(5)
    outdf["BrandNameEng"] = fullTitleEng.str.translate(dc.mapping.charTranslateTable).apply(lambda x: re.sub("(?i)[\(（]repeat[\)）].*$", "", x)).str.strip()
    outdf["BrandNameChi"]= fullTitleChi[fullTitleChi.notnull()].apply(lambda x: dc.utils.jianfan(x)).str.translate(dc.mapping.charTranslateTable).apply(lambda x: re.sub("\s*[\(（]\s*重播\s*[\)）].*$", "", x)).str.strip()
    outdf["BrandNameChi"].fillna(outdf["BrandNameEng"], inplace=True)
    outdf["BrandNameEng"].fillna(outdf["BrandNameChi"], inplace=True)
    outdf["SynopsisEng"] = indfEng["SynopsisEng"].apply(lambda x: str(x).strip()).str.replace('(\s*,\s*|\s*、\s*|\s*，\s*|\s*﹑\s*|\s*ʼ\s*)', ', ').str.translate(dc.mapping.charTranslateTable)
    outdf["SynopsisChi"].fillna(outdf["SynopsisEng"])
    outdf["ShortSynopsisEng"] = outdf["SynopsisEng"][outdf["SynopsisEng"].notnull()].apply(lambda x: x[0:dc.settings.char_limit.get("ShortSynopsisEng")])
    outdf["ShortSynopsisChi"] = outdf["SynopsisChi"][outdf["SynopsisChi"].notnull()].apply(lambda x: x[0:dc.settings.char_limit.get("ShortSynopsisChi")])
    outdf["PremierOld"] = "0"
    outdf["Premier"] = "N"
    outdf["IsLive"] = "N"
    outdf["Genre"] = "Infotainment"
    outdf["SubGenre"] = tempExtractedInfo[0]
    outdf["SubGenre"].fillna(tempExtractedInfo[1], inplace=True)
    outdf["SubGenre"].fillna(tempExtractedInfo[2], inplace=True)
    outdf["SubGenre"].fillna(indfEng["Genre"], inplace=True)
    outdf["SubGenre"] = outdf["SubGenre"].str.split("/").apply(lambda subGenres: getSubGerneName(subGenres)).str.join("/").fillna(outdf["SubGenre"]).apply(lambda x: dc.utils.jianfan(x[0:dc.settings.char_limit.get("SubGenre")]) if pd.notnull(x) else x)
    dc.utils.title_df(outdf)
    outdf[["FullTitleEng", "FullTitleChi"]] = outdf.apply(dc.utils.constructFullTitle, axis=1)
    outdf[["ProgrammeNameEng", "ProgrammeNameChi"]] = outdf.apply(dc.utils.constructProgrammeName, axis=1)
    dc.utils.translateChar(outdf)
    dc.utils.chopCharacters(outdf)


    # Append EOF
    dc.utils.appendEOF(outdf)


    # Fill Channel #
    outdf['OwnerChannel'] = owner_channel
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)

    # Write output file
    dc.io.to_io(outdf, out_io, out_format)

def convert_nonlinear(in_io, out_io, format="preV1506", owner_channel="Southern Television"):
    "Convert nonlinear for Southern Television"
    return None

