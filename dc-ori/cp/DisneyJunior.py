import pandas as pd
import numpy as np
import datetime
import math
import csv
import re
import calendar
from collections import OrderedDict
import dc.utils
import dc.io
import dc.header
import dc.mapping
import dc.settings

def convert_linear(in_io, out_io, out_format="preV1506", owner_channel="Disney Junior"):
    "Convert linear schedule for Disney Junior"

    # Read input file
    indf = pd.read_excel(in_io, sheetname=0, skiprows=0)
    indf.columns = ["".join(s.split()).replace("(", "").replace(")", "").replace("/", "").lower() for s in indf.columns]

    
    # Create output DataFrame
    outdf = pd.DataFrame(columns=dc.header.internalHeader)

    # Extract data from full title


    # Fill output DataFrame with related info
    excepted_column = ["PID", "Premier", "Recordable", "Is NPVR Prog", "Is Restart TV", "effective_date", "expiration_date", "Media ID (max. 12 chars)"]
    for internal_column, indf_column in dc.header.postV1506HeaderMapping.items():
        if (indf_column not in excepted_column): 
            edited_indf_column = "".join(indf_column.split()).replace("(", "").replace(")", "").replace("/", "").lower()
            if (edited_indf_column in indf.columns):
                outdf[internal_column] = indf[edited_indf_column]
                if (outdf[internal_column].dtype == object):
                    outdf[internal_column] = outdf[internal_column].where(outdf[internal_column].apply(lambda x: pd.notnull(x) and len(str(x).strip()) > 0))
    mask = outdf["ActualTime"].str.split(":").apply(lambda x: int(x[0])) > 23
    outdf["TXDate"] = outdf["TXDate"][mask].apply(lambda x: (pd.to_datetime(x, format="%Y%m%d") + pd.Timedelta(1,'D')).strftime("%Y%m%d")).combine_first(outdf["TXDate"]).apply(lambda x: str(x))
    outdf["ActualTime"] = outdf["ActualTime"][mask].str.split(":").apply(lambda x: "{0}:{1}".format(int(x[0])-24, x[1])).combine_first(outdf["ActualTime"]).str.zfill(5)
    outdf["EpisodeNameEng"] = outdf["EpisodeNameEng"].apply(lambda x: re.sub("^\s*\"*(.*?)\"*\s*$", "\\1", x) if pd.notnull(x) else None)
    outdf["EpisodeNameChi"] = outdf["EpisodeNameChi"].apply(lambda x: re.sub("^\s*\"*(.*?)\"*\s*$", "\\1", x) if pd.notnull(x) else None)
    outdf["EpisodeNameEng"] = outdf["EpisodeNameEng"][outdf["EpisodeNameEng"].notnull()].str.replace("\\", "/")
    outdf["EpisodeNameChi"] = outdf["EpisodeNameChi"][outdf["EpisodeNameChi"].notnull()].str.replace("\\", "/")
    dc.utils.fixArticle(outdf)
    outdf["EpisodeNameEng"] = outdf[["BrandNameEng", "EpisodeNameEng"]].apply(lambda x: None if pd.notnull(x["BrandNameEng"]) and pd.notnull(x["EpisodeNameEng"]) and x["BrandNameEng"].strip() == x["EpisodeNameEng"].strip() else x["EpisodeNameEng"], axis=1)
    outdf["EpisodeNameChi"] = outdf[["BrandNameChi", "EpisodeNameChi"]].apply(lambda x: None if pd.notnull(x["BrandNameChi"]) and pd.notnull(x["EpisodeNameChi"]) and x["BrandNameChi"].strip() == x["EpisodeNameChi"].strip() else x["EpisodeNameChi"], axis=1)
    outdf["SponsorTextStuntChi"].fillna(outdf["SponsorTextStuntEng"], inplace=True)
    outdf["SponsorTextStuntEng"].fillna(outdf["SponsorTextStuntChi"], inplace=True)
    outdf["BrandNameChi"].fillna(outdf["BrandNameEng"], inplace=True)
    outdf["BrandNameEng"].fillna(outdf["BrandNameChi"], inplace=True)
    outdf["SeasonNameChi"].fillna(outdf["SeasonNameEng"], inplace=True)
    outdf["SeasonNameEng"].fillna(outdf["SeasonNameChi"], inplace=True)
    outdf["EpisodeNameChi"].fillna(outdf["EpisodeNameEng"], inplace=True)
    outdf["EpisodeNameEng"].fillna(outdf["EpisodeNameChi"], inplace=True)
    outdf["EpisodeNameChi"].fillna(outdf["EpisodeNameEng"], inplace=True)
    outdf["EpisodeNameEng"].fillna(outdf["EpisodeNameChi"], inplace=True)
    outdf["SynopsisChi"].fillna(outdf["SynopsisEng"], inplace=True)
    outdf["SynopsisEng"].fillna(outdf["SynopsisChi"], inplace=True)
    outdf["ShortSynopsisChi"].fillna(outdf["ShortSynopsisEng"], inplace=True)
    outdf["ShortSynopsisEng"].fillna(outdf["ShortSynopsisChi"], inplace=True)
    outdf["TXDate"] = outdf["TXDate"][outdf["TXDate"].notnull()].astype(int).astype(str)
    outdf["SeasonNo"] = outdf["SeasonNo"][outdf["SeasonNo"].notnull()].astype(int).astype(str)
    outdf["EpisodeNo"] = outdf["EpisodeNo"][outdf["EpisodeNo"].notnull()].astype(int).astype(str)
    text_column = ["SponsorTextStuntEng", "SponsorTextStuntChi", "BrandNameEng", "BrandNameChi", "EditionVersionEng", "EditionVersionChi", "SeasonNameEng", "SeasonNameChi", "EpisodeNameEng", "EpisodeNameChi", "SynopsisEng", "SynopsisChi", "ShortSynopsisEng", "ShortSynopsisChi", "DirectorProducerEng", "DirectorProducerChi", "CastEng", "CastChi"]
    for column in text_column:
        outdf[column] = outdf[column].astype(str).where(outdf[column].notnull()).str.translate(dc.mapping.charTranslateTable)
    dc.utils.title_df(outdf)
    outdf[["FullTitleEng", "FullTitleChi"]] = outdf.apply(dc.utils.constructFullTitle, axis=1)
    outdf[["ProgrammeNameEng", "ProgrammeNameChi"]] = outdf.apply(dc.utils.constructProgrammeName, axis=1)
    dc.utils.translateChar(outdf)
    dc.utils.chopCharacters(outdf)


    # Append EOF
    dc.utils.appendEOF(outdf)


    # Fill Channel #
    outdf['OwnerChannel'] = owner_channel
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)

    # Write output file
    dc.io.to_io(outdf, out_io, out_format)

def convert_nonlinear(in_io, out_io, format="preV1506", owner_channel="Disney Junior"):
    "Convert nonlinear for Disney Junior"
    return None

