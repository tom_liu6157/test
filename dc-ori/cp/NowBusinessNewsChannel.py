import pandas as pd
import numpy as np
import datetime
import math
import csv
import re
import calendar
from collections import OrderedDict
import dc.utils
import dc.io
import dc.header
import dc.mapping
import dc.settings

def convert_linear(in_io, out_io, out_format="preV1506", owner_channel="Now Business News Channel"):
    "Convert linear schedule for Now Business News Channel"

    # Read input file
    import dc.TestReadingExcel
    indf = dc.TestReadingExcel.read_excel(in_io, sheetname=None)
    if type(in_io) != str:
        in_io.seek(0)
    sheetname = 0
    if 'EPG' in indf.keys():
        sheetname = list(indf.keys()).index('EPG')
    elif 'now news' in indf.keys():
        sheetname = list(indf.keys()).index('now news')
    elif 'NOW DIRECT' in indf.keys():
        sheetname = list(indf.keys()).index('NOW DIRECT')
    rowSize = 0
    isColumn = False
    for i in list(indf.items())[sheetname][1]:
        for j in i:
            if (j == 'Genre'):
                isColumn = True
                break
        if isColumn:
            break
        rowSize +=1
    indf = pd.read_excel(in_io, sheetname=sheetname, skiprows=rowSize)
    if len(indf.columns) <= 8:
        day = indf.columns[0]
        BrandNameEng = "Program Name (English)"
        BrandNameChi = "Program Name (Chinese)"
        SynopsisEng = "Program Details (English)"
        SynopsisChi = "Program Details (Chinese)"
        time = indf.columns[1]
        SubGenre = "Sub Genre"
    else:
        day = "TX Date"
        BrandNameEng = "Brand name (English)"
        BrandNameChi = "Brand name (Chinese)"
        SynopsisEng = "Synopsis (English)"
        SynopsisChi = "Synopsis (Chinese)"
        time = "Actual Time"
        SubGenre = "Sub-Genre"
    count = 0
    preDate = ""
    for date in indf[day] :
        if (pd.isnull(date)) :
            indf.loc[count, day] = preDate
        else :
            preDate = date 
        count = count + 1
    count = 0
    for name in indf[BrandNameEng] :            
        if (pd.isnull(name) or name == BrandNameEng or re.sub("\s","",str(name)).upper() == 'ENDOFFILE') :
            for key in indf :
                del indf[key][count]
        count = count + 1
    count = 0
    for name in indf[BrandNameChi] :            
        if re.sub("\s","",str(name)).upper() == 'ENDOFFILE' :
            for key in indf :
                del indf[key][count]
        count = count + 1
    Premier = ""
    IsLive = ""
    IsEpisodic = ""
    for i in indf.columns:
        if re.sub("\s","",str(i)).replace("(", "").replace(")", "").replace("/", "").replace("_","").replace("-","").replace(".","").lower() == "premiereyn":
                Premier = str(i)
        elif re.sub("\s","",str(i)).replace("(", "").replace(")", "").replace("/", "").replace("_","").replace("-","").replace(".","").lower() == "isliveyn":
                IsLive = str(i)
        elif re.sub("\s","",str(i)).replace("(", "").replace(")", "").replace("/", "").replace("_","").replace("-","").replace(".","").lower() == "episodicyn": 
                IsEpisodic = str(i)

    
    # Create output DataFrame
    outdf = pd.DataFrame(columns=dc.header.internalHeader)

    # Extract data from full title
    if owner_channel == "Now Business News Channel":
        extractedInfoEng = pd.DataFrame(columns=["SponsorTextStuntEng", "BrandNameEng", "EpisodeNo", "EditionVersionEng", "EpisodeNameEng"])
        extractedInfoChi = pd.DataFrame(columns=["SponsorTextStuntChi", "BrandNameChi", "EpisodeNo", "EditionVersionChi", "EpisodeNameChi"])
        fullTitleEng = indf[BrandNameEng].apply(lambda x: str(x).strip())
        fullTitleChi = indf[BrandNameChi].apply(lambda x: str(x).strip())
        tempExtractedInfoEng = [
            fullTitleEng.str.extract("((?P<SponsorTextStuntEng>.+[^\d]):)*", expand=False),
        ]
        tempExtractedInfoChi = [
            fullTitleChi.str.extract("((?P<SponsorTextStuntChi>.+[^\d])︰)*", expand=False),
        ]
        extractedInfoEng["SponsorTextStuntEng"] = tempExtractedInfoEng[0]["SponsorTextStuntEng"].apply(lambda x: None if x == "" else x)
        extractedInfoChi["SponsorTextStuntChi"] = tempExtractedInfoChi[0]["SponsorTextStuntChi"].apply(lambda x: None if x == "" else x)
        outdf["BrandNameEng"]=  indf[BrandNameEng].apply(lambda x:re.sub(".+[^\d]:\s*", "", x)).str.translate(dc.mapping.charTranslateTable)
        outdf["BrandNameChi"] = indf[BrandNameChi].apply(lambda x:re.sub(".+[^\d]︰\s*", "", x)).str.translate(dc.mapping.charTranslateTable)                         
        outdf["SponsorTextStuntEng"] = extractedInfoEng["SponsorTextStuntEng"]
        outdf["SponsorTextStuntChi"] =  extractedInfoChi["SponsorTextStuntChi"]
        outdf["SponsorTextStuntEng"]=outdf["SponsorTextStuntEng"][outdf["SponsorTextStuntEng"].notnull()].apply(lambda x:str(x).translate(dc.mapping.charTranslateTable))
        outdf["SponsorTextStuntChi"]=outdf["SponsorTextStuntChi"][outdf["SponsorTextStuntChi"].notnull()].apply(lambda x:str(x).translate(dc.mapping.charTranslateTable))
    elif owner_channel == "Now NEWS":
        outdf["BrandNameEng"]= indf[BrandNameEng].str.translate(dc.mapping.charTranslateTable)
        outdf["BrandNameChi"] = indf[BrandNameChi].str.translate(dc.mapping.charTranslateTable)
    else:
        extractedInfo = pd.DataFrame(columns=["SponsorTextStuntEng", "BrandNameEng", "EpisodeNo", "EditionVersionEng", "EpisodeNameEng"])
        fullTitleEng = indf[BrandNameEng].apply(lambda x: str(x).strip())
        tempExtractedInfoEng = [
            fullTitleEng.str.extract("(.*#\s*(?P<EpisodeNo>\d+))*", expand=False),
        ]
        extractedInfo["EpisodeNo"] = tempExtractedInfoEng[0]["EpisodeNo"].apply(lambda x: None if x == "" else x)
        outdf["BrandNameEng"]= indf[BrandNameEng].apply(lambda x:re.sub("\s*#.*", "", x))                           
        outdf["BrandNameEng"] = outdf["BrandNameEng"].str.translate(dc.mapping.charTranslateTable)
        outdf["EpisodeNo"] = extractedInfo["EpisodeNo"]
        outdf["BrandNameChi"] = indf[BrandNameChi].apply(lambda x:re.sub("\s*#.*", "", x))    


    # Fill output DataFrame with related info
    outdf["TXDate"] = indf[day].apply(lambda x: re.match("^([0-9]+)(\.0+)?$", str(x)).groups()[0] if re.match("^([0-9]+)(\.0+)?$", str(x)) else x.strftime("%Y%m%d") if isinstance(x, datetime.datetime) or isinstance(x, datetime.time) else x).str.translate({ord('\x03'):''})                      
    outdf["ActualTime"] = indf[time].astype('str').str.slice(-8).str.slice(0, 5).str.zfill(5)
    t0 = outdf["TXDate"][0]
    if IsEpisodic != "":
        outdf["IsEpisodic"] = indf[IsEpisodic]
    else:
        outdf["IsEpisodic"] = ((outdf["EpisodeNo"].notnull() & ~outdf["EpisodeNo"].astype(str).str.match("^[\s]*$"))
                           | (outdf["EpisodeNameEng"].notnull() & ~outdf["EpisodeNameEng"].astype(str).str.match("^[\s]*$"))
                           | (outdf["EpisodeNameChi"].notnull() & ~outdf["EpisodeNameChi"].astype(str).str.match("^[\s]*$"))).map({True:"Y", False:"N"})
    if Premier != "":
        outdf["Premier"] = indf[Premier]
    else:
        outdf["Premier"] = "N"
    if IsLive != "":
        outdf["IsLive"] = indf[IsLive]
    else:
        outdf["IsLive"] = "N"
    if owner_channel == "Now Business News Channel": 
        outdf[["EpisodeNameEng","EpisodeNameChi","Premier","IsLive","IsEpisodic"]] = outdf[["TXDate", "ActualTime","BrandNameEng","EpisodeNameEng","EpisodeNameChi","BrandNameChi","Premier","IsLive","IsEpisodic"]].apply(lambda x: 
                                                                                                                    pd.Series([x["TXDate"],x["TXDate"],"Y","N","Y"]) 
                                                                                                                    if (re.sub("\s","",str(x["BrandNameEng"])).translate(dc.mapping.VODSymbols).upper() == re.sub("\s","","Property Weekly").translate(dc.mapping.VODSymbols).upper() or re.sub("\s","",str(x["BrandNameChi"])) == re.sub("\s","","一周睇樓團").translate(dc.mapping.VODSymbols)) and 
                                                                                                                    (str(pd.to_datetime(x["TXDate"], format="%Y%m%d") - pd.to_datetime(t0, format="%Y%m%d")).split()[0] == "5") and 
                                                                                                                    (x["ActualTime"] == "09:30")
                                                                                                                    else
                                                                                                                    pd.Series([x["TXDate"],x["TXDate"],"N","N","Y"]) 
                                                                                                                    if (re.sub("\s","",str(x["BrandNameEng"])).translate(dc.mapping.VODSymbols).upper() == re.sub("\s","","Property Weekly").translate(dc.mapping.VODSymbols).upper() or re.sub("\s","",str(x["BrandNameChi"])) == re.sub("\s","","一周睇樓團").translate(dc.mapping.VODSymbols)) and 
                                                                                                                    (str(pd.to_datetime(x["TXDate"], format="%Y%m%d") - pd.to_datetime(t0, format="%Y%m%d")).split()[0] == "5") and 
                                                                                                                    (x["ActualTime"] == "17:30")
                                                                                                                    else
                                                                                                                    pd.Series([(pd.to_datetime(x["TXDate"], format="%Y%m%d") - np.timedelta64(1,'D')).strftime("%Y%m%d"),(pd.to_datetime(x["TXDate"], format="%Y%m%d") - np.timedelta64(1,'D')).strftime("%Y%m%d"),"N","N","Y"]) 
                                                                                                                    if (re.sub("\s","",str(x["BrandNameEng"])).translate(dc.mapping.VODSymbols).upper() == re.sub("\s","","Property Weekly").translate(dc.mapping.VODSymbols).upper() or re.sub("\s","",str(x["BrandNameChi"])) == re.sub("\s","","一周睇樓團").translate(dc.mapping.VODSymbols)) and 
                                                                                                                    (str(pd.to_datetime(x["TXDate"], format="%Y%m%d") - pd.to_datetime(t0, format="%Y%m%d")).split()[0] == "6") and
                                                                                                                    (x["ActualTime"] == "09:30" or x["ActualTime"] == "17:30")
                                                                                                                    else
                                                                                                                    pd.Series([(pd.to_datetime(x["TXDate"], format="%Y%m%d") - np.timedelta64(2,'D')).strftime("%Y%m%d"),(pd.to_datetime(x["TXDate"], format="%Y%m%d") - np.timedelta64(2,'D')).strftime("%Y%m%d"),"N","N","Y"]) 
                                                                                                                    if (re.sub("\s","",str(x["BrandNameEng"])).translate(dc.mapping.VODSymbols).upper() == re.sub("\s","","Property Weekly").translate(dc.mapping.VODSymbols).upper() or re.sub("\s","",str(x["BrandNameChi"])) == re.sub("\s","","一周睇樓團").translate(dc.mapping.VODSymbols)) and 
                                                                                                                    x["TXDate"] == t0 and
                                                                                                                    (x["ActualTime"] == "05:30")
                                                                                                                    else 
                                                                                                                    pd.Series([x["TXDate"]+" "+"Part 1",x["TXDate"]+" "+"第一節","Y","Y","Y"]) 
                                                                                                                    if (re.sub("\s","",str(x["BrandNameEng"])).translate(dc.mapping.VODSymbols).upper() == re.sub("\s","","Trading Hour").translate(dc.mapping.VODSymbols).upper() or re.sub("\s","",str(x["BrandNameChi"])).translate(dc.mapping.VODSymbols) == re.sub("\s","","交易時段").translate(dc.mapping.VODSymbols))
                                                                                                                    and (x["ActualTime"] == "09:30") 
                                                                                                                    and (str(pd.to_datetime(x["TXDate"], format="%Y%m%d") - pd.to_datetime(t0, format="%Y%m%d")).split()[0] != "6" and str(pd.to_datetime(x["TXDate"], format="%Y%m%d") - pd.to_datetime(t0, format="%Y%m%d")).split()[0] != "5")
                                                                                                                    else pd.Series([x["TXDate"]+" "+"Part 2",x["TXDate"]+" "+"第二節","Y","Y","Y"]) 
                                                                                                                    if (re.sub("\s","",str(x["BrandNameEng"])).translate(dc.mapping.VODSymbols).upper() == re.sub("\s","","Trading Hour").translate(dc.mapping.VODSymbols).upper() or re.sub("\s","",str(x["BrandNameChi"])).translate(dc.mapping.VODSymbols) == re.sub("\s","","交易時段").translate(dc.mapping.VODSymbols)) 
                                                                                                                    and (x["ActualTime"] == "13:00")
                                                                                                                    and (str(pd.to_datetime(x["TXDate"], format="%Y%m%d") - pd.to_datetime(t0, format="%Y%m%d")).split()[0] != "6" and str(pd.to_datetime(x["TXDate"], format="%Y%m%d") - pd.to_datetime(t0, format="%Y%m%d")).split()[0] != "5")
                                                                                                                    else 
                                                                                                                    pd.Series([x["TXDate"]+" "+"Part 1",x["TXDate"]+" "+"第一節","N","N","Y"]) 
                                                                                                                    if (re.sub("\s","",str(x["BrandNameEng"])).translate(dc.mapping.VODSymbols).upper() == re.sub("\s","","Trading Hour").translate(dc.mapping.VODSymbols).upper() or re.sub("\s","",str(x["BrandNameChi"])).translate(dc.mapping.VODSymbols) == re.sub("\s","","交易時段").translate(dc.mapping.VODSymbols))
                                                                                                                    and (x["ActualTime"] == "09:30") 
                                                                                                                    and (str(pd.to_datetime(x["TXDate"], format="%Y%m%d") - pd.to_datetime(t0, format="%Y%m%d")).split()[0] == "6" or str(pd.to_datetime(x["TXDate"], format="%Y%m%d") - pd.to_datetime(t0, format="%Y%m%d")).split()[0] == "5")
                                                                                                                    else pd.Series([x["TXDate"]+" "+"Part 2",x["TXDate"]+" "+"第二節","N","N","Y"]) 
                                                                                                                    if (re.sub("\s","",str(x["BrandNameEng"])).translate(dc.mapping.VODSymbols).upper() == re.sub("\s","","Trading Hour").translate(dc.mapping.VODSymbols).upper() or re.sub("\s","",str(x["BrandNameChi"])).translate(dc.mapping.VODSymbols) == re.sub("\s","","交易時段").translate(dc.mapping.VODSymbols))  
                                                                                                                    and (x["ActualTime"] == "13:00")
                                                                                                                    and (str(pd.to_datetime(x["TXDate"], format="%Y%m%d") - pd.to_datetime(t0, format="%Y%m%d")).split()[0] == "6" or str(pd.to_datetime(x["TXDate"], format="%Y%m%d") - pd.to_datetime(t0, format="%Y%m%d")).split()[0] == "5")
                                                                                                                    else pd.Series([x["TXDate"],x["TXDate"],"Y","N","Y"]) 
                                                                                                                    if (re.sub("\s","",str(x["BrandNameEng"])).translate(dc.mapping.VODSymbols).upper() == re.sub("\s","","Trading Hour Highlight").translate(dc.mapping.VODSymbols).upper() or re.sub("\s","",str(x["BrandNameChi"])).translate(dc.mapping.VODSymbols) == re.sub("\s","","交易時段精華").translate(dc.mapping.VODSymbols)) 
                                                                                                                    and  (x["ActualTime"] == "23:30")
                                                                                                                    and (str(pd.to_datetime(x["TXDate"], format="%Y%m%d") - pd.to_datetime(t0, format="%Y%m%d")).split()[0] != "6" and str(pd.to_datetime(x["TXDate"], format="%Y%m%d") - pd.to_datetime(t0, format="%Y%m%d")).split()[0] != "5")
                                                                                                                    else pd.Series([x["TXDate"],x["TXDate"],"N","N","Y"]) 
                                                                                                                    if (re.sub("\s","",str(x["BrandNameEng"])).translate(dc.mapping.VODSymbols).upper() == re.sub("\s","","Trading Hour Highlight").translate(dc.mapping.VODSymbols).upper() or re.sub("\s","",str(x["BrandNameChi"])).translate(dc.mapping.VODSymbols) == re.sub("\s","","交易時段精華").translate(dc.mapping.VODSymbols)) 
                                                                                                                    and ((x["ActualTime"] != "23:30")
                                                                                                                    or (str(pd.to_datetime(x["TXDate"], format="%Y%m%d") - pd.to_datetime(t0, format="%Y%m%d")).split()[0] == "6" or str(pd.to_datetime(x["TXDate"], format="%Y%m%d") - pd.to_datetime(t0, format="%Y%m%d")).split()[0] == "5"))
                                                                                                                    else pd.Series([x["TXDate"],x["TXDate"],"Y","Y","Y"])  
                                                                                                                    if (re.sub("\s","",str(x["BrandNameEng"])).translate(dc.mapping.VODSymbols).upper() == re.sub("\s","","Midday Market Wrap").translate(dc.mapping.VODSymbols).upper() or re.sub("\s","",str(x["BrandNameChi"])).translate(dc.mapping.VODSymbols) == re.sub("\s","","午間股評").translate(dc.mapping.VODSymbols)) 
                                                                                                                    and (x["ActualTime"] == "12:00")
                                                                                                                    and (str(pd.to_datetime(x["TXDate"], format="%Y%m%d") - pd.to_datetime(t0, format="%Y%m%d")).split()[0] != "6" and str(pd.to_datetime(x["TXDate"], format="%Y%m%d") - pd.to_datetime(t0, format="%Y%m%d")).split()[0] != "5")
                                                                                                                    else pd.Series([x["TXDate"],x["TXDate"],"N","N","Y"])  
                                                                                                                    if (re.sub("\s","",str(x["BrandNameEng"])).translate(dc.mapping.VODSymbols).upper() == re.sub("\s","","Midday Market Wrap").translate(dc.mapping.VODSymbols).upper() or re.sub("\s","",str(x["BrandNameChi"])).translate(dc.mapping.VODSymbols) == re.sub("\s","","午間股評").translate(dc.mapping.VODSymbols))
                                                                                                                    and ((x["ActualTime"] != "12:00")
                                                                                                                    or (str(pd.to_datetime(x["TXDate"], format="%Y%m%d") - pd.to_datetime(t0, format="%Y%m%d")).split()[0] == "6" or str(pd.to_datetime(x["TXDate"], format="%Y%m%d") - pd.to_datetime(t0, format="%Y%m%d")).split()[0] == "5"))
                                                                                                                    else pd.Series([(pd.to_datetime(x["TXDate"], format="%Y%m%d") - np.timedelta64(1,'D')).strftime("%Y%m%d"),(pd.to_datetime(x["TXDate"], format="%Y%m%d") - np.timedelta64(1,'D')).strftime("%Y%m%d"),"N","N","Y"]) 
                                                                                                                    if (re.sub("\s","",str(x['BrandNameEng'])).translate(dc.mapping.VODSymbols).upper() == re.sub("\s","","City Forum (RTHK)").translate(dc.mapping.VODSymbols).translate(dc.mapping.VODSymbols).upper() or re.sub("\s","",str(x['BrandNameChi'])).translate(dc.mapping.VODSymbols) == re.sub("\s","","城市論壇(港台)").translate(dc.mapping.VODSymbols)) 
                                                                                                                    and (x["ActualTime"] == "00:00")
                                                                                                                    and x["TXDate"] == t0
                                                                                                                    else pd.Series([x["TXDate"],x["TXDate"],"Y","N","Y"]) 
                                                                                                                    if (re.sub("\s","",str(x['BrandNameEng'])).translate(dc.mapping.VODSymbols).upper() == re.sub("\s","","City Forum (RTHK)").translate(dc.mapping.VODSymbols).translate(dc.mapping.VODSymbols).upper() or re.sub("\s","",str(x['BrandNameChi'])).translate(dc.mapping.VODSymbols) == re.sub("\s","","城市論壇(港台)").translate(dc.mapping.VODSymbols)) 
                                                                                                                    and (x["ActualTime"] == "16:00")
                                                                                                                    and str(pd.to_datetime(x["TXDate"], format="%Y%m%d") - pd.to_datetime(t0, format="%Y%m%d")).split()[0] == "6"
                                                                                                                    else pd.Series([(pd.to_datetime(x["TXDate"], format="%Y%m%d") - np.timedelta64(1,'D')).strftime("%Y%m%d"),(pd.to_datetime(x["TXDate"], format="%Y%m%d") - np.timedelta64(1,'D')).strftime("%Y%m%d"),"N","N","Y"]) 
                                                                                                                    if (re.sub("\s","",str(x["BrandNameEng"])).translate(dc.mapping.VODSymbols).upper() == re.sub("\s","","Now Forum").translate(dc.mapping.VODSymbols).upper() or re.sub("\s","",str(x["BrandNameChi"])).translate(dc.mapping.VODSymbols) == re.sub("\s","","大鳴大放").translate(dc.mapping.VODSymbols)) 
                                                                                                                    and (x["ActualTime"] == "01:30" or x["ActualTime"] == "03:30")
                                                                                                                    and (str(pd.to_datetime(x["TXDate"], format="%Y%m%d") - pd.to_datetime(t0, format="%Y%m%d")).split()[0] == "6" or x["TXDate"] == t0)
                                                                                                                    else pd.Series([x["TXDate"],x["TXDate"],"Y","N","Y"]) 
                                                                                                                    if (re.sub("\s","",str(x["BrandNameEng"])).translate(dc.mapping.VODSymbols).upper() == re.sub("\s","","Now Forum").translate(dc.mapping.VODSymbols).upper() or re.sub("\s","",str(x["BrandNameChi"])).translate(dc.mapping.VODSymbols) == re.sub("\s","","大鳴大放").translate(dc.mapping.VODSymbols)) 
                                                                                                                    and (x["ActualTime"] == "20:30")
                                                                                                                    and (str(pd.to_datetime(x["TXDate"], format="%Y%m%d") - pd.to_datetime(t0, format="%Y%m%d")).split()[0] == "6" or str(pd.to_datetime(x["TXDate"], format="%Y%m%d") - pd.to_datetime(t0, format="%Y%m%d")).split()[0] == "5") 
                                                                                                                    else pd.Series([(pd.to_datetime(x["TXDate"], format="%Y%m%d") - np.timedelta64(1,'D')).strftime("%Y%m%d"),(pd.to_datetime(x["TXDate"], format="%Y%m%d") - np.timedelta64(1,'D')).strftime("%Y%m%d"),"N","N","Y"]) 
                                                                                                                    if (re.sub("\s","",str(x["BrandNameEng"])).translate(dc.mapping.VODSymbols).upper() == re.sub("\s","","Now Forum").translate(dc.mapping.VODSymbols).upper() or re.sub("\s","",str(x["BrandNameChi"])).translate(dc.mapping.VODSymbols) == re.sub("\s","","大鳴大放").translate(dc.mapping.VODSymbols)) 
                                                                                                                    and (x["ActualTime"] == "01:30" or x["ActualTime"] == "03:30")
                                                                                                                    and (str(pd.to_datetime(x["TXDate"], format="%Y%m%d") - pd.to_datetime(t0, format="%Y%m%d")).split()[0] != "6" and x["TXDate"] != t0) 
                                                                                                                    else pd.Series([x["TXDate"],x["TXDate"],"N","N","Y"]) 
                                                                                                                    if (re.sub("\s","",str(x["BrandNameEng"])).translate(dc.mapping.VODSymbols).upper() == re.sub("\s","","Now Forum").translate(dc.mapping.VODSymbols).upper() or re.sub("\s","",str(x["BrandNameChi"])).translate(dc.mapping.VODSymbols) == re.sub("\s","","大鳴大放").translate(dc.mapping.VODSymbols)) 
                                                                                                                    and (x["ActualTime"] == "20:30")
                                                                                                                    and (str(pd.to_datetime(x["TXDate"], format="%Y%m%d") - pd.to_datetime(t0, format="%Y%m%d")).split()[0] != "6" and str(pd.to_datetime(x["TXDate"], format="%Y%m%d") - pd.to_datetime(t0, format="%Y%m%d")).split()[0] != "5")
                                                                                                                    else 
                                                                                                                    pd.Series([x["TXDate"],x["TXDate"],"Y","Y","Y"]) 
                                                                                                                    if (re.sub("\s","",str(x["BrandNameEng"])).translate(dc.mapping.VODSymbols).upper() == re.sub("\s","","Market Wrap").translate(dc.mapping.VODSymbols).upper() or re.sub("\s","",str(x["BrandNameChi"])).translate(dc.mapping.VODSymbols) == re.sub("\s","","收市檢討").translate(dc.mapping.VODSymbols)) 
                                                                                                                    and (x["ActualTime"] == "16:00")
                                                                                                                    and (str(pd.to_datetime(x["TXDate"], format="%Y%m%d") - pd.to_datetime(t0, format="%Y%m%d")).split()[0] != "6" and str(pd.to_datetime(x["TXDate"], format="%Y%m%d") - pd.to_datetime(t0, format="%Y%m%d")).split()[0] != "5")
                                                                                                                    else pd.Series([x["TXDate"],x["TXDate"],"Y","N","Y"])
                                                                                                                    if  (re.sub("\s","",str(x["BrandNameEng"])).translate(dc.mapping.VODSymbols).upper() == re.sub("\s","","Medicine Online").translate(dc.mapping.VODSymbols).upper() or re.sub("\s","",str(x["BrandNameChi"])).translate(dc.mapping.VODSymbols) == re.sub("\s","","杏林在線").translate(dc.mapping.VODSymbols))
                                                                                                                    and (x["ActualTime"] == "22:30")
                                                                                                                    and str(pd.to_datetime(x["TXDate"], format="%Y%m%d") - pd.to_datetime(t0, format="%Y%m%d")).split()[0] == "5"
                                                                                                                    else pd.Series([(pd.to_datetime(x["TXDate"], format="%Y%m%d") - np.timedelta64(1,'D')).strftime("%Y%m%d"),(pd.to_datetime(x["TXDate"], format="%Y%m%d") - np.timedelta64(1,'D')).strftime("%Y%m%d"),"N","N","Y"])
                                                                                                                    if (re.sub("\s","",str(x["BrandNameEng"])).translate(dc.mapping.VODSymbols).upper() == re.sub("\s","","Medicine Online").translate(dc.mapping.VODSymbols).upper() or re.sub("\s","",str(x["BrandNameChi"])).translate(dc.mapping.VODSymbols) == re.sub("\s","","杏林在線").translate(dc.mapping.VODSymbols))
                                                                                                                    and (x["ActualTime"] == "02:30" or x["ActualTime"] == "04:30")
                                                                                                                    and str(pd.to_datetime(x["TXDate"], format="%Y%m%d") - pd.to_datetime(t0, format="%Y%m%d")).split()[0] == "6"
                                                                                                                    else pd.Series([x["TXDate"],x["TXDate"],"N","N","Y"])
                                                                                                                    if (re.sub("\s","",str(x["BrandNameEng"])).translate(dc.mapping.VODSymbols).upper() == re.sub("\s","","Medicine Online").translate(dc.mapping.VODSymbols).upper() or re.sub("\s","",str(x["BrandNameChi"])).translate(dc.mapping.VODSymbols) == re.sub("\s","","杏林在線").translate(dc.mapping.VODSymbols))
                                                                                                                    and (x["ActualTime"] == "22:30")
                                                                                                                    and str(pd.to_datetime(x["TXDate"], format="%Y%m%d") - pd.to_datetime(t0, format="%Y%m%d")).split()[0] != "5"
                                                                                                                    else pd.Series([(pd.to_datetime(x["TXDate"], format="%Y%m%d") - np.timedelta64(1,'D')).strftime("%Y%m%d"),(pd.to_datetime(x["TXDate"], format="%Y%m%d") - np.timedelta64(1,'D')).strftime("%Y%m%d"),"N","N","Y"])
                                                                                                                    if (re.sub("\s","",str(x["BrandNameEng"])).translate(dc.mapping.VODSymbols).upper() == re.sub("\s","","Medicine Online").translate(dc.mapping.VODSymbols).upper() or re.sub("\s","",str(x["BrandNameChi"])).translate(dc.mapping.VODSymbols) == re.sub("\s","","杏林在線").translate(dc.mapping.VODSymbols))
                                                                                                                    and (x["ActualTime"] == "02:30" or x["ActualTime"] == "04:30")
                                                                                                                    and str(pd.to_datetime(x["TXDate"], format="%Y%m%d") - pd.to_datetime(t0, format="%Y%m%d")).split()[0] != "6"
                                                                                                                    else
                                                                                                                    pd.Series([x["TXDate"],x["TXDate"],"Y","N","Y"]) 
                                                                                                                    if (re.sub("\s","",str(x["BrandNameEng"])).translate(dc.mapping.VODSymbols).upper() == re.sub("\s","","Property Daily").translate(dc.mapping.VODSymbols).upper() or re.sub("\s","",str(x["BrandNameChi"])).translate(dc.mapping.VODSymbols) == re.sub("\s","","樓市每日睇").translate(dc.mapping.VODSymbols))  
                                                                                                                    and (x["ActualTime"] == "21:00")
                                                                                                                    and (str(pd.to_datetime(x["TXDate"], format="%Y%m%d") - pd.to_datetime(t0, format="%Y%m%d")).split()[0] != "6" and str(pd.to_datetime(x["TXDate"], format="%Y%m%d") - pd.to_datetime(t0, format="%Y%m%d")).split()[0] != "5")
                                                                                                                    else
                                                                                                                    pd.Series([(pd.to_datetime(x["TXDate"], format="%Y%m%d") - np.timedelta64(1,'D')).strftime("%Y%m%d"),(pd.to_datetime(x["TXDate"], format="%Y%m%d") - np.timedelta64(1,'D')).strftime("%Y%m%d"),"N","N","Y"]) 
                                                                                                                    if  (re.sub("\s","",str(x["BrandNameEng"])).translate(dc.mapping.VODSymbols).upper() == re.sub("\s","","Property Daily").translate(dc.mapping.VODSymbols).upper() or re.sub("\s","",str(x["BrandNameChi"])).translate(dc.mapping.VODSymbols) == re.sub("\s","","樓市每日睇").translate(dc.mapping.VODSymbols))    
                                                                                                                    and (x["ActualTime"] == "02:00")
                                                                                                                    and (str(pd.to_datetime(x["TXDate"], format="%Y%m%d") - pd.to_datetime(t0, format="%Y%m%d")).split()[0] != "6" and x["TXDate"] != t0)
                                                                                                                    else pd.Series([x["TXDate"],x["TXDate"],"Y","N","Y"]) 
                                                                                                                   if (re.sub("\s","",str(x["BrandNameEng"])).translate(dc.mapping.VODSymbols).upper() == re.sub("\s","","Market Today").translate(dc.mapping.VODSymbols).upper() or re.sub("\s","",str(x["BrandNameChi"])).translate(dc.mapping.VODSymbols) == re.sub("\s","","港股今日睇").translate(dc.mapping.VODSymbols)) 
                                                                                                                   and (x["ActualTime"] == "21:30")
                                                                                                                   and (str(pd.to_datetime(x["TXDate"], format="%Y%m%d") - pd.to_datetime(t0, format="%Y%m%d")).split()[0] != "6" and str(pd.to_datetime(x["TXDate"], format="%Y%m%d") - pd.to_datetime(t0, format="%Y%m%d")).split()[0] != "5")
                                                                                                                   else pd.Series([(pd.to_datetime(x["TXDate"], format="%Y%m%d") - np.timedelta64(1,'D')).strftime("%Y%m%d"),(pd.to_datetime(x["TXDate"], format="%Y%m%d") - np.timedelta64(1,'D')).strftime("%Y%m%d"),"N","N","Y"]) 
                                                                                                                   if (re.sub("\s","",str(x["BrandNameEng"])).translate(dc.mapping.VODSymbols).upper() == re.sub("\s","","Market Today").translate(dc.mapping.VODSymbols).upper() or re.sub("\s","",str(x["BrandNameChi"])).translate(dc.mapping.VODSymbols) == re.sub("\s","","港股今日睇").translate(dc.mapping.VODSymbols)) 
                                                                                                                   and (x["ActualTime"] == "00:00")
                                                                                                                   and (str(pd.to_datetime(x["TXDate"], format="%Y%m%d") - pd.to_datetime(t0, format="%Y%m%d")).split()[0] != "6" and x["TXDate"] != t0)
                                                                                                                   else pd.Series([x["TXDate"],x["TXDate"],"Y","Y","Y"]) 
                                                                                                                   if (re.sub("\s","",str(x['BrandNameEng'])).translate(dc.mapping.VODSymbols).upper() == re.sub("\s","","Pre-Opening").translate(dc.mapping.VODSymbols).upper() or re.sub("\s","",str(x["BrandNameChi"])).translate(dc.mapping.VODSymbols)  == re.sub("\s","","牛熊先機").translate(dc.mapping.VODSymbols)) 
                                                                                                                   and (x["ActualTime"] == "08:30")
                                                                                                                   and (str(pd.to_datetime(x["TXDate"], format="%Y%m%d") - pd.to_datetime(t0, format="%Y%m%d")).split()[0] != "6" and str(pd.to_datetime(x["TXDate"], format="%Y%m%d") - pd.to_datetime(t0, format="%Y%m%d")).split()[0] != "5")
                                                                                                                   else pd.Series([x["TXDate"],x["TXDate"],"Y","Y","Y"]) 
                                                                                                                   if (re.sub("\s","",str(x["BrandNameEng"])).translate(dc.mapping.VODSymbols).upper() == re.sub("\s","","Global Market Express").translate(dc.mapping.VODSymbols).upper() or re.sub("\s","",str(x["BrandNameChi"])).translate(dc.mapping.VODSymbols) == re.sub("\s","","環球金融快線").translate(dc.mapping.VODSymbols))  
                                                                                                                   and (x["ActualTime"] == "22:30")
                                                                                                                   and (str(pd.to_datetime(x["TXDate"], format="%Y%m%d") - pd.to_datetime(t0, format="%Y%m%d")).split()[0] != "6" and str(pd.to_datetime(x["TXDate"], format="%Y%m%d") - pd.to_datetime(t0, format="%Y%m%d")).split()[0] != "5")
                                                                                                                   else pd.Series([(pd.to_datetime(x["TXDate"], format="%Y%m%d") - np.timedelta64(1,'D')).strftime("%Y%m%d"),(pd.to_datetime(x["TXDate"], format="%Y%m%d") - np.timedelta64(1,'D')).strftime("%Y%m%d"),"N","N","Y"]) 
                                                                                                                   if (re.sub("\s","",str(x["BrandNameEng"])).translate(dc.mapping.VODSymbols).upper() == re.sub("\s","","Global Market Express").translate(dc.mapping.VODSymbols).upper() or re.sub("\s","",str(x["BrandNameChi"])).translate(dc.mapping.VODSymbols) == re.sub("\s","","環球金融快線").translate(dc.mapping.VODSymbols)) 
                                                                                                                   and (x["ActualTime"] == "02:30")
                                                                                                                   and (str(pd.to_datetime(x["TXDate"], format="%Y%m%d") - pd.to_datetime(t0, format="%Y%m%d")).split()[0] != "6" and x["TXDate"] != t0)
                                                                                                                   else  
                                                                                                                   pd.Series([x["TXDate"],x["TXDate"],"Y","N","Y"]) 
                                                                                                                   if (re.sub("\s","",str(x["BrandNameEng"])).translate(dc.mapping.VODSymbols).upper() == re.sub("\s","","Now Report").translate(dc.mapping.VODSymbols).upper() or re.sub("\s","",str(x["BrandNameChi"])).translate(dc.mapping.VODSymbols) == re.sub("\s","","經緯線").translate(dc.mapping.VODSymbols))  
                                                                                                                   and (x["ActualTime"] == "22:30")
                                                                                                                   and str(pd.to_datetime(x["TXDate"], format="%Y%m%d") - pd.to_datetime(t0, format="%Y%m%d")).split()[0] == "6"
                                                                                                                   else
                                                                                                                   pd.Series([(pd.to_datetime(x["TXDate"], format="%Y%m%d") - np.timedelta64(1,'D')).strftime("%Y%m%d"),(pd.to_datetime(x["TXDate"], format="%Y%m%d") - np.timedelta64(1,'D')).strftime("%Y%m%d"),"N","N","Y"]) 
                                                                                                                   if (re.sub("\s","",str(x["BrandNameEng"])).translate(dc.mapping.VODSymbols).upper() == re.sub("\s","","Now Report").translate(dc.mapping.VODSymbols).upper() or re.sub("\s","",str(x["BrandNameChi"])).translate(dc.mapping.VODSymbols) == re.sub("\s","","經緯線").translate(dc.mapping.VODSymbols)) 
                                                                                                                   and (x["ActualTime"] == "02:30" or x["ActualTime"] == "04:30")
                                                                                                                   and x["TXDate"] == t0
                                                                                                                   else
                                                                                                                   pd.Series([x["TXDate"],x["TXDate"],"Y","N","Y"]) 
                                                                                                                   if (re.sub("\s","",str(x["BrandNameEng"])).translate(dc.mapping.VODSymbols).upper() == re.sub("\s","","Legco Review (RTHK)").translate(dc.mapping.VODSymbols).upper() or re.sub("\s","",str(x["BrandNameChi"])).translate(dc.mapping.VODSymbols) == re.sub("\s","","議事論事 (港台)").translate(dc.mapping.VODSymbols)) 
                                                                                                                   and (x["ActualTime"] == "14:30")
                                                                                                                   and str(pd.to_datetime(x["TXDate"], format="%Y%m%d") - pd.to_datetime(t0, format="%Y%m%d")).split()[0] == "5"
                                                                                                                   else
                                                                                                                   pd.Series([(pd.to_datetime(x["TXDate"], format="%Y%m%d") - np.timedelta64(1,'D')).strftime("%Y%m%d"),(pd.to_datetime(x["TXDate"], format="%Y%m%d") - np.timedelta64(1,'D')).strftime("%Y%m%d"),"N","N","Y"]) 
                                                                                                                   if (re.sub("\s","",str(x["BrandNameEng"])).translate(dc.mapping.VODSymbols).upper() == re.sub("\s","","Legco Review (RTHK)").translate(dc.mapping.VODSymbols).upper() or re.sub("\s","",str(x["BrandNameChi"])).translate(dc.mapping.VODSymbols) == re.sub("\s","","議事論事 (港台)").translate(dc.mapping.VODSymbols)) 
                                                                                                                   and (x["ActualTime"] == "14:30")
                                                                                                                   and str(pd.to_datetime(x["TXDate"], format="%Y%m%d") - pd.to_datetime(t0, format="%Y%m%d")).split()[0] == "6"
                                                                                                                   else pd.Series([x["TXDate"],x["TXDate"],"Y","N","Y"]) 
                                                                                                                   if (re.sub("\s","",str(x["BrandNameEng"])).translate(dc.mapping.VODSymbols).upper() == re.sub("\s","","HK Connection (RTHK)").translate(dc.mapping.VODSymbols).upper() or re.sub("\s","",str(x["BrandNameChi"])).translate(dc.mapping.VODSymbols) == re.sub("\s","","鏗鏘集 (港台)").translate(dc.mapping.VODSymbols)) 
                                                                                                                   and (x["ActualTime"] == "18:30")
                                                                                                                   and str(pd.to_datetime(x["TXDate"], format="%Y%m%d") - pd.to_datetime(t0, format="%Y%m%d")).split()[0] == "5"
                                                                                                                   else pd.Series([(pd.to_datetime(x["TXDate"], format="%Y%m%d") - np.timedelta64(1,'D')).strftime("%Y%m%d"),(pd.to_datetime(x["TXDate"], format="%Y%m%d") - np.timedelta64(1,'D')).strftime("%Y%m%d"),"N","N","Y"]) 
                                                                                                                   if (re.sub("\s","",str(x["BrandNameEng"])).translate(dc.mapping.VODSymbols).upper() == re.sub("\s","","HK Connection (RTHK)").translate(dc.mapping.VODSymbols).upper() or re.sub("\s","",str(x["BrandNameChi"])).translate(dc.mapping.VODSymbols) == re.sub("\s","","鏗鏘集 (港台)").translate(dc.mapping.VODSymbols)) 
                                                                                                                   and (x["ActualTime"] == "18:30")
                                                                                                                   and str(pd.to_datetime(x["TXDate"], format="%Y%m%d") - pd.to_datetime(t0, format="%Y%m%d")).split()[0] == "6"
                                                                                                                   else pd.Series([x["TXDate"],x["TXDate"],"N","N","Y"])
                                                                                                                   if (re.sub("\s","",str(x["BrandNameEng"])).translate(dc.mapping.VODSymbols).upper() == re.sub("\s","","Pre-Opening I").translate(dc.mapping.VODSymbols).upper() or re.sub("\s","",str(x["BrandNameChi"])).translate(dc.mapping.VODSymbols) == re.sub("\s","","牛熊先機 I").translate(dc.mapping.VODSymbols))
                                                                                                                   else             
                                                                                                                   pd.Series([x["EpisodeNameEng"],x["EpisodeNameChi"],x["Premier"],x["IsLive"],x["IsEpisodic"]]), axis=1)
    elif owner_channel == "Now NEWS":
        outdf[["EpisodeNameEng","EpisodeNameChi","Premier","IsLive","IsEpisodic"]] = outdf[["BrandNameEng", "BrandNameChi", "TXDate","ActualTime", "EpisodeNameEng", "EpisodeNameChi","Premier","IsLive","IsEpisodic"]].apply(lambda x: 
                     pd.Series([x["TXDate"],x["TXDate"],"N","N","Y"]) 
                    if (re.sub("\s","",str(x["BrandNameEng"])).translate(dc.mapping.VODSymbols).upper() == re.sub("\s","","News Magazine").translate(dc.mapping.VODSymbols).upper() or re.sub("\s","",str(x["BrandNameChi"])).translate(dc.mapping.VODSymbols) == re.sub("\s","","時事全方位").translate(dc.mapping.VODSymbols))  
                    and (x["ActualTime"] == "09:30") 
                    and (str(pd.to_datetime(x["TXDate"], format="%Y%m%d") - pd.to_datetime(t0, format="%Y%m%d")).split()[0] == "6" or str(pd.to_datetime(x["TXDate"], format="%Y%m%d") - pd.to_datetime(t0, format="%Y%m%d")).split()[0] == "5") 
                    else
                    pd.Series([x["TXDate"],x["TXDate"],"Y","Y","Y"]) 
                    if (re.sub("\s","",str(x["BrandNameEng"])).translate(dc.mapping.VODSymbols).upper() == re.sub("\s","","News Magazine").translate(dc.mapping.VODSymbols).upper() or re.sub("\s","",str(x["BrandNameChi"])).translate(dc.mapping.VODSymbols) == re.sub("\s","","時事全方位").translate(dc.mapping.VODSymbols))  
                    and (x["ActualTime"] == "09:30") 
                    and (str(pd.to_datetime(x["TXDate"], format="%Y%m%d") - pd.to_datetime(t0, format="%Y%m%d")).split()[0] != "6" and str(pd.to_datetime(x["TXDate"], format="%Y%m%d") - pd.to_datetime(t0, format="%Y%m%d")).split()[0] != "5") 
                    else pd.Series([x["TXDate"],x["TXDate"],"N","N","Y"]) 
                    if (re.sub("\s","",str(x["BrandNameEng"])).translate(dc.mapping.VODSymbols).upper() == re.sub("\s","","Medicine Online").translate(dc.mapping.VODSymbols).upper() or re.sub("\s","",str(x["BrandNameChi"])).translate(dc.mapping.VODSymbols) == re.sub("\s","","杏林在線").translate(dc.mapping.VODSymbols)) and 
                        (x["ActualTime"] == "21:45")
                    else pd.Series([x["TXDate"],x["TXDate"],"Y","N","Y"])
                    if (re.sub("\s","",str(x["BrandNameEng"])).translate(dc.mapping.VODSymbols).upper() == re.sub("\s","","Medicine Online").translate(dc.mapping.VODSymbols).upper() or re.sub("\s","",str(x["BrandNameChi"])).translate(dc.mapping.VODSymbols) == re.sub("\s","","杏林在線").translate(dc.mapping.VODSymbols)) and 
                    (x["ActualTime"] == "21:40") and
                    ((x["TXDate"] == t0) or (str(pd.to_datetime(x["TXDate"], format="%Y%m%d") - pd.to_datetime(t0, format="%Y%m%d")).split()[0] == "4"))
                    else pd.Series([(pd.to_datetime(x["TXDate"], format="%Y%m%d") - np.timedelta64(3,'D')).strftime("%Y%m%d"),(pd.to_datetime(x["TXDate"], format="%Y%m%d") - np.timedelta64(3,'D')).strftime("%Y%m%d"),"N","N","Y"])
                    if (re.sub("\s","",str(x["BrandNameEng"])).translate(dc.mapping.VODSymbols).upper() == re.sub("\s","","Medicine Online").translate(dc.mapping.VODSymbols).upper() or re.sub("\s","",str(x["BrandNameChi"])).translate(dc.mapping.VODSymbols) == re.sub("\s","","杏林在線").translate(dc.mapping.VODSymbols)) and 
                    ((x["ActualTime"] == "16:15") or (x["ActualTime"] == "16:45")) and 
                    (x["TXDate"] == t0)
                    else pd.Series([(pd.to_datetime(x["TXDate"], format="%Y%m%d") - np.timedelta64(1,'D')).strftime("%Y%m%d"),(pd.to_datetime(x["TXDate"], format="%Y%m%d") - np.timedelta64(1,'D')).strftime("%Y%m%d"),"N","N","Y"])
                    if (re.sub("\s","",str(x["BrandNameEng"])).translate(dc.mapping.VODSymbols).upper() == re.sub("\s","","Medicine Online").translate(dc.mapping.VODSymbols).upper() or re.sub("\s","",str(x["BrandNameChi"])).translate(dc.mapping.VODSymbols) == re.sub("\s","","杏林在線").translate(dc.mapping.VODSymbols)) and 
                    ((x["ActualTime"] == "16:15") or (x["ActualTime"] == "16:45")) and
                    (str(pd.to_datetime(x["TXDate"], format="%Y%m%d") - pd.to_datetime(t0, format="%Y%m%d")).split()[0] == "1")
                    else pd.Series([(pd.to_datetime(x["TXDate"], format="%Y%m%d") - np.timedelta64(5,'D')).strftime("%Y%m%d"),(pd.to_datetime(x["TXDate"], format="%Y%m%d") - np.timedelta64(5,'D')).strftime("%Y%m%d"),"N","N","Y"])
                    if (re.sub("\s","",str(x["BrandNameEng"])).translate(dc.mapping.VODSymbols).upper() == re.sub("\s","","Medicine Online").translate(dc.mapping.VODSymbols).upper() or re.sub("\s","",str(x["BrandNameChi"])).translate(dc.mapping.VODSymbols) == re.sub("\s","","杏林在線").translate(dc.mapping.VODSymbols)) and 
                    ((x["ActualTime"] == "10:15") or (x["ActualTime"] == "10:45")) and
                    (str(pd.to_datetime(x["TXDate"], format="%Y%m%d") - pd.to_datetime(t0, format="%Y%m%d")).split()[0] == "5")
                    else pd.Series([(pd.to_datetime(x["TXDate"], format="%Y%m%d") - np.timedelta64(2,'D')).strftime("%Y%m%d"),(pd.to_datetime(x["TXDate"], format="%Y%m%d") - np.timedelta64(2,'D')).strftime("%Y%m%d"),"N","N","Y"])
                    if (re.sub("\s","",str(x["BrandNameEng"])).translate(dc.mapping.VODSymbols).upper() == re.sub("\s","","Medicine Online").translate(dc.mapping.VODSymbols).upper() or re.sub("\s","",str(x["BrandNameChi"])).translate(dc.mapping.VODSymbols) == re.sub("\s","","杏林在線").translate(dc.mapping.VODSymbols)) and 
                    ((x["ActualTime"] == "10:15") or (x["ActualTime"] == "10:45")) and
                    (str(pd.to_datetime(x["TXDate"], format="%Y%m%d") - pd.to_datetime(t0, format="%Y%m%d")).split()[0] == "6")
                    else pd.Series([(pd.to_datetime(x["TXDate"], format="%Y%m%d") - np.timedelta64(1,'D')).strftime("%Y%m%d"),(pd.to_datetime(x["TXDate"], format="%Y%m%d") - np.timedelta64(1,'D')).strftime("%Y%m%d"),"N","N","Y"]) 
                    if (re.sub("\s","",str(x["BrandNameEng"])).translate(dc.mapping.VODSymbols).upper() == re.sub("\s","","Now Report").translate(dc.mapping.VODSymbols).upper() or re.sub("\s","",str(x["BrandNameChi"])).translate(dc.mapping.VODSymbols) == re.sub("\s","","經緯線").translate(dc.mapping.VODSymbols))
                    and ((x["ActualTime"] == "13:30") or (x["ActualTime"] == "22:30")) and 
                    (x["TXDate"] == t0)
                    else pd.Series([x["EpisodeNameEng"],x["EpisodeNameChi"],x["Premier"],x["IsLive"],x["IsEpisodic"]]),axis = 1)
    outdf['OwnerChannel'] = owner_channel
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)
    if owner_channel != "Now Direct":
        outdf["SynopsisEng"] = indf[SynopsisEng].str.translate(dc.mapping.charTranslateTable)
        outdf["SynopsisChi"] = indf[SynopsisChi].str.translate(dc.mapping.charTranslateTable)
        outdf["ShortSynopsisEng"] = outdf["SynopsisEng"][outdf["SynopsisEng"].notnull()].apply(lambda x: x[0:dc.settings.char_limit.get("ShortSynopsisEng")])
        outdf["ShortSynopsisChi"] = outdf["SynopsisChi"][outdf["SynopsisChi"].notnull()].apply(lambda x: x[0:dc.settings.char_limit.get("ShortSynopsisChi")])
        outdf["Genre"] = indf["Genre"]
        outdf["SubGenre"] = indf[SubGenre]
        outdf[["Genre","SubGenre"]] = outdf.apply(dc.utils.genMapping, axis=1)
    else:
        outdf["Genre"] = indf["Genre"][indf["Genre"].notnull()].str.lower().map(dc.mapping.genreMapping).fillna(indf["Genre"])
        outdf["SubGenre"] = indf[SubGenre][indf[SubGenre].notnull()].str.lower().map(dc.mapping.subGenreMapping).str.join("/").fillna(indf["Sub Genre"])
    if "Recordable" in indf.columns:
        outdf["Recordable"] = indf["Recordable"]
    if "Is NPVR Prog" in indf.columns:
        outdf["IsNPVRProg"] = indf["Is NPVR Prog"]
    if "Is Restart TV" in indf.columns:
        outdf["IsRestartTV"] = indf["Is Restart TV"]
    dc.utils.title_df(outdf)
    outdf[["FullTitleEng", "FullTitleChi"]] = outdf.apply(dc.utils.constructFullTitle, axis=1)
    outdf[["ProgrammeNameEng", "ProgrammeNameChi"]] = outdf.apply(dc.utils.constructProgrammeName, axis=1)
    dc.utils.translateChar(outdf)
    dc.utils.chopCharacters(outdf)


    # Append EOF
    if owner_channel != "Now Direct":
        dc.utils.appendEOF(outdf)
    else:
        x = pd.to_datetime(outdf["TXDate"] + " " + outdf["ActualTime"], format="%Y%m%d %H:%M")
        lastProgStartTimestamp = x.max()
        eofTimestamp = None
        oneDay = pd.Timedelta(1,"D")
        oneWeek = pd.Timedelta(1,"W")
        oneDayBefore = lastProgStartTimestamp - oneDay
        oneWeekBefore = lastProgStartTimestamp - oneWeek
        if x.min() <= oneWeekBefore:
            eofTimestamp = x[x > oneWeekBefore].min() + oneWeek
        elif x.min() <= oneDayBefore:
            eofTimestamp = x[x > oneDayBefore].min() + oneDay
        else:
            eofTimestamp = x.min() + oneDay
        outdf.loc[outdf.last_valid_index(), ["TXDate", "ActualTime", "FullTitleEng", "FullTitleChi", "ProgrammeNameEng", "ProgrammeNameChi"]] = [eofTimestamp.strftime("%Y%m%d"), eofTimestamp.strftime("%H:%M"), "END OF FILE", "END OF FILE", "END OF FILE", "END OF FILE"]


    # Fill Channel #
    outdf['OwnerChannel'] = owner_channel
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)

    # Write output file
    dc.io.to_io(outdf, out_io, out_format)

def convert_nonlinear(in_io, out_io, format="preV1506", owner_channel="Now Business News Channel"):
    "Convert nonlinear for Now Business News Channel"
    return None

