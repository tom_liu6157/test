import pandas as pd
import numpy as np
import datetime
import math
import csv
import re
import calendar
from collections import OrderedDict
import dc.utils
import dc.io
import dc.header
import dc.mapping
import dc.settings

def convert_linear(in_io, out_io, out_format="preV1506", owner_channel="Sony Channel"):
    "Convert linear schedule for Sony Channel"

    # Read input file
    indf = pd.read_excel(in_io, sheetname=0)
    indf.dropna(how='all', inplace=True)

    
    # Create output DataFrame
    outdf = pd.DataFrame(columns=dc.header.internalHeader)

    # Extract data from full title


    # Fill output DataFrame with related info
    synopsis_col_name = "Synopsis"
    if owner_channel == "Sony Channel":
        synopsis_col_name = "Synopsis (En)"
    titleEn = indf["Title (En)"][indf["Title (En)"].notnull()].str.translate(dc.mapping.charTranslateTable)
    titleCn = indf["Title (Ch)"][indf["Title (Ch)"].notnull()].str.translate(dc.mapping.charTranslateTable)
    outdf["TXDate"] = indf["Date"][indf["Date"].notnull()].apply(lambda x: pd.to_datetime(x, format="%d/%m/%Y")).apply(lambda x:x.strftime("%Y%m%d"))
    outdf["ActualTime"] = indf["Time"][indf["Time"].notnull()].apply(lambda x: pd.to_datetime(x, format="%H:%M:%S")).apply(lambda x:x.strftime("%H:%M"))
    start_hour = 6
    outdf["TXDate"] = outdf["TXDate"].where(outdf["ActualTime"].apply(lambda x: True if pd.notnull(x) and (int(x[:2]) < start_hour) else False)).apply(lambda x: pd.to_datetime(x, format="%Y%m%d") if pd.notnull(x) else None).apply(lambda x: x + np.timedelta64(1,'D') if pd.notnull(x) else None).apply(lambda x: x.strftime("%Y%m%d") if pd.notnull(x) else None).fillna(outdf["TXDate"])
    outdf["FullTitleEng"] = titleEn.str.replace('\s*[\(:]\s*SEASON.*\)*', '')
    outdf["FullTitleChi"] = titleCn.str.replace('\s*[\(\uFF08]*\s*第.*[\)\uFF09].*', '').str.replace('\s*[\(:]\s*SEASON.*\)*', '')
    known_brandname = ["MISSION: IMPOSSIBLE 2", "13 GOING ON 30"]
    brandname_season = outdf["FullTitleEng"].where(outdf["FullTitleEng"].apply(lambda x: not any([x.startswith(bn) for bn in known_brandname]))).str.extract('^(?P<BrandNameEng>.*?)\s+(?P<SeasonNo>\d+)$', expand=False)
    outdf["BrandNameEng"] = brandname_season['BrandNameEng'].fillna(outdf["FullTitleEng"])
    outdf["BrandNameChi"] = outdf["FullTitleChi"]
    outdf["SeasonNo"] = titleEn.str.replace('^((?!\(*\s*SEASON\s*\d*\)*).)*', '').str.replace('\(*\s*SEASON\s*(\d*).*', r'\1').apply(lambda x: x if pd.notnull(x) and len(x)>0 else None)
    outdf["SeasonNo"].fillna(brandname_season['SeasonNo'], inplace=True)
    outdf["EpisodeNo"] = indf[indf["Episode No."].notnull() & (indf["Genre"].isnull() | ~indf["Genre"][indf["Genre"].notnull()].str.contains("Film", case=False))]["Episode No."].astype(int).astype(str)
    outdf["EpisodeNameEng"] = indf[indf["Episode Title"].notnull() & (indf["Genre"].isnull() | ~indf["Genre"][indf["Genre"].notnull()].str.contains("Film", case=False))]["Episode Title"].astype('str').str.replace("\s*EPISODE\s*\d*", "")#.str.replace("\s*PROD\s+YEAR\s*\d*", "")
    outdf["EpisodeNameChi"].fillna(outdf["EpisodeNameEng"], inplace=True) # Fill empty Chi episode name with Eng episode name
    outdf["SynopsisEng"] = indf[synopsis_col_name][indf[synopsis_col_name].notnull()].astype(str).str.translate(dc.mapping.charTranslateTable)
    outdf["SynopsisChi"] = indf["Synopsis (Ch)"].fillna(outdf["SynopsisEng"])
    outdf["ShortSynopsisEng"] = outdf["SynopsisEng"][outdf["SynopsisEng"].notnull()].apply(lambda x: x[0:dc.settings.char_limit.get("ShortSynopsisEng")])
    outdf["ShortSynopsisChi"] = outdf["SynopsisChi"][outdf["SynopsisChi"].notnull()].apply(lambda x: x[0:dc.settings.char_limit.get("ShortSynopsisChi")])
    outdf["PremierOld"] = "0"
    outdf["Premier"] = "N"
    outdf["IsLive"] = "N"
    outdf["SubGenre"] = indf["Genre"][indf["Genre"].notnull()].str.translate(dc.mapping.charTranslateTable)
    outdf["DirectorProducerEng"] = indf["Directors"][indf["Directors"].notnull()].apply(lambda x: str(x).strip().translate(dc.mapping.charTranslateTable))
    outdf["DirectorProducerChi"] = outdf["DirectorProducerEng"]
    outdf["CastEng"] = indf["Cast"][indf["Cast"].notnull()].str.translate(dc.mapping.charTranslateTable).str.replace("^[\s,]*(.*)[\s,]*$", r"\1")
    outdf["CastChi"] = outdf["CastEng"]
    outdf["FirstReleaseYear"] = indf[indf["Episode Title"].astype('str').str.contains("PROD YEAR", case=False) | indf["Genre"].str.contains("Film", case=False)]["Episode Title"].astype('str').str.replace("[^0-9]*", "")
    outdf["IsEpisodic"] = (outdf["EpisodeNo"].notnull() | outdf["EpisodeNameEng"].notnull()).map({True:"Y", False:"N"})
    dc.utils.title_df(outdf)
    outdf[["FullTitleEng", "FullTitleChi"]] = outdf.apply(dc.utils.constructFullTitle, axis=1)
    outdf[["ProgrammeNameEng", "ProgrammeNameChi"]] = outdf.apply(dc.utils.constructProgrammeName, axis=1)
    dc.utils.translateChar(outdf)
    dc.utils.chopCharacters(outdf)


    # Append EOF
    lastProgDuration = pd.Timedelta(str(indf["Duration"][indf.last_valid_index()]))
    dc.utils.appendEOF(outdf, lastprog_duration=lastProgDuration)


    # Fill Channel #
    outdf['OwnerChannel'] = owner_channel
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)

    # Write output file
    dc.io.to_io(outdf, out_io, out_format)

def convert_nonlinear(in_io, out_io, format="preV1506", owner_channel="Sony Channel"):
    "Convert nonlinear for Sony Channel"
    return None

