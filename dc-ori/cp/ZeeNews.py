import pandas as pd
import numpy as np
import datetime
import math
import csv
import re
import calendar
from collections import OrderedDict
import dc.utils
import dc.io
import dc.header
import dc.mapping
import dc.settings

def convert_linear(in_io, out_io, out_format="preV1506", owner_channel="Zee News"):
    "Convert linear schedule for Zee News"

    # Read input file
    indf = pd.read_excel(in_io, sheetname=0, skiprows=0)

    
    # Create output DataFrame
    outdf = pd.DataFrame(columns=dc.header.internalHeader)

    # Extract data from full title


    # Fill output DataFrame with related info
    outdf["TXDate"] = indf["Airing Date"].apply(lambda x: pd.to_datetime(x, format="%d/%m/%Y")).apply(lambda x: x.strftime("%Y%m%d"))
    outdf["ActualTime"] = indf["Airing Time"].apply(lambda x: pd.to_datetime(x, format="%H:%M:%S")).apply(lambda x:x.strftime("%H:%M"))
    outdf["BrandNameEng"] = indf["Program Title"].str.translate(dc.mapping.charTranslateTable)
    outdf["BrandNameChi"].fillna(outdf["BrandNameEng"], inplace=True)
    outdf["EpisodeNo"] = indf["Program Episode Number"]
    outdf["SynopsisEng"] = indf["Program Description"].str.translate(dc.mapping.charTranslateTable)
    outdf["SynopsisChi"].fillna(outdf["SynopsisEng"], inplace=True)
    outdf["ShortSynopsisEng"] = outdf["SynopsisEng"][outdf["SynopsisEng"].notnull()].apply(lambda x: x[0:dc.settings.char_limit.get("ShortSynopsisEng")])
    outdf["ShortSynopsisChi"].fillna(outdf["ShortSynopsisEng"], inplace=True)
    outdf["PremierOld"] = "0"
    outdf["Premier"] = "N"
    outdf["IsLive"] = "N"
    outdf["Genre"] = indf["Program Category"].apply(lambda x: str(x).split('&')[0].lower().strip()).map(dc.mapping.genreMapping).fillna(indf["Program Category"])
    outdf["SubGenre"] = indf["Program Sub Category"].str.lower().map(dc.mapping.subGenreMapping).str.join('/').fillna(indf["Program Sub Category"])
    outdf["OriginalLang"] = indf["Program Language"][indf["Program Language"].notnull()].astype(str).str.split('; ').apply(lambda audioLangs: [dc.mapping.audioLangMapping[audioLang.lower()] for audioLang in audioLangs]).str.join(', ')
    outdf["AudioLang"] = indf["Program Stereo"][indf["Program Stereo"].notnull()].astype(str).str.split('; ').apply(lambda audioLangs: [dc.mapping.audioLangMapping[audioLang.lower()] for audioLang in audioLangs]).str.join(', ')
    outdf["IsEpisodic"] = (outdf["EpisodeNo"].notnull() | outdf["EpisodeNameEng"].notnull()).map({True:"Y", False:"N"})
    dc.utils.title_df(outdf)
    outdf[["FullTitleEng", "FullTitleChi"]] = outdf.apply(dc.utils.constructFullTitle, axis=1)
    outdf[["ProgrammeNameEng", "ProgrammeNameChi"]] = outdf.apply(dc.utils.constructProgrammeName, axis=1)
    dc.utils.translateChar(outdf)
    dc.utils.chopCharacters(outdf)


    # Append EOF
    dc.utils.appendEOF(outdf)


    # Fill Channel #
    outdf['OwnerChannel'] = owner_channel
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)

    # Write output file
    dc.io.to_io(outdf, out_io, out_format)

def convert_nonlinear(in_io, out_io, format="preV1506", owner_channel="Zee News"):
    "Convert nonlinear for Zee News"
    return None

