import pandas as pd
import numpy as np
import datetime
import math
import csv
import re
import calendar
from collections import OrderedDict
import dc.utils
import dc.io
import dc.header
import dc.mapping
import dc.settings

def convert_linear(in_io, out_io, out_format="preV1506", owner_channel="MTV India"):
    "Convert linear schedule for MTV India"

    # Read input file
    indf = pd.read_excel(in_io, sheetname=0, skiprows=0)
    indf.columns = ["".join(s.split()).replace("(", "").replace(")", "").replace("/", "").lower() for s in indf.columns]

    
    # Create output DataFrame
    outdf = pd.DataFrame(columns=dc.header.internalHeader)

    # Extract data from full title


    # Fill output DataFrame with related info
    outdf["TXDate"] = indf["txdate"].apply(lambda x: pd.to_datetime(x, format="%d %b, %Y")).apply(lambda x:x.strftime("%Y%m%d"))
    outdf["ActualTime"] = indf["actualstarttime"].apply(lambda x: pd.to_datetime(x, format="%H:%M:%S")).apply(lambda x:x.strftime("%H:%M"))
    outdf["BrandNameEng"]=indf["fulltitleenglish"]
    outdf["BrandNameChi"]=indf["fulltitleenglish"]
    outdf["EpisodeNo"] = indf["programepisodenumber"][indf["programepisodenumber"].notnull()].astype(int).astype(str)
    outdf["SynopsisEng"] = indf["programepgtext"].str.translate(dc.mapping.charTranslateTable)
    outdf["SynopsisChi"] = outdf["SynopsisEng"]
    outdf["ShortSynopsisEng"] = outdf["SynopsisEng"][outdf["SynopsisEng"].notnull()].apply(lambda x: x[0:dc.settings.char_limit.get("ShortSynopsisEng")])
    outdf["ShortSynopsisChi"] = outdf["SynopsisChi"][outdf["SynopsisChi"].notnull()].apply(lambda x: x[0:dc.settings.char_limit.get("ShortSynopsisChi")])
    outdf["PremierOld"] = "0"
    outdf["Premier"] = "N"
    outdf["IsLive"] = "N"
    outdf["Genre"] = indf["programcategory"][indf["programcategory"].notnull()].str.lower().map(dc.mapping.genreMapping).fillna(indf["programcategory"])
    outdf["IsEpisodic"] = (outdf["EpisodeNo"].notnull() | outdf["EpisodeNameEng"].notnull()).map({True:"Y", False:"N"})
    dc.utils.title_df(outdf)
    outdf[["FullTitleEng", "FullTitleChi"]] = outdf.apply(dc.utils.constructFullTitle, axis=1)
    outdf[["ProgrammeNameEng", "ProgrammeNameChi"]] = outdf.apply(dc.utils.constructProgrammeName, axis=1)
    dc.utils.translateChar(outdf)
    dc.utils.chopCharacters(outdf)


    # Append EOF
    lastProgDuration = pd.Timedelta("1:00:00")
    dc.utils.appendEOF(outdf, lastprog_duration=lastProgDuration)


    # Fill Channel #
    outdf['OwnerChannel'] = owner_channel
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)

    # Write output file
    dc.io.to_io(outdf, out_io, out_format)

def convert_nonlinear(in_io, out_io, format="preV1506", owner_channel="MTV India"):
    "Convert nonlinear for MTV India"
    return None

