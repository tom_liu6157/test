import pandas as pd
import numpy as np
import datetime
import math
import csv
import re
import calendar
from collections import OrderedDict
import dc.utils
import dc.io
import dc.header
import dc.mapping
import dc.settings

def convert_linear(in_io, out_io, out_format="preV1506", owner_channel="China Business Network"):
    "Convert linear schedule for China Business Network"

    # Read input file
    names = ['Date','Time','ChiName','EngName','ChiSynopsis','EngSynopsis']
    indf = pd.read_excel(in_io, sheetname=0, skiprows=1, header=None, parse_cols=[0,1,3,5,8,9], names=names)
    count = 0
    for name in indf["ChiName"] :            
        if (pd.isnull(name)) :
            for key in indf :
                del indf[key][count]
        count = count + 1

    
    # Create output DataFrame
    outdf = pd.DataFrame(columns=dc.header.internalHeader)

    # Extract data from full title


    # Fill output DataFrame with related info
    outdf["TXDate"] = indf['Date'].apply(lambda x: pd.to_datetime(x, format="%Y%m%d")).apply(lambda x:x.strftime("%Y%m%d"))
    outdf["ActualTime"] = indf['Time'].apply(lambda x: pd.to_datetime(x, format="%H:%M:%S")).apply(lambda x:x.strftime("%H:%M"))
    start_hour = 6
    outdf["TXDate"] = outdf["TXDate"].where(outdf["ActualTime"].apply(lambda x: int(pd.to_datetime(x, format="%H:%M").strftime("%H"))) < start_hour).apply(lambda x: (pd.to_datetime(x, format="%Y%m%d") + pd.to_timedelta("1 days")).strftime("%Y%m%d") if pd.notnull(x) else None).fillna(outdf["TXDate"])
    outdf["Premier"] = "N"
    outdf["IsLive"] = "N"
    outdf["IsEpisodic"] = (outdf["EpisodeNo"].notnull() | outdf["EpisodeNameEng"].notnull()).map({True:"Y", False:"N"})
    outdf["SynopsisEng"] = indf['EngSynopsis'].str.translate(dc.mapping.charTranslateTable)
    outdf["SynopsisChi"] = indf['ChiSynopsis'][indf['ChiSynopsis'].notnull()].apply(lambda x: dc.utils.jianfan(x)).str.translate(dc.mapping.charTranslateTable)
    outdf["SynopsisChi"].fillna(outdf["SynopsisEng"], inplace=True)
    outdf["SynopsisEng"].fillna(outdf["SynopsisChi"], inplace=True)
    outdf["ShortSynopsisEng"] = outdf["SynopsisEng"][outdf["SynopsisEng"].notnull()].apply(lambda x: x[0:dc.settings.char_limit.get("ShortSynopsisEng")])
    outdf["ShortSynopsisChi"] = outdf["SynopsisChi"][outdf["SynopsisChi"].notnull()].apply(lambda x: dc.utils.jianfan(x)).apply(lambda x: x[0:dc.settings.char_limit.get("ShortSynopsisChi")])
    outdf["ShortSynopsisChi"].fillna(outdf["ShortSynopsisEng"], inplace=True)
    outdf["ShortSynopsisEng"].fillna(outdf["ShortSynopsisChi"], inplace=True)
    outdf["BrandNameEng"] = indf['EngName'].str.translate(dc.mapping.charTranslateTable)
    outdf["BrandNameChi"] = indf['ChiName'][indf['ChiName'].notnull()].apply(lambda x: dc.utils.jianfan(x)).str.translate(dc.mapping.charTranslateTable)
    outdf["BrandNameChi"].fillna(outdf["BrandNameEng"], inplace=True)
    outdf["BrandNameEng"].fillna(outdf["BrandNameChi"], inplace=True)
    dc.utils.title_df(outdf)
    outdf[["FullTitleEng", "FullTitleChi"]] = outdf.apply(dc.utils.constructFullTitle, axis=1)
    outdf[["ProgrammeNameEng", "ProgrammeNameChi"]] = outdf.apply(dc.utils.constructProgrammeName, axis=1)
    outdf["PremierOld"] = "0"
    dc.utils.translateChar(outdf)
    dc.utils.chopCharacters(outdf)


    # Append EOF
    x = pd.to_datetime(outdf["TXDate"] + " " + outdf["ActualTime"], format="%Y%m%d %H:%M")
    lastProgStartTimestamp = x.max()
    eofTimestamp = None
    oneDay = pd.Timedelta(1,"D")
    oneWeek = pd.Timedelta(1,"W")
    oneDayBefore = lastProgStartTimestamp - oneDay
    oneWeekBefore = lastProgStartTimestamp - oneWeek
    if x.min() <= oneWeekBefore:
        eofTimestamp = x[x > oneWeekBefore].min() + oneWeek
    elif x.min() <= oneDayBefore:
        eofTimestamp = x[x > oneDayBefore].min() + oneDay
    else:
        eofTimestamp = x.min() + oneDay
    outdf.loc[count, ["TXDate", "ActualTime", "FullTitleEng", "FullTitleChi", "BrandNameEng", "BrandNameChi", "ProgrammeNameEng", "ProgrammeNameChi"]] = [eofTimestamp.strftime("%Y%m%d"), eofTimestamp.strftime("%H:%M"), "END OF FILE", "END OF FILE", "END OF FILE", "END OF FILE", "END OF FILE", "END OF FILE"]


    # Fill Channel #
    outdf['OwnerChannel'] = owner_channel
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)

    # Write output file
    dc.io.to_io(outdf, out_io, out_format)

def convert_nonlinear(in_io, out_io, format="preV1506", owner_channel="China Business Network"):
    "Convert nonlinear for China Business Network"
    return None

