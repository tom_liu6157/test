import pandas as pd
import numpy as np
import datetime
import math
import csv
import re
import calendar
from collections import OrderedDict
import dc.utils
import dc.io
import dc.header
import dc.mapping
import dc.settings

def convert_linear(in_io, out_io, out_format="preV1506", owner_channel="TFC"):
    "Convert linear schedule for TFC"

    # Read input file
    indfs = pd.read_excel(in_io, sheetname=None)
    indf = pd.DataFrame()
    for key, value in indfs.items():
        indf = indf.append(value, ignore_index=True)
    indf.columns = ["".join(s.split()).replace("(", "").replace(")", "").replace("/", "").lower() for s in indf.columns]
    indf = indf[indf['fulltitleenglishmax40characters'].notnull()]
    indf.sort_values(by=['txdate', 'actualtime'], ascending=[1, 1], inplace=True)
    indf.index = range(0,len(indf))

    
    # Create output DataFrame
    outdf = pd.DataFrame(columns=dc.header.internalHeader)

    # Extract data from full title


    # Fill output DataFrame with related info
    excepted_column = ["PID", "Recordable", "Is NPVR Prog", "Is Restart TV", "effective_date", "expiration_date", "Media ID (max. 12 chars)"]
    for internal_column, indf_column in dc.header.postV1506HeaderMapping.items():
        if (indf_column not in excepted_column): 
            edited_indf_column = "".join(indf_column.split()).replace("(", "").replace(")", "").replace("/", "").lower()
            if (edited_indf_column in indf.columns):
                outdf[internal_column] = indf[edited_indf_column]
                if (outdf[internal_column].dtype == object):
                    outdf[internal_column] = outdf[internal_column].where(outdf[internal_column].apply(lambda x: pd.notnull(x) and len(str(x).strip()) > 0))
    outdf['ChannelNo'] = 725
    outdf["TXDate"] = outdf["TXDate"].apply(lambda x: pd.to_datetime(x, format="%y-%b-%d")).apply(lambda x: x.strftime("%Y%m%d"))
    outdf["ActualTime"] = outdf["ActualTime"].apply(lambda x: pd.to_datetime(x, format="%H:%M:%S")).apply(lambda x: x.strftime("%H:%M"))
    outdf['FirstReleaseYear'] = outdf['FirstReleaseYear'][outdf['FirstReleaseYear'].notnull()].apply(lambda x: str(int(x)))
    outdf["BrandNameEng"] = indf["brandnameenglish"].str.strip().str.extract("(.*?)\(.*?\)$", expand=False).str.strip()
    outdf["BrandNameEng"].fillna(indf["brandnameenglish"].str.strip(), inplace=True)
    outdf["BrandNameEng"] = outdf["BrandNameEng"].str.replace("®", "").str.strip()
    outdf["BrandNameChi"] = indf["brandnamechinese"].str.strip().str.extract("(.*?)\(.*?\)$", expand=False).str.strip()
    outdf["BrandNameChi"].fillna(indf["brandnamechinese"].str.strip(), inplace=True)
    outdf["BrandNameChi"] = outdf["BrandNameChi"].str.replace("®", "").str.strip()
    outdf["EpisodeNameEng"] = outdf["EpisodeNameEng"].apply(lambda x: x.strftime("%Y%m%d") if isinstance(x, datetime.date) else x)
    outdf["EpisodeNameChi"] = outdf["EpisodeNameChi"].apply(lambda x: x.strftime("%Y%m%d") if isinstance(x, datetime.date) else x)
    outdf["SponsorTextStuntChi"].fillna(outdf["SponsorTextStuntEng"], inplace=True)
    outdf["SponsorTextStuntEng"].fillna(outdf["SponsorTextStuntChi"], inplace=True)
    outdf["BrandNameChi"].fillna(outdf["BrandNameEng"], inplace=True)
    outdf["BrandNameEng"].fillna(outdf["BrandNameChi"], inplace=True)
    outdf["SeasonNameChi"].fillna(outdf["SeasonNameEng"], inplace=True)
    outdf["SeasonNameEng"].fillna(outdf["SeasonNameChi"], inplace=True)
    outdf["EpisodeNameChi"].fillna(outdf["EpisodeNameEng"], inplace=True)
    outdf["EpisodeNameEng"].fillna(outdf["EpisodeNameChi"], inplace=True)
    outdf["EpisodeNameChi"].fillna(outdf["EpisodeNameEng"], inplace=True)
    outdf["EpisodeNameEng"].fillna(outdf["EpisodeNameChi"], inplace=True)
    outdf["SynopsisChi"].fillna(outdf["SynopsisEng"], inplace=True)
    outdf["SynopsisEng"].fillna(outdf["SynopsisChi"], inplace=True)
    outdf["ShortSynopsisChi"].fillna(outdf["ShortSynopsisEng"], inplace=True)
    outdf["ShortSynopsisEng"].fillna(outdf["ShortSynopsisChi"], inplace=True)
    outdf["SeasonNo"] = outdf["SeasonNo"][outdf["SeasonNo"].notnull()].astype(int).astype(str)
    outdf["EpisodeNo"] = outdf["EpisodeNo"][outdf["EpisodeNo"].notnull()].astype(int).astype(str)
    outdf["PremierOld"] = "0"
    outdf["IsEpisodic"] = (outdf["EpisodeNo"].notnull() | outdf["EpisodeNameEng"].notnull() | outdf["EpisodeNameChi"].notnull()).map({True:"Y", False:"N"})
    text_column = ["SponsorTextStuntEng", "SponsorTextStuntChi", "BrandNameEng", "BrandNameChi", "EditionVersionEng", "EditionVersionChi", "SeasonNameEng", "SeasonNameChi", "EpisodeNameEng", "EpisodeNameChi", "SynopsisEng", "SynopsisChi", "ShortSynopsisEng", "ShortSynopsisChi", "DirectorProducerEng", "DirectorProducerChi", "CastEng", "CastChi"]
    for column in text_column:
        outdf[column] = outdf[column].astype(str).where(outdf[column].notnull()).str.translate(dc.mapping.charTranslateTable)
    dc.utils.title_df(outdf)
    outdf[["FullTitleEng", "FullTitleChi"]] = outdf.apply(dc.utils.constructFullTitle, axis=1)
    outdf[["ProgrammeNameEng", "ProgrammeNameChi"]] = outdf.apply(dc.utils.constructProgrammeName, axis=1)
    dc.utils.translateChar(outdf)
    dc.utils.chopCharacters(outdf)


    # Append EOF
    dc.utils.appendEOF(outdf)


    # Fill Channel #
    outdf['OwnerChannel'] = owner_channel
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)

    # Write output file
    dc.io.to_io(outdf, out_io, out_format)

def convert_nonlinear(in_io, out_io, format="preV1506", owner_channel="TFC"):
    "Convert nonlinear for TFC"
    return None

