import pandas as pd
import numpy as np
import datetime
import math
import csv
import re
import calendar
from collections import OrderedDict
import dc.utils
import dc.io
import dc.header
import dc.mapping
import dc.settings

def convert_linear(in_io, out_io, out_format="preV1506", owner_channel="KiMoChi Channel"):
    "Convert linear schedule for KiMoChi Channel"

    # Read input file
    indf = pd.read_excel(in_io, sheetname=1, skiprows=0, header=None)

    
    # Create output DataFrame
    outdf = pd.DataFrame(columns=dc.header.internalHeader)

    # Extract data from full title
    vdate = pd.Series()
    vtime = pd.Series()
    vtitleEng = pd.Series()
    vtitleChi = pd.Series()
    counter = 1
    numberofrecord = 0
    tempnumberofrecord = 0
    x = 1
    datecounter = 1
    titlecounter = 1
    emptyrow = False
    for index, row in indf.iterrows():
        if index > 1:
            if counter == 1:
                for temp in row:
                    if str(indf.loc[index,x]) == "nan":
                        emptyrow = True
                        counter = 3
                        break
                    temp = pd.to_datetime(indf.loc[0,x], format="%d%m%Y").strftime("%Y%m%d")
                    vdate = vdate.set_value(numberofrecord, temp)
                    temp = indf.loc[index,0].strftime("%H:%M")
                    vtime = vtime.set_value(numberofrecord, temp)
                    vtitleChi = vtitleChi.set_value(numberofrecord, indf.loc[index,x])
                    x = x + 2
                    numberofrecord = numberofrecord + 1
                    if x == len(indf.columns):
                        break
                x = 1
                tempvar = numberofrecord
                numberofrecord = tempnumberofrecord
                tempnumberofrecord = tempvar
                counter = 2
            else:
                if counter == 2:
                    for temp in row:
                        if emptyrow:
                            emtyrow = True
                            break
                        vtitleEng = vtitleEng.set_value(numberofrecord, indf.loc[index,x])
                        x = x + 2
                        numberofrecord = numberofrecord + 1
                        if x == len(indf.columns):
                            break
                    x = 1
                    counter = 3
                else:
                    if counter == 3:
                        counter = 1
                        emptyrow = False
                        continue


    # Fill output DataFrame with related info
    outdf["TXDate"] = vdate
    outdf["ActualTime"] = vtime
    outdf["BrandNameEng"] = vtitleEng
    outdf["BrandNameChi"] = vtitleChi
    outdf["Genre"] = "Movie"
    dc.utils.title_df(outdf)
    outdf[["FullTitleEng", "FullTitleChi"]] = outdf.apply(dc.utils.constructFullTitle, axis=1)
    outdf[["ProgrammeNameEng", "ProgrammeNameChi"]] = outdf.apply(dc.utils.constructProgrammeName, axis=1)
    outdf = outdf.sort_values(by=['TXDate','ActualTime'])
    dc.utils.translateChar(outdf)
    dc.utils.chopCharacters(outdf)


    # Append EOF
    dc.utils.appendEOF(outdf)


    # Fill Channel #
    outdf['OwnerChannel'] = owner_channel
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)

    # Write output file
    dc.io.to_io(outdf, out_io, out_format)

def convert_nonlinear(in_io, out_io, format="preV1506", owner_channel="KiMoChi Channel"):
    "Convert nonlinear for KiMoChi Channel"
    return None

