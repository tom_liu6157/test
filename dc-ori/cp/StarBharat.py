import pandas as pd
import numpy as np
import datetime
import math
import csv
import re
import calendar
from collections import OrderedDict
import dc.utils
import dc.io
import dc.header
import dc.mapping
import dc.settings

def convert_linear(in_io, out_io, out_format="preV1506", owner_channel="Star Bharat"):
    "Convert linear schedule for Star Bharat"

    # Read input file
    indf = pd.read_excel(in_io, sheetname=0)

    
    # Create output DataFrame
    outdf = pd.DataFrame(columns=dc.header.internalHeader)

    # Extract data from full title
    extractedInfo = pd.DataFrame(columns=["BrandNameEng", "EpisodeNameEng"])
    fullTitleEng = indf["Programme Name"][indf["Programme Name"].notnull()].apply(lambda x: str(x).strip())
    fullTitleChi = indf["Programme Name"][indf["Programme Name"].notnull()].apply(lambda x: str(x).strip())
    tempExtractedInfo = [
        fullTitleEng.str.extract("((?P<BrandNameEng>.+)?\s*(-))", expand=False),
        fullTitleEng.str.extract("(.*(?i)(-)\s*(?P<EpisodeNameEng>.*))", expand=False)
    ]
    extractedInfo["BrandNameEng"] = tempExtractedInfo[0]["BrandNameEng"].apply(lambda x: None if x == "" else x)
    extractedInfo["BrandNameEng"].fillna(fullTitleEng, inplace=True)
    extractedInfo["EpisodeNameEng"] = tempExtractedInfo[1]["EpisodeNameEng"].apply(lambda x: None if x == "" else x)


    # Fill output DataFrame with related info
    outdf["TXDate"] = indf["Start Date (HK - SG)"].apply(lambda x: pd.to_datetime(x, format="%Y-%m-%d")).apply(lambda x:x.strftime("%Y%m%d"))
    outdf["ActualTime"] = indf["Start Time (HK - SG)"].apply(lambda x: pd.to_datetime(x, format="%H:%M:%S")).apply(lambda x:x.strftime("%H:%M"))
    outdf["BrandNameEng"] = extractedInfo["BrandNameEng"]
    outdf["BrandNameChi"].fillna(outdf["BrandNameEng"], inplace=True)
    outdf["SynopsisEng"] = indf["Synopsis"].str.translate(dc.mapping.charTranslateTable)
    outdf["SynopsisChi"].fillna(outdf["SynopsisEng"], inplace=True)
    outdf["ShortSynopsisEng"] = outdf["SynopsisEng"][outdf["SynopsisEng"].notnull()].apply(lambda x: x[0:dc.settings.char_limit.get("ShortSynopsisEng")])
    outdf["ShortSynopsisChi"].fillna(outdf["ShortSynopsisEng"], inplace=True)
    outdf["PremierOld"] = "0"
    outdf["Premier"] = "N"
    outdf["IsLive"] = "N"
    outdf["IsEpisodic"] = (outdf["EpisodeNo"].notnull() | outdf["EpisodeNameEng"].notnull()).map({True:"Y", False:"N"})
    outdf["EpisodeNameEng"] = extractedInfo["EpisodeNameEng"]
    outdf["EpisodeNameChi"].fillna(outdf["EpisodeNameEng"], inplace=True)
    dc.utils.title_df(outdf)
    outdf[["FullTitleEng", "FullTitleChi"]] = outdf.apply(dc.utils.constructFullTitle, axis=1)
    outdf[["ProgrammeNameEng", "ProgrammeNameChi"]] = outdf.apply(dc.utils.constructProgrammeName, axis=1)
    dc.utils.translateChar(outdf)
    dc.utils.chopCharacters(outdf)


    # Append EOF
    temp =indf["Duration"][indf.last_valid_index()].strftime("%H:%M:%S")
    lastProgDuration = pd.Timedelta(temp)
    dc.utils.appendEOF(outdf, lastprog_duration=lastProgDuration)


    # Fill Channel #
    outdf['OwnerChannel'] = owner_channel
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)

    # Write output file
    dc.io.to_io(outdf, out_io, out_format)

def convert_nonlinear(in_io, out_io, format="preV1506", owner_channel="Star Bharat"):
    "Convert nonlinear for Star Bharat"
    return None

