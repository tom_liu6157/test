import pandas as pd
import numpy as np
import datetime
import math
import csv
import re
import calendar
from collections import OrderedDict
import dc.utils
import dc.io
import dc.header
import dc.mapping
import dc.settings

def convert_linear(in_io, out_io, out_format="preV1506", owner_channel="Da Ai"):
    "Convert linear schedule for Da Ai"

    # Read input file
    indf = pd.read_excel(in_io, sheetname=0)
    indf.dropna(subset=["TX Date", "Actual Start Time"], inplace=True)

    
    # Create output DataFrame
    outdf = pd.DataFrame(columns=dc.header.internalHeader)

    # Extract data from full title
    fullTitleEng = indf["Full Title (English) Max 40 character"][indf["Full Title (English) Max 40 character"].notnull()].apply(lambda x: str(x).strip())
    fullTitleChi = indf["Full Title (Chinese) Max 24 character"][indf["Full Title (Chinese) Max 24 character"].notnull()].apply(lambda x: str(x).strip())
    extractedInfo = pd.DataFrame(columns=["BrandNameEng", "BrandNameChi", "SeasonNo", "EpisodeNo", "EpisodeNameEng", "EpisodeNameChi"])
    tempExtractedInfo = [
        fullTitleEng.str.extract("(?P<BrandNameEng>.*?)\s+S(?P<SeasonNo>\d)\s(?P<EpisodeNo>\d+)\\s*:*\s*(?P<EpisodeNameEng>.*)", expand=False),
        fullTitleEng.str.extract("(?P<BrandNameEng>HOLLYWOOD ON SET)\s+(?P<EpisodeNo>\d+)", expand=False),
        fullTitleChi.str.extract("(?P<BrandNameChi>.*?)\((第(?P<SeasonNo>\d+)季)*第(?P<EpisodeNo>\d+)集\)", expand=False)
    ]
    extractedInfo["BrandNameEng"] = tempExtractedInfo[0]["BrandNameEng"]
    extractedInfo["BrandNameEng"].fillna(tempExtractedInfo[1]["BrandNameEng"], inplace=True)
    extractedInfo["BrandNameChi"] = tempExtractedInfo[2]["BrandNameChi"]


    # Fill output DataFrame with related info
    outdf["TXDate"] = indf["TX Date"].apply(lambda x: pd.to_datetime(x, format="%Y%m%d")).apply(lambda x:x.strftime("%Y%m%d"))
    outdf["ActualTime"] = indf["Actual Start Time"].apply(lambda x: pd.to_datetime(x, format="%H:%M:%S")).apply(lambda x:x.strftime("%H:%M"))
    outdf["BrandNameEng"] = extractedInfo["BrandNameEng"]
    outdf["BrandNameChi"] = extractedInfo["BrandNameChi"]
    outdf["BrandNameEng"].fillna(fullTitleEng, inplace=True)
    outdf["BrandNameChi"].fillna(fullTitleChi, inplace=True)
    outdf["BrandNameEng"] = outdf["BrandNameEng"].str.translate(dc.mapping.charTranslateTable)
    outdf["BrandNameChi"] = outdf["BrandNameChi"].str.translate(dc.mapping.charTranslateTable)
    outdf["BrandNameChi"].fillna(outdf["BrandNameEng"], inplace=True)
    outdf["BrandNameEng"].fillna(outdf["BrandNameChi"], inplace=True)
    outdf["SynopsisEng"] = indf["Program Synopsis (English) Max 180 character"].str.translate(dc.mapping.charTranslateTable)
    outdf["ShortSynopsisEng"] = outdf["SynopsisEng"][outdf["SynopsisEng"].notnull()].apply(lambda x: x[0:dc.settings.char_limit.get("ShortSynopsisEng")])
    outdf["SynopsisChi"] = indf["Program Synopsis (Chinese) Max 130 character"].str.translate(dc.mapping.charTranslateTable)
    outdf["ShortSynopsisChi"] = outdf["SynopsisChi"][outdf["SynopsisChi"].notnull()].apply(lambda x: x[0:dc.settings.char_limit.get("ShortSynopsisChi")])
    outdf["SynopsisChi"].fillna(outdf["SynopsisEng"], inplace=True)
    outdf["SynopsisEng"].fillna(outdf["SynopsisChi"], inplace=True)
    outdf["ShortSynopsisChi"].fillna(outdf["ShortSynopsisEng"], inplace=True)
    outdf["ShortSynopsisEng"].fillna(outdf["ShortSynopsisChi"], inplace=True)
    outdf["PremierOld"] = "0"
    outdf["Premier"] = "N"
    outdf["IsLive"] = "N"
    outdf["Classification"] = indf["Classification"]
    outdf["IsEpisodic"] = (outdf["EpisodeNo"].notnull() | outdf["EpisodeNameEng"].notnull() | outdf["EpisodeNameChi"].notnull()).map({True:"Y", False:"N"})
    dc.utils.title_df(outdf)
    outdf[["FullTitleEng", "FullTitleChi"]] = outdf.apply(dc.utils.constructFullTitle, axis=1)
    outdf[["ProgrammeNameEng", "ProgrammeNameChi"]] = outdf.apply(dc.utils.constructProgrammeName, axis=1)
    dc.utils.translateChar(outdf)
    dc.utils.chopCharacters(outdf)


    # Append EOF
    dc.utils.appendEOF(outdf)


    # Fill Channel #
    outdf['OwnerChannel'] = owner_channel
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)

    # Write output file
    dc.io.to_io(outdf, out_io, out_format)

def convert_nonlinear(in_io, out_io, format="preV1506", owner_channel="Da Ai"):
    "Convert nonlinear for Da Ai"
    return None

