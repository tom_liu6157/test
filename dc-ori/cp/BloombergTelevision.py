import pandas as pd
import numpy as np
import datetime
import math
import csv
import re
import calendar
from collections import OrderedDict
import dc.utils
import dc.io
import dc.header
import dc.mapping
import dc.settings

def convert_linear(in_io, out_io, out_format="preV1506", owner_channel="Bloomberg Television"):
    "Convert linear schedule for Bloomberg Television"

    # Read input file
    indf = pd.read_excel(in_io, sheetname=0)

    
    # Create output DataFrame
    outdf = pd.DataFrame(columns=dc.header.internalHeader)

    # Extract data from full title
    fullTitleEng = indf["Program"].str.strip()
    extractedInfo = fullTitleEng.str.extract("(?P<BrandNameEng>.*?):*\s*(S(?P<SeasonNo>\d+))*\s*(Ep\s*(?P<EpisodeNo>\d+))\s*(?P<EpisodeNameEng>.*)", expand=False)
    extractedInfo["BrandNameEng"].fillna(fullTitleEng, inplace=True)


    # Fill output DataFrame with related info
    outdf["TXDate"] = indf["Date"].apply(lambda x:x.strftime("%Y%m%d"))
    outdf["ActualTime"] = indf['Time'].apply(lambda x: x.strftime("%H:%M"))
    outdf["BrandNameEng"] = extractedInfo["BrandNameEng"]
    outdf["BrandNameChi"].fillna(outdf["BrandNameEng"], inplace=True)
    outdf["SeasonNo"] = extractedInfo["SeasonNo"]
    outdf["EpisodeNo"] = extractedInfo["EpisodeNo"]
    outdf["EpisodeNameEng"] = extractedInfo["EpisodeNameEng"]
    outdf["EpisodeNameChi"].fillna(outdf["EpisodeNameEng"], inplace=True)
    outdf["SynopsisEng"] = indf["Synopsis"]
    outdf["SynopsisChi"].fillna(outdf["SynopsisEng"], inplace=True)
    outdf["ShortSynopsisEng"] = outdf["SynopsisEng"]
    outdf["ShortSynopsisChi"] = outdf["SynopsisChi"]
    outdf["PremierOld"] = "0"
    outdf["Premier"] = "N"
    outdf["IsLive"] = "N"
    outdf["IsEpisodic"] = (outdf["EpisodeNo"].notnull() | outdf["EpisodeNameEng"].notnull()).map({True:"Y", False:"N"})
    outdf["Genre"] = "News/Finance"
    text_column = ["SponsorTextStuntEng", "SponsorTextStuntChi", "BrandNameEng", "BrandNameChi", "EditionVersionEng", "EditionVersionChi", "SeasonNameEng", "SeasonNameChi", "EpisodeNameEng", "EpisodeNameChi", "SynopsisEng", "SynopsisChi", "ShortSynopsisEng", "ShortSynopsisChi", "DirectorProducerEng", "DirectorProducerChi", "CastEng", "CastChi"]
    for column in text_column:
        outdf[column] = outdf[column].astype(str).where(outdf[column].notnull()).str.translate(dc.mapping.charTranslateTable)
    dc.utils.title_df(outdf)
    outdf[["FullTitleEng", "FullTitleChi"]] = outdf.apply(dc.utils.constructFullTitle, axis=1)
    outdf[["ProgrammeNameEng", "ProgrammeNameChi"]] = outdf.apply(dc.utils.constructProgrammeName, axis=1)
    dc.utils.translateChar(outdf)
    dc.utils.chopCharacters(outdf)


    # Append EOF
    dailyStartTime = pd.to_datetime("00:00", format="%H:%M")
    dc.utils.appendEOF(outdf, daily_starttime=dailyStartTime)


    # Fill Channel #
    outdf['OwnerChannel'] = owner_channel
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)

    # Write output file
    dc.io.to_io(outdf, out_io, out_format)

def convert_nonlinear(in_io, out_io, format="preV1506", owner_channel="Bloomberg Television"):
    "Convert nonlinear for Bloomberg Television"
    return None

