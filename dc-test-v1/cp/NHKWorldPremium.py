import pandas as pd
import numpy as np
import datetime
import math
import csv
import re
import calendar
from collections import OrderedDict
import dc.utils
import dc.io
import dc.header
import dc.mapping
import dc.settings

def convert_linear(in_io, out_io, out_format="preV1506", owner_channel="NHK World Premium"):
    "Convert linear schedule for NHK World Premium"

    # Read input file
    indf = pd.read_excel(in_io, sheetname=0, skiprows=9)

    
    # Create output DataFrame
    outdf = pd.DataFrame(columns=dc.header.internalHeader)

    # Extract data from full title
    extractedInfo = pd.DataFrame(columns=["SponsorTextStuntEng", "BrandNameEng", "EpisodeNo", "EditionVersionEng", "EpisodeNameEng"])
    fullTitleEng = indf["Title"].apply(lambda x: str(x).strip())
    tempExtractedInfo = [
        fullTitleEng.str.extract("((?P<SponsorTextStuntEng>.+):)*", expand=False),
        fullTitleEng.str.extract("(.*(?i)(Episode|eps|ep)\.?\s*(?P<EpisodeNo>\d+))*", expand=False),
        fullTitleEng.str.extract("(.*\((?P<EditionVersionEng>[\w\s]{2,})\))*", expand=False),
        fullTitleEng.str.extract("(.*(?i)(part)\s*(?P<EpisodeNameEng>\d+))*", expand=False)
    ]
    extractedInfo["SponsorTextStuntEng"] = tempExtractedInfo[0]["SponsorTextStuntEng"].apply(lambda x: None if x == "" else x)
    extractedInfo["EpisodeNo"] = tempExtractedInfo[1]["EpisodeNo"].apply(lambda x: None if x == "" else x)
    extractedInfo["EditionVersionEng"] = tempExtractedInfo[2]["EditionVersionEng"].apply(lambda x: None if x == "" else x)
    extractedInfo["EpisodeNameEng"] = tempExtractedInfo[3]["EpisodeNameEng"].apply(lambda x: None if x == "" else x)


    # Fill output DataFrame with related info
    outdf["BrandNameEng"]=indf["Title"].apply(lambda x:re.sub(".+:\s*", "", x)).apply(lambda x:re.sub("\s*(Episode|eps|ep)\.?\s*\d+.*", "", x, flags=re.I)).apply(lambda x:re.sub("\s*\(.*\)", "", x)).apply(lambda x:re.sub("\s*Part\s*\d+.*", "", x, flags=re.I))
    outdf["SponsorTextStuntEng"] = extractedInfo["SponsorTextStuntEng"][extractedInfo["SponsorTextStuntEng"].notnull()].astype(str).str.translate(dc.mapping.charTranslateTable)
    outdf["BrandNameEng"] = outdf["BrandNameEng"][outdf["BrandNameEng"].notnull()].astype(str).str.translate(dc.mapping.charTranslateTable)
    outdf["EpisodeNo"] = extractedInfo["EpisodeNo"]
    outdf["EditionVersionEng"] = extractedInfo["EditionVersionEng"][extractedInfo["EditionVersionEng"].notnull()].astype(str).str.translate(dc.mapping.charTranslateTable)
    if extractedInfo["EpisodeNameEng"][extractedInfo["EpisodeNameEng"].notnull()].count() > 0:
        outdf["EpisodeNameEng"] = extractedInfo["EpisodeNameEng"][extractedInfo["EpisodeNameEng"].notnull()].apply(lambda x:"Part " + x).str.translate(dc.mapping.charTranslateTable)
    outdf["SponsorTextStuntChi"] = outdf["SponsorTextStuntEng"]
    outdf["BrandNameChi"] = outdf["BrandNameEng"]
    outdf["EditionVersionChi"] = outdf["EditionVersionEng"]
    outdf["EpisodeNameChi"] = outdf["EpisodeNameEng"]
    outdf["TXDate"] = indf["Date.1"].apply(lambda x: pd.to_datetime(x, format="%Y%m%d %H%M%S")).apply(lambda x:x.strftime("%Y%m%d"))
    outdf["ActualTime"] = indf["Start.1"].astype('str').str.slice(-8).str.slice(0, 5).str.zfill(5)
    outdf["SynopsisEng"] = indf["Synopsis"][indf["Synopsis"].notnull()].astype(str).str.translate(dc.mapping.charTranslateTable)
    outdf["SynopsisChi"] = indf["Synopsis"][indf["Synopsis"].notnull()].astype(str).str.translate(dc.mapping.charTranslateTable)
    outdf["ShortSynopsisEng"] = outdf["SynopsisEng"][outdf["SynopsisEng"].notnull()].astype(str).apply(lambda x: x[0:dc.settings.char_limit.get("ShortSynopsisEng")]).str.translate(dc.mapping.charTranslateTable)
    outdf["ShortSynopsisChi"] = outdf["SynopsisChi"][outdf["SynopsisChi"].notnull()].astype(str).apply(lambda x: x[0:dc.settings.char_limit.get("ShortSynopsisChi")]).str.translate(dc.mapping.charTranslateTable)
    outdf["Genre"] = indf["Genre"][indf["Genre"].notnull()].astype(str).str.lower().map(dc.mapping.genreMapping).fillna(indf["Genre"])
    outdf["IsEpisodic"] = (outdf["EpisodeNo"].notnull() | outdf["EpisodeNameEng"].notnull() | outdf["EpisodeNameChi"].notnull()).map({True:"Y", False:"N"})
    outdf["PremierOld"] = "0"
    outdf["Premier"] = "N"
    outdf["IsLive"] = "N"
    dc.utils.title_df(outdf)
    outdf[["FullTitleEng", "FullTitleChi"]] = outdf.apply(dc.utils.constructFullTitle, axis=1)
    outdf[["ProgrammeNameEng", "ProgrammeNameChi"]] = outdf.apply(dc.utils.constructProgrammeName, axis=1)
    dc.utils.translateChar(outdf)
    dc.utils.chopCharacters(outdf)


    # Append EOF
    temp = indf["hh:mm:ss"][indf.last_valid_index()].strftime('%H:%M:%S')
    lastProgDuration = pd.Timedelta(temp)
    dc.utils.appendEOF(outdf, lastprog_duration=lastProgDuration)


    # Fill Channel #
    outdf['OwnerChannel'] = owner_channel
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)

    # Write output file
    dc.io.to_io(outdf, out_io, out_format)

def convert_nonlinear(in_io, out_io, format="preV1506", owner_channel="NHK World Premium"):
    "Convert nonlinear for NHK World Premium"
    return None

