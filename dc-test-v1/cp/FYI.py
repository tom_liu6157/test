import pandas as pd
import numpy as np
import datetime
import math
import csv
import re
import calendar
from collections import OrderedDict
import dc.utils
import dc.io
import dc.header
import dc.mapping
import dc.settings

def convert_linear(in_io, out_io, out_format="preV1506", owner_channel="FYI"):
    "Convert linear schedule for FYI"

    # Read input file
    indf = pd.read_excel(in_io, sheetname=0, na_values=["n/a"], keep_default_na=True, skiprows=0)
    indf.columns = ["".join(s.split()).replace("(", "").replace(")", "").replace("/", "").lower() for s in indf.columns]

    
    # Create output DataFrame
    outdf = pd.DataFrame(columns=dc.header.internalHeader)

    # Extract data from full title


    # Fill output DataFrame with related info
    excepted_column = ["PID", "Premier", "Recordable", "Is NPVR Prog", "Is Restart TV", "effective_date", "expiration_date", "Media ID (max. 12 chars)"]
    for internal_column, indf_column in dc.header.postV1506HeaderMapping.items():
        if (indf_column not in excepted_column): 
            edited_indf_column = "".join(indf_column.split()).replace("(", "").replace(")", "").replace("/", "").lower()
            if (edited_indf_column in indf.columns):
                outdf[internal_column] = indf[edited_indf_column]
                if (outdf[internal_column].dtype == object):
                    outdf[internal_column] = outdf[internal_column].where(outdf[internal_column].apply(lambda x: pd.notnull(x) and len(str(x).strip()) > 0))
    outdf["TXDate"] = outdf["TXDate"].apply(lambda x: str(x) if type(x) is int else x.astype(int).astype(str) if type(x) is str else x.strftime("%Y%m%d"))
    outdf["ActualTime"] = outdf["ActualTime"].apply(lambda x: x if type(x) is str else x.strftime("%H:%M"))
    fullNameEng = indf["fulltitleenglishmax40characters"].str.strip()
    fullNameChi = indf["fulltitlechinesemax24characters"].str.strip()
    brandNameEng = indf["brandnameenglish"][indf["brandnameenglish"].notnull()].apply(lambda x: str(x).strip())
    brandNameChi = indf["brandnamechinese"][indf["brandnamechinese"].notnull()].apply(lambda x: str(x).strip())
    extractedInfo = pd.DataFrame(columns=["BrandNameEng", "BrandNameChi", "SeasonNo", "EpisodeNo", "EpisodeNameChi", "EpisodeNameEng"])
    tempExtractedInfo = [
        brandNameChi.str.extract("(?P<BrandNameChi>.*?)\s?[\(（]+S(?P<SeasonNo>\d+)[\)）]+$", expand=False)
        ,brandNameChi.str.extract("(?P<BrandNameChi>.*)", expand=False)
        ,brandNameEng.str.extract("(?P<BrandNameEng>.*?)\s?\(+S(?P<SeasonNo>\d+)\)+$", expand=False)
        ,brandNameEng.str.extract("(?P<BrandNameEng>.*)", expand=False)
        ,fullNameChi.str.extract("(?P<BrandNameChi>.*?)-(?P<EpisodeNameChi>.+?)$", expand=False)
    ]
    extractedInfo["BrandNameChi"]= tempExtractedInfo[0]["BrandNameChi"]
    extractedInfo["BrandNameChi"].fillna(tempExtractedInfo[1], inplace=True)
    extractedInfo["BrandNameEng"]= tempExtractedInfo[2]["BrandNameEng"]
    extractedInfo["BrandNameEng"].fillna(tempExtractedInfo[3], inplace=True)
    outdf["EpisodeNameChi"].fillna(tempExtractedInfo[4]["EpisodeNameChi"], inplace=True)
    outdf["EpisodeNameEng"] = outdf["EpisodeNameEng"].str.replace("^#\s?\d*\s?", "")
    outdf["EpisodeNameChi"] = outdf["EpisodeNameChi"].str.replace("^#\s?\d*\s?", "")
    outdf["SponsorTextStuntChi"].fillna(outdf["SponsorTextStuntEng"], inplace=True)
    outdf["SponsorTextStuntEng"].fillna(outdf["SponsorTextStuntChi"], inplace=True)
    outdf["BrandNameChi"] = extractedInfo["BrandNameChi"]
    outdf["BrandNameEng"] = extractedInfo["BrandNameEng"]
    outdf["BrandNameChi"].fillna(outdf["BrandNameEng"], inplace=True)
    outdf["BrandNameEng"].fillna(outdf["BrandNameChi"], inplace=True)
    outdf["SeasonNameChi"].fillna(outdf["SeasonNameEng"], inplace=True)
    outdf["SeasonNameEng"].fillna(outdf["SeasonNameChi"], inplace=True)
    outdf["EpisodeNameChi"].fillna(outdf["EpisodeNameEng"], inplace=True)
    outdf["EpisodeNameEng"].fillna(outdf["EpisodeNameChi"], inplace=True)
    outdf["SynopsisChi"].fillna(outdf["SynopsisEng"], inplace=True)
    outdf["SynopsisEng"].fillna(outdf["SynopsisChi"], inplace=True)
    outdf["ShortSynopsisEng"].fillna(outdf["SynopsisEng"][outdf["SynopsisEng"].notnull()].apply(lambda x: str(x)[0:dc.settings.char_limit.get("ShortSynopsisEng")]), inplace=True)
    outdf["ShortSynopsisChi"].fillna(outdf["SynopsisChi"][outdf["SynopsisChi"].notnull()].apply(lambda x: str(x)[0:dc.settings.char_limit.get("ShortSynopsisChi")]), inplace=True) 
    outdf["ShortSynopsisChi"].fillna(outdf["ShortSynopsisEng"], inplace=True)
    outdf["ShortSynopsisEng"].fillna(outdf["ShortSynopsisChi"], inplace=True)
    outdf["SeasonNo"] = outdf["SeasonNo"][outdf["SeasonNo"].notnull()].astype(int).astype(str)
    outdf["EpisodeNo"] = outdf["EpisodeNo"][outdf["EpisodeNo"].notnull()].astype(int).astype(str)
    outdf['IsLive'] = outdf['IsLive'].replace("No","N")
    outdf['IsLive'] = outdf['IsLive'].replace("Yes","Y")
    outdf["IsEpisodic"] = (outdf["EpisodeNo"].notnull() | outdf["EpisodeNameEng"].notnull() | outdf["EpisodeNameChi"].notnull()).map({True:"Y", False:"N"})
    text_column = ["SponsorTextStuntEng", "SponsorTextStuntChi", "BrandNameEng", "BrandNameChi", "EditionVersionEng", "EditionVersionChi", "SeasonNameEng", "SeasonNameChi", "EpisodeNameEng", "EpisodeNameChi", "SynopsisEng", "SynopsisChi", "ShortSynopsisEng", "ShortSynopsisChi", "DirectorProducerEng", "DirectorProducerChi", "CastEng", "CastChi"]
    for column in text_column:
        outdf[column] = outdf[column].astype(str).where(outdf[column].notnull()).str.translate(dc.mapping.charTranslateTable)
    dc.utils.title_df(outdf)
    outdf[["FullTitleEng", "FullTitleChi"]] = outdf.apply(dc.utils.constructFullTitle, axis=1)
    outdf[["ProgrammeNameEng", "ProgrammeNameChi"]] = outdf.apply(dc.utils.constructProgrammeName, axis=1)
    dc.utils.translateChar(outdf)
    dc.utils.chopCharacters(outdf)


    # Append EOF
    dc.utils.appendEOF(outdf)


    # Fill Channel #
    outdf['OwnerChannel'] = owner_channel
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)

    # Write output file
    dc.io.to_io(outdf, out_io, out_format)

def convert_nonlinear(in_io, out_io, format="preV1506", owner_channel="FYI"):
    "Convert nonlinear for FYI"
    return None

