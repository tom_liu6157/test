import pandas as pd
import numpy as np
import datetime
import math
import csv
import re
import calendar
from collections import OrderedDict
import dc.utils
import dc.io
import dc.header
import dc.mapping
import dc.settings

def convert_linear(in_io, out_io, out_format="preV1506", owner_channel="Setanta Sports Channel"):
    "Convert linear schedule for Setanta Sports Channel"

    # Read input file
    indf = pd.read_excel(in_io, sheetname=0, skiprows=0, skip_footer=0)
    count = 0
    for name in indf["TX Date"] :            
        if (pd.isnull(name) or name == "Brand_Name_English" or re.sub("\s","",str(name)).upper() == '00:00:00') :
            for key in indf :
                del indf[key][count]
        count = count + 1

    
    # Create output DataFrame
    outdf = pd.DataFrame(columns=dc.header.internalHeader)

    # Extract data from full title


    # Fill output DataFrame with related info
    known_episodenames = ["Highlights Show",
                          "Highlights GP 14",
                          "HighlightsGP 14"
    ]
    known_episode_seq = ("(","|".join([known_episodename for known_episodename in known_episodenames]),")")
    known_episode_regex = "".join(known_episode_seq)
    tempExtractString4 = indf["Full Title (English)\nMax 40 Characters"].str.extract("(?P<BrandNameEng>.*" +")(?P<EpisodeNameEng>" + known_episode_regex + ".*)$", expand=False)
    outdf["BrandNameEng"] = tempExtractString4["BrandNameEng"]
    outdf["EpisodeNameEng"] = tempExtractString4["EpisodeNameEng"][tempExtractString4["EpisodeNameEng"].notnull()].astype('str').str.strip()
    known_brandnames5 = ["2017",
                       "2018"
    ]
    known_brand_seq5 = ("(","|".join([known_brandname for known_brandname in known_brandnames5]),")")
    known_brand_regex5 = "".join(known_brand_seq5)
    tempExtractString5 = indf["Full Title (English)\nMax 40 Characters"].str.extract("(?P<BrandNameEng>.*?" + known_brand_regex5 + ")(?P<EpisodeNameEng>[^/]" +".*)$", expand=False)
    outdf["BrandNameEng"].fillna(tempExtractString5["BrandNameEng"], inplace=True)
    outdf["EpisodeNameEng"].fillna(tempExtractString5["EpisodeNameEng"], inplace=True)
    known_brandnames = ["Aviva Premiership Highlights",
                        "Aviva Premiership Highlight",
                        "Autumn Internationals",
                        "Awards Highlights",
                        "Bledisloe Cup Test",
                        "Broncos Insider",
                        "B & I Lions Tour 2017",
                        "Challenge Cup 2015",
                        "Challenge Cup 2014",
                        "Currie Cup 2017",
                        "Currie Cup 2016",
                        "Currie Cup 2015",
                        "Currie Cup",
                        "European Challenge Cup 15/16",
                        "European Challenge Cup  16/17",
                        "European Challenge Cup 16/17",
                        "European Champions Cup 15/16",
                        "European Champions Cup 16/17",
                        "Guinness Pro 12 Highlights",
                        "International Rugby League",
                        "International Rugby",
                        "International  Rugby",
                        "Itm Cup 2016",
                        "Itm Cup 2015",
                        "ITM Cup",
                        "Japanese Top League",
                        "Mitre 10 Cup 2017",
                        "Mitre 10 Cup",
                        "NRL Fulltime",
                        "NRL Holden Cup",
                        "NRL 2016 Holden Cup",
                        "Pro 12 D2", 
                        "Rugby Championship 2017",
                        "Rugby Championship 2016",
                        "Rugby Friendly",
                        "Rugby League Championship",
                        "Rugby League Four Nations",
                        "Rugby League Test",
                        "Rugby Sevens Highlights",
                        "Rugby Summer Tours 2018",
                        "Rugby Asia",
                        "Super League Rugby 2016",
                        "Super League Rugby 2015",
                        "Super League Fulltime",
                        "Super Rugby 2016",  
                        "Super Rugby 2015",
                        "SANZAR Classic",
                        "Summer Tour Rugby",
                        "State of Origin 2016",
                        "State of Origin",
                        "Top 14 Highlights",
                        "Top 14 Rugby",
                        "Super League Rugby 2017",
                        "Super Rugby 2017",
                        "Women's French Domestic League",
                        "Womens 6 Nations",
                        "NRL 2017",
                        "World Rugby 2018",
                        "World Rugby"
                       ]
    known_brandnames2 = ["The Rugby Championship", 
                        "Aviva Premiership",
                        "European Challenge Cup", 
                        "European Champions Cup",
                        "Guinness Pro 12",
                        "NRL 2016",
                        "Super League Rugby",
                        "Super League",
                        "Top 14",
                        "Guinness Pro 14 Highlights",
                        "Guinness Pro 14",
                        "Super Rugby 2018",
                        "Sanzar Classics",
                        "Setanta Sports"]
    known_brandnames3 = ["Challenge Cup",
                        "Nrl",
                        "NRL",
                        "TBC",
                        "European Champions"]
    known_brand_seq = ("(","|".join([known_brandname for known_brandname in known_brandnames]),")")
    known_brand_seq2 = ("(","|".join([known_brandname for known_brandname in known_brandnames2]),")")
    known_brand_seq3 = ("(","|".join([known_brandname for known_brandname in known_brandnames3]),")")
    known_brand_regex = "".join(known_brand_seq)
    known_brand_regex2 = "".join(known_brand_seq2)
    known_brand_regex3 = "".join(known_brand_seq3)
    tempExtractString = indf["Full Title (English)\nMax 40 Characters"].str.extract("(?P<BrandNameEng>.*?" + known_brand_regex + ")(?P<EpisodeNameEng>.*)$", expand=False)
    tempExtractString2 = indf["Full Title (English)\nMax 40 Characters"].str.extract("(?P<BrandNameEng>.*?" + known_brand_regex2 + ")(?P<EpisodeNameEng>.*)$", expand=False)
    tempExtractString3 = indf["Full Title (English)\nMax 40 Characters"].str.extract("(?P<BrandNameEng>.*?" + known_brand_regex3 + ")(?P<EpisodeNameEng>.*)$", expand=False)
    outdf["BrandNameEng"].fillna(tempExtractString["BrandNameEng"], inplace=True)
    outdf["BrandNameEng"].fillna(tempExtractString2["BrandNameEng"], inplace=True)
    outdf["BrandNameEng"].fillna(tempExtractString3["BrandNameEng"], inplace=True)
    outdf["EpisodeNameEng"].fillna(tempExtractString["EpisodeNameEng"].str.strip(), inplace=True)
    outdf["EpisodeNameEng"].fillna(tempExtractString2["EpisodeNameEng"], inplace=True)
    outdf["EpisodeNameEng"].fillna(tempExtractString3["EpisodeNameEng"], inplace=True)
    outdf["BrandNameEng"].fillna(indf["Full Title (English)\nMax 40 Characters"], inplace=True)
    outdf["IsLive"] = outdf[["BrandNameEng","EpisodeNameEng"]].apply(lambda x: "Y" if str(x["BrandNameEng"]).strip().startswith("(L)") or str(x["BrandNameEng"]).strip().startswith("LIVE") or str(x["EpisodeNameEng"]).strip().startswith("(L)") or str(x["EpisodeNameEng"]).strip().startswith("LIVE") else "N", axis=1)
    outdf["EpisodeNameEng"] = outdf["EpisodeNameEng"][outdf["EpisodeNameEng"].notnull()].astype('str').str.replace("LIVE", "").str.replace("\(L\)", "").str.translate(dc.mapping.charTranslateTable).str.strip().str.split(" v ").apply(lambda x: [name.upper() if re.match('^[^a-zA-Z]*([a-zA-Z]{2,3})[^a-zA-Z]*$', name) is not None else name for name in x] if len(x) > 1 else x).str.join(" v ")
    outdf["BrandNameEng"] = outdf["BrandNameEng"][outdf["BrandNameEng"].notnull()].astype('str').str.replace("LIVE", "").str.replace("\(L\)", "").str.translate(dc.mapping.charTranslateTable).str.strip()
    outdf["TXDate"] = indf["TX Date"].apply(lambda x: pd.to_datetime(x, format="%Y/%m/%d %H:%M:%S")).apply(lambda x:x.strftime("%Y%m%d"))
    outdf["ActualTime"] = indf["Actual Time"].astype('str').str.slice(-8).str.slice(0, 5).str.zfill(5)
    outdf["BrandNameChi"].fillna(outdf["BrandNameEng"], inplace=True)
    outdf["EpisodeNameChi"].fillna(outdf["EpisodeNameEng"], inplace=True)
    outdf["SynopsisEng"] = indf["Programme Synopsis (English) \nMax 180 Characters"][indf["Programme Synopsis (English) \nMax 180 Characters"].notnull()].str.translate(dc.mapping.charTranslateTable).apply(lambda x: x[0:dc.settings.char_limit.get("SynopsisEng")])
    outdf["SynopsisChi"].fillna(outdf["SynopsisEng"], inplace=True)
    outdf["ShortSynopsisEng"] = outdf["SynopsisEng"][outdf["SynopsisEng"].notnull()].apply(lambda x: x[0:dc.settings.char_limit.get("ShortSynopsisEng")])
    outdf["ShortSynopsisChi"] = outdf["SynopsisChi"][outdf["SynopsisChi"].notnull()].apply(lambda x: x[0:dc.settings.char_limit.get("ShortSynopsisChi")])
    outdf["PremierOld"] = indf["Premier"][indf["Premier"].notnull()].apply(lambda x: "1" if x==1 else "0")
    outdf["PremierOld"].fillna("0", inplace=True)
    outdf["Premier"] = indf["Premier"][indf["Premier"].notnull()].apply(lambda x: "Y" if x==1 else "N")
    outdf["Premier"].fillna("N", inplace=True)
    outdf["Genre"] = indf["Genre"][indf["Genre"].notnull()].str.lower().map(dc.mapping.genreMapping).fillna(indf["Genre"])
    outdf["SubGenre"] = indf["Sub-Genre"].str.split("/")[indf["Sub-Genre"].str.split("/").notnull()].apply(lambda subGenres: sorted(set([item for subGenre in subGenres for item in dc.mapping.subGenreMapping.get(subGenre.lower().strip(), [subGenre])]))).str.join("/").fillna(indf["Sub-Genre"]).apply(lambda x: x[0:dc.settings.char_limit.get("SubGenre")])
    outdf["IsEpisodic"].fillna(((outdf["EpisodeNo"].notnull() & ~outdf["EpisodeNo"].astype(str).str.match("^[\s]*$")) | (outdf["EpisodeNameEng"].notnull() & ~outdf["EpisodeNameEng"].astype(str).str.match("^[\s]*$")) | (outdf["EpisodeNameChi"].notnull() & ~outdf["EpisodeNameChi"].astype(str).str.match("^[\s]*$"))).map({True:"Y", False:"N"}), inplace=True)
    outdf["IsEpisodic"] = outdf[["IsEpisodic","SubGenre","EpisodeNo","EpisodeNameEng","EpisodeNameChi"]].apply(lambda x : "N" if "Match".upper() in re.sub('\s','',str(x["SubGenre"])).upper().split("/") else x["IsEpisodic"],axis=1)
    outdf["RecordableForCatchUp"] = indf['SVOD'].replace("Yes","Y")
    outdf[["FullTitleEng", "FullTitleChi"]] = outdf.apply(dc.utils.constructFullTitle, axis=1)
    outdf[["ProgrammeNameEng", "ProgrammeNameChi"]] = outdf.apply(dc.utils.constructProgrammeName, axis=1)
    dc.utils.translateChar(outdf)
    dc.utils.chopCharacters(outdf)


    # Append EOF
    dc.utils.appendEOF(outdf)


    # Fill Channel #
    outdf['OwnerChannel'] = owner_channel
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)

    # Write output file
    dc.io.to_io(outdf, out_io, out_format)

def convert_nonlinear(in_io, out_io, format="preV1506", owner_channel="Setanta Sports Channel"):
    "Convert nonlinear for Setanta Sports Channel"
    return None

