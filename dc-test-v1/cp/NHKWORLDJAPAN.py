import pandas as pd
import numpy as np
import datetime
import math
import csv
import re
import calendar
from collections import OrderedDict
import dc.utils
import dc.io
import dc.header
import dc.mapping
import dc.settings

def convert_linear(in_io, out_io, out_format="preV1506", owner_channel="NHK WORLD-JAPAN"):
    "Convert linear schedule for NHK WORLD-JAPAN"

    # Read input file
    indfs = pd.read_excel(in_io, sheetname=None, header=0)
    indf = pd.DataFrame()
    for key, value in indfs.items():
        indf = indf.append(value, ignore_index=True)
    indf.index = range(1,len(indf) + 1)   # fix index
    indf = indf.dropna(subset = ["TX Date"]) # Drop row if TX Date column is NaN
    indf.columns = ["".join(s.split()).replace("(", "").replace(")", "").replace("/", "").lower() for s in indf.columns]

    
    # Create output DataFrame
    outdf = pd.DataFrame(columns=dc.header.internalHeader)

    # Extract data from full title
    fullTitleEng = indf["fulltitleenglishmax40characters"][indf["fulltitleenglishmax40characters"].notnull()].apply(lambda x: str(x).strip())


    # Fill output DataFrame with related info
    excepted_column = ["PID", "Premier", "Recordable", "Is NPVR Prog", "Is Restart TV", "effective_date", "expiration_date", "Media ID (max. 12 chars)"]
    for internal_column, indf_column in dc.header.postV1506HeaderMapping.items():
        if (indf_column not in excepted_column): 
            edited_indf_column = "".join(indf_column.split()).replace("(", "").replace(")", "").replace("/", "").lower() #indf_column
            if (edited_indf_column in indf.columns):
                outdf[internal_column] = indf[edited_indf_column]
                if (outdf[internal_column].dtype == object):
                     outdf[internal_column] = outdf[internal_column].where(outdf[internal_column].apply(lambda x: pd.notnull(x) and len(str(x).strip()) > 0))
    hkt = indf.apply(lambda x: pd.to_datetime(pd.to_datetime(x["txdate"], format="%Y%m%d").strftime("%Y%m%d") + " " + pd.to_datetime(x["actualtime"], format="%H:%M").strftime("%H:%M"), format="%Y%m%d %H:%M"), axis=1)
    outdf["TXDate"] = hkt.apply(lambda x: x.strftime("%Y%m%d"))
    outdf["ActualTime"] = hkt.apply(lambda x: x.strftime("%H:%M"))
    outdf["BrandNameChi"].fillna(outdf["BrandNameEng"], inplace=True)
    outdf["EpisodeNameEng"] = indf["episodenameenglish"]
    outdf["EpisodeNameChi"] = indf["episodenamechinese"]
    outdf["SynopsisEng"] = indf["synopsisenglish"].apply(lambda x: str(x).strip()).str.replace('(\s*,\s*|\s*、\s*|\s*，\s*|\s*﹑\s*|\s*ʼ\s*)', ', ').str.translate(dc.mapping.charTranslateTable)
    outdf["SynopsisChi"] = outdf["SynopsisEng"]
    outdf["ShortSynopsisEng"] = outdf["SynopsisEng"][outdf["SynopsisEng"].notnull()].apply(lambda x: x[0:dc.settings.char_limit.get("ShortSynopsisEng")])
    outdf["ShortSynopsisChi"] = outdf["SynopsisChi"][outdf["SynopsisChi"].notnull()].apply(lambda x: x[0:dc.settings.char_limit.get("ShortSynopsisChi")])
    outdf["PremierOld"] = "0"
    outdf["Premier"] = "N"
    outdf["IsLive"] = "N"
    outdf["Genre"] = indf["genre"].apply(lambda x: dc.mapping.genreMapping.get(x.lower(), x) if pd.notnull(x) else x)
    outdf["SubGenre"] = indf["sub-genre"].apply(lambda x: dc.mapping.subGenreMapping.get(x.lower(), x) if pd.notnull(x) else x).apply(lambda y: "/".join(y)[0:dc.settings.char_limit.get("SubGenre")] if np.all(pd.notnull(y)) and not isinstance(y,str) else y).fillna(indf["sub-genre"])
    dc.utils.title_df(outdf)
    outdf[["ProgrammeNameEng", "ProgrammeNameChi"]] = outdf.apply(dc.utils.constructProgrammeName, axis=1)
    dc.utils.translateChar(outdf)
    dc.utils.chopCharacters(outdf)


    # Append EOF
    dc.utils.appendEOF(outdf)


    # Fill Channel #
    outdf['OwnerChannel'] = owner_channel
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)

    # Write output file
    dc.io.to_io(outdf, out_io, out_format)

def convert_nonlinear(in_io, out_io, format="preV1506", owner_channel="NHK WORLD-JAPAN"):
    "Convert nonlinear for NHK WORLD-JAPAN"
    return None

