import pandas as pd
import numpy as np
import datetime
import math
import csv
import re
import calendar
from collections import OrderedDict
import dc.utils
import dc.io
import dc.header
import dc.mapping
import dc.settings

def convert_linear(in_io, out_io, out_format="preV1506", owner_channel="HKSTV Zhonghe"):
    "Convert linear schedule for HKSTV Zhonghe"

    # Read input file
    indf = pd.read_excel(in_io, sheetname=0, skiprows=0, skip_footer=0)
    indf = indf[1:]

    
    # Create output DataFrame
    outdf = pd.DataFrame(columns=dc.header.internalHeader)

    # Extract data from full title
    tmpValue = indf["TX Date"][1]
    tmpIndex = 0
    for item in indf["TX Date"]:
        if pd.notnull(item) and re.match("^([0-9]+([-/][0-9]{0,2}){0,2}|([0-9]{0,2}[-/]){0,2}[0-9]+)(\s[0-9]{2}(:{0,1}[0-9]{2}){0,2}){0,1}(\.[0-9]+){0,1}$",str(item).strip()) is not None:
            tmpValue=item
        tmpIndex += 1
        indf.set_value(tmpIndex, "TX Date", tmpValue)


    # Fill output DataFrame with related info
    outdf["TXDate"] = indf["TX Date"].apply(lambda x: pd.to_datetime(str(x), format="%d/%m/%Y") if "/" in str(x) else pd.to_datetime(str(x), format="%Y-%m-%d") if "-" in str(x) else str(x)).apply(lambda x:x.strftime("%Y%m%d"))
    outdf["ActualTime"] = indf["Actual \nStart \nTime"].astype('str').str.slice(-8).str.slice(0, 5).str.zfill(5)
    outdf["BrandNameChi"] = indf[indf.columns[2]][indf[indf.columns[2]].notnull()].astype(str).str.translate(dc.mapping.charTranslateTable).apply(lambda x: x[0:dc.settings.char_limit.get("BrandNameChi")])
    outdf["BrandNameEng"].fillna(outdf["BrandNameChi"], inplace=True)
    outdf["PremierOld"] = "0"
    outdf["Premier"] = "N"
    outdf["IsLive"] = "N"
    outdf["IsEpisodic"] = (outdf["EpisodeNo"].notnull() | outdf["EpisodeNameEng"].notnull()).map({True:"Y", False:"N"})
    dc.utils.title_df(outdf)
    outdf[["FullTitleEng", "FullTitleChi"]] = outdf.apply(dc.utils.constructFullTitle, axis=1)
    outdf[["ProgrammeNameEng", "ProgrammeNameChi"]] = outdf.apply(dc.utils.constructProgrammeName, axis=1)
    dc.utils.translateChar(outdf)
    dc.utils.chopCharacters(outdf)


    # Append EOF
    dc.utils.appendEOF(outdf)


    # Fill Channel #
    outdf['OwnerChannel'] = owner_channel
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)

    # Write output file
    dc.io.to_io(outdf, out_io, out_format)

def convert_nonlinear(in_io, out_io, format="preV1506", owner_channel="HKSTV Zhonghe"):
    "Convert nonlinear for HKSTV Zhonghe"
    return None

