import pandas as pd
import numpy as np
import datetime
import math
import csv
import re
import calendar
from collections import OrderedDict
import dc.utils
import dc.io
import dc.header
import dc.mapping
import dc.settings

def convert_linear(in_io, out_io, out_format="preV1506", owner_channel="DW (Deutsch)"):
    "Convert linear schedule for DW (Deutsch)"

    # Read input file
    import dc.TestReadingExcel
    from pandas.compat import (lrange)
    data = dc.TestReadingExcel.read_excel(in_io, sheetname=0)
    columnSize = 0
    rowSize = 0
    isColumn = False
    dataSize = 0
    isData = False
    parsedCol = 0
    for i in data:
        colSize = 0
        for j in i:
            if (j == 'Episode_Title'):
                columnSize = rowSize
                isColumn = True
            elif str(j).strip() != '' and pd.notnull(j) and dc.utils.validateDatetime(j):
                dataSize = rowSize
                parsedCol = colSize
                isData = True 
            if isColumn and isData:
                break
            colSize +=1
        if isColumn and isData:
            break
        rowSize +=1
    if type(in_io) != str:
        in_io.seek(0)
    indf = pd.read_excel(in_io, sheetname=0, parse_cols=lrange(parsedCol,len(data[0])), skiprows=columnSize)
    indf = indf.ix[(dataSize-columnSize-1):]

    
    # Create output DataFrame
    outdf = pd.DataFrame(columns=dc.header.internalHeader)

    # Extract data from full title
    ProgramTitle = ''
    for i in indf.columns:
        tmp = re.sub("\s","",str(i)).replace("(", "").replace(")", "").replace("/", "").replace("_","").replace("-","").replace(".","").lower() 
        if 'ProgrammeTitle'.lower() in tmp:
            ProgramTitle = str(i)


    # Fill output DataFrame with related info
    outdf["TXDate"] = pd.to_datetime(indf[indf.columns[0]].apply(lambda x: x.strftime("%Y-%m-%d")) + " " + indf[indf.columns[1]].apply(lambda x: x.strftime("%H:%M:%S"))).apply(lambda x: x + np.timedelta64(8,'h')).apply(lambda x:x.strftime("%Y%m%d"))
    outdf["ActualTime"] = pd.to_datetime(indf[indf.columns[0]].apply(lambda x: (str(x))) + " " + indf[indf.columns[1]].apply(lambda x: (str(x)))).apply(lambda x: x + np.timedelta64(8,'h')).apply(lambda x:x.strftime("%H:%M"))
    outdf["FullTitleEng"] = indf[ProgramTitle][indf[ProgramTitle].notnull()].astype(str).str.translate(dc.mapping.charTranslateTable)
    outdf["FullTitleChi"].fillna(outdf["FullTitleEng"], inplace=True)
    outdf["BrandNameEng"]=indf[ProgramTitle][indf[ProgramTitle].notnull()].astype(str).str.translate(dc.mapping.charTranslateTable)
    outdf["BrandNameChi"].fillna(outdf["BrandNameEng"], inplace=True)
    outdf["EpisodeNameEng"] = indf["Episode_Title"].apply(lambda x: np.nan if x=='-' else x)
    outdf["EpisodeNameEng"] = outdf["EpisodeNameEng"][outdf["EpisodeNameEng"].notnull()].astype(str).str.translate(dc.mapping.charTranslateTable)
    outdf["EpisodeNameChi"].fillna(outdf["EpisodeNameEng"], inplace=True)
    outdf["SynopsisEng"] = indf["Synopsis"][indf["Synopsis"].notnull()].astype(str).str.translate(dc.mapping.charTranslateTable)
    outdf["SynopsisChi"].fillna(outdf["SynopsisEng"], inplace=True)
    outdf["ShortSynopsisEng"] = outdf["SynopsisEng"][outdf["SynopsisEng"].notnull()].astype(str).apply(lambda x: x[0:dc.settings.char_limit.get("ShortSynopsisEng")])
    outdf["ShortSynopsisChi"].fillna(outdf["ShortSynopsisEng"], inplace=True)
    outdf["PremierOld"] = "0"
    outdf["Premier"] = "N"
    outdf["IsLive"] = "N"
    if "PG Rating" in indf.columns:
        outdf["Classification"] = indf["PG Rating"]
    if owner_channel == "DW (Deutsch)":
        if "Genre" in indf.columns:
            outdf["Genre"].fillna(indf["Genre"], inplace=True)
        if "Programme_Genre1*" in indf.columns:
            outdf["Genre"].fillna(indf["Programme_Genre1*"], inplace=True)
        if "Programme_Sub_Genre1" in indf.columns:
            outdf["SubGenre"].fillna(indf["Programme_Sub_Genre1"], inplace=True)
        if "Programme_Language_ID*" in indf.columns:
            outdf["OriginalLang"] = indf["Programme_Language_ID*"][indf["Programme_Language_ID*"].notnull()].str.translate(dc.mapping.charTranslateTable).apply(lambda x: dc.mapping.audioLangMapping[x.lower()] if x.lower() in dc.mapping.audioLangMapping else x.lower())
        if "Audio_Track" in indf.columns:
            outdf["AudioLang"] = indf["Audio_Track"][indf["Audio_Track"].notnull()].str.translate(dc.mapping.charTranslateTable)
        outdf["AudioLang"].fillna(outdf["OriginalLang"], inplace=True)
    else:
        outdf["Genre"] = "News"
        outdf["SubGenre"] = "News"
        outdf["AudioLang"] = "English"
    outdf["IsEpisodic"] = ((outdf["EpisodeNo"].notnull() & ~outdf["EpisodeNo"].astype(str).str.match("^[\s]*$"))
                           | (outdf["EpisodeNameEng"].notnull() & ~outdf["EpisodeNameEng"].astype(str).str.match("^[\s]*$"))).map({True:"Y", False:"N"})
    dc.utils.title_df(outdf)
    outdf[["ProgrammeNameEng", "ProgrammeNameChi"]] = outdf.apply(dc.utils.constructProgrammeName, axis=1)
    outdf[["FullTitleEng", "FullTitleChi"]] = outdf.apply(dc.utils.constructFullTitle, axis=1)
    dc.utils.translateChar(outdf)
    dc.utils.chopCharacters(outdf)


    # Append EOF
    lastProgDuration = outdf["TXDate"].last_valid_index() + np.timedelta64(1,'D') - outdf["TXDate"].last_valid_index() - outdf["ActualTime"].last_valid_index()# default end of program time is 23:59
    lastProgDateTime = outdf["TXDate"].last_valid_index() + outdf["ActualTime"].last_valid_index()
    lastProgram = indf[outdf["BrandNameEng"] == outdf["BrandNameEng"][outdf["BrandNameEng"].last_valid_index()]]
    if len(lastProgram.index)>1: # last program has been played in previous timeslot
        firstProgIndex = lastProgram.index.values[0]
        lastProgDuration = pd.to_datetime((outdf["TXDate"][firstProgIndex+1] + ' ' + outdf["ActualTime"][firstProgIndex+1]), format="%Y%m%d %H:%M") - pd.to_datetime((outdf["TXDate"][firstProgIndex] + ' ' + outdf["ActualTime"][firstProgIndex]), format="%Y%m%d %H:%M")
    dc.utils.appendEOF(outdf, lastprog_duration=lastProgDuration)


    # Fill Channel #
    outdf['OwnerChannel'] = owner_channel
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)

    # Write output file
    dc.io.to_io(outdf, out_io, out_format)

def convert_nonlinear(in_io, out_io, format="preV1506", owner_channel="DW (Deutsch)"):
    "Convert nonlinear for DW (Deutsch)"
    return None

