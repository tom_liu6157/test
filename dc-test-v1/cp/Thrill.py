import pandas as pd
import numpy as np
import datetime
import math
import csv
import re
import calendar
from collections import OrderedDict
import dc.utils
import dc.io
import dc.header
import dc.mapping
import dc.settings

def convert_linear(in_io, out_io, out_format="preV1506", owner_channel="Thrill"):
    "Convert linear schedule for Thrill"

    # Read input file
    indf = pd.read_excel(in_io, sheetname=0, skiprows=0)

    
    # Create output DataFrame
    outdf = pd.DataFrame(columns=dc.header.internalHeader)

    # Extract data from full title
    fullTitleEng = indf["Programme_Title"][indf["Programme_Title"].notnull()].apply(lambda x: str(x).strip())
    fullTitleChi = indf["Programme_Title (Chinese)"][indf["Programme_Title (Chinese)"].notnull()].apply(lambda x: str(x).strip())
    extractedInfo = pd.DataFrame(columns=["BrandNameEng", "BrandNameChi", "SeasonNo"])
    tempExtractedInfo = [
        fullTitleChi.str.extract("(?P<BrandNameChi>.*?)\s?第(?P<SeasonNo>.+)季", expand=False)
        ,fullTitleEng.str.extract("(?P<BrandNameEng>.*?)\s?S(?P<SeasonNo>\d+)", expand=False)
        ,fullTitleEng.str.extract("(?P<BrandNameEng>.*?)\s?S(?P<SeasonNo>\d+), The\s*", expand=False)
        ,fullTitleEng.str.extract("(?P<BrandNameEng>.*?),\s?The\s*", expand=False)
        ,fullTitleChi.str.extract("(?P<BrandNameChi>.*?)\s*S(?P<SeasonNo>\d+)", expand=False)
    ]
    extractedInfo["BrandNameEng"]= "The " + tempExtractedInfo[2]["BrandNameEng"]
    extractedInfo["BrandNameEng"].fillna("The " + tempExtractedInfo[3], inplace=True)
    extractedInfo["BrandNameEng"].fillna(tempExtractedInfo[1]["BrandNameEng"], inplace=True)
    extractedInfo["BrandNameEng"].fillna(fullTitleEng, inplace=True)
    extractedInfo["SeasonNo"] = tempExtractedInfo[0]["SeasonNo"]
    extractedInfo["SeasonNo"].fillna(tempExtractedInfo[4]["SeasonNo"], inplace=True)
    extractedInfo["SeasonNo"].fillna(tempExtractedInfo[1]["SeasonNo"], inplace=True)
    extractedInfo["SeasonNo"].fillna(tempExtractedInfo[2]["SeasonNo"], inplace=True)
    extractedInfo["BrandNameChi"] = tempExtractedInfo[0]["BrandNameChi"]
    extractedInfo["BrandNameChi"].fillna(tempExtractedInfo[4]["BrandNameChi"], inplace=True)
    extractedInfo["BrandNameChi"].fillna(fullTitleChi, inplace=True)


    # Fill output DataFrame with related info
    outdf["TXDate"] = indf["Start Date"].apply(lambda x: pd.to_datetime(x, format="%Y/%m/%d")).apply(lambda x:x.strftime("%Y%m%d"))
    outdf["ActualTime"] = indf["Start Time"].apply(lambda x: pd.to_datetime(x, format="%H:%M")).apply(lambda x:x.strftime("%H:%M"))
    outdf["BrandNameEng"]=extractedInfo["BrandNameEng"]
    outdf["BrandNameChi"]=extractedInfo["BrandNameChi"]
    outdf["SeasonNo"] = extractedInfo["SeasonNo"].apply(lambda x: dc.utils.chinese2num(x) if pd.notnull(x) and isinstance(x, str) and not x.isdigit() else x)
    outdf["EpisodeNo"] = indf["Episode_Number"][indf["Episode_Number"].notnull()].astype(int).astype(str)
    outdf["SynopsisEng"] = indf["EPG Synopsis"].str.translate(dc.mapping.charTranslateTable)
    outdf["SynopsisChi"] = indf["Synopsis (Chinese)"].str.translate(dc.mapping.charTranslateTable)
    outdf["ShortSynopsisEng"] = outdf["SynopsisEng"][outdf["SynopsisEng"].notnull()].apply(lambda x: x[0:dc.settings.char_limit.get("ShortSynopsisEng")])
    outdf["ShortSynopsisChi"] = outdf["SynopsisChi"][outdf["SynopsisChi"].notnull()].apply(lambda x: x[0:dc.settings.char_limit.get("ShortSynopsisChi")])
    outdf["PremierOld"] = "0"
    outdf["Premier"] = "N"
    outdf["IsLive"] = "N"
    outdf["Classification"] = indf["Classification"]
    outdf["CastEng"] = indf["Cast"][indf["Cast"].notnull()].apply(lambda x: str(x).strip()).str.replace('(\s*,\s*|\s*、\s*|\s*，\s*)', ', ').str.translate(dc.mapping.charTranslateTable)
    outdf["CastChi"] = indf["Cast (Chinese)"][indf["Cast (Chinese)"].notnull()].apply(lambda x: str(x).strip()).str.replace('(\s*,\s*|\s*、\s*|\s*，\s*)', ', ').str.translate(dc.mapping.charTranslateTable)
    outdf["CastChi"].fillna(outdf["CastEng"], inplace=True)
    outdf["DirectorProducerEng"] = indf["Director"][indf["Director"].notnull()].apply(lambda x: str(x).strip()).str.replace('(\s*,\s*|\s*、\s*|\s*，\s*)', ', ').str.translate(dc.mapping.charTranslateTable)
    outdf["DirectorProducerChi"] = indf["Director (Chinese)"][indf["Director (Chinese)"].notnull()].apply(lambda x: str(x).strip()).str.replace('(\s*,\s*|\s*、\s*|\s*，\s*)', ', ').str.translate(dc.mapping.charTranslateTable)
    outdf["DirectorProducerChi"].fillna(outdf["DirectorProducerEng"], inplace=True)
    outdf["Genre"] = indf["Programme_Genre1"][indf["Programme_Genre1"].notnull()].str.lower().map(dc.mapping.genreMapping).fillna(indf["Programme_Genre1"])
    outdf["SubGenre"] = indf["Programme_Sub_Genre_1"].str.split("-").apply(lambda subGenres: sorted(set([item for subGenre in subGenres for item in dc.mapping.subGenreMapping.get(subGenre.strip().lower(), [subGenre])]))).str.join("/").fillna(indf["Programme_Sub_Genre_1"])
    outdf["OriginalLang"] = indf["Programme_Language_ID"][indf["Programme_Language_ID"].notnull()].str.lower().map(dc.mapping.audioLangMapping).fillna(indf["Programme_Language_ID"])
    outdf["AudioLang"] = outdf['OriginalLang']
    outdf["RegionCode"] = indf["Origin"][indf["Origin"].notnull()].str.lower().map(dc.mapping.regionCodeMapping).fillna(indf["Origin"])
    outdf["IsEpisodic"] = (outdf["EpisodeNo"].notnull() | outdf["EpisodeNameEng"].notnull()).map({True:"Y", False:"N"})
    outdf["FirstReleaseYear"] = indf["Year of Production"][indf["Year of Production"].apply(lambda x: pd.notnull(x) and x != "TBC")].astype(int).astype(str)
    dc.utils.title_df(outdf)
    outdf[["FullTitleEng", "FullTitleChi"]] = outdf.apply(dc.utils.constructFullTitle, axis=1)
    outdf[["ProgrammeNameEng", "ProgrammeNameChi"]] = outdf.apply(dc.utils.constructProgrammeName, axis=1)
    dc.utils.translateChar(outdf)
    dc.utils.chopCharacters(outdf)


    # Append EOF
    temp =str(indf["Slot Duration"][len(indf.index)-1]+":00")
    lastProgDuration = pd.Timedelta(temp)
    dc.utils.appendEOF(outdf, lastprog_duration=lastProgDuration)


    # Fill Channel #
    outdf['OwnerChannel'] = owner_channel
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)

    # Write output file
    dc.io.to_io(outdf, out_io, out_format)

def convert_nonlinear(in_io, out_io, format="preV1506", owner_channel="Thrill"):
    "Convert nonlinear for Thrill"
    return None

