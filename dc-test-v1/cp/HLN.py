import pandas as pd
import numpy as np
import datetime
import math
import csv
import re
import calendar
from collections import OrderedDict
import dc.utils
import dc.io
import dc.header
import dc.mapping
import dc.settings

def convert_linear(in_io, out_io, out_format="preV1506", owner_channel="HLN"):
    "Convert linear schedule for HLN"

    # Read input file
    titleMonth = ""
    indf_dict = pd.read_excel(in_io, sheetname=None, skiprows=0, skip_footer=0) # read all sheets into dict of Data Frame
    for item in indf_dict:
        if item.find("Detail")>-1:
            tmpindfref = indf_dict.get(item)
        if item.find("20")>-1:
            titleMonth = item+"-01"
            tmpindf = indf_dict.get(item)
    tmpindf = tmpindf[1:]
    titleMonth = pd.to_datetime(titleMonth, format="%Y-%m-%d")
    dateM = titleMonth
    indfdw0 = tmpindf[["HKT", "CNN HLN Monday"]].copy(deep=True)
    indfdw0.rename(columns={'HKT': 'Time', 'CNN HLN Monday': 'Title'}, inplace=True)
    indfdw1 = tmpindf[["HKT.1", "CNN HLN Tuesday"]].copy(deep=True)
    indfdw1.rename(columns={'HKT.1': 'Time', 'CNN HLN Tuesday': 'Title'}, inplace=True)
    indfdw2 = tmpindf[["HKT.2", "CNN HLN Wednesday"]].copy(deep=True)
    indfdw2.rename(columns={'HKT.2': 'Time', 'CNN HLN Wednesday': 'Title'}, inplace=True)
    indfdw3 = tmpindf[["HKT.3", "CNN HLN Thursday"]].copy(deep=True)
    indfdw3.rename(columns={'HKT.3': 'Time', 'CNN HLN Thursday': 'Title'}, inplace=True)
    indfdw4 = tmpindf[["HKT.4", "CNN HLN Friday"]].copy(deep=True)
    indfdw4.rename(columns={'HKT.4': 'Time', 'CNN HLN Friday': 'Title'}, inplace=True)
    indfdw5 = tmpindf[["HKT.5", "CNN HLN Saturday"]].copy(deep=True)
    indfdw5.rename(columns={'HKT.5': 'Time', 'CNN HLN Saturday': 'Title'}, inplace=True)
    indfdw6 = tmpindf[["HKT.6", "CNN HLN Sunday"]].copy(deep=True)
    indfdw6.rename(columns={'HKT.6': 'Time', 'CNN HLN Sunday': 'Title'}, inplace=True)
    for num in range(0,calendar.monthrange(titleMonth.year,titleMonth.month)[1]):
        if dateM.dayofweek==0:
            indfdw0["Date"] = dateM.strftime("%Y%m%d")
            if num==0: indf = indfdw0.copy(deep=True)
            else: indf = pd.concat([indf,indfdw0.copy(deep=True)], ignore_index='Yes')
        elif dateM.dayofweek==1:
            indfdw1["Date"] = dateM.strftime("%Y%m%d")
            if num==0: indf = indfdw1.copy(deep=True)
            else: indf = pd.concat([indf,indfdw1.copy(deep=True)], ignore_index='Yes')
        elif dateM.dayofweek==2:
            indfdw2["Date"] = dateM.strftime("%Y%m%d")
            if num==0: indf = indfdw2.copy(deep=True)
            else: indf = pd.concat([indf,indfdw2.copy(deep=True)], ignore_index='Yes')
        elif dateM.dayofweek==3:
            indfdw3["Date"] = dateM.strftime("%Y%m%d")
            if num==0: indf = indfdw3.copy(deep=True)
            else: indf = pd.concat([indf,indfdw3.copy(deep=True)], ignore_index='Yes')
        elif dateM.dayofweek==4:
            indfdw4["Date"] = dateM.strftime("%Y%m%d")
            if num==0: indf = indfdw4.copy(deep=True)
            else: indf = pd.concat([indf,indfdw4.copy(deep=True)], ignore_index='Yes')
        elif dateM.dayofweek==5:
            indfdw5["Date"] = dateM.strftime("%Y%m%d")
            if num==0: indf = indfdw5.copy(deep=True)
            else: indf = pd.concat([indf,indfdw5.copy(deep=True)], ignore_index='Yes')
        elif dateM.dayofweek==6:
            indfdw6["Date"] = dateM.strftime("%Y%m%d")
            if num==0: indf = indfdw6.copy(deep=True)
            else: indf = pd.concat([indf,indfdw6.copy(deep=True)], ignore_index='Yes')
        dateM += pd.Timedelta("1 days")
    indf = pd.merge(indf, tmpindfref, how='left', on=['Title'])

    
    # Create output DataFrame
    outdf = pd.DataFrame(columns=dc.header.internalHeader)

    # Extract data from full title


    # Fill output DataFrame with related info
    outdf["TXDate"] = indf["Date"]
    outdf["ActualTime"] = indf["Time"].apply(lambda x: (str(x)+":00") if len(str(x))==5 else str(x)).apply(lambda x: pd.to_datetime(x, format="%H:%M:%S")).apply(lambda x:x.strftime("%H:%M"))
    outdf["BrandNameEng"] = indf["Title"][indf["Title"].notnull()].str.translate(dc.mapping.charTranslateTable).apply(lambda x: x[0:dc.settings.char_limit.get("BrandNameEng")])
    outdf["BrandNameChi"].fillna(outdf["BrandNameEng"], inplace=True)
    outdf["SynopsisEng"] = indf["Synopsis"][indf["Synopsis"].notnull()].str.translate(dc.mapping.charTranslateTable).apply(lambda x: x[0:dc.settings.char_limit.get("SynopsisEng")])
    outdf["SynopsisChi"].fillna(outdf["SynopsisEng"], inplace=True)
    outdf["ShortSynopsisEng"] = outdf["SynopsisEng"][outdf["SynopsisEng"].notnull()].apply(lambda x: x[0:dc.settings.char_limit.get("ShortSynopsisEng")])
    outdf["ShortSynopsisChi"] = outdf["SynopsisChi"][outdf["SynopsisChi"].notnull()].apply(lambda x: x[0:dc.settings.char_limit.get("ShortSynopsisChi")])
    outdf["PremierOld"] = "0"
    outdf["Premier"] = "N"
    outdf["CastEng"] = indf["Cast"][indf["Cast"].notnull()].str.replace('(\s*,\s*|\s*、\s*|\s*，\s*|\s*;\s*|\s*-\s*|\s*and\s*)', ', ').str.translate(dc.mapping.charTranslateTable).apply(lambda x: str(x).strip()).apply(lambda x: x[0:dc.settings.char_limit.get("CastEng")])
    outdf["IsEpisodic"] = (outdf["EpisodeNo"].notnull() | outdf["EpisodeNameEng"].notnull()).map({True:"Y", False:"N"})
    dc.utils.title_df(outdf)
    outdf[["FullTitleEng", "FullTitleChi"]] = outdf.apply(dc.utils.constructFullTitle, axis=1)
    outdf[["ProgrammeNameEng", "ProgrammeNameChi"]] = outdf.apply(dc.utils.constructProgrammeName, axis=1)
    dc.utils.translateChar(outdf)
    dc.utils.chopCharacters(outdf)


    # Append EOF
    dc.utils.appendEOF(outdf)


    # Fill Channel #
    outdf['OwnerChannel'] = owner_channel
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)

    # Write output file
    dc.io.to_io(outdf, out_io, out_format)

def convert_nonlinear(in_io, out_io, format="preV1506", owner_channel="HLN"):
    "Convert nonlinear for HLN"
    return None

