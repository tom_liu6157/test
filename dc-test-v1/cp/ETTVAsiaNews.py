import pandas as pd
import numpy as np
import datetime
import math
import csv
import re
import calendar
from collections import OrderedDict
import dc.utils
import dc.io
import dc.header
import dc.mapping
import dc.settings

def convert_linear(in_io, out_io, out_format="preV1506", owner_channel="ETTV Asia News"):
    "Convert linear schedule for ETTV Asia News"

    # Read input file
    indf = pd.read_excel(in_io, sheetname=0, skiprows=0, skip_footer=0)
    indf.columns = ["".join(s.split()).replace("(", "").replace(")", "").replace("/", "").lower() for s in indf.columns]

    
    # Create output DataFrame
    outdf = pd.DataFrame(columns=dc.header.internalHeader)

    # Extract data from full title


    # Fill output DataFrame with related info
    excepted_column = ["PID", "Premier", "Recordable", "Is NPVR Prog", "Is Restart TV", "effective_date", "expiration_date", "Media ID (max. 12 chars)"]
    for internal_column, indf_column in dc.header.postV1506HeaderMapping.items():
        if (indf_column not in excepted_column): 
            edited_indf_column = "".join(indf_column.split()).replace("(", "").replace(")", "").replace("/", "").lower()
            if (edited_indf_column in indf.columns):
                outdf[internal_column] = indf[edited_indf_column]
                if (outdf[internal_column].dtype != np.float64) and (outdf[internal_column].dtype != np.int64):
                    outdf[internal_column] = outdf[internal_column].where(outdf[internal_column].apply(lambda x: pd.notnull(x) and len(str(x).strip()) > 0))
    outdf["TXDate"] = outdf["TXDate"].str.split(".").apply(lambda x: "{}{}{}".format(x[0], x[1].zfill(2), x[2].zfill(2)))
    tempExtractedInfoEng = outdf["FullTitleEng"].str.extract("(?P<BrandNameEng>.*)\s*#\s*(?P<EpisodeNo>\d+)$", expand=False)
    tempExtractedInfoEng2 = outdf["FullTitleEng"].str.extract("(?P<BrandNameEng>.*)\s*#\s*(?P<EpisodeNameEng>.*?)\s*$", expand=False)
    tempExtractedInfoChi = outdf["FullTitleChi"].str.extract("(?P<BrandNameChi>.*)\s*#\s*(?P<EpisodeNo>\d+)$", expand=False)
    tempExtractedInfoChi2 = outdf["FullTitleChi"].str.extract("(?P<BrandNameChi>.*)\s*#\s*(?P<EpisodeNameChi>.*?)\s*$", expand=False)
    outdf["BrandNameEng"] = tempExtractedInfoEng["BrandNameEng"].fillna(tempExtractedInfoEng2["BrandNameEng"]).fillna(outdf["FullTitleEng"])
    outdf["BrandNameChi"] = tempExtractedInfoChi["BrandNameChi"].fillna(tempExtractedInfoChi2["BrandNameChi"]).fillna(outdf["FullTitleChi"])
    outdf["EpisodeNo"] = tempExtractedInfoEng["EpisodeNo"].fillna(tempExtractedInfoChi["EpisodeNo"])
    outdf["EpisodeNameEng"] = tempExtractedInfoEng2["EpisodeNameEng"][tempExtractedInfoEng2["EpisodeNameEng"] != tempExtractedInfoEng["EpisodeNo"]]
    outdf["EpisodeNameChi"] = tempExtractedInfoChi2["EpisodeNameChi"][tempExtractedInfoChi2["EpisodeNameChi"] != tempExtractedInfoChi["EpisodeNo"]]
    outdf["Genre"] = outdf["Genre"][outdf["Genre"].notnull()].str.lower().map(dc.mapping.genreMapping).fillna(outdf["Genre"])
    outdf["SponsorTextStuntChi"].fillna(outdf["SponsorTextStuntEng"], inplace=True)
    outdf["SponsorTextStuntEng"].fillna(outdf["SponsorTextStuntChi"], inplace=True)
    outdf["BrandNameChi"].fillna(outdf["BrandNameEng"], inplace=True)
    outdf["BrandNameEng"].fillna(outdf["BrandNameChi"], inplace=True)
    outdf["SeasonNameChi"].fillna(outdf["SeasonNameEng"], inplace=True)
    outdf["SeasonNameEng"].fillna(outdf["SeasonNameChi"], inplace=True)
    outdf["EpisodeNameChi"].fillna(outdf["EpisodeNameEng"], inplace=True)
    outdf["EpisodeNameEng"].fillna(outdf["EpisodeNameChi"], inplace=True)
    outdf["EpisodeNameChi"].fillna(outdf["EpisodeNameEng"], inplace=True)
    outdf["EpisodeNameEng"].fillna(outdf["EpisodeNameChi"], inplace=True)
    outdf["SynopsisChi"].fillna(outdf["SynopsisEng"], inplace=True)
    outdf["SynopsisEng"].fillna(outdf["SynopsisChi"], inplace=True)
    outdf["ShortSynopsisChi"].fillna(outdf["ShortSynopsisEng"], inplace=True)
    outdf["ShortSynopsisEng"].fillna(outdf["ShortSynopsisChi"], inplace=True)
    outdf["EpisodeNo"] = outdf["EpisodeNo"][outdf["EpisodeNo"].notnull()].astype(int).astype(str)
    outdf["Premier"] = "N"
    outdf["PremierOld"] = ((outdf["Premier"] == "Y") | (outdf["IsLive"] == "Y")).map({True: "1", False: "0"})
    text_column = ["SponsorTextStuntEng", "SponsorTextStuntChi", "BrandNameEng", "BrandNameChi", "EditionVersionEng", "EditionVersionChi", "SeasonNameEng", "SeasonNameChi", "EpisodeNameEng", "EpisodeNameChi", "SynopsisEng", "SynopsisChi", "ShortSynopsisEng", "ShortSynopsisChi", "DirectorProducerEng", "DirectorProducerChi", "CastEng", "CastChi"]
    for column in text_column:
        outdf[column] = outdf[column].astype(str).where(outdf[column].notnull()).str.translate(dc.mapping.charTranslateTable)
        if len(outdf[column][outdf[column].notnull()].index) > 0:
            outdf[column] = outdf[column].str.strip()
    outdf["IsEpisodic"] = (outdf["EpisodeNo"].notnull() | outdf["EpisodeNameEng"].notnull()).map({True:"Y", False:"N"})
    dc.utils.title_df(outdf)
    outdf[["FullTitleEng", "FullTitleChi"]] = outdf.apply(dc.utils.constructFullTitle, axis=1)
    outdf[["ProgrammeNameEng", "ProgrammeNameChi"]] = outdf.apply(dc.utils.constructProgrammeName, axis=1)
    dc.utils.translateChar(outdf)
    dc.utils.chopCharacters(outdf)


    # Append EOF
    dc.utils.appendEOF(outdf)


    # Fill Channel #
    outdf['OwnerChannel'] = owner_channel
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)

    # Write output file
    dc.io.to_io(outdf, out_io, out_format)

def convert_nonlinear(in_io, out_io, format="preV1506", owner_channel="ETTV Asia News"):
    "Convert nonlinear for ETTV Asia News"
    return None

