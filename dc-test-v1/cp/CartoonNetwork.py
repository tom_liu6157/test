import pandas as pd
import numpy as np
import datetime
import math
import csv
import re
import calendar
from collections import OrderedDict
import dc.utils
import dc.io
import dc.header
import dc.mapping
import dc.settings

def convert_linear(in_io, out_io, out_format="preV1506", owner_channel="Cartoon Network"):
    "Convert linear schedule for Cartoon Network"

    # Read input file
    indf = pd.read_excel(in_io, sheetname=0, skiprows=0)
    indf.columns = ["".join(s.split()).replace("(", "").replace(")", "").replace("/", "").lower() for s in indf.columns]

    
    # Create output DataFrame
    outdf = pd.DataFrame(columns=dc.header.internalHeader)

    # Extract data from full title


    # Fill output DataFrame with related info
    excepted_column = ["PID", "Premier", "Recordable", "Is NPVR Prog", "Is Restart TV", "effective_date", "expiration_date", "Media ID (max. 12 chars)"]
    for internal_column, indf_column in dc.header.postV1506HeaderMapping.items():
        if (indf_column not in excepted_column): 
            edited_indf_column = "".join(indf_column.split()).replace("(", "").replace(")", "").replace("/", "").lower()
            validate_similarity = [dc.utils.validateSimilarity(edited_indf_column, i) for i in indf.columns]
            if (edited_indf_column in indf.columns) or any(validate_similarity):
                if edited_indf_column not in indf.columns:
                    edited_indf_column = indf.columns[validate_similarity.index(True)]
                outdf[internal_column] = indf[edited_indf_column]
                if (outdf[internal_column].dtype == object):
                    outdf[internal_column] = outdf[internal_column].where(outdf[internal_column].apply(lambda x: pd.notnull(x) and len(str(x).strip()) > 0))
    outdf["BrandNameEng"].fillna(outdf["FullTitleEng"], inplace=True)
    outdf["BrandNameChi"].fillna(outdf["FullTitleChi"], inplace=True)
    outdf["SeasonNameEng"] = outdf[["BrandNameEng", "SeasonNameEng"]].apply(lambda x: None if pd.notnull(x["SeasonNameEng"]) and pd.notnull(x["BrandNameEng"]) and x["SeasonNameEng"].startswith(x["BrandNameEng"]) and re.search(".*?Season\s*\d+\s*(#\s*\d+\s*)*$", x["SeasonNameEng"]) else x["SeasonNameEng"], axis=1)
    outdf["SeasonNameChi"] = outdf[["BrandNameChi", "SeasonNameChi"]].apply(lambda x: None if pd.notnull(x["SeasonNameChi"]) and re.search("^\s*:\s*第.*季\s*(第.*集\s*)*$", x["SeasonNameChi"]) else x["SeasonNameChi"], axis=1)
    outdf["SeasonNameChi"] = outdf[["BrandNameChi", "SeasonNameChi"]].apply(lambda x: None if pd.notnull(x["SeasonNameChi"]) and pd.notnull(x["BrandNameChi"]) and x["SeasonNameChi"].startswith(x["BrandNameChi"]) and re.search(".*?第.*季\s*(第.*集\s*)*$", x["SeasonNameChi"]) else x["SeasonNameChi"], axis=1)
    def group_episodes(li):
        if len(li) > 0:
            out = []
            last = li[0] - 1
            for x in li:
                if x != last + 1:
                    yield out
                    out = []
                out.append(x)
                last = x
            yield out
    tmp = outdf[["BrandNameEng", "EpisodeNameEng"]].apply(lambda x: x["EpisodeNameEng"] if x["EpisodeNameEng"].startswith(x["BrandNameEng"]) else None, axis=1)
    tmp2 = tmp[tmp.notnull()].str.split("/").apply(lambda x: ", ".join(["Ep. {}-{}".format(g[0], g[-1]) if g[0] != g[-1] else "Ep. {}".format(g[0]) for g in list(group_episodes([int(re.search(".*?\s*Ep#(\d+)$", episodename).group(1)) for episodename in x if re.search(".*?\s*Ep#\d+$", episodename)]))])).apply(lambda x: x if len(x) > 0 else None)
    outdf["EpisodeNameEng"] = pd.Series(index=outdf["EpisodeNameEng"].index).fillna(tmp2).fillna(outdf["EpisodeNameEng"])
    outdf["EpisodeNameEng"] = outdf[["BrandNameEng", "EpisodeNameEng"]].apply(lambda x: None if pd.notnull(x["BrandNameEng"]) and pd.notnull(x["EpisodeNameEng"]) and x["BrandNameEng"].strip() == x["EpisodeNameEng"].strip() else x["EpisodeNameEng"], axis=1)
    outdf["EpisodeNameChi"] = outdf[["BrandNameChi", "EpisodeNameChi"]].apply(lambda x: None if pd.notnull(x["BrandNameChi"]) and pd.notnull(x["EpisodeNameChi"]) and x["BrandNameChi"].strip() == x["EpisodeNameChi"].strip() else x["EpisodeNameChi"], axis=1)
    outdf["EpisodeNameEng"] = outdf["EpisodeNameEng"].apply(lambda x: None if pd.notnull(x) and x.startswith("**This Episode has been banned all feeds") else x)
    outdf["EpisodeNameChi"] = outdf["EpisodeNameChi"].apply(lambda x: None if pd.notnull(x) and x.startswith("**This Episode has been banned all feeds") else x)
    outdf["EpisodeNameEng"] = outdf["EpisodeNameEng"].apply(lambda x: re.sub("^:\s*", "", x) if pd.notnull(x) else None)
    outdf["EpisodeNameChi"] = outdf["EpisodeNameChi"].apply(lambda x: re.sub("^:\s*", "", x) if pd.notnull(x) else None)
    outdf["TXDate"] = outdf["TXDate"].apply(lambda x: str(x) if type(x) is int                                         else x.strftime('%Y%m%d') if outdf["TXDate"].dtype == "<M8[ns]"                                         else x)
    outdf["SeasonNo"] = outdf["SeasonNo"][outdf["SeasonNo"].notnull()].astype(int).astype(str).apply(lambda x: "" if str(x) == "0" else x)
    outdf["EpisodeNo"] = outdf["EpisodeNo"][outdf["EpisodeNo"].notnull()].astype(str).apply(lambda x: "" if str(x) == "0" else x)
    outdf["SynopsisEng"] = outdf["SynopsisEng"].apply(lambda x: re.sub("^\**\s*CHECK\s*SYNOPSIS\s*\**\s*", "", x) if pd.notnull(x) else None)
    outdf["SynopsisChi"] = outdf["SynopsisChi"].apply(lambda x: re.sub("^\**\s*CHECK\s*SYNOPSIS\s*\**\s*", "", x) if pd.notnull(x) else None)
    outdf["ShortSynopsisEng"].fillna(outdf["SynopsisEng"], inplace=True)
    outdf["ShortSynopsisChi"].fillna(outdf["SynopsisChi"], inplace=True)
    outdf["EpisodeNo"] = outdf["EpisodeNo"].where(outdf["IsEpisodic"].apply(lambda x: False if x=="N" else True))
    outdf["EpisodeNameEng"] = outdf["EpisodeNameEng"].where(outdf["IsEpisodic"].apply(lambda x: False if x=="N" else True))
    outdf["EpisodeNameChi"] = outdf["EpisodeNameChi"].where(outdf["IsEpisodic"].apply(lambda x: False if x=="N" else True)).apply(lambda x: re.match("^\/\/*([^/].*)*$",str(x)).groups()[0] if pd.notnull(re.match("^\/\/*([^/].*)*$",str(x))) else x).apply(lambda x: np.NaN if pd.notnull(x) and str(x).strip() == "" else x)
    outdf["SponsorTextStuntChi"].fillna(outdf["SponsorTextStuntEng"], inplace=True)
    outdf["SponsorTextStuntEng"].fillna(outdf["SponsorTextStuntChi"], inplace=True)
    outdf["BrandNameChi"].fillna(outdf["BrandNameEng"], inplace=True)
    outdf["BrandNameEng"].fillna(outdf["BrandNameChi"], inplace=True)
    outdf["SeasonNameChi"].fillna(outdf["SeasonNameEng"], inplace=True)
    outdf["SeasonNameEng"].fillna(outdf["SeasonNameChi"], inplace=True)
    outdf["EpisodeNameChi"].fillna(outdf["EpisodeNameEng"], inplace=True)
    outdf["EpisodeNameEng"].fillna(outdf["EpisodeNameChi"], inplace=True)
    outdf["EpisodeNameEng"] = outdf[["EpisodeNameEng", "EpisodeNo"]].apply(lambda x: re.match("^[0-9]+([a-zA-Z]+)$",str(x["EpisodeNo"])).groups()[0]+". "+str(x["EpisodeNameEng"]) if (pd.notnull(x["EpisodeNameEng"]) and pd.isnull(re.match("^[\s]*$",str(x["EpisodeNameEng"])))) and pd.notnull(re.match("^[0-9]+[a-zA-Z]+$",str(x["EpisodeNo"]))) else re.match("^[0-9]+([a-zA-Z]+)$",str(x["EpisodeNo"])).groups()[0] if (pd.isnull(x["EpisodeNameEng"]) or pd.notnull(re.match("^[\s]*$",str(x["EpisodeNameEng"])))) and pd.notnull(re.match("^[0-9]+[a-zA-Z]+$",str(x["EpisodeNo"]))) else x["EpisodeNameEng"], axis=1)
    outdf["EpisodeNo"] = outdf["EpisodeNo"].apply(lambda x: re.match("^([0-9]+)[a-zA-Z]+$",str(x)).groups()[0] if pd.notnull(re.match("^[0-9]+[a-zA-Z]+$",str(x))) else x)
    outdf["EpisodeNameChi"].fillna(outdf["EpisodeNameEng"], inplace=True)
    outdf["EpisodeNameEng"].fillna(outdf["EpisodeNameChi"], inplace=True)
    outdf["SynopsisChi"].fillna(outdf["SynopsisEng"], inplace=True)
    outdf["SynopsisEng"].fillna(outdf["SynopsisChi"], inplace=True)
    outdf["ShortSynopsisChi"].fillna(outdf["ShortSynopsisEng"], inplace=True)
    outdf["ShortSynopsisEng"].fillna(outdf["ShortSynopsisChi"], inplace=True)
    outdf["PremierOld"] = "0"
    outdf["Premier"] = "N"
    text_column = ["SponsorTextStuntEng", "SponsorTextStuntChi", "BrandNameEng", "BrandNameChi", "EditionVersionEng", "EditionVersionChi", "SeasonNameEng", "SeasonNameChi", "EpisodeNameEng", "EpisodeNameChi", "SynopsisEng", "SynopsisChi", "ShortSynopsisEng", "ShortSynopsisChi", "DirectorProducerEng", "DirectorProducerChi", "CastEng", "CastChi"]
    for column in text_column:
        outdf[column] = outdf[column].astype(str).where(outdf[column].notnull()).str.translate(dc.mapping.charTranslateTable)
    outdf["OriginalLang"] = outdf["OriginalLang"][outdf["OriginalLang"].notnull()].str.split('; ').apply(lambda audioLangs: [dc.mapping.audioLangMapping[audioLang.lower()] for audioLang in audioLangs]).str.join(', ')
    dc.utils.title_df(outdf)
    outdf[["FullTitleEng", "FullTitleChi"]] = outdf.apply(dc.utils.constructFullTitle, axis=1)
    outdf[["ProgrammeNameEng", "ProgrammeNameChi"]] = outdf.apply(dc.utils.constructProgrammeName, axis=1)
    dc.utils.translateChar(outdf)
    dc.utils.chopCharacters(outdf)


    # Append EOF
    dc.utils.appendEOF(outdf)


    # Fill Channel #
    outdf['OwnerChannel'] = owner_channel
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)

    # Write output file
    dc.io.to_io(outdf, out_io, out_format)

def convert_nonlinear(in_io, out_io, format="preV1506", owner_channel="Cartoon Network"):
    "Convert nonlinear for Cartoon Network"
    return None

