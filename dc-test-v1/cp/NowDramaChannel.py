import pandas as pd
import numpy as np
import datetime
import math
import csv
import re
import calendar
from collections import OrderedDict
import dc.utils
import dc.io
import dc.header
import dc.mapping
import dc.settings

def convert_linear(in_io, out_io, out_format="preV1506", owner_channel="Now Drama Channel"):
    "Convert linear schedule for Now Drama Channel"

    # Read input file
    indf = pd.read_excel(in_io, sheetname=0, skiprows=0)
    indf.dropna(how="all", inplace=True)
    indf.columns = ["".join(s.split()).replace("(", "").replace(")", "").replace("/", "").replace("_","").replace("-","").replace(".","").lower() for s in indf.columns]

    
    # Create output DataFrame
    outdf = pd.DataFrame(columns=dc.header.internalHeader)

    # Extract data from full title


    # Fill output DataFrame with related info
    excepted_column = ["PID", "Premier", "Recordable", "Is NPVR Prog", "Is Restart TV", "effective_date", "expiration_date", "Media ID (max. 12 chars)"]
    for internal_column, indf_column in dc.header.postV1506HeaderMapping.items():
        if (indf_column not in excepted_column): 
            edited_indf_column = "".join(indf_column.split()).replace("(", "").replace(")", "").replace("/", "").replace("_","").replace("-","").replace(".","").lower() #indf_column
            if (edited_indf_column in indf.columns):
                outdf[internal_column] = indf[edited_indf_column]
                if (outdf[internal_column].dtype == object):
                     outdf[internal_column] = outdf[internal_column].where(outdf[internal_column].apply(lambda x: pd.notnull(x) and len(str(x).strip()) > 0))
    EpisodeNo = ''
    for i in indf.columns:
        if re.sub("\s","",str(i)).replace("(", "").replace(")", "").replace("/", "").replace("_","").replace("-","").replace(".","").lower() == 'EPISODENUMBER'.lower():
            EpisodeNo = str(i)
            break
    if owner_channel == "Now Drama Channel" or owner_channel == "NowJelli":
        if EpisodeNo == '':
            EpisodeNo = indf.columns[13]
    else:
        if EpisodeNo == '':
            EpisodeNo = indf.columns[5]    
    if all(pd.isnull(outdf["EpisodeNo"])):
        outdf["EpisodeNo"] = indf[EpisodeNo]
    number_translate_column1 = ["SeasonNo", "SeasonNameEng", "SeasonNameChi", "EpisodeNo"]   
    for float_to_int in number_translate_column1:
        outdf[float_to_int] = outdf[float_to_int].apply(lambda x: re.match("^([0-9]+)(?:\.0+|[0-9]*)$", str(x)).groups()[0] if re.match("^[0-9]+(?:\.0+|[0-9]*)$", str(x)) else x)
    outdf["TXDate"] = outdf["TXDate"].apply(lambda x: pd.to_datetime(x, format="%Y-%m-%d").strftime('%Y%m%d'))
    outdf["ActualTime"] = outdf["ActualTime"].astype('str').str.slice(-8).str.slice(0, 5).str.zfill(5)
    if owner_channel == "Now Chinese Drama Channel": 
        outdf["SynopsisEng"].fillna(outdf["SynopsisChi"], inplace=True)
        outdf["ShortSynopsisEng"].fillna(outdf["SynopsisChi"], inplace=True)
        outdf["ShortSynopsisChi"].fillna(outdf["SynopsisChi"], inplace=True)
        outdf["ShortSynopsisEng"] = outdf["ShortSynopsisEng"][outdf["ShortSynopsisEng"].notnull()].apply(lambda x: x[0:dc.settings.char_limit.get("ShortSynopsisEng")])
        outdf["ShortSynopsisChi"] = outdf["ShortSynopsisChi"][outdf["ShortSynopsisChi"].notnull()].apply(lambda x: x[0:dc.settings.char_limit.get("ShortSynopsisChi")])
        outdf["CastEng"].fillna(outdf["CastChi"], inplace=True)
    elif owner_channel == "Now Drama Channel":  
        outdf["SynopsisChi"].fillna(outdf["SynopsisEng"], inplace=True)
        outdf["SynopsisEng"].fillna(outdf["SynopsisChi"], inplace=True)
        outdf["ShortSynopsisChi"].fillna(outdf["ShortSynopsisEng"], inplace=True)
        outdf["ShortSynopsisEng"].fillna(outdf["ShortSynopsisChi"], inplace=True)
    outdf["Genre"] = outdf["Genre"].apply(lambda x: dc.mapping.genreMapping.get(x.lower(), x) if pd.notnull(x) else x)
    outdf["SubGenre"] = outdf["SubGenre"][outdf["SubGenre"].notnull()].astype(str).str.split("/").apply(lambda subGenres: sorted(set([item for subGenre in subGenres for item in dc.mapping.subGenreMapping.get(subGenre.lower(), [subGenre])]))).str.join("/").apply(lambda x: x[0:dc.settings.char_limit.get("SubGenre")])
    outdf["IsEpisodic"] = outdf[["IsEpisodic","SubGenre","EpisodeNo","EpisodeNameEng","EpisodeNameChi"]].apply(lambda x : "Y" if (pd.notnull(x["EpisodeNo"]) and str(x["EpisodeNo"]).strip() != "") or (pd.notnull(x["EpisodeNameEng"]) and str(x["EpisodeNameEng"]).strip() != "") or (pd.notnull(x["EpisodeNameChi"]) and str(x["EpisodeNameChi"]).strip() != "") else x["IsEpisodic"],axis=1)
    dc.utils.title_df(outdf)
    outdf[["FullTitleEng", "FullTitleChi"]] = outdf.apply(dc.utils.constructFullTitle, axis=1)
    outdf[["ProgrammeNameEng", "ProgrammeNameChi"]] = outdf.apply(dc.utils.constructProgrammeName, axis=1)
    dc.utils.translateChar(outdf)
    dc.utils.chopCharacters(outdf)


    # Append EOF
    dc.utils.appendEOF(outdf)


    # Fill Channel #
    outdf['OwnerChannel'] = owner_channel
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)

    # Write output file
    dc.io.to_io(outdf, out_io, out_format)

def convert_nonlinear(in_io, out_io, format="preV1506", owner_channel="Now Drama Channel"):
    "Convert nonlinear for Now Drama Channel"
    return None

