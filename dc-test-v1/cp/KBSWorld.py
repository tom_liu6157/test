import pandas as pd
import numpy as np
import datetime
import math
import csv
import re
import calendar
from collections import OrderedDict
import dc.utils
import dc.io
import dc.header
import dc.mapping
import dc.settings

def convert_linear(in_io, out_io, out_format="preV1506", owner_channel="KBS World"):
    "Convert linear schedule for KBS World"

    # Read input file
    indf_dict = pd.read_excel(in_io, sheetname=None, skiprows=0, skip_footer=0)
    indf_dict = {str(k).replace("(", "").replace(")", "").replace("/", "").replace("_","").replace("-","").replace(".","").lower()[-3:]:v for k, v in indf_dict.items()} # rename the dict keys to last 3 characters of original keys, which is supposed to indicate the language of the sheet
    indf = indf_dict.get('eng')
    indfchi = indf_dict.get('chi')
    if len(indfchi.index) != len(indf.index):
        raise ValueError('Number of rows does not match in Sheet 1 & 2')

    
    # Create output DataFrame
    outdf = pd.DataFrame(columns=dc.header.internalHeader)

    # Extract data from full title
    fullTitleEng = indf["Title"][indf["Title"].notnull()].apply(lambda x: str(x).strip())
    fullTitleChi = indfchi["节目名称"][indfchi["节目名称"].notnull()].apply(lambda x: str(x).strip()).apply(lambda x: dc.utils.jianfan(x))
    extractedInfo = pd.DataFrame(columns=["SponsorTextEng", "SponsorTextChi", "BrandNameEng", "BrandNameChi", "SeasonNo", "SeasonNameEng", "SeasonNameChi", "EpisodeNo", "EpisodeNameEng", "EpisodeNameChi", "RegionCode", "FirstReleaseYear", "isLive"])
    tempExtractedInfo = [
        fullTitleEng.str.extract("^(<(?P<SponsorTextEng>.*)>)*\s*(?P<BrandNameEng>.*?)\s*-*\s*(Season\s*(?P<SeasonNo>\d+))*\s*(\[(?P<isLive>.*)\])*$", expand=False),
        fullTitleChi.str.extract("^(<(?P<SponsorTextChi>.*)>)*\s*《*(?P<BrandNameChi>.*?)》*\s*-*\s*(Season\s*(\d+))*\s*(\[(.*)\])*$", expand=False)
    ]
    extractedInfo["BrandNameEng"] = tempExtractedInfo[0]["BrandNameEng"]
    extractedInfo["BrandNameChi"] = tempExtractedInfo[1]["BrandNameChi"]
    extractedInfo["BrandNameChi"].fillna(outdf["BrandNameEng"], inplace=True)
    extractedInfo["SeasonNo"] = tempExtractedInfo[0]["SeasonNo"]
    extractedInfo["isLive"] = tempExtractedInfo[0]["isLive"][tempExtractedInfo[0]["isLive"].notnull()].apply(lambda x: "Y" if x=="LIVE" else "N")
    extractedInfo["isLive"].fillna("N", inplace=True)


    # Fill output DataFrame with related info
    EpisodeNameEng = "" 
    EpisodeNameChi = ""
    CastEng = ""
    CastChi = ""
    for i in indf.columns:
        if "EpisodeTitle".upper() in str(i).upper().replace(" ","").replace("(", "").replace(")", "").replace("/", "").replace("_","").replace("-","").replace(".",""):
            EpisodeNameEng = str(i)
        elif "Cast".upper() in str(i).upper().replace(" ","").replace("(", "").replace(")", "").replace("/", "").replace("_","").replace("-","").replace(".",""):
            CastEng = str(i) 
    for i in indfchi.columns:
        if "EpisodeTitle".upper() in str(i).upper().replace(" ","").replace("(", "").replace(")", "").replace("/", "").replace("_","").replace("-","").replace(".",""):
            EpisodeNameChi = str(i)
        elif "Cast".upper() in str(i).upper().replace(" ","").replace("(", "").replace(")", "").replace("/", "").replace("_","").replace("-","").replace(".",""):
            CastChi = str(i) 
    if EpisodeNameEng != "" and all(pd.isnull(outdf["EpisodeNameEng"])):
        outdf["EpisodeNameEng"] = indf[EpisodeNameEng]
    if EpisodeNameChi != "" and all(pd.isnull(outdf["EpisodeNameChi"])):
        outdf["EpisodeNameChi"] = indfchi[EpisodeNameChi]
    if CastEng != "" and all(pd.isnull(outdf["CastEng"])):
        outdf["CastEng"] = indf[CastEng]
    if CastChi != "" and all(pd.isnull(outdf["CastChi"])):
        outdf["CastChi"] = indfchi[CastChi]
    outdf["TXDate"] = indf["Date"].apply(lambda x: pd.to_datetime(x, format="%Y%m%d")).apply(lambda x:x.strftime("%Y%m%d"))
    outdf["ActualTime"] = indf["Start time"].astype('str').str.slice(-8).str.slice(0, 5).str.zfill(5)
    outdf["BrandNameEng"] = extractedInfo["BrandNameEng"][extractedInfo["BrandNameEng"].notnull()].str.translate(dc.mapping.charTranslateTable).apply(lambda x: x[0:dc.settings.char_limit.get("BrandNameEng")])
    outdf["BrandNameChi"] = extractedInfo["BrandNameChi"][extractedInfo["BrandNameChi"].notnull()].str.translate(dc.mapping.charTranslateTable).apply(lambda x: x[0:dc.settings.char_limit.get("BrandNameChi")])
    outdf["BrandNameEng"].fillna(outdf["BrandNameChi"], inplace=True)
    outdf["BrandNameChi"].fillna(outdf["BrandNameEng"], inplace=True)
    outdf["SeasonNo"] = indf["Season no"].fillna(extractedInfo["SeasonNo"])
    outdf["EpisodeNo"] = indf["Episode"][indf["Episode"].notnull()].astype(str).str.translate(dc.mapping.charTranslateTable).str.strip()
    outdf["SeasonNo"] = outdf["SeasonNo"][outdf["SeasonNo"].notnull()].astype(int).astype(str)
    outdf["EpisodeNo"] = outdf["EpisodeNo"][outdf["EpisodeNo"].notnull()].astype(int).astype(str)
    outdf["SynopsisEng"] = indf["Synop"][indf["Synop"].notnull()].astype(str).str.translate(dc.mapping.charTranslateTable).apply(lambda x: x[0:dc.settings.char_limit.get("SynopsisEng")])
    outdf["SynopsisChi"] = indfchi["Synop"][indfchi["Synop"].notnull()].apply(lambda x: dc.utils.jianfan(x)).astype(str).str.translate(dc.mapping.charTranslateTable).apply(lambda x: x[0:dc.settings.char_limit.get("SynopsisChi")])
    outdf["SynopsisEng"].fillna(outdf["SynopsisChi"], inplace=True)
    outdf["SynopsisChi"].fillna(outdf["SynopsisEng"], inplace=True)
    outdf["ShortSynopsisEng"] = outdf["SynopsisEng"][outdf["SynopsisEng"].notnull()].apply(lambda x: x[0:dc.settings.char_limit.get("ShortSynopsisEng")])
    outdf["ShortSynopsisChi"] = outdf["SynopsisChi"][outdf["SynopsisChi"].notnull()].apply(lambda x: x[0:dc.settings.char_limit.get("ShortSynopsisChi")])
    outdf["PremierOld"] = "0"
    outdf["Premier"] = "N"
    outdf["IsLive"] = extractedInfo["isLive"]
    outdf["Genre"] = indf["Genre"][indf["Genre"].notnull()].astype(str).str.lower().map(dc.mapping.genreMapping).fillna(indf["Genre"])
    outdf["SubGenre"] = indf["Sub genre"].astype(str).str.split("/")[indf["Sub genre"].notnull()].apply(lambda subGenres: sorted(set([item for subGenre in subGenres for item in dc.mapping.subGenreMapping.get(subGenre.lower().strip(), [subGenre])]))).str.join("/").fillna(indf["Sub genre"]).apply(lambda x: x[0:dc.settings.char_limit.get("SubGenre")])
    tmpsub = (indf["Subtitle(E)"].astype(str).str.replace("Y", "Eng").str.replace("^(.*)$", "\\1,")+indf["Subtitle(C)"].astype(str).str.replace("Y", "Chi")).str.replace(",-","").str.replace("-,","").str.replace("^-$","").apply(lambda x: np.nan if x=="" else x)
    outdf["SubtitleLang"] = tmpsub[tmpsub.notnull()].astype(str).str.split(',').apply(lambda subtitles: [dc.mapping.subtitleMapping[subtitle.lower()] for subtitle in subtitles]).str.join(', ')
    outdf["IsEpisodic"] = (outdf["EpisodeNo"].notnull() | outdf["EpisodeNameEng"].notnull()).map({True:"Y", False:"N"})
    dc.utils.title_df(outdf)
    outdf[["FullTitleEng", "FullTitleChi"]] = outdf.apply(dc.utils.constructFullTitle, axis=1)
    outdf[["ProgrammeNameEng", "ProgrammeNameChi"]] = outdf.apply(dc.utils.constructProgrammeName, axis=1)
    dc.utils.translateChar(outdf)
    dc.utils.chopCharacters(outdf)


    # Append EOF
    temp =str(indf["Duration"][len(indf.index)-1])
    lastProgDuration = pd.Timedelta(temp)
    dc.utils.appendEOF(outdf, lastprog_duration=lastProgDuration)


    # Fill Channel #
    outdf['OwnerChannel'] = owner_channel
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)

    # Write output file
    dc.io.to_io(outdf, out_io, out_format)

def convert_nonlinear(in_io, out_io, format="preV1506", owner_channel="KBS World"):
    "Convert nonlinear for KBS World"
    return None

