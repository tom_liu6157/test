import pandas as pd
import numpy as np
import datetime
import math
import csv
import re
import calendar
from collections import OrderedDict
import dc.utils
import dc.io
import dc.header
import dc.mapping
import dc.settings

def convert_linear(in_io, out_io, out_format="preV1506", owner_channel="CNBC"):
    "Convert linear schedule for CNBC"

    # Read input file
    indf = pd.read_excel(in_io, sheetname=0)

    
    # Create output DataFrame
    outdf = pd.DataFrame(columns=dc.header.internalHeader)

    # Extract data from full title


    # Fill output DataFrame with related info
    outdf["TXDate"] = indf["TX Date"].apply(lambda x: pd.to_datetime(x, format="%d-%m-%Y")).apply(lambda x:x.strftime("%Y%m%d"))
    outdf["ActualTime"] = indf["Start Time"].apply(lambda x:x.strftime("%H:%M"))
    outdf["BrandNameEng"]=indf["Programme Title"].str.translate(dc.mapping.charTranslateTable)
    outdf["BrandNameChi"] = outdf["BrandNameEng"]
    outdf["SynopsisEng"] = indf["Synopsis"].str.translate(dc.mapping.charTranslateTable)
    outdf["SynopsisChi"] = indf["Synopsis"].str.translate(dc.mapping.charTranslateTable)
    outdf["ShortSynopsisEng"] = outdf["SynopsisEng"].apply(lambda x: x[0:dc.settings.char_limit.get("ShortSynopsisEng")])
    outdf["ShortSynopsisChi"] = outdf["SynopsisChi"].apply(lambda x: x[0:dc.settings.char_limit.get("ShortSynopsisChi")])
    outdf["PremierOld"] = "0"
    outdf["Premier"] = "N"
    outdf["IsLive"] = "N"
    outdf["IsEpisodic"] = "N"
    outdf["Genre"] = "News/Finance"
    outdf["SubGenre"] = indf["Programme Genre"][indf["Programme Genre"].notnull()].str.lower().map(dc.mapping.subGenreMapping).str.join("/").fillna(indf["Programme Genre"])
    outdf["SubGenre"] = outdf["SubGenre"].apply(lambda x : "" if x == "-" else x)
    outdf["AudioLang"] = "English"
    outdf["SubtitleLang"] = "English"
    outdf["CastChi"] = indf["Anchors"].str.translate(dc.mapping.charTranslateTable)
    outdf["CastEng"] = indf["Anchors"].str.translate(dc.mapping.charTranslateTable)
    dc.utils.title_df(outdf)
    outdf[["FullTitleEng", "FullTitleChi"]] = outdf.apply(dc.utils.constructFullTitle, axis=1)
    outdf[["ProgrammeNameEng", "ProgrammeNameChi"]] = outdf.apply(dc.utils.constructProgrammeName, axis=1)
    dc.utils.translateChar(outdf)
    dc.utils.chopCharacters(outdf)


    # Append EOF
    temp =str(indf["Duration"][indf.last_valid_index()]).replace('mins', 'min')
    lastProgDuration = pd.Timedelta(temp)
    dc.utils.appendEOF(outdf, lastprog_duration=lastProgDuration)


    # Fill Channel #
    outdf['OwnerChannel'] = owner_channel
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)

    # Write output file
    dc.io.to_io(outdf, out_io, out_format)

def convert_nonlinear(in_io, out_io, format="preV1506", owner_channel="CNBC"):
    "Convert nonlinear for CNBC"
    return None

