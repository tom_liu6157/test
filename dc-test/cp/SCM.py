import pandas as pd
import numpy as np
import datetime
import math
import csv
import re
import calendar
from collections import OrderedDict
import dc.utils
import dc.io
import dc.header
import dc.mapping
import dc.settings

def convert_linear(in_io, out_io, out_format="preV1506", owner_channel="SCM"):
    "Convert linear schedule for SCM"

    # Read input file
    indf = pd.read_excel(in_io, sheetname=0, skiprows=[0,1], na_values=["NIL"], keep_default_na=True)

    
    # Create output DataFrame
    outdf = pd.DataFrame(columns=dc.header.internalHeader)

    # Extract data from full title
    fullTitleChi = indf["Full Title (Chinese)"][indf["Full Title (Chinese)"].notnull()].apply(lambda x: str(x).strip())
    fullTitleEng = indf["Full Title (English)"][indf["Full Title (English)"].notnull()].apply(lambda x: str(x).strip())
    extractedInfo = pd.DataFrame(columns=["SponsorTextEng", "SponsorTextChi", "BrandNameEng", "BrandNameChi", "EpisodeNo", "EpisodeNameEng", "EpisodeNameChi", "RegionCode", "FirstReleaseYear"])
    tempExtractedInfo = [
        fullTitleEng.str.extract("^(<(?P<SponsorTextEng>.*)>)*\s*(?P<BrandNameEng>.*?)\s*((#|ep)(?P<EpisodeNo>\d+))*\s*(\((?P<SupplementInfo>.*)\))*$", expand=False),
        fullTitleChi.str.extract("^(<(?P<SponsorTextChi>.*)>)*\s*(?P<BrandNameChi>.*?)\s*(第.*集)*\s*(\((?P<SupplementInfo>.*)\))*$", expand=False)
    ]
    extractedInfo["SponsorTextEng"] = tempExtractedInfo[0]["SponsorTextEng"]
    extractedInfo["SponsorTextChi"] = tempExtractedInfo[1]["SponsorTextChi"]
    extractedInfo["BrandNameEng"] = tempExtractedInfo[0]["BrandNameEng"]
    extractedInfo["BrandNameChi"] = tempExtractedInfo[1]["BrandNameChi"]
    extractedInfo["EpisodeNo"] = tempExtractedInfo[0]["EpisodeNo"]
    suppInfo = tempExtractedInfo[0]["SupplementInfo"]
    regionCode = suppInfo[suppInfo.notnull()].apply(lambda x: dc.mapping.regionCodeMapping.get(x.lower()))
    extractedInfo["RegionCode"] = regionCode[regionCode.notnull()]


    # Fill output DataFrame with related info
    outdf["TXDate"] = indf["TX Date"].apply(lambda x: pd.to_datetime(str(x)).strftime('%Y%m%d'))
    outdf["ActualTime"] = indf["Actual Time"].apply(lambda x: pd.to_datetime(x, format="%H:%M")).apply(lambda x:x.strftime("%H:%M"))
    outdf["SponsorTextStuntEng"] = indf["Sponsor text / Stunt (English)"][indf["Sponsor text / Stunt (English)"].notnull()].astype(str).str.translate(dc.mapping.charTranslateTable)
    outdf["SponsorTextStuntChi"] = indf["Sponsor text / Stunt (Chinese)"][indf["Sponsor text / Stunt (Chinese)"].notnull()].astype(str).str.translate(dc.mapping.charTranslateTable)
    outdf["BrandNameEng"] = indf["Brand Name (English)"][indf["Brand Name (English)"].notnull()].astype('str').str.translate(dc.mapping.charTranslateTable)
    outdf["BrandNameChi"] = indf["Brand Name (Chinese)"][indf["Brand Name (Chinese)"].notnull()].astype('str').str.translate(dc.mapping.charTranslateTable)
    outdf["BrandNameChi"].fillna(outdf["BrandNameEng"], inplace=True)
    outdf["BrandNameEng"].fillna(outdf["BrandNameChi"], inplace=True)
    outdf["EpisodeNo"] = indf["Episode No."].apply(lambda x: str(int(re.match("^([0-9]+)(\.0*)?$", str(x)).groups()[0])) if re.match("^([0-9]+)(\.0*)?$", str(x)) else None)
    if any(indf['Episode Name (English)'].notnull()):
        outdf["EpisodeNameEng"] = indf['Episode Name (English)'].str.translate(dc.mapping.charTranslateTable)
    if any(indf['Episode Name (Chinese)'].notnull()):
        outdf["EpisodeNameChi"] = indf['Episode Name (Chinese)'].str.translate(dc.mapping.charTranslateTable)
    outdf["EpisodeNameChi"].fillna(outdf["EpisodeNameEng"], inplace=True)
    outdf["EpisodeNameEng"].fillna(outdf["EpisodeNameChi"], inplace=True)
    outdf["SynopsisEng"] = indf["Synopsis (English)"].str.translate(dc.mapping.charTranslateTable)
    outdf["SynopsisChi"] = indf["Synopsis (Chinese)"].str.translate(dc.mapping.charTranslateTable)
    outdf["SynopsisChi"].fillna(outdf["SynopsisEng"], inplace=True)
    outdf["SynopsisEng"].fillna(outdf["SynopsisChi"], inplace=True)
    outdf["ShortSynopsisEng"] = outdf["SynopsisEng"][outdf["SynopsisEng"].notnull()].apply(lambda x: x[0:dc.settings.char_limit.get("ShortSynopsisEng")])
    outdf["ShortSynopsisChi"] = outdf["SynopsisChi"][outdf["SynopsisChi"].notnull()].apply(lambda x: x[0:dc.settings.char_limit.get("ShortSynopsisChi")])
    outdf["ShortSynopsisChi"].fillna(outdf["ShortSynopsisEng"], inplace=True)
    outdf["ShortSynopsisEng"].fillna(outdf["ShortSynopsisChi"], inplace=True)
    outdf["PremierOld"] = "0"
    outdf["Premier"] = indf["Premiere (Y/N)"]
    outdf["IsLive"] = indf["Is Live (Y/N)"]
    outdf["Genre"]=indf["Genre"]
    outdf["SubGenre"] = indf["Sub-Genre"]
    outdf["DirectorProducerEng"] = indf["Director / Producer (English)"][indf["Director / Producer (English)"].notnull()].apply(lambda x: str(x).strip()).str.replace('(\s*,\s*|\s*、\s*|\s*，\s*)', ', ').str.translate(dc.mapping.charTranslateTable)
    outdf["DirectorProducerChi"] = indf["Director / Producer (Chinese)"][indf["Director / Producer (Chinese)"].notnull()].apply(lambda x: str(x).strip()).str.replace('(\s*,\s*|\s*、\s*|\s*，\s*)', ', ').str.translate(dc.mapping.charTranslateTable)
    outdf["CastEng"] = indf["Cast (English)"][indf["Cast (English)"].notnull()].apply(lambda x: str(x).strip()).str.replace('(\s*,\s*|\s*、\s*|\s*，\s*)', ', ').str.translate(dc.mapping.charTranslateTable)
    outdf["CastChi"] = indf["Cast (Chinese)"][indf["Cast (Chinese)"].notnull()].apply(lambda x: str(x).strip()).str.replace('(\s*,\s*|\s*、\s*|\s*，\s*)', ', ').str.translate(dc.mapping.charTranslateTable)
    outdf["RegionCode"] = indf["Region Code"]
    outdf["IsEpisodic"] = indf["Episodic (Y/N)"]
    outdf["Classification"]=indf["Classificaion"]
    outdf["OriginalLang"] = indf["Original language"]
    outdf["AudioLang"] = indf["Original language"]
    outdf["SubtitleLang"] = indf["Subtitle language"]
    outdf["FirstReleaseYear"] = indf["First Release Year"].apply(lambda x: str(int(re.match("^([0-9]+)(\.0*)?$", str(x)).groups()[0])) if re.match("^([0-9]+)(\.0*)?$", str(x)) else None)
    outdf["PortraitImage"] = indf['Portrait Image']
    outdf["ImageWithTitle"] = indf['Image with title (Y/N)']
    outdf["RecordableForCatchUp"] = indf['Recordable for Catch-Up (Y/N)']
    dc.utils.title_df(outdf)
    outdf[["FullTitleEng", "FullTitleChi"]] = outdf.apply(dc.utils.constructFullTitle, axis=1)
    outdf[["ProgrammeNameEng", "ProgrammeNameChi"]] = outdf.apply(dc.utils.constructProgrammeName, axis=1)
    dc.utils.translateChar(outdf)
    dc.utils.chopCharacters(outdf)


    # Append EOF
    if "Slot Duration" in indf.columns:
        indf["Slot Duration"] = indf["Slot Duration"][indf[["Brand Name (English)","Brand Name (Chinese)"]].apply(lambda x: False if re.sub("[\s]","",str(x["Brand Name (English)"])).upper() == "ENDOFFILE" or re.sub("[\s]","",str(x["Brand Name (Chinese)"])).upper() == "ENDOFFILE" else True, axis = 1)]
        temp =str(indf["Slot Duration"][len(indf.index)-1]+":00")
        lastProgDuration = pd.Timedelta(temp)
        dc.utils.appendEOF(outdf, lastprog_duration=lastProgDuration)
    else:
        outdf = outdf[outdf[["BrandNameEng","BrandNameChi"]].apply(lambda x: False if re.sub("[\s]","",str(x["BrandNameEng"])).upper() == "ENDOFFILE" or re.sub("[\s]","",str(x["BrandNameChi"])).upper() == "ENDOFFILE" else True, axis = 1)]
        dc.utils.appendEOF(outdf)


    # Fill Channel #
    outdf['OwnerChannel'] = owner_channel
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)

    # Write output file
    dc.io.to_io(outdf, out_io, out_format)

def convert_nonlinear(in_io, out_io, format="preV1506", owner_channel="SCM"):
    "Convert nonlinear for SCM"
    return None

