import pandas as pd
import numpy as np
import datetime
import math
import csv
import re
import calendar
from collections import OrderedDict
import dc.utils
import dc.io
import dc.header
import dc.mapping
import dc.settings

def convert_linear(in_io, out_io, out_format="preV1506", owner_channel="Oh!K"):
    "Convert linear schedule for Oh!K"

    # Read input file
    indf = pd.read_excel(in_io, sheetname=0)
    indf.columns = ["".join(s.split()).replace("(", "").replace(")", "").replace("/", "").lower() for s in indf.columns]
    indf.dropna(how="any", subset=["txdate", "actualtime"], inplace=True)

    
    # Create output DataFrame
    outdf = pd.DataFrame(columns=dc.header.internalHeader)

    # Extract data from full title


    # Fill output DataFrame with related info
    excepted_column = ["PID", "Premier", "Recordable", "Is NPVR Prog", "Is Restart TV", "effective_date", "expiration_date", "Media ID (max. 12 chars)"]
    for internal_column, indf_column in dc.header.postV1506HeaderMapping.items():
        if (indf_column not in excepted_column): 
            edited_indf_column = "".join(indf_column.split()).replace("(", "").replace(")", "").replace("/", "").lower()
            if (edited_indf_column in indf.columns):
                outdf[internal_column] = indf[edited_indf_column]
                if (outdf[internal_column].dtype == object):
                    outdf[internal_column] = outdf[internal_column].where(outdf[internal_column].apply(lambda x: pd.notnull(x) and len(str(x).strip()) > 0))
    outdf["TXDate"] = outdf["TXDate"].apply(lambda x: str(x) if type(x) is int else x.astype(int).astype(str) if type(x) is str else x.strftime("%Y%m%d"))
    outdf["ActualTime"] = outdf["ActualTime"].apply(lambda x: (x[:5]) if type(x) is str and re.match("^[0-9]{2}:[0-9]{2}:[0-9]{2}$", x) else x.strftime("%H:%M"))
    outdf["SeasonNo"] = outdf["SeasonNo"][outdf["SeasonNo"].notnull()].apply(lambda x: str(dc.utils.transferNumeric(x))).apply(lambda x: "" if str(x) == "0" else str(x))
    mask = ~outdf["EpisodeNameEng"].str.startswith("Prod Year ").fillna(False)
    outdf["EpisodeNo"] = outdf["EpisodeNo"][mask].apply(lambda x: str(int(re.match("^([0-9]+)(\.0*)?$", str(x)).groups()[0])) if re.match("^([0-9]+)(\.0*)?$", str(x)) else None)
    outdf["EpisodeNameEng"] = outdf["EpisodeNameEng"][mask]
    outdf["EpisodeNameChi"] = outdf["EpisodeNameChi"][mask]
    outdf["SponsorTextStuntChi"].fillna(outdf["SponsorTextStuntEng"], inplace=True)
    outdf["SponsorTextStuntEng"].fillna(outdf["SponsorTextStuntChi"], inplace=True)
    outdf["BrandNameChi"].fillna(outdf["BrandNameEng"], inplace=True)
    outdf["BrandNameEng"].fillna(outdf["BrandNameChi"], inplace=True)
    outdf["SeasonNameChi"].fillna(outdf["SeasonNameEng"], inplace=True)
    outdf["SeasonNameEng"].fillna(outdf["SeasonNameChi"], inplace=True)
    outdf["EpisodeNameChi"].fillna(outdf["EpisodeNameEng"], inplace=True)
    outdf["EpisodeNameEng"].fillna(outdf["EpisodeNameChi"], inplace=True)
    outdf["EpisodeNameChi"].fillna(outdf["EpisodeNameEng"], inplace=True)
    outdf["EpisodeNameEng"].fillna(outdf["EpisodeNameChi"], inplace=True)
    outdf["SynopsisChi"].fillna(outdf["SynopsisEng"], inplace=True)
    outdf["SynopsisEng"].fillna(outdf["SynopsisChi"], inplace=True)
    outdf["ShortSynopsisChi"].fillna(outdf["ShortSynopsisEng"], inplace=True)
    outdf["ShortSynopsisEng"].fillna(outdf["ShortSynopsisChi"], inplace=True)
    outdf["CastChi"].fillna(outdf["CastEng"], inplace=True)
    outdf["CastEng"].fillna(outdf["CastChi"], inplace=True)
    outdf["FirstReleaseYear"] = outdf["FirstReleaseYear"].apply(lambda x: str(int(re.match("^([0-9]+)(\.0*)?$", str(x)).groups()[0])) if re.match("^([0-9]+)(\.0*)?$", str(x)) else None)
    text_column = ["SponsorTextStuntEng", "SponsorTextStuntChi", "BrandNameEng", "BrandNameChi", "EditionVersionEng", "EditionVersionChi", "SeasonNameEng", "SeasonNameChi", "EpisodeNameEng", "EpisodeNameChi", "SynopsisEng", "SynopsisChi", "ShortSynopsisEng", "ShortSynopsisChi", "DirectorProducerEng", "DirectorProducerChi", "CastEng", "CastChi"]
    for column in text_column:
        outdf[column] = outdf[column].astype(str).where(outdf[column].notnull()).str.translate(dc.mapping.charTranslateTable)
    dc.utils.title_df(outdf)
    outdf[["FullTitleEng", "FullTitleChi"]] = outdf.apply(dc.utils.constructFullTitle, axis=1)
    outdf[["ProgrammeNameEng", "ProgrammeNameChi"]] = outdf.apply(dc.utils.constructProgrammeName, axis=1)
    dc.utils.translateChar(outdf)
    dc.utils.chopCharacters(outdf)


    # Append EOF
    dc.utils.appendEOF(outdf)


    # Fill Channel #
    outdf['OwnerChannel'] = owner_channel
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)

    # Write output file
    dc.io.to_io(outdf, out_io, out_format)

def convert_nonlinear(in_io, out_io, format="preV1506", owner_channel="Oh!K"):
    "Convert nonlinear for Oh!K"
    
    # Read input file
    indf = pd.read_excel(in_io, sheetname=0, keep_default_na=False)

    
    # Create output DataFrame
    outdf = pd.DataFrame(columns=dc.utils.extendnlInternal(indf.columns))

    # Extract data from full title


    # Fill output DataFrame with related info
    excepted_column = []
    dc.utils.fillOutdf(outdf,indf,excepted_column)
    excepted_column = ["AdvisorySlate", "TVClassification"]
    for internal_column in dc.utils.extendnlInternal(indf.columns):
        if (internal_column not in excepted_column):
            outdf[internal_column] = outdf[internal_column].apply(lambda x: None if isinstance(x, str) and x.lower().strip() in ["n/a", ""] else x)
    outdf["StartDate"] = outdf["StartDate"].apply(lambda x: re.match("^([0-9]+)(?:\.0+|[0-9]*)$", str(x)).groups()[0] if re.match("^[0-9]+(?:\.0+|[0-9]*)$", str(x)) else x.strftime("%Y%m%d") if isinstance(x, datetime.datetime) or isinstance(x, datetime.time) else x)
    outdf["EndDate"] = outdf["EndDate"].apply(lambda x: re.match("^([0-9]+)(?:\.0+|[0-9]*)$", str(x)).groups()[0] if re.match("^[0-9]+(?:\.0+|[0-9]*)$", str(x)) else x.strftime("%Y%m%d") if isinstance(x, datetime.datetime) or isinstance(x, datetime.time) else x)
    outdf["Duration"] = outdf["Duration"].astype(str)
    outdf["BrandNameEng"] = outdf["BrandNameEng"].astype(str).str.extract("^(.*?)\s*(S\d+)*\s*(\(Ep\.\s*\d+\))*$", expand=False)[0]
    outdf["BrandNameChi"] = outdf["BrandNameChi"].astype(str).str.extract("^(.*?)(\s*-\s*(第\d+季)*\s*(第\d+集)*)*$", expand=False)[0]
    outdf["DirectorProducerEng"] = outdf["DirectorProducerEng"][outdf["DirectorProducerEng"].notnull()].astype(str).str.split(",").apply(lambda x: [item.strip() for item in x]).str.join(', ')
    outdf["DirectorProducerChi"] = outdf["DirectorProducerChi"][outdf["DirectorProducerChi"].notnull()].astype(str).str.split("、").apply(lambda x: [item.strip() for item in x]).str.join(', ')
    outdf["DirectorProducerChi"] = outdf["DirectorProducerChi"][outdf["DirectorProducerChi"].notnull()].astype(str).str.split(",").apply(lambda x: [item.strip() for item in x]).str.join(', ')
    outdf["CastEng"] = outdf["CastEng"][outdf["CastEng"].notnull()].astype(str).str.split(",").apply(lambda x: [item.strip() for item in x]).str.join(', ')
    outdf["CastChi"] = outdf["CastChi"][outdf["CastChi"].notnull()].astype(str).str.split("、").apply(lambda x: [item.strip() for item in x]).str.join(', ')
    outdf["CastChi"] = outdf["CastChi"][outdf["CastChi"].notnull()].astype(str).str.split(",").apply(lambda x: [item.strip() for item in x]).str.join(', ')
    outdf["Genre"] = outdf["Genre"].astype(str).str.lower().map(dc.mapping.genreMapping)
    outdf["SubGenre"] = outdf["SubGenre"][outdf["SubGenre"].notnull()].astype(str).str.split("/").apply(lambda subGenres: sorted(set([subGenre.strip() for subGenre in subGenres]))).str.join("/").fillna(outdf["SubGenre"])
    outdf["SubGenre"] = outdf[["SubGenre","Genre"]].apply(lambda x: str(x["SubGenre"])+"/Korean" if pd.notnull(x["SubGenre"]) and "Korean" not in str(x["SubGenre"]) and x["Genre"] == "TV Series" else "Korean" if (pd.isnull(x["SubGenre"]) or str(x["SubGenre"]).strip() == "") and x["Genre"] == "TV Series" else x,axis=1)
    outdf["OriginalLang"] = outdf["OriginalLang"].astype(str).str.lower().map(dc.mapping.audioLangMapping)
    outdf["AudioLang"] = outdf["AudioLang"].astype(str).str.lower().map(dc.mapping.audioLangMapping)
    outdf["SubtitleLang"] = outdf["SubtitleLang"].astype(str).str.lower().map(dc.mapping.subtitleMapping)
    outdf["RegionCode"] = outdf["RegionCode"].astype(str).str.lower().map(dc.mapping.regionCodeMapping)


    # Write output file
    dc.io.to_io_nonlinear(outdf, out_io, format)

