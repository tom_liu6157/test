import pandas as pd
import numpy as np
import datetime
import math
import csv
import re
import calendar
from collections import OrderedDict
import dc.utils
import dc.io
import dc.header
import dc.mapping
import dc.settings

def convert_linear(in_io, out_io, out_format="preV1506", owner_channel="FOX SPORTS 3"):
    "Convert linear schedule for FOX SPORTS 3"

    # Read input file
    indf = pd.read_excel(in_io, sheetname=0)
    if type(in_io) != str:
        in_io.seek(0)
    if "Version" in indf.columns[0]:
        indf = pd.read_excel(in_io, sheetname=0, skiprows=2)
    indf.dropna(how='all', inplace=True)

    
    # Create output DataFrame
    outdf = pd.DataFrame(columns=dc.header.internalHeader)

    # Extract data from full title


    # Fill output DataFrame with related info
    outdf["TXDate"] = indf["Air Date"].apply(lambda x: pd.to_datetime(x, format="%d/%m/%Y")).apply(lambda x:x.strftime("%Y%m%d"))
    outdf["ActualTime"] = indf["Time"].astype('str').str.slice(-8).str.slice(0, 5).str.zfill(5)
    outdf["FullTitleEng"] = indf["Title"][indf["Title"].notnull()].astype('str').str.translate({ord("\xae") : ""}).str.translate(dc.mapping.charTranslateTable).apply(lambda x : np.NaN if str(x).strip() == "" else x)
    if owner_channel != "FOX SPORTS":
        outdf["FullTitleChi"].fillna(outdf["FullTitleEng"], inplace=True)
        outdf["BrandNameEng"].fillna(outdf["FullTitleEng"], inplace=True)
        outdf["BrandNameChi"].fillna(outdf["FullTitleChi"], inplace=True)
    else:
        extractedInfo = outdf["FullTitleEng"].astype('str').str.extract("(?P<BrandNameEng>.*?)\s+S(?P<SeasonNo>\d+)", expand=False)
        outdf["BrandNameEng"].fillna(extractedInfo["BrandNameEng"], inplace=True)
        outdf["BrandNameEng"].fillna(outdf["FullTitleEng"], inplace=True)
        outdf["BrandNameChi"].fillna(outdf["BrandNameEng"], inplace=True)
        outdf["SeasonNo"].fillna(extractedInfo["SeasonNo"], inplace=True)
    if len(indf.columns) == 10:
        cond1 = indf[["Genre","Matchup","Episode No."]].apply(lambda x: (re.sub('\s','',str(x["Genre"])).upper() == "Magazine".upper()) and 
                                                              (re.match("^([0-9]+)(?:\.[\d]*(e\+(\d)+){0,1}|[0-9]*)$",re.sub('\s','',str(x["Episode No."])).upper(),re.I) is not None) and 
                                                              (re.match("^.+#([0-9]+)([\D]{0,1}.*LIVE){0,1}$",re.sub('\s','',str(x["Matchup"])).upper()) is not None) and 
                                                             (int(re.match("^.+#([0-9]+)([\D]{0,1}.*LIVE){0,1}$",re.sub('\s','',str(x["Matchup"])).upper()).groups()[0]) == dc.utils.transferNumeric(x["Episode No."])),axis=1)
        cond2 = indf[["Matchup","Episode No."]].apply(lambda x: (re.match("^#([0-9]+)([\D]{0,1}.*LIVE){0,1}$",re.sub('\s','',str(x["Matchup"])).upper()) is not None) and 
                                                              (re.match("^([0-9]+)(?:\.[\d]*(e\+(\d)+){0,1}|[0-9]*)$",re.sub('\s','',str(x["Episode No."])).upper(),re.I) is not None) and 
                                                             (int(re.match("^#([0-9]+)([\D]{0,1}.*LIVE){0,1}$",re.sub('\s','',str(x["Matchup"])).upper()).groups()[0]) == dc.utils.transferNumeric(x["Episode No."])),axis=1)
        outdf["EpisodeNo"] = indf["Episode No."][cond1 | cond2 | (indf["Genre"] == "Sports News") | indf["Matchup"].apply(lambda x: any([x.lower().startswith(keyword) for keyword in ['pre-show', 'post-show', 'highlights']])) | (outdf["BrandNameEng"].astype(str).str.lower().str.endswith('highlights')) | (outdf["BrandNameEng"].astype(str).str.lower().str.endswith('preview'))]
        outdf["EpisodeNo"] = outdf["EpisodeNo"][outdf["EpisodeNo"].notnull()].astype(str).str.replace("\D.*", "").apply(lambda x: str(dc.utils.transferNumeric(x))).apply(lambda x: "" if str(x) == "0" else x)
        outdf["EpisodeNameEng"] = indf["Matchup"][indf["Matchup"].notnull()].astype(str).str.translate({ord("\xae") : ""}).str.translate(dc.mapping.charTranslateTable).str.replace("\s*#\s*\d*.*$", "").apply(lambda x : np.NaN if str(x).strip() == "" else x)
        outdf["EpisodeNameChi"].fillna(outdf["EpisodeNameEng"], inplace=True)
        outdf["IsLive"] = indf[["LTS", "Matchup"]].apply(lambda x: "Y" if x["LTS"]=="LIVE" else "Y" if re.match("^.*LIVE$",str(x["Matchup"]),re.I) is not None else "N",axis=1)
    else:
        cond1 = indf["Event Date"].apply(lambda x: re.match("^[\s]*$",str(x)) is not None or pd.isnull(x)) 
        cond2 = indf["Round"].apply(lambda x: re.match("^[\s]*$",str(x)) is not None or pd.isnull(x)) 
        cond3 = indf["Matchup"].apply(lambda x: re.match("^[\s]*$",str(x)) is not None or pd.isnull(x))
        outdf["EpisodeNo"] = indf["Episode No."].where(np.logical_and(np.logical_and(cond1,cond2),cond3)).apply(lambda x: str(int(re.match("^([0-9]+)(?:\.0+|[0-9]*)$", str(x)).groups()[0])) if re.match("^[0-9]+(?:\.0+|[0-9]*)$", str(x)) else None)
        outdf[["EpisodeNameEng"]] = indf.apply(dc.utils.constructEpisodeNameEng1, axis=1)
        outdf["EpisodeNameChi"].fillna(outdf["EpisodeNameEng"], inplace=True)
        outdf["IsLive"] = indf["LTS"].apply(lambda x: "Y" if x=="L" else "N")
    outdf["PremierOld"] = "0"
    outdf["Premier"] = "N"
    outdf["Genre"] = "Sports"
    outdf["SubGenre"] = indf["Genre"][indf["Genre"].notnull()].astype(str).str.split("/").apply(lambda subGenres: sorted(set([item for subGenre in subGenres for item in dc.mapping.subGenreMapping.get(subGenre.lower(), [subGenre])]))).str.join("/")
    if len(indf.columns) == 12:
        outdf["SubGenre"] = outdf["SubGenre"].where(indf["Event Date"].notnull() & ~indf["Event Date"].astype(str).str.match("^[\s]*$")).apply(lambda x: str(x)+" / Match" if pd.notnull(x) and str(x).strip() != "" else x).fillna(outdf["SubGenre"])
    outdf["IsEpisodic"].fillna(np.logical_and(((outdf["EpisodeNameChi"].notnull() & ~outdf["EpisodeNameChi"].astype(str).str.match("^[\s]*$")) | (outdf["EpisodeNameEng"].notnull() & ~outdf["EpisodeNameEng"].astype(str).str.match("^[\s]*$")) | (outdf["EpisodeNo"].notnull() & ~outdf["EpisodeNo"].astype(str).str.match("^[\s]*$"))), indf["Genre"].apply(lambda x: str(x) == "Magazine")).map({True:"Y", False:"N"}), inplace=True)
    outdf["IsEpisodic"] = outdf[["IsEpisodic","SubGenre","EpisodeNo","EpisodeNameEng","EpisodeNameChi"]].apply(lambda x : "N" if "Match".upper() in re.sub('\s','',str(x["SubGenre"])).upper().split("/") else x["IsEpisodic"],axis=1)
    outdf['OwnerChannel'] = "FOX SPORTS 3"
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)
    dc.utils.title_df(outdf)
    outdf[["FullTitleEng", "FullTitleChi"]] = outdf.apply(dc.utils.constructFullTitle, axis=1)
    outdf[["ProgrammeNameEng", "ProgrammeNameChi"]] = outdf.apply(dc.utils.constructProgrammeName, axis=1)
    dc.utils.translateChar(outdf)
    dc.utils.chopCharacters(outdf)


    # Append EOF
    if len(indf.columns) == 10:
        temp =str(str(indf[indf.columns[3]][indf[indf.columns[3]].last_valid_index()])[0:5]+":00")
        lastProgDuration = pd.Timedelta(temp)
        dc.utils.appendEOF(outdf, lastprog_duration=lastProgDuration)
    else:
        dc.utils.appendEOF(outdf)


    # Fill Channel #
    outdf['OwnerChannel'] = owner_channel
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)

    # Write output file
    dc.io.to_io(outdf, out_io, out_format)

def convert_nonlinear(in_io, out_io, format="preV1506", owner_channel="FOX SPORTS 3"):
    "Convert nonlinear for FOX SPORTS 3"
    return None

