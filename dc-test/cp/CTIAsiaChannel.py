import pandas as pd
import numpy as np
import datetime
import math
import csv
import re
import calendar
from collections import OrderedDict
import dc.utils
import dc.io
import dc.header
import dc.mapping
import dc.settings

def convert_linear(in_io, out_io, out_format="preV1506", owner_channel="CTI Asia Channel"):
    "Convert linear schedule for CTI Asia Channel"

    # Read input file
    indf = pd.read_excel(in_io, sheetname=0, parse_cols="B:G")

    
    # Create output DataFrame
    outdf = pd.DataFrame(columns=dc.header.internalHeader)

    # Extract data from full title
    fullTitleEng = indf["Title.1"][indf["Title.1"].notnull()].apply(lambda x: str(x).strip())
    fullTitleChi = indf["Title"][indf["Title"].notnull()].apply(lambda x: str(x).strip())
    extractedInfo = pd.DataFrame(columns=["BrandNameEng", "BrandNameChi"])
    tempExtractedInfo = [
        fullTitleEng.str.extract("(?P<BrandNameEng>.*?)\(.*\)", expand=False),
        fullTitleChi.str.extract("(?P<BrandNameChi>.*?)\(.*\)", expand=False)
    ]
    extractedInfo["BrandNameEng"] = tempExtractedInfo[0]
    extractedInfo["BrandNameEng"].fillna(fullTitleEng, inplace=True)
    extractedInfo["BrandNameChi"] = tempExtractedInfo[1]
    extractedInfo["BrandNameChi"].fillna(fullTitleChi, inplace=True)


    # Fill output DataFrame with related info
    outdf["TXDate"] = indf["Date"].apply(lambda x:x.strftime("%Y%m%d"))
    outdf["ActualTime"] = indf["Time"].apply(lambda x:x.strftime("%H:%M"))
    outdf["BrandNameEng"] = extractedInfo["BrandNameEng"].str.translate(dc.mapping.charTranslateTable)
    outdf["BrandNameChi"] = extractedInfo["BrandNameChi"].str.translate(dc.mapping.charTranslateTable)
    outdf["BrandNameChi"].fillna(outdf["BrandNameEng"], inplace=True)
    outdf["BrandNameEng"].fillna(outdf["BrandNameChi"], inplace=True)
    outdf["PremierOld"] = "0"
    outdf["Premier"] = "N"
    outdf["IsLive"] = "N"
    dc.utils.title_df(outdf)
    outdf[["FullTitleEng", "FullTitleChi"]] = outdf.apply(dc.utils.constructFullTitle, axis=1)
    outdf[["ProgrammeNameEng", "ProgrammeNameChi"]] = outdf.apply(dc.utils.constructProgrammeName, axis=1)
    dc.utils.translateChar(outdf)
    dc.utils.chopCharacters(outdf)


    # Append EOF
    dc.utils.appendEOF(outdf)


    # Fill Channel #
    outdf['OwnerChannel'] = owner_channel
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)

    # Write output file
    dc.io.to_io(outdf, out_io, out_format)

def convert_nonlinear(in_io, out_io, format="preV1506", owner_channel="CTI Asia Channel"):
    "Convert nonlinear for CTI Asia Channel"
    return None

