import pandas as pd
import numpy as np
import datetime
import math
import csv
import re
import calendar
from collections import OrderedDict
import dc.utils
import dc.io
import dc.header
import dc.mapping
import dc.settings

def convert_linear(in_io, out_io, out_format="preV1506", owner_channel="Discovery Science"):
    "Convert linear schedule for Discovery Science"

    # Read input file
    import dc.TestReadingExcel
    data = dc.TestReadingExcel.read_excel(in_io, sheetname=0)
    timecol = 2
    isTime = False
    for i in data:
        colSize = 0
        for j in i:
            if re.match("^\d+:\d+$", re.sub("\s","",str(j)), re.I) is not None:
                timecol = colSize
                isTime = True
                break
            colSize +=1
        if isTime:
            break
    if type(in_io) != str:
        in_io.seek(0)
    indf = pd.read_excel(in_io, sheetname=0, skiprows=0, na_values=["n/a"], keep_default_na=True)
    indf.columns = ["".join(s.strip().split()).replace(".", "").replace("(", "").replace(")", "").replace("/", "").lower() for s in indf.columns]              
    indf = indf.rename(columns={'date': 'TX Date'})
    indf = indf.rename(columns={'time': 'Actual Time'})
    indf = indf.rename(columns={'fulltitleenglish': 'fulltitleenglishmax40characters'})
    indf = indf.rename(columns={'fulltitlechinese': 'fulltitlechinesemax24characters'})
    indf = indf.rename(columns={'seasonno': 'seasonnomax3digits'})
    indf = indf.rename(columns={'seasonnumber': 'seasonnomax3digits'})
    indf = indf.rename(columns={'episodeno': 'episodenomax5digits'})
    indf = indf.rename(columns={'programsynopsisenglish': 'Synopsis (English)'})
    indf = indf.rename(columns={'programsynopsischinese': 'Synopsis (Chinese)'})
    indf = indf.rename(columns={'shortsynopsisenglish': 'Short Synopsis (English) (max 120 chars)'})
    indf = indf.rename(columns={'shortsynopsischinese': 'Short Synopsis (Chinese) (max 120 chars)'})
    indf = indf.rename(columns={'originallanguage': 'Original language (max 50 chars)'})
    indf = indf.rename(columns={'regioncode': 'Region Code (max 2 chars)'})
    indf = indf.rename(columns={'firstreleaseyear': 'First release year (max 4 digits)'})

    
    # Create output DataFrame
    outdf = pd.DataFrame(columns=dc.header.internalHeader)

    # Extract data from full title


    # Fill output DataFrame with related info
    excepted_column = ["PID", "Premier", "Recordable", "Is NPVR Prog", "Is Restart TV", "effective_date", "expiration_date", "Media ID (max. 12 chars)"]
    for internal_column, indf_column in dc.header.postV1506HeaderMapping.items():
        if (indf_column not in excepted_column): 
            edited_indf_column = "".join(indf_column.split()).replace(".", "").replace("(", "").replace(")", "").replace("/", "").lower()
            validate_similarity = [dc.utils.validateSimilarity(edited_indf_column, i) for i in indf.columns]
            if (edited_indf_column in indf.columns) or any(validate_similarity):
                if edited_indf_column not in indf.columns:
                    edited_indf_column = indf.columns[validate_similarity.index(True)]
                outdf[internal_column] = indf[edited_indf_column]
                if (outdf[internal_column].dtype == object):
                    outdf[internal_column] = outdf[internal_column].where(outdf[internal_column].apply(lambda x: pd.notnull(x) and len(str(x).strip()) > 0))
    if all(pd.isnull(outdf['ActualTime'])):
        outdf['ActualTime'] = indf[indf.columns[timecol]]
    outdf['TXDate'] = outdf['TXDate'][outdf['TXDate'].notnull()].apply(lambda x: str(x))
    outdf['ActualTime'] = outdf['ActualTime'].astype('str').str.slice(-8).str.slice(0, 5).str.zfill(5)
    tempExtractedInfo = [
        outdf["BrandNameEng"].str.extract("(?P<BrandNameEng>.*?)\s?(-\s)?Series\s(?P<SeasonNo>\d+)$", expand=False)
        ,outdf["BrandNameEng"].str.extract("(?P<BrandNameEng>.+)$", expand=False)
    ]
    tempExtractedInfoChi = [
        outdf["BrandNameChi"].str.extract("(?P<BrandNameChi>.*?)\s?(-\s)?第(?P<SeasonNo>\d+)季$", expand=False)
        ,outdf["BrandNameChi"].str.extract("(?P<BrandNameChi>.+)$", expand=False)
    ]
    outdf["BrandNameEng"] = tempExtractedInfo[0]["BrandNameEng"]
    outdf["BrandNameEng"].fillna(tempExtractedInfo[1], inplace=True)
    outdf["BrandNameEng"] = outdf["BrandNameEng"].str.replace("\(Wt\)$", "")
    outdf["BrandNameEng"] = outdf["BrandNameEng"].str.replace("\(Pilot\)$", "")
    outdf["BrandNameEng"] = outdf["BrandNameEng"].apply(lambda x: np.nan if isinstance(x, str) and len(x.strip()) == 0 else x)
    outdf["BrandNameEng"] = outdf["BrandNameEng"].str.replace("\(Eng(lish)?\sVersion\)", "")
    tempVersion = outdf["BrandNameEng"].apply(lambda x: "" if re.compile("\(Eng(lish)?\sVersion\)").search(x) is None else "Eng Ver")
    outdf["EditionVersionEng"].fillna(tempVersion, inplace=True)
    outdf["SeasonNo"] = tempExtractedInfo[0]["SeasonNo"]
    outdf["SeasonNo"].fillna(indf["seasonnomax3digits"], inplace=True)
    outdf["BrandNameChi"] = tempExtractedInfoChi[0]["BrandNameChi"]
    outdf["BrandNameChi"].fillna(tempExtractedInfoChi[1], inplace=True)
    outdf["BrandNameChi"].fillna(outdf["BrandNameEng"], inplace=True)
    outdf["BrandNameEng"].fillna(outdf["BrandNameChi"], inplace=True)
    outdf["EpisodeNameChi"] = outdf["EpisodeNameChi"].apply(lambda x: np.nan if isinstance(x, str) and len(x.strip()) == 0 else x)
    outdf["EpisodeNameEng"] = outdf["EpisodeNameEng"].apply(lambda x: np.nan if isinstance(x, str) and len(x.strip()) == 0 else x)
    tempExtractedEpisode = [
        outdf["EpisodeNameEng"].str.extract("(?i)Ep(?:isode|) (?P<EpisodeNo>\d+)", expand=False)
        ,outdf["EpisodeNameEng"].str.extract("^(?P<EpisodeNameEng>.+)$", expand=False)
    ]
    outdf["EpisodeNo"].fillna(tempExtractedEpisode[0], inplace=True)
    outdf["EpisodeNameEng"] = outdf["EpisodeNameEng"].str.replace("(?i)Ep(?:isode|) \d+", "").str.replace("第\d+集", "")
    outdf["EpisodeNameChi"] = outdf["EpisodeNameChi"].str.replace("第\d+集", "").str.replace("(?i)Ep(?:isode|) \d+", "")    
    outdf["EpisodeNameEng"] = outdf["EpisodeNameEng"].replace(outdf["BrandNameEng"][outdf["BrandNameEng"].notnull()], '')
    outdf["EpisodeNameChi"] = outdf["EpisodeNameChi"].replace(outdf["BrandNameChi"][outdf["BrandNameChi"].notnull()], '')
    outdf["SponsorTextStuntChi"].fillna(outdf["SponsorTextStuntEng"], inplace=True)
    outdf["SponsorTextStuntEng"].fillna(outdf["SponsorTextStuntChi"], inplace=True)
    outdf["BrandNameChi"].fillna(outdf["BrandNameEng"], inplace=True)
    outdf["BrandNameEng"].fillna(outdf["BrandNameChi"], inplace=True)
    outdf["SeasonNameChi"].fillna(outdf["SeasonNameEng"], inplace=True)
    outdf["SeasonNameEng"].fillna(outdf["SeasonNameChi"], inplace=True)
    outdf["EpisodeNameChi"] = outdf["EpisodeNameChi"].apply(lambda x: np.nan if isinstance(x, str) and len(x.strip()) == 0 else x)
    outdf["EpisodeNameEng"] = outdf["EpisodeNameEng"].apply(lambda x: np.nan if isinstance(x, str) and len(x.strip()) == 0 else x)
    outdf["EpisodeNameChi"] = outdf["EpisodeNameChi"][outdf["EpisodeNameChi"].notnull()].apply(lambda x: dc.utils.jianfan(str(x))).astype(str).str.translate(dc.mapping.charTranslateTable)
    outdf["EpisodeNameChi"].fillna(outdf["EpisodeNameEng"], inplace=True)
    outdf["EpisodeNameEng"].fillna(outdf["EpisodeNameChi"], inplace=True)
    outdf["EpisodeNameChi"].fillna("", inplace=True)
    outdf["EpisodeNameEng"].fillna("", inplace=True)
    outdf["SynopsisEng"].fillna(outdf["ShortSynopsisEng"], inplace=True)
    outdf["SynopsisEng"].fillna(outdf["SynopsisChi"], inplace=True)
    outdf["SynopsisChi"].fillna(outdf["SynopsisEng"], inplace=True)
    outdf["SynopsisChi"] = outdf["SynopsisChi"][outdf["SynopsisChi"].notnull()].apply(lambda x: dc.utils.jianfan(str(x))).astype(str).str.translate(dc.mapping.charTranslateTable)
    outdf["ShortSynopsisChi"].fillna(outdf["SynopsisChi"], inplace=True)
    outdf["ShortSynopsisChi"] = outdf["ShortSynopsisChi"][outdf["ShortSynopsisChi"].notnull()].apply(lambda x: dc.utils.jianfan(str(x))).astype(str).str.translate(dc.mapping.charTranslateTable)
    outdf["ShortSynopsisChi"] = outdf["ShortSynopsisChi"][outdf["ShortSynopsisChi"].notnull()].apply(lambda x: x[0:dc.settings.char_limit.get("ShortSynopsisChi")])
    outdf["ShortSynopsisEng"].fillna(outdf["SynopsisEng"], inplace=True)
    outdf["ShortSynopsisEng"] = outdf["ShortSynopsisEng"][outdf["ShortSynopsisEng"].notnull()].apply(lambda x: x[0:dc.settings.char_limit.get("ShortSynopsisEng")])
    outdf["SeasonNo"] = outdf["SeasonNo"][outdf["SeasonNo"].notnull()].astype(int).astype(str)
    outdf["SeasonNo"] = outdf["SeasonNo"].str.replace("^0$", "")
    outdf["EpisodeNo"] = outdf["EpisodeNo"][outdf["EpisodeNo"].notnull()].astype(int).astype(str)
    outdf["EpisodeNo"] = outdf["EpisodeNo"].str.replace("^0$", "")
    outdf["PremierOld"] = "0"
    outdf["FirstReleaseYear"] = outdf["FirstReleaseYear"][outdf["FirstReleaseYear"].notnull()].astype(int).astype(str)
    outdf["FirstReleaseYear"] = outdf["FirstReleaseYear"].mask(outdf["FirstReleaseYear"].astype(str).str.replace("\.0*","") == "0")                 
    text_column = ["SponsorTextStuntEng", "SponsorTextStuntChi", "BrandNameEng", "BrandNameChi", "EditionVersionEng", "EditionVersionChi", "SeasonNameEng", "SeasonNameChi", "EpisodeNameEng", "EpisodeNameChi", "SynopsisEng", "SynopsisChi", "ShortSynopsisEng", "ShortSynopsisChi", "DirectorProducerEng", "DirectorProducerChi", "CastEng", "CastChi"]
    for column in text_column:
        outdf[column] = outdf[column].astype(str).where(outdf[column].notnull()).str.translate(dc.mapping.charTranslateTable)
    dc.utils.title_df(outdf)
    outdf[["FullTitleEng", "FullTitleChi"]] = outdf.apply(dc.utils.constructFullTitle, axis=1)
    outdf[["ProgrammeNameEng", "ProgrammeNameChi"]] = outdf.apply(dc.utils.constructProgrammeName, axis=1)
    dc.utils.translateChar(outdf)
    dc.utils.chopCharacters(outdf)


    # Append EOF
    dc.utils.appendEOF(outdf)


    # Fill Channel #
    outdf['OwnerChannel'] = owner_channel
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)

    # Write output file
    dc.io.to_io(outdf, out_io, out_format)

def convert_nonlinear(in_io, out_io, format="preV1506", owner_channel="Discovery Science"):
    "Convert nonlinear for Discovery Science"
    return None

