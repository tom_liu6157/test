import pandas as pd
import numpy as np
import datetime
import math
import csv
import re
import calendar
from collections import OrderedDict
import dc.utils
import dc.io
import dc.header
import dc.mapping
import dc.settings

def convert_linear(in_io, out_io, out_format="preV1506", owner_channel="Nick Jr."):
    "Convert linear schedule for Nick Jr."

    # Read input file
    import dateutil
    indf1 = pd.read_excel(in_io, sheetname=0)
    if type(in_io) != str:
        in_io.seek(0)
    headerIndex = indf1[indf1.columns[0]][indf1[indf1.columns[0]].notnull()].index.min()
    skiprows = indf1[indf1.columns[0]].tolist().index("Date")
    topic = ""
    if re.match("^NICK JUNIOR GLOBAL.*$",indf1[indf1.columns[0]][headerIndex]) is not None:
        topic = indf1[indf1.columns[0]][headerIndex]
        headerIndex = 1
    else:
        topic = indf1.columns[0]
        headerIndex = 0
    indf = pd.read_excel(in_io, sheetname=0,header=headerIndex,skiprows=skiprows+1)
    if type(in_io) != str:
        in_io.seek(0)
    if indf.columns[0] !="Date":
        indf = pd.read_excel(in_io, sheetname=0,skiprows=skiprows+2)
    month = dateutil.parser.parserinfo().month(topic.split()[-2].strip())
    if(int(month) == 12):
        monthNext = "01"
    else:
        monthNext = "0"+str(int(month)+1)
    indf = indf[indf[indf.columns[1]].notnull()]
    indf = indf[indf["Date"].notnull()]

    
    # Create output DataFrame
    outdf = pd.DataFrame(columns=dc.header.internalHeader)

    # Extract data from full title


    # Fill output DataFrame with related info
    outdf["TXDate"] = indf["Date"].apply(lambda x: re.match("^([0-9]+)(?:\.0+|[0-9]*)$", str(x)).groups()[0] if re.match("^[0-9]+(?:\.0+|[0-9]*)$", str(x)) else pd.to_datetime(x, format="%d/%m/%y").strftime("%Y%m%d") if "/" in str(x) else x.strftime("%Y%m%d") if isinstance(x, datetime.datetime) or isinstance(x, datetime.time) else x)
    outdf["TXDate"] = outdf["TXDate"].apply(lambda x: x[0:4]+x[6:]+x[4:6] if int(x[4:6]) != int(month) and x[4:] != monthNext+"01" else x)
    if "Synopsis " in indf.columns:
        Synopsis="Synopsis "
    else:
        Synopsis="Short Episode Synopsis"
    outdf["ActualTime"] = indf[indf.columns[1]].astype('str').str.slice(-8).str.slice(0, 5).str.zfill(5)
    outdf["BrandNameEng"]=indf["Program Title"]
    outdf["BrandNameChi"]=outdf["BrandNameEng"]
    outdf["EpisodeNameEng"]=indf["Episode Title"]
    outdf["EpisodeNameChi"]=outdf["EpisodeNameEng"]
    outdf["SeasonNo"] = indf["Season Number"][indf["Season Number"].notnull()].apply(lambda x: str(int(re.match("^([0-9]+)(?:\.0+|[0-9]*)$", str(x)).groups()[0])) if re.match("^[0-9]+(?:\.0+|[0-9]*)$", str(x)) else None)
    outdf["SeasonNameChi"]= indf["Season Number"].where(indf["Season Number"].apply(lambda x: pd.isnull(re.match("^[0-9]+(?:\.0+|[0-9]*)$", str(x))))).fillna(outdf["SeasonNameChi"])
    outdf["SeasonNameEng"]= indf["Season Number"].where(indf["Season Number"].apply(lambda x: pd.isnull(re.match("^[0-9]+(?:\.0+|[0-9]*)$", str(x))))).fillna(outdf["SeasonNameEng"])
    outdf["EpisodeNo"] = indf["Episode Number"][indf["Episode Number"].notnull()].astype(int).astype(str).str.replace("^(0|00)$","").apply(lambda x: x if len(x) > 0 else None)
    outdf["SynopsisEng"] = indf[Synopsis].str.translate(dc.mapping.charTranslateTable)
    outdf["SynopsisChi"] = outdf["SynopsisEng"]
    outdf["ShortSynopsisEng"] = outdf["SynopsisEng"][outdf["SynopsisEng"].notnull()].apply(lambda x: x[0:dc.settings.char_limit.get("ShortSynopsisEng")])
    outdf["ShortSynopsisChi"] = outdf["ShortSynopsisEng"]
    outdf["PremierOld"] = "0"
    outdf["Premier"] = "N"
    outdf["IsLive"] = "N"
    outdf["IsEpisodic"] = (outdf["EpisodeNo"].notnull() | outdf["EpisodeNameEng"].notnull()).map({True:"Y", False:"N"})
    text_column = ["SponsorTextStuntEng", "SponsorTextStuntChi", "BrandNameEng", "BrandNameChi", "EditionVersionEng", "EditionVersionChi", "SeasonNameEng", "SeasonNameChi", "EpisodeNameEng", "EpisodeNameChi", "SynopsisEng", "SynopsisChi", "ShortSynopsisEng", "ShortSynopsisChi", "DirectorProducerEng", "DirectorProducerChi", "CastEng", "CastChi"]
    for column in text_column:
        outdf[column] = outdf[column].astype(str).where(outdf[column].notnull()).str.translate(dc.mapping.charTranslateTable)
    dc.utils.title_df(outdf)
    outdf[["FullTitleEng", "FullTitleChi"]] = outdf.apply(dc.utils.constructFullTitle, axis=1)
    outdf[["ProgrammeNameEng", "ProgrammeNameChi"]] = outdf.apply(dc.utils.constructProgrammeName, axis=1)
    dc.utils.translateChar(outdf)
    dc.utils.chopCharacters(outdf)


    # Append EOF
    lastProgDuration = pd.Timedelta("00:00:00")
    dc.utils.appendEOF(outdf, lastprog_duration=lastProgDuration)


    # Fill Channel #
    outdf['OwnerChannel'] = owner_channel
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)

    # Write output file
    dc.io.to_io(outdf, out_io, out_format)

def convert_nonlinear(in_io, out_io, format="preV1506", owner_channel="Nick Jr."):
    "Convert nonlinear for Nick Jr."
    return None

