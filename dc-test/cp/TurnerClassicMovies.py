import pandas as pd
import numpy as np
import datetime
import math
import csv
import re
import calendar
from collections import OrderedDict
import dc.utils
import dc.io
import dc.header
import dc.mapping
import dc.settings

def convert_linear(in_io, out_io, out_format="preV1506", owner_channel="Turner Classic Movies"):
    "Convert linear schedule for Turner Classic Movies"

    # Read input file
    indf = pd.read_excel(in_io, sheetname=0, skiprows=0)

    
    # Create output DataFrame
    outdf = pd.DataFrame(columns=dc.header.internalHeader)

    # Extract data from full title
    fullEpisodeTitle = indf["Episode Title"][indf["Episode Title"].notnull()].apply(lambda x: str(x).strip()).astype('str')
    fullTitleEng = indf["Programme Title"][indf["Programme Title"].notnull()].apply(lambda x: str(x).strip())
    extractedInfo = pd.DataFrame(columns=["FirstReleaseYear", "EpisodeNameEng", "EditionVersionEng", "BrandNameEng"])
    tempExtractedInfo = [
        fullEpisodeTitle.str.extract("Prod Year\s*(?P<FirstReleaseYear>\d*)", expand=False)
        ,fullTitleEng.str.extract("(?P<BrandNameEng>.+)\s*\((?P<EditionVersionEng>\d+)\)", expand=False)
    ]
    extractedInfo["BrandNameEng"] = fullTitleEng
    extractedInfo["FirstReleaseYear"] = tempExtractedInfo[0].notnull()
    extractedInfo["EpisodeNameEng"] = fullEpisodeTitle.str.replace("Prod Year\s*\d*","")
    extractedInfo["EditionVersionEng"] = tempExtractedInfo[1]["EditionVersionEng"]
    extractedInfo["BrandNameEng"] = tempExtractedInfo[1]["BrandNameEng"].str.strip()
    extractedInfo["BrandNameEng"].fillna(fullTitleEng, inplace=True)


    # Fill output DataFrame with related info
    outdf["TXDate"] = indf["Date"].apply(lambda x: pd.to_datetime(x, format="%d-%b-%Y")).apply(lambda x:x.strftime("%Y%m%d"))
    outdf["ActualTime"] = indf["Start Time"].apply(lambda x: pd.to_datetime(x, format="%H:%M")).apply(lambda x:x.strftime("%H:%M"))
    outdf["BrandNameEng"]=extractedInfo["BrandNameEng"]
    outdf["BrandNameEng"].fillna(indf["Programme Title"], inplace=True)
    outdf["BrandNameChi"]=outdf["BrandNameEng"]
    outdf["EditionVersionEng"] = extractedInfo["EditionVersionEng"]
    outdf["EditionVersionChi"] = extractedInfo["EditionVersionEng"]
    outdf = outdf[outdf["BrandNameEng"].notnull()]
    outdf["EpisodeNameEng"] = extractedInfo["EpisodeNameEng"]
    outdf["EpisodeNameChi"] = extractedInfo["EpisodeNameEng"]
    outdf["SynopsisEng"] = indf["Website Synopsis"].str.translate(dc.mapping.charTranslateTable)
    outdf["SynopsisChi"] = outdf["SynopsisEng"]
    outdf["ShortSynopsisEng"] = indf["Digital EPG Synopsis"][indf["Digital EPG Synopsis"].notnull()].apply(lambda x: x[0:dc.settings.char_limit.get("ShortSynopsisEng")])
    outdf["ShortSynopsisChi"] = outdf["ShortSynopsisEng"]
    outdf["PremierOld"] = "0"
    outdf["Premier"] = "N"
    outdf["IsLive"] = "N"
    outdf["Classification"] = indf["Classification"]
    outdf["CastEng"] = indf["Actors"][indf["Actors"].notnull()].apply(lambda x: str(x).strip()).str.replace('(\s*,\s*|\s*、\s*|\s*，\s*)', ', ').str.translate(dc.mapping.charTranslateTable)
    outdf["CastChi"] = outdf["CastEng"]
    outdf["DirectorProducerEng"] = indf["Director"][indf["Director"].notnull()].apply(lambda x: str(x).strip()).str.replace('(\s*,\s*|\s*、\s*|\s*，\s*)', ',').str.translate(dc.mapping.charTranslateTable)
    outdf["DirectorProducerEng"] = outdf["DirectorProducerEng"].str.strip().str.replace('(^,|,$)', '')
    outdf["DirectorProducerChi"] = outdf["DirectorProducerEng"]
    outdf["Genre"] = indf["Major Programme Genre"]
    indf["Sub Genre"].fillna("", inplace=True)
    outdf["SubGenre"] = indf["Sub Genre"].str.split("/").apply(lambda subGenres: sorted(set([item for subGenre in subGenres for item in dc.mapping.subGenreMapping.get(subGenre.strip().lower(), [subGenre])]))).str.join("/").fillna(indf["Sub Genre"])
    outdf["OriginalLang"] = indf["Language"]
    outdf["AudioLang"] = outdf['OriginalLang']
    outdf["IsEpisodic"] = (outdf["EpisodeNo"].notnull() | outdf["EpisodeNameEng"].notnull() | outdf["EpisodeNameChi"].notnull()).map({True:"Y", False:"N"})
    outdf["FirstReleaseYear"].fillna(indf["Year of Production"][indf["Year of Production"].notnull()].astype(int).astype(str), inplace=True)
    outdf["FirstReleaseYear"] = outdf["FirstReleaseYear"][outdf["FirstReleaseYear"].notnull()].replace('t.b.c.', '')
    dc.utils.title_df(outdf)
    outdf[["FullTitleEng", "FullTitleChi"]] = outdf.apply(dc.utils.constructFullTitle, axis=1)
    outdf[["ProgrammeNameEng", "ProgrammeNameChi"]] = outdf.apply(dc.utils.constructProgrammeName, axis=1)
    dc.utils.translateChar(outdf)
    dc.utils.chopCharacters(outdf)


    # Append EOF
    lastProgDurationInMinutes = indf["Nominal Length"][indf.last_valid_index()].strip().replace(' mins', '')
    lastProgDuration = pd.to_timedelta(int(lastProgDurationInMinutes)*60, unit='s')
    dc.utils.appendEOF(outdf, lastprog_duration=lastProgDuration)


    # Fill Channel #
    outdf['OwnerChannel'] = owner_channel
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)

    # Write output file
    dc.io.to_io(outdf, out_io, out_format)

def convert_nonlinear(in_io, out_io, format="preV1506", owner_channel="Turner Classic Movies"):
    "Convert nonlinear for Turner Classic Movies"
    return None

