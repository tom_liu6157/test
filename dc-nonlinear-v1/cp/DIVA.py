import pandas as pd
import numpy as np
import datetime
import math
import csv
import re
import calendar
from collections import OrderedDict
import dc.utils
import dc.io
import dc.header
import dc.mapping
import dc.settings

def convert_linear(in_io, out_io, out_format="preV1506", owner_channel="DIVA"):
    "Convert linear schedule for DIVA"

    # Read input file
    indf = pd.read_excel(in_io, sheetname=0, skiprows=0)
    indf.dropna(subset=['Start Date', 'StartTime'], inplace=True)

    
    # Create output DataFrame
    outdf = pd.DataFrame(columns=dc.header.internalHeader)

    # Extract data from full title
    fullTitleChi = indf["Traditional Chinese Title"][indf["Traditional Chinese Title"].notnull()].apply(lambda x: str(x).strip())
    fullTitleEng = indf["Programme_Title"][indf["Programme_Title"].notnull()].apply(lambda x: str(x).strip())
    extractedInfo = pd.DataFrame(columns=["SponsorTextEng", "SponsorTextChi", "BrandNameEng", "BrandNameChi", "SeasonNo", "SeasonNameEng", "SeasonNameChi", "EpisodeNo", "EpisodeNameEng", "EpisodeNameChi", "RegionCode", "FirstReleaseYear"])
    hardCodeBrandNameEng = "My Kitchen Rules NZ|Law & Order: Special Victims Unit|Say Yes To The Dress: Atlanta|Say Yes To The Dress: Canada|Just For Laughs: Gags|Katherine Mills: Mind Games|Supermodelme: Sirens|Clean House: Search For The Messiest Home In The Country|Little Big Shots: Forever Young"
    hardCodeBrandNameChi = "我的廚房我作主：紐西蘭版|法網遊龍：特案組|法網游龍：特案組|我的夢幻婚紗：亞特蘭大篇|我的夢幻婚紗：加拿大篇|超模是我：魅惑女伶|百變模特兒大戰：美國版|幫我做造型：亞洲版|狗窩大作戰：尋覓終極狗窩|狗窩大作戰：紐約"
    hardCodeSponsorTextEng = "LXTV"
    tempExtractedInfo = [
        fullTitleEng.str.extract("^((?P<SponsorTextEng>"+hardCodeSponsorTextEng+"):)*\s*(?P<BrandNameEng>("+hardCodeBrandNameEng+"|.+?))\s*(:\s(?P<SeasonNameEng1>[^\(]+))*\s*(?P<SeasonNameEng2>S\d[^\(]+)*\s*(\(S(?P<SeasonNo>\d+[aAbB]*)\))*\s*(\(S\d+[aAbB]*\).+)*\s*$", expand=False),
        fullTitleChi.str.extract("^(<(?P<SponsorTextChi>.*)>:)*\s*(?P<BrandNameChi>("+hardCodeBrandNameChi+"|.+?))\s*(：(?P<SeasonNameChi1>[^-＜<〈]+))*\s*(([＜<〈]*(?P<SeasonNoChi1>第(.*?)季[aAbB]*)[>＞〉]*)*\s*(?P<SeasonNameChi2>[＜<〈]*(?P<SeasonNoChi2>第(.*?)季[aAbB]*)[>＞〉]*[^-]+)*-\s*(第.*集)*(?P<EpisodeNameChi>.*))*$", expand=False)
    ]
    extractedInfo["SponsorTextEng"] = tempExtractedInfo[0]["SponsorTextEng"]
    extractedInfo["SponsorTextChi"] = tempExtractedInfo[1]["SponsorTextChi"]
    extractedInfo["BrandNameEng"] = tempExtractedInfo[0]["BrandNameEng"].where(indf["Eps"][indf["Programme_Title"].notnull()].notnull()).fillna(fullTitleEng)
    extractedInfo["BrandNameChi"] = tempExtractedInfo[1]["BrandNameChi"].where(indf["Eps"][indf["Traditional Chinese Title"].notnull()].notnull()).fillna(fullTitleChi)
    extractedInfo["SeasonNo"] = tempExtractedInfo[0]["SeasonNo"]
    tempExtractedInfo[1]["SeasonNoChi1"].fillna(tempExtractedInfo[1]["SeasonNoChi2"], inplace=True)
    extractedInfo["SeasonNo"].fillna(tempExtractedInfo[1]["SeasonNoChi1"].apply(lambda x: dc.utils.chinese2num(x)), inplace=True)
    tempExtractedInfo[0]["SeasonNameEng1"].fillna(tempExtractedInfo[0]["SeasonNameEng2"], inplace=True)
    extractedInfo["SeasonNameEng"] = tempExtractedInfo[0]["SeasonNameEng1"].where(indf["Eps"][indf["Programme_Title"].notnull()].notnull())
    tempExtractedInfo[1]["SeasonNameChi1"].fillna(tempExtractedInfo[1]["SeasonNameChi2"], inplace=True)
    extractedInfo["SeasonNameChi"] = tempExtractedInfo[1]["SeasonNameChi1"].where(indf["Eps"][indf["Traditional Chinese Title"].notnull()].notnull())
    extractedInfo['SeasonNameChi'] = extractedInfo['SeasonNameChi'][extractedInfo['SeasonNameChi'].notnull()].str.replace('\s*[＜<〈]第.*季[>＞〉]\s*', '')
    extractedInfo['SeasonNameChi'] = extractedInfo['SeasonNameChi'][extractedInfo['SeasonNameChi'].notnull()].str.replace('\s*\(首播無字幕\)\s*', '')
    extractedInfo["EpisodeNo"] = indf["Eps"][indf["Eps"].notnull()].str.replace('(Ep\s*)', '')
    extractedInfo["SeasonNameEng"] = tempExtractedInfo[0][7].where(((extractedInfo["BrandNameEng"] == "Say Yes To The Dress: Canada") & ((tempExtractedInfo[0][7] == '(S1a)') | (tempExtractedInfo[0][7] == '(S1b)')))).fillna(extractedInfo["SeasonNameEng"])
    if extractedInfo["SeasonNameEng"].count() > 0:
        extractedInfo["SeasonNameEng"] = extractedInfo["SeasonNameEng"].str.replace("\s*\((S\d+[abAB])\)\s*", "\\1")
    extractedInfo["SeasonNameChi"] = tempExtractedInfo[1][7].where(((extractedInfo["BrandNameEng"] == "Say Yes To The Dress: Canada") & ((tempExtractedInfo[0][7] == '(S1a)') | (tempExtractedInfo[0][7] == '(S1b)')))).fillna(extractedInfo["SeasonNameChi"])
    if extractedInfo["SeasonNameChi"].count() > 0:
        extractedInfo["SeasonNameChi"] = extractedInfo["SeasonNameChi"].str.replace("\s*[＜<〈](第.*季[abAB])[>＞〉]\s*", "\\1")
    extractedInfo["EpisodeNameEng"] = indf["Episode_Title"][indf["Episode_Title"].notnull()].str.replace('(Episode\s*\d*)', '')
    extractedInfo["EpisodeNameChi"] = tempExtractedInfo[1]["EpisodeNameChi"].where(indf["Eps"][indf["Traditional Chinese Title"].notnull()].notnull())
    extractedInfo["EpisodeNameChi"]=extractedInfo["EpisodeNameChi"].str.replace('\s*[＜<〈]第.*集[>＞〉]\s*', '')
    tmpIndex = 0
    for item in indf["Eps"]:
        if pd.isnull(item):
            if not pd.isnull(extractedInfo["EpisodeNameChi"][tmpIndex]):
                extractedInfo["BrandNameChi"][tmpIndex]=extractedInfo["BrandNameChi"][tmpIndex]+"-"+extractedInfo["EpisodeNameChi"][tmpIndex]
                extractedInfo["EpisodeNameChi"][tmpIndex]=None
        tmpIndex += 1


    # Fill output DataFrame with related info
    outdf["TXDate"] = indf["Start Date"].apply(lambda x: pd.to_datetime(x, format="%d/%m/%Y")).apply(lambda x:x.strftime("%Y%m%d"))
    outdf["ActualTime"] = indf["StartTime"].apply(lambda x: pd.to_datetime(x, format="%H:%M:%S")).apply(lambda x:x.strftime("%H:%M"))
    outdf["SponsorTextStuntEng"] = extractedInfo["SponsorTextEng"].str.translate(dc.mapping.charTranslateTable)
    outdf["SponsorTextStuntChi"] = extractedInfo["SponsorTextChi"].str.translate(dc.mapping.charTranslateTable)
    outdf["SponsorTextStuntEng"].fillna(outdf["SponsorTextStuntChi"], inplace=True)
    outdf["SponsorTextStuntChi"].fillna(outdf["SponsorTextStuntEng"], inplace=True)
    outdf["BrandNameEng"] = extractedInfo["BrandNameEng"][extractedInfo["BrandNameEng"].notnull()].str.translate(dc.mapping.charTranslateTable).apply(lambda x: x[0:dc.settings.char_limit.get("BrandNameEng")])
    outdf["BrandNameChi"] = extractedInfo["BrandNameChi"][extractedInfo["BrandNameChi"].notnull()].str.translate(dc.mapping.charTranslateTable).apply(lambda x: x[0:dc.settings.char_limit.get("BrandNameChi")])
    outdf["BrandNameChi"].fillna(outdf["BrandNameEng"], inplace=True)
    outdf["BrandNameEng"].fillna(outdf["BrandNameChi"], inplace=True)
    outdf["SeasonNo"] = extractedInfo["SeasonNo"]
    if extractedInfo["SeasonNameEng"].count() > 0:
        outdf["SeasonNameEng"] = extractedInfo["SeasonNameEng"][extractedInfo["SeasonNameEng"].notnull()].str.translate(dc.mapping.charTranslateTable).apply(lambda x: x[0:dc.settings.char_limit.get("SeasonNameEng")])
    if extractedInfo["SeasonNameChi"].count() > 0:
        outdf["SeasonNameChi"] = extractedInfo["SeasonNameChi"][extractedInfo["SeasonNameChi"].notnull()].str.translate(dc.mapping.charTranslateTable).apply(lambda x: x[0:dc.settings.char_limit.get("SeasonNameChi")])
    outdf["SeasonNameChi"].fillna(outdf["SeasonNameEng"], inplace=True)
    outdf["SeasonNameEng"].fillna(outdf["SeasonNameChi"], inplace=True)
    outdf["EpisodeNo"] = extractedInfo["EpisodeNo"]
    outdf["EpisodeNameEng"] = extractedInfo["EpisodeNameEng"][extractedInfo["EpisodeNameEng"].notnull()].str.translate(dc.mapping.charTranslateTable).apply(lambda x: x[0:dc.settings.char_limit.get("EpisodeNameEng")])
    outdf[["EpisodeNameEng"]] = outdf.apply(dc.utils.constructEpisodeNameEng, axis=1)
    outdf["EpisodeNameChi"] = extractedInfo["EpisodeNameChi"][extractedInfo["EpisodeNameChi"].notnull()].str.translate(dc.mapping.charTranslateTable).apply(lambda x: x[0:dc.settings.char_limit.get("EpisodeNameChi")])
    outdf[["EpisodeNameChi"]] = outdf.apply(dc.utils.constructEpisodeNameChi, axis=1)
    outdf["EpisodeNameEng"].fillna(outdf["EpisodeNameChi"], inplace=True)
    outdf["EpisodeNameChi"].fillna(outdf["EpisodeNameEng"], inplace=True)
    outdf["SynopsisEng"] = indf["Synopsis"][indf["Synopsis"].notnull()].str.translate(dc.mapping.charTranslateTable).apply(lambda x: x[0:dc.settings.char_limit.get("SynopsisEng")])
    outdf["SynopsisChi"] = indf["Synopsis"][indf["Synopsis"].notnull()].str.translate(dc.mapping.charTranslateTable).apply(lambda x: x[0:dc.settings.char_limit.get("SynopsisChi")])
    outdf["SynopsisEng"].fillna(outdf["SynopsisChi"], inplace=True)
    outdf["SynopsisChi"].fillna(outdf["SynopsisEng"], inplace=True)
    outdf["ShortSynopsisEng"] = outdf["SynopsisEng"][outdf["SynopsisEng"].notnull()].apply(lambda x: x[0:dc.settings.char_limit.get("ShortSynopsisEng")])
    outdf["ShortSynopsisChi"] = outdf["SynopsisChi"][outdf["SynopsisChi"].notnull()].apply(lambda x: x[0:dc.settings.char_limit.get("ShortSynopsisChi")])
    outdf["PremierOld"] = "0"
    outdf["Premier"] = "N"
    outdf["IsLive"] = "N"
    outdf["Classification"] = indf["Classification"].apply(lambda x: str(x).strip())
    outdf["Genre"] = indf["Programme_Genre1"][indf["Programme_Genre1"].notnull()].str.lower().map(dc.mapping.genreMapping).fillna(indf["Programme_Genre1"])
    outdf["SubGenre"] = indf["Programme_Sub_Genre1"].str.split("/")[indf["Programme_Sub_Genre1"].notnull()].apply(lambda subGenres: sorted(set([item for subGenre in subGenres for item in dc.mapping.subGenreMapping.get(subGenre.lower().strip(), [subGenre.strip()])]))).str.join("/").fillna(indf["Programme_Sub_Genre1"]).apply(lambda x: x[0:dc.settings.char_limit.get("SubGenre")])
    outdf["IsEpisodic"] = (outdf["EpisodeNo"].notnull() | outdf["EpisodeNameEng"].notnull()).map({True:"Y", False:"N"})
    dc.utils.title_df(outdf)
    outdf[["FullTitleEng", "FullTitleChi"]] = outdf.apply(dc.utils.constructFullTitle, axis=1)
    outdf[["ProgrammeNameEng", "ProgrammeNameChi"]] = outdf.apply(dc.utils.constructProgrammeName, axis=1)
    outdf["SeasonNameEng"].fillna(outdf["SeasonNo"].where(outdf["SeasonNo"].apply(lambda x: re.match("^[0-9]+[a-zA-Z]+$",str(x)) is not None)).apply(lambda x: "S"+str(x) if pd.notnull(x) else np.NaN), inplace=True)
    outdf["SeasonNameChi"].fillna(tempExtractedInfo[1]["SeasonNoChi1"].where(outdf["SeasonNo"].apply(lambda x: re.match("^[0-9]+[a-zA-Z]+$",str(x)) is not None)), inplace=True)
    outdf["FullTitleChi"] = outdf[["FullTitleChi", "SeasonNameChi","SeasonNo"]].apply(lambda x: re.sub("第[0-9]+[a-zA-Z]+季",str(x["SeasonNameChi"]),x["FullTitleChi"]) if pd.notnull(re.match("^[0-9]+[a-zA-Z]+$",str(x["SeasonNo"]))) else x["FullTitleChi"], axis=1)
    dc.utils.translateChar(outdf)
    dc.utils.chopCharacters(outdf)


    # Append EOF
    dc.utils.appendEOF(outdf)


    # Fill Channel #
    outdf['OwnerChannel'] = owner_channel
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)

    # Write output file
    dc.io.to_io(outdf, out_io, out_format)

def convert_nonlinear(in_io, out_io, format="preV1506", owner_channel="DIVA"):
    "Convert nonlinear for DIVA"
    return None

