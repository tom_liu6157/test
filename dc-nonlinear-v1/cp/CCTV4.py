import pandas as pd
import numpy as np
import datetime
import math
import csv
import re
import calendar
from collections import OrderedDict
import dc.utils
import dc.io
import dc.header
import dc.mapping
import dc.settings

def convert_linear(in_io, out_io, out_format="preV1506", owner_channel="CCTV- 4"):
    "Convert linear schedule for CCTV- 4"

    # Read input file
    channel_no = dc.mapping.ownerChannelMapping.get(owner_channel, owner_channel)
    if channel_no == "327" or channel_no == "715":
        indf = pd.read_csv(in_io, encoding="UTF-16",names=['a1', 'a2'], sep="\t+", engine='python')
    else:
        indf = pd.read_csv(in_io, encoding="UTF-8",names=['a1', 'a2'], sep="\t+", engine='python')
    indf.dropna(how="all", inplace=True)

    
    # Create output DataFrame
    outdf = pd.DataFrame(columns=dc.header.internalHeader)

    # Extract data from full title
    vdate = pd.Series()
    vtime = pd.Series()
    vtitle = pd.Series()
    vtimechi = pd.Series()
    vtitlechi = pd.Series()
    datecounter = 0
    othercounter = 0
    chiTitle = False
    if channel_no == "327" or channel_no == "715":
        temp = indf['a1'][~indf['a1'].astype('str').str.match("^\x00$")].astype('str').str.strip().str.replace("\x00","")
        tempExtractedInfo = [
            temp.str.extract("^(?P<ActualTime>(([0-9]+([-/][0-9]{2}){0,2}|([0-9]{2}[-/]){0,2}[0-9]+)*\s*[0-9]{2}(:[0-9]{2}){1,2}))*\s*(?P<BrandNameEng>.*)\s*$",expand=False)
        ]
        tmpTXDate = ""
        tmpIndex = 0
        tmpActualTime = ""
        tmpTXYear = ""
        for row in temp:
            isDateRow = False
            for month in list(dc.mapping.month.keys()):
                if month.upper() in str(row).upper().split():
                    tmpTXMonth=dc.mapping.month.get(month)
                    tmpTXDay = str(row).upper().split()[str(row).upper().split().index(month.upper())-1]
                    tmpTXYear = str(row).upper().split()[str(row).upper().split().index(month.upper())+1]
                    if (len(tmpTXYear)<4):
                        tmpTXDay = str(row).upper().split()[str(row).upper().split().index(month.upper())+1]
                        tmpTXYear = str(row).upper().split()[str(row).upper().split().index(month.upper())-1]
                    if (len(tmpTXDay)<2):
                        tmpTXDay = "0"+tmpTXDay
                    isDateRow = True
                    break
            outdf.set_value(tmpIndex, "TXDate", tmpTXYear+tmpTXMonth+tmpTXDay)  
            tmpIndex+=1
        count = 0
        for i in tempExtractedInfo[0]["ActualTime"] :            
            outdf.set_value(count, "ActualTime", i)
            count = count + 1
        count1 = 0
        for i in tempExtractedInfo[0]["BrandNameEng"] :            
            outdf.set_value(count1, "BrandNameEng", i)
            count1 = count1 + 1
        outdf["BrandNameChi"].fillna(outdf["BrandNameEng"],inplace=True)
    else:
        for index, row in indf.iterrows():
            if row['a2'] == None:
                if "day" not in row["a1"]:
                    if "中央电视台四套节目单" not in row["a1"]:
                        date = pd.to_datetime(row["a1"]).strftime("%Y%m%d")
                    else:
                        if chiTitle == False:
                            chiTitle = True
                            othercounter = 0
            else:
                if row['a1'] !=None and row['a2'] != None:
                    if chiTitle == False:
                        temp = pd.to_datetime(row["a1"], format="%H:%M").strftime("%H:%M")
                        vtime = vtime.set_value(othercounter, temp)
                        vtitle = vtitle.set_value(othercounter, row['a2'])
                        vdate = vdate.set_value(othercounter, date)
                        othercounter = othercounter + 1
                    else:
                        vtitlechi = vtitlechi.set_value(othercounter, dc.utils.jianfan(row['a2']))
                        othercounter = othercounter + 1
        outdf["TXDate"] = vdate
        outdf["ActualTime"] = vtime
        temp = pd.to_datetime(outdf["TXDate"] + " " + outdf["ActualTime"], format="%Y%m%d %H:%M")
        temp = temp.apply(lambda x: x + pd.DateOffset(hours=24) if x.hour < 4 else x)
        outdf["TXDate"] = temp.apply(lambda x: x.strftime("%Y%m%d"))
        outdf["BrandNameChi"] = vtitlechi
        outdf["BrandNameEng"] = vtitle


    # Fill output DataFrame with related info
    outdf["BrandNameEng"] = outdf["BrandNameEng"].astype(str).where(outdf["BrandNameEng"].notnull()).str.translate(dc.mapping.charTranslateTable)
    outdf["BrandNameChi"] = outdf["BrandNameChi"].astype(str).where(outdf["BrandNameChi"].notnull()).str.translate(dc.mapping.charTranslateTable)    
    outdf=outdf[outdf["ActualTime"].notnull()]
    dc.utils.title_df(outdf)
    outdf[["FullTitleEng", "FullTitleChi"]] = outdf.apply(dc.utils.constructFullTitle, axis=1)
    outdf["IsEpisodic"] = ((outdf["EpisodeNo"].notnull() & ~outdf["EpisodeNo"].astype(str).str.match("^[\s]*$"))
                           | (outdf["EpisodeNameEng"].notnull() & ~outdf["EpisodeNameEng"].astype(str).str.match("^[\s]*$"))).map({True:"Y", False:"N"})
    dc.utils.translateChar(outdf)
    dc.utils.chopCharacters(outdf)


    # Append EOF
    dc.utils.appendEOF(outdf)


    # Fill Channel #
    outdf['OwnerChannel'] = owner_channel
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)

    # Write output file
    dc.io.to_io(outdf, out_io, out_format)

def convert_nonlinear(in_io, out_io, format="preV1506", owner_channel="CCTV- 4"):
    "Convert nonlinear for CCTV- 4"
    return None

