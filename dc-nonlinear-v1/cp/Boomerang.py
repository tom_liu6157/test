import pandas as pd
import numpy as np
import datetime
import math
import csv
import re
import calendar
from collections import OrderedDict
import dc.utils
import dc.io
import dc.header
import dc.mapping
import dc.settings

def convert_linear(in_io, out_io, out_format="preV1506", owner_channel="Boomerang"):
    "Convert linear schedule for Boomerang"

    # Read input file
    indf = pd.read_excel(in_io, sheetname=0)
    indf.columns = ["".join(s.split()).replace("(", "").replace(")", "").replace("/", "").lower() for s in indf.columns]

    
    # Create output DataFrame
    outdf = pd.DataFrame(columns=dc.header.internalHeader)

    # Extract data from full title


    # Fill output DataFrame with related info
    indf = indf.rename(columns={'episodeno.mx5digits': 'episodeno.max5digits'})
    excepted_column = ["PID", "Premier", "Recordable", "Is NPVR Prog", "Is Restart TV", "effective_date", "expiration_date", "Media ID (max. 12 chars)"]
    for internal_column, indf_column in dc.header.postV1506HeaderMapping.items():
        if (indf_column not in excepted_column): 
            edited_indf_column = "".join(indf_column.split()).replace("(", "").replace(")", "").replace("/", "").lower()
            if (edited_indf_column in indf.columns):
                outdf[internal_column] = indf[edited_indf_column]
                if (outdf[internal_column].dtype != np.float64) and (outdf[internal_column].dtype != np.int64):
                    outdf[internal_column] = outdf[internal_column].where(outdf[internal_column].apply(lambda x: pd.notnull(x) and len(str(x).strip()) > 0))
    outdf["TXDate"] = outdf["TXDate"].apply(lambda x: pd.to_datetime(str(x), format="%Y%m%d") if isinstance(x, int) else x).apply(lambda x: x.strftime("%Y%m%d"))
    outdf["ActualTime"] = outdf["ActualTime"].astype('str').str.slice(-8).str.slice(0, 5).str.zfill(5)
    outdf["SeasonNo"] = outdf["SeasonNo"].apply(lambda x: str(int(re.match("^([0-9]+)(\.0*)?$", str(x)).groups()[0])) if re.match("^([0-9]+)(\.0*)?$", str(x)) else None).apply(lambda x: np.NaN if str(x).replace(".0","") =="0" else x)
    dc.utils.fixArticle(outdf)
    outdf["EpisodeNameEng"] = outdf[["BrandNameEng", "EpisodeNameEng"]].apply(lambda x: None if pd.notnull(x["BrandNameEng"]) and pd.notnull(x["EpisodeNameEng"]) and str(x["BrandNameEng"]).strip() == str(x["EpisodeNameEng"]).strip() else x["EpisodeNameEng"], axis=1)
    outdf["EpisodeNameChi"] = outdf[["BrandNameChi", "EpisodeNameChi"]].apply(lambda x: None if pd.notnull(x["BrandNameChi"]) and pd.notnull(x["EpisodeNameChi"]) and str(x["BrandNameChi"]).strip() == str(x["EpisodeNameChi"]).strip() else x["EpisodeNameChi"], axis=1)
    outdf["SponsorTextStuntChi"].fillna(outdf["SponsorTextStuntEng"], inplace=True)
    outdf["SponsorTextStuntEng"].fillna(outdf["SponsorTextStuntChi"], inplace=True)
    outdf["BrandNameChi"].fillna(outdf["BrandNameEng"], inplace=True)
    outdf["BrandNameEng"].fillna(outdf["BrandNameChi"], inplace=True)
    outdf["SeasonNameChi"].fillna(outdf["SeasonNameEng"], inplace=True)
    outdf["SeasonNameEng"].fillna(outdf["SeasonNameChi"], inplace=True)
    outdf["EpisodeNameChi"] = outdf["EpisodeNameChi"].apply(lambda x: re.match("^\/\/*([^/].*)*$",str(x)).groups()[0] if pd.notnull(re.match("^\/\/*([^/].*)*$",str(x))) else x).apply(lambda x: np.NaN if pd.notnull(x) and str(x).strip() == "" else x)
    outdf["EpisodeNameChi"].fillna(outdf["EpisodeNameEng"], inplace=True)
    outdf["EpisodeNameEng"].fillna(outdf["EpisodeNameChi"], inplace=True)
    outdf["EpisodeNameEng"] = outdf[["EpisodeNameEng", "EpisodeNo"]].apply(lambda x: x["EpisodeNo"] if (pd.isnull(x["EpisodeNameEng"]) or pd.notnull(re.match("^[\s]*$",str(x["EpisodeNameEng"])))) and pd.notnull(re.match("^[0-9]+[a-zA-Z]+$",str(x["EpisodeNo"]))) else x["EpisodeNo"]+" -"+x["EpisodeNameEng"] if (pd.notnull(x["EpisodeNameEng"]) and pd.isnull(re.match("^[\s]*$",str(x["EpisodeNameEng"])))) and pd.notnull(re.match("^[0-9]+[a-zA-Z]+$",str(x["EpisodeNo"]))) else x["EpisodeNameEng"], axis=1)
    outdf["EpisodeNameChi"] = outdf[["EpisodeNameChi", "EpisodeNo"]].apply(lambda x: x["EpisodeNo"] if (pd.isnull(x["EpisodeNameChi"]) or pd.notnull(re.match("^[\s]*$",str(x["EpisodeNameChi"])))) and pd.notnull(re.match("^[0-9]+[a-zA-Z]+$",str(x["EpisodeNo"]))) else x["EpisodeNo"]+" -"+x["EpisodeNameChi"] if (pd.notnull(x["EpisodeNameChi"]) and pd.isnull(re.match("^[\s]*$",str(x["EpisodeNameChi"])))) and pd.notnull(re.match("^[0-9]+[a-zA-Z]+$",str(x["EpisodeNo"]))) else x["EpisodeNameChi"], axis=1)
    outdf["EpisodeNameChi"].fillna(outdf["EpisodeNameEng"], inplace=True)
    outdf["EpisodeNameEng"].fillna(outdf["EpisodeNameChi"], inplace=True)
    outdf["SynopsisEng"] = outdf["SynopsisEng"].str.replace("\*\*MISSING\*\*", "")
    outdf["SynopsisChi"] = outdf["SynopsisEng"].str.replace("\*\*MISSING\*\*", "")
    outdf["ShortSynopsisChi"] = outdf["SynopsisEng"].str.replace("\*\*MISSING\*\*", "")
    outdf["ShortSynopsisEng"] = outdf["SynopsisEng"].str.replace("\*\*MISSING\*\*", "")
    outdf["SynopsisChi"].fillna(outdf["SynopsisEng"], inplace=True)
    outdf["SynopsisEng"].fillna(outdf["SynopsisChi"], inplace=True)
    outdf["ShortSynopsisChi"].fillna(outdf["ShortSynopsisEng"], inplace=True)
    outdf["ShortSynopsisEng"].fillna(outdf["ShortSynopsisChi"], inplace=True)
    outdf["EpisodeNo"] = outdf["EpisodeNo"][outdf["EpisodeNo"].notnull()].apply(lambda x: "" if not re.match("^[0-9]+[a-zA-Z]+$",str(x)) is None else x)
    text_column = ["SponsorTextStuntEng", "SponsorTextStuntChi", "BrandNameEng", "BrandNameChi", "EditionVersionEng", "EditionVersionChi", "SeasonNameEng", "SeasonNameChi", "EpisodeNameEng", "EpisodeNameChi", "SynopsisEng", "SynopsisChi", "ShortSynopsisEng", "ShortSynopsisChi", "DirectorProducerEng", "DirectorProducerChi", "CastEng", "CastChi"]
    for column in text_column:
        outdf[column] = outdf[column].astype(str).where(outdf[column].notnull()).str.translate(dc.mapping.charTranslateTable)
    dc.utils.title_df(outdf)
    outdf[["FullTitleEng", "FullTitleChi"]] = outdf.apply(dc.utils.constructFullTitle, axis=1)
    outdf[["ProgrammeNameEng", "ProgrammeNameChi"]] = outdf.apply(dc.utils.constructProgrammeName, axis=1)
    dc.utils.translateChar(outdf)
    dc.utils.chopCharacters(outdf)


    # Append EOF
    dc.utils.appendEOF(outdf)


    # Fill Channel #
    outdf['OwnerChannel'] = owner_channel
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)

    # Write output file
    dc.io.to_io(outdf, out_io, out_format)

def convert_nonlinear(in_io, out_io, format="preV1506", owner_channel="Boomerang"):
    "Convert nonlinear for Boomerang"
    return None

