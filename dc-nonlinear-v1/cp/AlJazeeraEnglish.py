import pandas as pd
import numpy as np
import datetime
import math
import csv
import re
import calendar
from collections import OrderedDict
import dc.utils
import dc.io
import dc.header
import dc.mapping
import dc.settings

def convert_linear(in_io, out_io, out_format="preV1506", owner_channel="Al Jazeera English"):
    "Convert linear schedule for Al Jazeera English"

    # Read input file
    indf = pd.read_csv(in_io, sep="\t", encoding='utf-16', header=0)
    indf_to_remove = [i for i, val in enumerate(indf["Full Title (English) 40 Characters"]) if "end of file" in val.lower()]
    indf = indf.drop(indf_to_remove)

    
    # Create output DataFrame
    outdf = pd.DataFrame(columns=dc.header.internalHeader)

    # Extract data from full title
    extractedInfo = pd.DataFrame(columns=["SponsorTextStuntEng", "BrandNameEng", "EpisodeNo", "EditionVersionEng", "EpisodeNameEng"])
    fullTitleEng = indf["Full Title (English) 40 Characters"].apply(lambda x: str(x).strip())
    tempExtractedInfo = [
        fullTitleEng.str.extract("(?P<BrandNameEng>.*?)\s*:\s*(?i)(?!(Episode|eps|ep))(?P<EpisodeNameEng>.*)", expand=False),
        fullTitleEng.str.extract("(.*(?i)(Episode|eps|ep)\.?\s*(?P<EpisodeNo>\d+))*", expand=False),
    ]
    extractedInfo["BrandNameEng"] = tempExtractedInfo[0]["BrandNameEng"]
    extractedInfo["EpisodeNameEng"] = tempExtractedInfo[0]["EpisodeNameEng"]
    extractedInfo["EpisodeNo"] = tempExtractedInfo[1]["EpisodeNo"].apply(lambda x: None if x == "" else x)


    # Fill output DataFrame with related info
    outdf["BrandNameEng"]=indf["Full Title (English) 40 Characters"].apply(lambda x:re.sub("\s*:.*", "", x)).apply(lambda x:re.sub("\s*(?i):?(Episode|eps|ep)\.?\s*\d+.*", "", x))
    outdf["BrandNameEng"] = outdf["BrandNameEng"].str.translate(dc.mapping.charTranslateTable)
    outdf["EpisodeNo"] = extractedInfo["EpisodeNo"]
    outdf["EpisodeNameEng"] = extractedInfo["EpisodeNameEng"][extractedInfo["EpisodeNameEng"].notnull()].str.translate(dc.mapping.charTranslateTable)
    outdf["BrandNameChi"] = outdf["BrandNameEng"]
    outdf["EpisodeNameChi"] = outdf["EpisodeNameEng"]
    count = 0
    preDate = ""
    for date in indf["TX Date"] :
        x = float(date)
        if (math.isnan(x)) :
            indf.loc[count,'TX Date'] = preDate
        else :
            preDate = date
        count = count + 1
    outdf["TXDate"] = indf["TX Date"].apply(lambda x: pd.to_datetime(x, format="%Y%m%d")).apply(lambda x:x.strftime("%Y%m%d"))
    outdf["ActualTime"] = indf["Actual Time"].apply(lambda x: pd.to_datetime(x, format="%H:%M")).apply(lambda x:x.strftime("%H:%M"))
    outdf["IsEpisodic"] = (outdf["EpisodeNo"].notnull() | outdf["EpisodeNameEng"].notnull() | outdf["EpisodeNameChi"].notnull()).map({True:"Y", False:"N"})
    outdf["PremierOld"] = "0"
    outdf["Premier"] = "N"
    outdf["IsLive"] = "N"
    dc.utils.title_df(outdf)
    outdf[["FullTitleEng", "FullTitleChi"]] = outdf.apply(dc.utils.constructFullTitle, axis=1)
    outdf[["ProgrammeNameEng", "ProgrammeNameChi"]] = outdf.apply(dc.utils.constructProgrammeName, axis=1)
    dc.utils.translateChar(outdf)
    dc.utils.chopCharacters(outdf)


    # Append EOF
    dc.utils.appendEOF(outdf)


    # Fill Channel #
    outdf['OwnerChannel'] = owner_channel
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)

    # Write output file
    dc.io.to_io(outdf, out_io, out_format)

def convert_nonlinear(in_io, out_io, format="preV1506", owner_channel="Al Jazeera English"):
    "Convert nonlinear for Al Jazeera English"
    return None

