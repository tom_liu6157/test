import pandas as pd
import numpy as np
import datetime
import math
import csv
import re
import calendar
from collections import OrderedDict
import dc.utils
import dc.io
import dc.header
import dc.mapping
import dc.settings

def convert_linear(in_io, out_io, out_format="preV1506", owner_channel="Baby TV"):
    "Convert linear schedule for Baby TV"

    # Read input file
    indf = pd.read_excel(in_io, sheetname=0, skiprows=0)

    
    # Create output DataFrame
    outdf = pd.DataFrame(columns=dc.header.internalHeader)

    # Extract data from full title


    # Fill output DataFrame with related info
    if len(indf.columns) == 12:
        outdf["TXDate"] = indf["Schedule Date"].apply(lambda x: pd.to_datetime(x, format="%d/%m/%Y")).apply(lambda x:x.strftime("%Y%m%d"))
        outdf["ActualTime"] = indf["Schedule Start Time"].apply(lambda x:x.strftime("%H:%M"))
        outdf["BrandNameEng"]=indf["EPG Title (English)                             Max 30 Characters"]
        outdf["BrandNameChi"]=outdf["BrandNameEng"]
        outdf["SynopsisEng"] = indf["Program Synopsis"].str.translate(dc.mapping.charTranslateTable)
        outdf["SynopsisChi"] = outdf["SynopsisEng"]
        outdf["ShortSynopsisEng"] = outdf["SynopsisEng"][outdf["SynopsisEng"].notnull()].apply(lambda x: x[0:dc.settings.char_limit.get("ShortSynopsisEng")])
        outdf["ShortSynopsisChi"] = outdf["ShortSynopsisEng"]
        outdf["PremierOld"] = "0"
        outdf["Premier"] = "N"
        outdf["IsLive"] = "N"
        outdf["SubGenre"] = indf["Classification"][indf["Classification"].notnull()].str.lower().map(dc.mapping.genreMapping).fillna(indf["Classification"])
        outdf["IsEpisodic"] = (outdf["EpisodeNo"].notnull() | outdf["EpisodeNameEng"].notnull()).map({True:"Y", False:"N"})
        dc.utils.title_df(outdf)
        outdf[["FullTitleEng", "FullTitleChi"]] = outdf.apply(dc.utils.constructFullTitle, axis=1)
        outdf[["ProgrammeNameEng", "ProgrammeNameChi"]] = outdf.apply(dc.utils.constructProgrammeName, axis=1)
    else:
        indf.columns = ["".join(s.split()).replace("(", "").replace(")", "").replace("/", "").lower() for s in indf.columns]
        excepted_column = ["PID", "Premier", "Recordable", "Is NPVR Prog", "Is Restart TV", "effective_date", "expiration_date", "Media ID (max. 12 chars)"]
        for internal_column, indf_column in dc.header.postV1506HeaderMapping.items():
            if (indf_column not in excepted_column): 
                edited_indf_column = "".join(indf_column.split()).replace("(", "").replace(")", "").replace("/", "").lower() #indf_column
                if (edited_indf_column in indf.columns):
                    outdf[internal_column] = indf[edited_indf_column]
                    if (outdf[internal_column].dtype == object):
                         outdf[internal_column] = outdf[internal_column].where(outdf[internal_column].apply(lambda x: pd.notnull(x) and len(str(x).strip()) > 0))
        outdf["TXDate"] = outdf["TXDate"].apply(lambda x: pd.to_datetime(x, format="%Y-%m-%d")).apply(lambda x:x.strftime("%Y%m%d"))                    
        outdf["ActualTime"] = outdf["ActualTime"].astype('str').str.slice(-8).str.slice(0, 5).str.zfill(5)
    dc.utils.translateChar(outdf)
    dc.utils.chopCharacters(outdf)


    # Append EOF
    lastProgDuration = pd.Timedelta("00:30:00")
    dc.utils.appendEOF(outdf, lastprog_duration=lastProgDuration)


    # Fill Channel #
    outdf['OwnerChannel'] = owner_channel
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)

    # Write output file
    dc.io.to_io(outdf, out_io, out_format)

def convert_nonlinear(in_io, out_io, format="preV1506", owner_channel="Baby TV"):
    "Convert nonlinear for Baby TV"
    return None

