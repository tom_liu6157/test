import pandas as pd
import numpy as np
import datetime
import math
import csv
import re
import calendar
from collections import OrderedDict
import dc.utils
import dc.io
import dc.header
import dc.mapping
import dc.settings

def convert_linear(in_io, out_io, out_format="preV1506", owner_channel="Variety and Travel"):
    "Convert linear schedule for Variety and Travel"

    # Read input file
    indf = pd.read_excel(in_io, header=None)
    indf.dropna(how='all', inplace=True)
    indf.columns = ["ChannelName", "TX1", "Date", "Time", "Duration", "TitleEn", "TitleCn", "DescriptionEn", "DescriptionCn", "Instruction", "1", "0", "ENG", "CHI", "0.2", "Dummy"]

    
    # Create output DataFrame
    outdf = pd.DataFrame(columns=dc.header.internalHeader)

    # Extract data from full title
    fullTitleEng = indf["TitleEn"][indf["TitleEn"].notnull()].apply(lambda x: str(x).strip())
    fullTitleChi = indf["TitleCn"][indf["TitleCn"].notnull()].apply(lambda x: str(x).strip())
    extractedInfo = pd.DataFrame(columns=["BrandNameEng", "BrandNameChi", "EpisodeNo", "EpisodeNameEng", "EpisodeNameChi", "OriginalLang"])
    tempExtractedInfo = [
        fullTitleEng.str.extract("(?P<BrandNameEng>.*?)\s*#\s*(?P<EpisodeNo>\d+)\s*\(*(?P<EpisodeNameEng>[^\[\]]*?)\)*\s*(\[(?P<OriginalLang>.*)\])*\s*$", expand=False),
        fullTitleChi.str.extract("(?P<BrandNameChi>.*?)\s*#\s*(?P<EpisodeNo>\d+)\s*\(*(?P<EpisodeNameChi>[^\[\]]*?)\)*\s*(\[(?P<OriginalLang>.*)\])*\s*$", expand=False),
        fullTitleEng.str.extract("(?P<BrandNameEng>.*?)\s*\[(?P<OriginalLang>.*)\]\s*$", expand=False),
        fullTitleChi.str.extract("(?P<BrandNameChi>.*?)\s*\[(?P<OriginalLang>.*)\]\s*$", expand=False),
    ]
    extractedInfo["BrandNameEng"] = tempExtractedInfo[0]["BrandNameEng"].fillna(tempExtractedInfo[2]["BrandNameEng"]).fillna(fullTitleEng).str.replace("\(Not Suitable for Children\)", "").str.replace("\(2nd run\)", "")
    extractedInfo["BrandNameChi"] = tempExtractedInfo[1]["BrandNameChi"].fillna(tempExtractedInfo[3]["BrandNameChi"]).fillna(fullTitleChi).str.replace("\(兒童不宜\)", "").str.replace("\(重\)", "")
    extractedInfo["EpisodeNo"] = tempExtractedInfo[0]["EpisodeNo"].fillna(tempExtractedInfo[1]["EpisodeNo"])
    extractedInfo["EpisodeNo"] = extractedInfo["EpisodeNo"][extractedInfo["EpisodeNo"].notnull()].astype(int).astype(str)
    extractedInfo["EpisodeNameEng"] = tempExtractedInfo[0]["EpisodeNameEng"]
    extractedInfo["EpisodeNameChi"] = tempExtractedInfo[1]["EpisodeNameChi"]
    extractedInfo["OriginalLang"] = tempExtractedInfo[0]["OriginalLang"].fillna(tempExtractedInfo[2]["OriginalLang"]).fillna(tempExtractedInfo[1]["OriginalLang"]).fillna(tempExtractedInfo[3]["OriginalLang"])
    for column in ["BrandNameEng", "BrandNameChi", "EpisodeNameEng", "EpisodeNameChi", "OriginalLang"]:
        extractedInfo[column] = extractedInfo[column].apply(lambda x: x.strip() if isinstance(x, str) and len(x.strip()) > 0 else np.NaN)


    # Fill output DataFrame with related info
    outdf["TXDate"] = indf["Date"].apply(lambda x: pd.to_datetime(x, format="%d/%m/%Y")).apply(lambda x:x.strftime("%Y%m%d"))
    outdf["ActualTime"] = indf["Time"].astype(str).apply(lambda x: pd.to_datetime(x[-8:], format="%H:%M:%S") if len(x) >=8 and x.count(":") == 2 else pd.to_datetime(x[-5:], format="%H:%M")).apply(lambda x: x.strftime("%H:%M"))
    stbn_eng = extractedInfo["BrandNameEng"].str.extract("^(?P<SponsorTextStuntEng>((?!Today's|now Sports).)*(Special|Specials|Presents)\s*)*:*(?P<BrandNameEng>\s*.*\s*)$", expand=False)
    stbn_chi = extractedInfo["BrandNameChi"].str.extract("^(?P<SponsorTextStuntChi>.*特約\s*)*:*(?P<BrandNameChi>\s*.*\s*)$", expand=False)
    outdf["SponsorTextStuntEng"].fillna(stbn_eng["SponsorTextStuntEng"], inplace=True)
    outdf["SponsorTextStuntChi"].fillna(stbn_chi["SponsorTextStuntChi"], inplace=True)
    outdf["BrandNameEng"] = stbn_eng["BrandNameEng"][stbn_eng["BrandNameEng"].notnull()].str.translate(dc.mapping.charTranslateTable).str.strip()
    outdf["BrandNameChi"] = stbn_chi["BrandNameChi"][stbn_chi["BrandNameChi"].notnull()].str.translate(dc.mapping.charTranslateTable).str.strip()
    outdf["BrandNameChi"].fillna(outdf["BrandNameEng"], inplace=True)
    outdf["BrandNameEng"].fillna(outdf["BrandNameChi"], inplace=True)
    outdf["EpisodeNo"] = extractedInfo["EpisodeNo"]
    outdf["EpisodeNameEng"] = extractedInfo["EpisodeNameEng"].apply(lambda x: x.translate(dc.mapping.charTranslateTable).strip() if pd.notnull(x) else x)
    outdf["EpisodeNameChi"] = extractedInfo["EpisodeNameChi"].apply(lambda x: x.translate(dc.mapping.charTranslateTable).strip() if pd.notnull(x) else x)
    outdf["EpisodeNameChi"].fillna(outdf["EpisodeNameEng"], inplace=True)
    outdf["EpisodeNameEng"].fillna(outdf["EpisodeNameChi"], inplace=True)
    outdf["SynopsisEng"] = indf["DescriptionEn"].apply(lambda x: x.translate(dc.mapping.charTranslateTable).strip() if pd.notnull(x) else x)
    outdf["SynopsisChi"] = indf["DescriptionCn"].apply(lambda x: x.translate(dc.mapping.charTranslateTable).strip() if pd.notnull(x) else x)
    outdf["SynopsisChi"].fillna(outdf["SynopsisEng"], inplace=True)
    outdf["SynopsisEng"].fillna(outdf["SynopsisChi"], inplace=True)
    outdf["ShortSynopsisEng"] = outdf["SynopsisEng"]
    outdf["ShortSynopsisChi"] = outdf["SynopsisChi"]
    outdf["PremierOld"] = "0"
    outdf["Premier"] = "N"
    outdf["IsLive"] = "N"
    outdf["Classification"] = indf["Instruction"].replace("A","G")
    outdf["OriginalLang"] = extractedInfo["OriginalLang"].apply(lambda x: ", ".join([dc.mapping.audioLangMapping.get(lang.lower(), lang) for lang in x.split("/")]) if pd.notnull(x) else None)
    outdf["AudioLang"] = outdf["OriginalLang"]
    outdf["IsEpisodic"] = (outdf["EpisodeNo"].notnull() | outdf["EpisodeNo"].str.isnumeric() | outdf["EpisodeNameEng"].notnull()).map({True:"Y", False:"N"})
    dc.utils.title_df(outdf)
    outdf[["FullTitleEng", "FullTitleChi"]] = outdf.apply(dc.utils.constructFullTitle, axis=1)
    outdf[["ProgrammeNameEng", "ProgrammeNameChi"]] = outdf.apply(dc.utils.constructProgrammeName, axis=1)
    dc.utils.translateChar(outdf)
    dc.utils.chopCharacters(outdf)


    # Append EOF
    temp =str(indf["Duration"][indf["Duration"].last_valid_index()])
    if re.match("^[0-9]{2}(:[0-9]{2}){1}$",temp.strip()) is not None:
        temp = temp.strip()+":00"
    lastProgDuration = pd.Timedelta(temp)
    dc.utils.appendEOF(outdf, lastprog_duration=lastProgDuration)


    # Fill Channel #
    outdf['OwnerChannel'] = owner_channel
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)

    # Write output file
    dc.io.to_io(outdf, out_io, out_format)

def convert_nonlinear(in_io, out_io, format="preV1506", owner_channel="Variety and Travel"):
    "Convert nonlinear for Variety and Travel"
    return None

