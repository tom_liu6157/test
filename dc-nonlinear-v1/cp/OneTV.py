import pandas as pd
import numpy as np
import datetime
import math
import csv
import re
import calendar
from collections import OrderedDict
import dc.utils
import dc.io
import dc.header
import dc.mapping
import dc.settings

def convert_linear(in_io, out_io, out_format="preV1506", owner_channel="One TV"):
    "Convert linear schedule for One TV"

    # Read input file
    indf = pd.read_excel(in_io, header=None, sheetname=-1)
    indf.dropna(how='all', inplace=True)
    indf.columns = ["Date", "Time", "TitleCn", "TitleEn", "Description", "Casts", "SubGenre"]

    
    # Create output DataFrame
    outdf = pd.DataFrame(columns=dc.header.internalHeader)

    # Extract data from full title


    # Fill output DataFrame with related info
    outdf["TXDate"] = indf["Date"].apply(lambda x: pd.to_datetime(x, format="%d/%m/Y")).apply(lambda x:x.strftime("%Y%m%d"))
    outdf["ActualTime"] = indf["Time"].astype(str).apply(lambda x: pd.to_datetime(x, format="%H:%M") if x.count(":")==1 else pd.to_datetime(x, format="%H:%M:%S")).apply(lambda x:x.strftime("%H:%M"))
    outdf["FullTitleEng"] = indf["TitleEn"][indf["TitleEn"].notnull()].str.translate(dc.mapping.charTranslateTable).str.replace("\s*#\s*\d*\s*$", "")
    outdf["FullTitleChi"] = indf["TitleCn"][indf["TitleCn"].notnull()].str.translate(dc.mapping.charTranslateTable).str.replace("(\(第.*輯\))?\s*#\s*\d*\s*$", "")
    outdf["FullTitleEng"].fillna(outdf["FullTitleChi"], inplace=True) # fill NaN title eng by title chi
    outdf["FullTitleChi"].fillna(outdf["FullTitleEng"], inplace=True) # fill NaN title chi by title eng
    outdf["BrandNameEng"].fillna(outdf["FullTitleEng"], inplace=True)
    outdf["BrandNameChi"].fillna(outdf["FullTitleChi"], inplace=True)
    outdf["SeasonNo"] = indf["TitleCn"][indf["TitleCn"].notnull()].apply(lambda x: dc.utils.chinese2num(re.sub(".*\(第(.*)輯\).*", r"\1", x).translate(dc.mapping.charTranslateTable)) if re.match(".*\(第.*輯\).*", x) else None)
    outdf["EpisodeNo"] = indf["TitleCn"][indf["TitleCn"].notnull()].str.replace('^((?!#\s*\d*).)*', '').str.translate(dc.mapping.charTranslateTable).str.replace(".*#\s*(\d*).*$", r"\1").apply(lambda x: x if x else None)
    outdf["SynopsisEng"] = indf["Description"][indf["Description"].notnull()].str.translate(dc.mapping.charTranslateTable)
    outdf["SynopsisChi"].fillna(outdf["SynopsisEng"], inplace=True)
    outdf["ShortSynopsisEng"] = outdf["SynopsisEng"][outdf["SynopsisEng"].notnull()].apply(lambda x: x[0:dc.settings.char_limit.get("ShortSynopsisEng")])
    outdf["ShortSynopsisChi"] = outdf["SynopsisChi"][outdf["SynopsisChi"].notnull()].apply(lambda x: x[0:dc.settings.char_limit.get("ShortSynopsisChi")])
    outdf["PremierOld"] = "0"
    outdf["Premier"] = "N"
    outdf["IsLive"] = "N"
    outdf["SubGenre"] = indf["SubGenre"][indf["SubGenre"].notnull()].str.strip().str.translate(dc.mapping.charTranslateTable).apply(lambda x: dc.mapping.subGenreMapping[x] if x in dc.mapping.subGenreMapping else x)
    outdf["CastEng"] = indf["Casts"][indf["Casts"].notnull()].str.translate(dc.mapping.charTranslateTable).str.replace("(\u3001)|( {2})", ", ")
    outdf["CastChi"].fillna(outdf["CastEng"], inplace=True)
    outdf["IsEpisodic"] = (outdf["EpisodeNo"].notnull() | outdf["EpisodeNo"].str.isnumeric() | outdf["EpisodeNameEng"].notnull()).map({True:"Y", False:"N"})
    dc.utils.title_df(outdf)
    outdf[["FullTitleEng", "FullTitleChi"]] = outdf.apply(dc.utils.constructFullTitle, axis=1)
    outdf[["ProgrammeNameEng", "ProgrammeNameChi"]] = outdf.apply(dc.utils.constructProgrammeName, axis=1)
    dc.utils.translateChar(outdf)
    dc.utils.chopCharacters(outdf)


    # Append EOF
    lastProgDuration = outdf["TXDate"].last_valid_index() + np.timedelta64(1,'D') - outdf["TXDate"].last_valid_index() - outdf["ActualTime"].last_valid_index()# default end of program time is 23:59
    lastProgDateTime = outdf["TXDate"].last_valid_index() + outdf["ActualTime"].last_valid_index()
    lastProgram = outdf[outdf["BrandNameEng"] == outdf["BrandNameEng"][outdf["BrandNameEng"].last_valid_index()]]
    if len(lastProgram.index)>1: # last program has been played in previous timeslot
        firstProgIndex = lastProgram.index.values[0]
        lastProgDuration = pd.to_datetime((outdf["TXDate"][firstProgIndex+1] + ' ' + outdf["ActualTime"][firstProgIndex+1]), format="%Y%m%d %H:%M") - pd.to_datetime((outdf["TXDate"][firstProgIndex] + ' ' + outdf["ActualTime"][firstProgIndex]), format="%Y%m%d %H:%M")
    dc.utils.appendEOF(outdf, lastprog_duration=lastProgDuration)


    # Fill Channel #
    outdf['OwnerChannel'] = owner_channel
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)

    # Write output file
    dc.io.to_io(outdf, out_io, out_format)

def convert_nonlinear(in_io, out_io, format="preV1506", owner_channel="One TV"):
    "Convert nonlinear for One TV"
    return None

