import pandas as pd
import numpy as np
import datetime
import math
import csv
import re
import calendar
from collections import OrderedDict
import dc.utils
import dc.io
import dc.header
import dc.mapping
import dc.settings

def convert_linear(in_io, out_io, out_format="preV1506", owner_channel="ABC Australia"):
    "Convert linear schedule for ABC Australia"

    # Read input file
    indf = pd.read_excel(in_io, sheetname=0, skiprows=0, skip_footer=0)

    
    # Create output DataFrame
    outdf = pd.DataFrame(columns=dc.header.internalHeader)

    # Extract data from full title
    fullTitleEng = indf["Title"][indf["Title"].notnull()].apply(lambda x: str(x).strip())
    extractedInfo = pd.DataFrame(columns=["SponsorTextEng", "SponsorTextChi", "BrandNameEng", "BrandNameChi", "EditionVersionEng", "EditionVersionChi", "SeasonNo", "SeasonNameEng", "SeasonNameChi", "EpisodeNo", "EpisodeNameEng", "EpisodeNameChi", "RegionCode", "FirstReleaseYear", "isLive"])
    hardCodeGenDescWithseparator = "8 to 14 yr olds|AFL|Childrens|Culture|Documentary Series|Drama Feature|Drama Series|Factual Entertainment|Factual|Lifestyle Series|Pre-School|Science|Short Stories|Sunday Lights"
    hardCodeProgrammeTitleWithseparator = "The World - News and Business|Play School - Adventure|Play School - Seasons|Play School - Again And Again|Play School - Music|Play School - Once Upon A Time|Play School - Up|Power Games: The Packer-Murdoch Story|A Country Road: The Nationals|Dance Academy: 1st Year"
    tempExtractedInfo = [
        fullTitleEng.str.extract("^(?P<GenDescMapEng>("+hardCodeGenDescWithseparator+")\s*:+\s*)*(?P<BrandNameEng>"+hardCodeProgrammeTitleWithseparator+"|.*?)\s*(\((?P<EditionVersionEng>.*Version)\))*\s*((Season|Series|s)\s*(?P<SeasonNo>\d+))*\s*(-\s*(Ep|Episode)\s*\d+(\s*(?P<EpisodeNameEng1>(\[|\()FINAL(\]|\))))*)*\s*((-|:)\s*(?P<EpisodeNameEng2>.*))*\s*$", expand=False)
    ]
    extractedInfo["BrandNameEng"] = tempExtractedInfo[0]["BrandNameEng"]
    extractedInfo["EditionVersionEng"] = tempExtractedInfo[0]["EditionVersionEng"]
    extractedInfo["EpisodeNameEng"] = tempExtractedInfo[0]["EpisodeNameEng1"]
    extractedInfo["EpisodeNameEng"].fillna(tempExtractedInfo[0]["EpisodeNameEng2"], inplace=True)
    tmpIndex = 0
    for item in indf["Episode Number"]:
        if pd.isnull(item):
            if not pd.isnull(extractedInfo["EpisodeNameEng"][tmpIndex]):
                extractedInfo["BrandNameEng"][tmpIndex]=extractedInfo["BrandNameEng"][tmpIndex]+"-"+extractedInfo["EpisodeNameEng"][tmpIndex]
                extractedInfo["EpisodeNameEng"][tmpIndex]=None
        tmpIndex += 1


    # Fill output DataFrame with related info
    outdf["TXDate"] = indf["Date"].apply(lambda x: pd.to_datetime(x, format="%d/%m/%Y")).apply(lambda x:x.strftime("%Y%m%d"))
    outdf["ActualTime"] = indf["Time"].apply(lambda x: (str(x)+":00") if len(str(x))==5 else str(x)).apply(lambda x: pd.to_datetime(x, format="%H:%M:%S")).apply(lambda x:x.strftime("%H:%M"))
    outdf["BrandNameEng"] = extractedInfo["BrandNameEng"][extractedInfo["BrandNameEng"].notnull()].str.translate(dc.mapping.charTranslateTable).apply(lambda x: x[0:dc.settings.char_limit.get("BrandNameEng")])
    outdf["BrandNameChi"].fillna(outdf["BrandNameEng"], inplace=True)
    outdf["EditionVersionEng"] = extractedInfo["EditionVersionEng"].str.translate(dc.mapping.charTranslateTable)
    outdf["EditionVersionChi"].fillna(outdf["EditionVersionEng"], inplace=True)
    seriesNumber = indf["Series Number"].apply(lambda x: dc.utils.transferNumeric(x) if isinstance(dc.utils.transferNumeric(x), int ) else None)    
    outdf["SeasonNo"] = seriesNumber[seriesNumber.apply(lambda x: pd.notnull(x) and x <= 999)].astype(int).astype(str)
    outdf["SeasonNameEng"] = seriesNumber[seriesNumber.apply(lambda x: pd.notnull(x) and x > 999)].astype(int).astype(str)
    outdf["SeasonNameChi"] = outdf["SeasonNameEng"]
    outdf["EpisodeNo"] = indf["Episode Number"][indf["Episode Number"].notnull()].astype(int).astype(str)
    outdf["EpisodeNameEng"] = extractedInfo["EpisodeNameEng"][extractedInfo["EpisodeNameEng"].notnull()].str.translate(dc.mapping.charTranslateTable).apply(lambda x: x[0:dc.settings.char_limit.get("EpisodeNameEng")])
    outdf[["EpisodeNameEng"]] = outdf.apply(dc.utils.constructEpisodeNameEng, axis=1)
    outdf["EpisodeNameChi"].fillna(outdf["EpisodeNameEng"], inplace=True)
    outdf["SynopsisEng"] = indf["Synopsis"]
    outdf["SynopsisEng"].fillna(indf["Short Synopsis"], inplace=True)
    outdf["SynopsisEng"] = outdf["SynopsisEng"][outdf["SynopsisEng"].notnull()].str.translate(dc.mapping.charTranslateTable).apply(lambda x: x[0:dc.settings.char_limit.get("SynopsisEng")])
    outdf["SynopsisChi"].fillna(outdf["SynopsisEng"], inplace=True)
    outdf["ShortSynopsisEng"] = outdf["SynopsisEng"][outdf["SynopsisEng"].notnull()].apply(lambda x: x[0:dc.settings.char_limit.get("ShortSynopsisEng")])
    outdf["ShortSynopsisChi"] = outdf["SynopsisChi"][outdf["SynopsisChi"].notnull()].apply(lambda x: x[0:dc.settings.char_limit.get("ShortSynopsisChi")])
    outdf["PremierOld"] = "0"
    outdf["Premier"] = "N"
    outdf["IsLive"] = "N"
    outdf["SubGenre"] = indf["Genre"].str.split("/")[indf["Genre"].notnull()].apply(lambda subGenres: sorted(set([item for subGenre in subGenres for item in dc.mapping.subGenreMapping.get(subGenre.lower().strip(), [subGenre])]))).str.join("/").fillna(indf["Genre"]).apply(lambda x: x[0:dc.settings.char_limit.get("SubGenre")])
    if "Year" in indf.columns:
        outdf["FirstReleaseYear"]= indf["Year"][indf["Year"].notnull()].astype(int).astype(str)
    outdf["IsEpisodic"] = (outdf["EpisodeNo"].notnull() | outdf["EpisodeNameEng"].notnull()).map({True:"Y", False:"N"})
    dc.utils.title_df(outdf)
    outdf[["FullTitleEng", "FullTitleChi"]] = outdf.apply(dc.utils.constructFullTitle, axis=1)
    outdf[["ProgrammeNameEng", "ProgrammeNameChi"]] = outdf.apply(dc.utils.constructProgrammeName, axis=1)
    dc.utils.translateChar(outdf)
    dc.utils.chopCharacters(outdf)


    # Append EOF
    temp =str(indf["Duration"][len(indf.index)-1])
    lastProgDuration = pd.Timedelta(temp)
    dc.utils.appendEOF(outdf, lastprog_duration=lastProgDuration)


    # Fill Channel #
    outdf['OwnerChannel'] = owner_channel
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)

    # Write output file
    dc.io.to_io(outdf, out_io, out_format)

def convert_nonlinear(in_io, out_io, format="preV1506", owner_channel="ABC Australia"):
    "Convert nonlinear for ABC Australia"
    return None

