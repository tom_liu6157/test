import pandas as pd
import numpy as np
import datetime
import math
import csv
import re
import calendar
from collections import OrderedDict
import dc.utils
import dc.io
import dc.header
import dc.mapping
import dc.settings

def convert_linear(in_io, out_io, out_format="preV1506", owner_channel="beIN – Now Sports 2"):
    "Convert linear schedule for beIN – Now Sports 2"

    # Read input file
    indf = pd.read_excel(in_io, sheetname=0, skiprows=0)
    indf.dropna(how="all", inplace=True)

    
    # Create output DataFrame
    outdf = pd.DataFrame(columns=dc.header.internalHeader)

    # Extract data from full title


    # Fill output DataFrame with related info
    indf["Episode Eng Synopsis"] = indf["Episode Eng Synopsis"].apply(lambda x: "" if x == "-" else x)
    indf["Programme Eng Synopsis"] = indf["Programme Eng Synopsis"].apply(lambda x: "" if x == "-" else x)
    indf["Episode Mini Synopsis"] = indf["Episode Mini Synopsis"].apply(lambda x: "" if x == "-" else x)
    indf["Programme Mini Synopsis"] = indf["Programme Mini Synopsis"].apply(lambda x: "" if x == "-" else x)
    indf["EPISODEALTTITLE"] = indf["EPISODEALTTITLE"].apply(lambda x: "" if x == "-" else x)
    indf["Programme Sponsorship Title"] = indf["Programme Sponsorship Title"].apply(lambda x: "" if x == "-" else x)
    indf["Sponsorship Start Date"] = indf["Sponsorship Start Date"].apply(lambda x: "" if x == "-" else x)
    indf["Sponsorship End Date"] = indf["Sponsorship End Date"].apply(lambda x: "" if x == "-" else x)
    indf["Season No"] = indf["Season No"].apply(lambda x: "" if x == "-" else x)
    indf["Season_Name_English"] = indf["Season_Name_English"].apply(lambda x: "" if x == "-" else x)
    indf["Season_Name_Chinese"] = indf["Season_Name_Chinese"].apply(lambda x: "" if x == "-" else x)
    indf["Edition_Version_English"] = indf["Edition_Version_English"].apply(lambda x: "" if x == "-" else x)
    indf["Edition_Version_Chinese"] = indf["Edition_Version_Chinese"].apply(lambda x: "" if x == "-" else x)
    indf["Episodic(Y/N)"] = indf["Episodic(Y/N)"].apply(lambda x: "" if x == "-" else x)
    indf["Portrait_Image"] = indf["Portrait_Image"].apply(lambda x: "" if x == "-" else x)
    indf["First Release Year"] = indf["First Release Year"].apply(lambda x: "" if x == "-" else x)
    indf["Media No"] = indf["Media No"].apply(lambda x: "" if x == "-" else x)
    indf["Brand_Name_English"] = indf["Brand_Name_English"].apply(lambda x: "" if x == "-" else x)
    indf["Brand_Name_Chinese"] = indf["Brand_Name_Chinese"].apply(lambda x: "" if x == "-" else x)
    indf["Episode Title"] = indf["Episode Title"].apply(lambda x: "" if x == "-" else x)
    indf["Episode#"] = indf["Episode#"].apply(lambda x: re.match("^([0-9]+)(\.0*)?$", str(x)).groups()[0] if re.match("^([0-9]+)(\.0*)?$", str(x)) else x)
    indf["EPISODEALTTITLE"] = indf["EPISODEALTTITLE"].apply(lambda x: re.match("^([0-9]+)(\.0*)?$", str(x)).groups()[0] if re.match("^([0-9]+)(\.0*)?$", str(x)) else x)
    indf["Programme Title"] = indf["Programme Title"].apply(lambda x: "" if x == "-" else x)
    indf["Programme Alt Title"] = indf["Programme Alt Title"].apply(lambda x: "" if x == "-" else x)
    channel_no = dc.mapping.ownerChannelMapping.get(owner_channel, owner_channel)
    outdf["SponsorTextStuntEng"] = indf["Programme Sponsorship Title"]
    outdf["SponsorTextStuntChi"] = indf["Programme Sponsorship Title"]
    outdf["EditionVersionEng"] = indf["Edition_Version_English"]
    outdf["EditionVersionChi"] = indf["Edition_Version_Chinese"]
    outdf["SeasonNo"] = indf["Season No"][np.logical_and(indf["Season No"].notnull(), indf["Season No"].astype('str') != "")]
    outdf["SeasonNameEng"] = indf["Season_Name_English"]
    outdf["SeasonNameChi"] = indf["Season_Name_Chinese"]
    outdf["Genre"] = indf["GENRE"]
    outdf["SubGenre"] = indf["SUBGENRE"]
    outdf["FirstReleaseYear"] = indf["First Release Year"]
    outdf["IsEpisodic"] = indf["Episodic(Y/N)"]
    outdf["PortraitImage"] = indf['Portrait_Image']
    outdf["TXDate"] = indf["Planner Date"][indf["Planner Date"].notnull()].apply(lambda x: pd.to_datetime(x, format="%Y-%m-%d").strftime('%Y%m%d'))
    outdf["ActualTime"] = indf["Planner Time"][indf["Planner Time"].notnull()].astype('str').str.slice(-8).str.slice(0, 5).str.zfill(5)
    outdf["BrandNameEng"] = indf["Brand_Name_English"].apply(lambda x: np.NaN if str(x).strip()=="" else x)
    outdf["BrandNameEng"].fillna(indf["Programme Title"], inplace=True)
    outdf["BrandNameChi"] = indf["Brand_Name_Chinese"].apply(lambda x: np.NaN if str(x).strip()=="" else x)
    outdf["BrandNameChi"].fillna(indf["Programme Alt Title"], inplace=True)
    outdf["BrandNameEng"] = outdf["BrandNameEng"].apply(lambda x: np.NaN if str(x).strip()=="" else x)
    outdf["BrandNameChi"] = outdf["BrandNameChi"].apply(lambda x: np.NaN if str(x).strip()=="" else x)
    outdf["BrandNameEng"].fillna(outdf["BrandNameChi"], inplace=True)
    outdf["BrandNameChi"].fillna(outdf["BrandNameEng"], inplace=True)
    outdf["EpisodeNo"] = indf[["Episode#","Episode Title"]].apply(lambda x: "" if (pd.notnull(x["Episode Title"]) and re.match("^\s*Week\s+(\d+)\s*$", str(x["Episode Title"]), re.I) is not None and re.search("^\s*Week\s+(\d+)\s*$", str(x["Episode Title"]), re.I).group(1) ==  str(x["Episode#"])) or str(x["Episode#"]) == "-" else x["Episode#"],axis=1)
    outdf["EpisodeNameEng"] = indf[["Episode#","Episode Title"]].apply(lambda x: "" if "Episode Number "+str(x["Episode#"]) == str(x["Episode Title"]) or re.match("^\s*Episode Number\s*\d*\s*$", str(x["Episode Title"]),re.I) is not None else x["Episode Title"],axis=1)
    outdf["EpisodeNameEng"] = outdf["EpisodeNameEng"][outdf["EpisodeNameEng"].notnull()].astype('str').str.replace("(?i)\(Bilingual\)","").str.strip().astype('str').str.replace("\(粵\/英\)","").str.strip()
    outdf["EpisodeNameEng"] = outdf["EpisodeNameEng"].apply(lambda x: np.NaN if str(x).strip()=="" else x).fillna(indf["EPISODEALTTITLE"][~indf["EPISODEALTTITLE"].astype(str).str.match("^\s*(?i)Episode Number \d*\s*$")])
    outdf["EpisodeNameChi"] = indf["EPISODEALTTITLE"]
    outdf["EpisodeNameChi"] = outdf["EpisodeNameChi"][outdf["EpisodeNameChi"].notnull()].astype('str').str.replace("(?i)\(Bilingual\)","").str.strip().astype('str').str.replace("\(粵\/英\)","").str.strip()
    outdf["EpisodeNameChi"] = outdf["EpisodeNameChi"].apply(lambda x: np.NaN if str(x).strip()=="" else x).fillna(indf["Episode Title"][~indf["Episode Title"].astype(str).str.match("^\s*(?i)Episode Number \d*\s*$")])
    outdf["Premier"] = np.where(indf["PLANNINPREMIER"].astype(str).str.upper() == "True".upper() , "Y",                             np.where(indf["PLANNINPREMIER"].astype(str).str.upper() == "False".upper(), "N",                             "")                            )
    outdf["IsLive"] = np.where(indf["SOURCEDESCRIPTION"].apply(lambda x: re.sub('\s','',str(x)).upper() == "LiveEvent".upper()) , "Y", "N")
    outdf["Genre"] = indf["GENRE"].apply(lambda x: dc.mapping.genreMapping.get(x.lower(), x) if pd.notnull(x) else None)
    outdf["SubGenre"] = indf["SUBGENRE"]
    outdf["SubGenre"] = outdf[["SubGenre", "EpisodeNameEng", "EpisodeNameChi"]].apply(lambda x: str(x["SubGenre"])+"/Match" 
                                if (re.match(".*\sv\s.*", str(x["EpisodeNameEng"]),re.I) is not None or re.match(".*\s對\s.*", str(x["EpisodeNameChi"]),re.I) is not None) and "Match".upper() not in re.sub('\s','',str(x["SubGenre"])).upper().split("/") and pd.notnull(x["SubGenre"]) and str(x["SubGenre"]).strip() != ''
                                else "Match" if (re.match(".*\sv\s.*", str(x["EpisodeNameEng"]),re.I) is not None or re.match(".*\s對\s.*", str(x["EpisodeNameChi"]),re.I) is not None) and (pd.isnull(x["SubGenre"]) or str(x["SubGenre"]).strip() == '')
                                else x["SubGenre"], axis = 1)
    outdf["Bilingual"] = np.where(indf["Episode Title"].astype(str).str.match(".*(?i)\(Bilingual\).*"), "Y", "N")
    outdf["FirstReleaseYear"] = indf["First Release Year"]
    outdf["IsEpisodic"] = indf["Episodic(Y/N)"]
    outdf["IsEpisodic"] = outdf[["IsEpisodic","SubGenre","EpisodeNo","EpisodeNameEng","EpisodeNameChi"]].apply(lambda x : "N" if ("Match".upper() in re.sub('\s','',str(x["SubGenre"])).upper().split("/")) or 
                                                                                                            (("Match".upper() not in re.sub('\s','',str(x["SubGenre"])).upper().split("/")) and (pd.isnull(x["EpisodeNo"]) or str(x["EpisodeNo"]).strip() == "") and (pd.isnull(x["EpisodeNameEng"]) or str(x["EpisodeNameEng"]).strip() == "") and (pd.isnull(x["EpisodeNameChi"]) or str(x["EpisodeNameChi"]).strip() == "")) 
                                                                                                               else "Y" if (pd.notnull(x["EpisodeNo"]) and str(x["EpisodeNo"]).strip() != "") or (pd.notnull(x["EpisodeNameEng"]) and str(x["EpisodeNameEng"]).strip() != "") or (pd.notnull(x["EpisodeNameChi"]) and str(x["EpisodeNameChi"]).strip() != "") else x["IsEpisodic"],axis=1)
    outdf["PortraitImage"] = indf["Portrait_Image"]
    outdf["Recordable"] = "Y"
    if channel_no not in ["616","617","618","619","682","683","684","630","680"]:
        outdf["IsNPVRProg"] = np.where(np.logical_and(indf["SOURCEDESCRIPTION"].apply(lambda x: re.sub('\s','',str(x)).upper() == "LiveEvent".upper()),np.logical_or(
        outdf["SubGenre"].apply(lambda x: (re.sub("\s","",str(x)).upper() == "LaLiga/Soccer/Match".upper()) 
                                       or (re.sub("\s","",str(x)).upper() == "LaLiga/Match/Soccer".upper())
                                       or (re.sub("\s","",str(x)).upper() == "Soccer/LaLiga/Match".upper())
                                       or (re.sub("\s","",str(x)).upper() == "Soccer/Match/LaLiga".upper())
                                       or (re.sub("\s","",str(x)).upper() == "Match/LaLiga/Soccer".upper())
                                       or (re.sub("\s","",str(x)).upper() == "Match/Soccer/LaLiga".upper())),
        outdf["SubGenre"].apply(lambda x: (re.sub("\s","",str(x)).upper() == "PL/Soccer/Match".upper())
                                       or (re.sub("\s","",str(x)).upper() == "Soccer/PL/Match".upper())
                                       or (re.sub("\s","",str(x)).upper() == "Match/Soccer/PL".upper())
                                       or (re.sub("\s","",str(x)).upper() == "Soccer/Match/PL".upper())
                                       or (re.sub("\s","",str(x)).upper() == "Match/PL/Soccer".upper())
                                       or (re.sub("\s","",str(x)).upper() == "PL/Match/Soccer".upper())
                               ))),"Y", "N")
    elif channel_no not in ["682","683","684","630","680"]:
        outdf["IsNPVRProg"] = np.where(np.logical_and(indf["SOURCEDESCRIPTION"].apply(lambda x: re.sub('\s','',str(x)).upper() == "LiveEvent".upper()),
        outdf["SubGenre"].apply(lambda x: ("Worldcup".upper() in re.sub('\s','',str(x)).upper().split("/"))
                                      and ("Soccer".upper() in re.sub('\s','',str(x)).upper().split("/"))
                                      and ("Match".upper() in re.sub('\s','',str(x)).upper().split("/")))
                                ),"Y", "N")
    outdf["IsRestartTV"] = "Y"
    outdf["EffectiveDate"] = ""
    if channel_no not in ["682","683","684","630","680"]:
        outdf.ix[(outdf["Recordable"] == "Y") & (outdf["IsNPVRProg"] == "N") & (outdf["IsRestartTV"] == "Y") & (indf["SOURCEDESCRIPTION"].apply(lambda x: re.sub('\\s','',str(x)).upper() != "LiveEvent".upper())), "ExpirationDate"] = 1
    outdf["EpisodeNo"] = outdf[["EpisodeNo","EpisodeNameEng","EpisodeNameChi"]].apply(lambda x: "" if (pd.notnull(x["EpisodeNameEng"]) and str(x["EpisodeNameEng"]).strip() != "") or (pd.notnull(x["EpisodeNameChi"]) and str(x["EpisodeNameChi"]).strip() != "") else x["EpisodeNo"],axis=1)
    if channel_no in ["682","630"]:
        outdf["Recordable"] = ""
        outdf["IsNPVRProg"] = ""
        outdf["IsRestartTV"] = ""
        outdf["ExpirationDate"] = ""
    elif channel_no in ["683","684","680"]:
        outdf["IsNPVRProg"] = "N"
        outdf["IsRestartTV"] = "N"  
    text_column = ["SponsorTextStuntEng", "SponsorTextStuntChi", "BrandNameEng", "BrandNameChi", "EditionVersionEng", "EditionVersionChi", "SeasonNameEng", "SeasonNameChi", "EpisodeNameEng", "EpisodeNameChi", "SynopsisEng", "SynopsisChi", "ShortSynopsisEng", "ShortSynopsisChi", "DirectorProducerEng", "DirectorProducerChi", "CastEng", "CastChi"]
    for column in text_column:
        outdf[column] = outdf[column].astype(str).where(outdf[column].notnull()).str.translate(dc.mapping.charTranslateTable)
    outdf["ChannelNo"] = channel_no
    outdf = outdf[outdf[["BrandNameEng","BrandNameChi"]].apply(lambda x: False if re.sub("[\s]","",str(x["BrandNameEng"])).upper() == "ENDOFFILE" or re.sub("[\s]","",str(x["BrandNameChi"])).upper() == "ENDOFFILE" else True, axis = 1)]
    dc.utils.title_df(outdf)
    outdf[["FullTitleEng", "FullTitleChi"]] = outdf.apply(dc.utils.constructFullTitle, axis=1)
    outdf[["ProgrammeNameEng", "ProgrammeNameChi"]] = outdf.apply(dc.utils.constructProgrammeName, axis=1)
    dc.utils.translateChar(outdf)
    dc.utils.chopCharacters(outdf)


    # Append EOF
    dc.utils.appendEOF(outdf)


    # Fill Channel #
    outdf['OwnerChannel'] = owner_channel
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)

    # Write output file
    dc.io.to_io(outdf, out_io, out_format)

def convert_nonlinear(in_io, out_io, format="preV1506", owner_channel="beIN – Now Sports 2"):
    "Convert nonlinear for beIN – Now Sports 2"
    return None

