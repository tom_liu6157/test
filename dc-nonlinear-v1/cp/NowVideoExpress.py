import pandas as pd
import numpy as np
import datetime
import math
import csv
import re
import calendar
from collections import OrderedDict
import dc.utils
import dc.io
import dc.header
import dc.mapping
import dc.settings

def convert_linear(in_io, out_io, out_format="preV1506", owner_channel="Now Video Express"):
    "Convert linear schedule for Now Video Express"

    # Read input file
    indf = pd.read_excel(in_io, sheetname=0)
    txStartDate=pd.to_datetime(str(indf.index[0][1]).strip(), format="%Y%m%d")
    txEndDate=pd.to_datetime(str(indf.index[1][1]).strip(), format="%Y%m%d")+np.timedelta64(1,'D')
    promoTextEng=str(indf.index[14][1]).strip()
    promoTextChi=str(indf.index[14][2]).strip()
    permuOption=str(indf.index[16][1]).strip()
    timeSeries=pd.date_range(txStartDate,txEndDate,freq='2h')
    timeSeries=timeSeries.delete(len(timeSeries)-1)
    day=int(str(pd.to_datetime(txEndDate, format="%Y%m%d")-pd.to_datetime(txStartDate, format="%Y%m%d")).split()[0])
    if type(in_io) != str:
        in_io.seek(0)
    indf = pd.read_excel(in_io, sheetname=0,skiprows=3,parse_cols=2)
    indf = indf[(indf[indf.columns[0]].astype(str).str.match("Movie\s*\d*",flags=re.I)) & (indf[indf.columns[2]].notnull() & (indf[indf.columns[2]].astype(str).str.strip() != "")) & (indf[indf.columns[1]].notnull() & (indf[indf.columns[1]].astype(str).str.strip() != ""))]

    
    # Create output DataFrame
    outdf = pd.DataFrame(columns=dc.header.internalHeader)

    # Extract data from full title


    # Fill output DataFrame with related info
    outdf["TXDate"] = pd.Series(timeSeries).apply(lambda x: str(x).split()[0].replace("-","").replace("/",""))
    outdf["ActualTime"] = pd.Series(timeSeries).apply(lambda x: str(x).split()[1]).astype('str').str.slice(-8).str.slice(0, 5).str.zfill(5)
    brandNameEng = indf[indf.columns[1]].apply(lambda x: str(x)+" "+promoTextEng if pd.notnull(x) and str(x).strip() != "" else x).tolist()
    brandNameChi = indf[indf.columns[2]].apply(lambda x: str(x)+promoTextChi if pd.notnull(x) and str(x).strip() != "" else x).tolist()
    if permuOption == "1":
        outdf["BrandNameEng"] = pd.Series(brandNameEng*(int(len(timeSeries)/len(indf[indf.columns[2]]))+1))[0:len(timeSeries)]                                                 
        outdf["BrandNameChi"] = pd.Series(brandNameChi*(int(len(timeSeries)/len(indf[indf.columns[2]]))+1))[0:len(timeSeries)]                
    else:
        import random
        for i in range(len(timeSeries)):
            outdf["BrandNameEng"][i] = random.choice(brandNameEng)   #shuffle
            outdf["BrandNameChi"][i] = random.choice(brandNameChi)   #shuffle
    outdf["PremierOld"] = "0"
    outdf["Premier"] = "N"
    outdf["IsLive"] = "N"
    outdf["Bilingual"] = "N"
    outdf["IsEpisodic"] = "N"
    outdf["IsVOD"] = "N"
    outdf["ImageWithTitle"] = "N"
    outdf["RecordableForCatchUp"] = "N"
    outdf["Recordable"] = "N"
    outdf["IsNPVRProg"] = "N"
    outdf["IsRestartTV"] = "N"
    outdf[["FullTitleEng", "FullTitleChi"]] = outdf.apply(dc.utils.constructFullTitle, axis=1)
    outdf[["ProgrammeNameEng", "ProgrammeNameChi"]] = outdf.apply(dc.utils.constructProgrammeName, axis=1)
    dc.utils.translateChar(outdf)
    dc.utils.chopCharacters(outdf)


    # Append EOF
    dc.utils.appendEOF(outdf)


    # Fill Channel #
    outdf['OwnerChannel'] = owner_channel
    outdf['ChannelNo'] = outdf['OwnerChannel'].replace(dc.mapping.ownerChannelMapping)

    # Write output file
    dc.io.to_io(outdf, out_io, out_format)

def convert_nonlinear(in_io, out_io, format="preV1506", owner_channel="Now Video Express"):
    "Convert nonlinear for Now Video Express"
    return None

